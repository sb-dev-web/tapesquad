<?php
App::uses('AppModel','Model');
class Friend extends AppModel {
	public $actsAs = array('Containable');
	public $belongsTo= array(
		'User'=>array(
		  'className'=>'User',
			'foreignKey'=>'user_id'
		),
		'Receiver'=>array(
			'className'=>'User',
			'foreignKey'=>'friend_id',
			
		),
	);




	public function is_friends($user1 = null , $user2 = null)	
	{
		$cond['OR'] = array(array('Friend.user_id'=>$user1,'Friend.friend_id'=>$user2),array('Friend.friend_id'=>$user1,'Friend.user_id'=>$user2));
		return $this->find('count',array('conditions'=>array('Friend.status'=>2,$cond)));
	}
}