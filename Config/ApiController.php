<?php 

/*tapesquad ios controller
* Controller: Api
* Description: Web services 
* Created By: Pooja chaudhary on 7-july-2017 */


App::uses('AppController', 'Controller');
//require_once(ROOT.'/app/Vendor'.DS.'quickstartguide'.DS.'vendor'.DS.'autoload.php');
require_once(ROOT.'/app/Vendor'.DS.'vendors'.DS.'autoload.php');
require_once(ROOT.'/app/Vendor'.DS.'stripe'.DS.'init.php');
class ApiController extends AppController {
        
        var $user_timezone = '';
		var $usertimestamp ='';
		public $layout = false; 
		public $components = array('Auth','Core','Session','Stripe');
		public $helpers=array('Form','Session','Paginate');
		var $allowedActions = array('user_signup','user_login','forgot_password','checkEmail');
    	public $uses=array('User');
    	public $autoRender=false;
    	var $sid ='';
    	var $token ='';
    	public $userId ='';
        public $deviceToken = '';
    	public $deviceType ='';
    	public $sessionId ='';

/**
 * @name Send Response
 * Purpose : This method return the response from any method when called
 * @author Pooja
 * @access private
 * @return the response data
 */
	private function __send_response()
	{ 
			echo str_replace('<null>', '""', str_replace(':null',':""',json_encode($this->response))) ;
			// flush all output
			// ob_end_flush();
			// ob_flush();
			// flush();
            exit;
	}


	public function beforeFilter()
	{ 
		
        //$this->Auth->allow(); return; 
        $this->user_timezone = '';
        
        if($this->request->header('timezone'))
		{
			$this->user_timezone = $this->request->header('timezone');
		}
		else
		{
			$this->user_timezone = 'UTC';
		}
		
        $action = $this->action;
		$this->deviceToken=$this->request->header('deviceToken'); 
		$this->deviceType=$this->request->header('deviceType');
        header('Content-type: application/json');
		parent::beforeFilter(); 
		ignore_user_abort(true);//avoid apache to kill the php running
		ob_start();//start buffer output
		if(!in_array($this->request->action,$this->allowedActions)){

    		$developer = $this->request->header('developer');
    		$this->userId = $this->request->header('userId');
    	    if($developer=='1'){
    	    		$this->Auth->allow();
    	    }else{

	    		$secret = SECRET_KEY_APP;
	    		$token = $this->request->header('token');
	    		$timestamp = $this->request->header('timestamp');
	    		
	    		$this->sessionId = $this->request->header('sessionId');
				$generate_token = sha1($action.$secret.$timestamp);
				
	    		//die;
	    		if($generate_token!=$token && $this->request->header('developer') != 1){
	    			$this->response = array('status'=>400,'message'=>'Invalid token');
	    			$this->__send_response();
				}else{
	                //$this->save_token($this->userId);
					$user = $this->_check_login();
					if(!empty($user))
					{
	                    //if($user['User']['status']=='Active'){
						   $this->Auth->allow();
	                   /* }else{
	                        $this->response = array('status'=>204,'message'=>'User email not verified');
	                        $this->__send_response();
	                    } */
					}
					else
					{
						$this->Auth->logout();
						$this->response = array('status'=>203,'message'=>'Session Expire');
						$this->__send_response();	
					}

				}
			}
		}else{

			$secret = SECRET_KEY_APP;
			$token = $this->request->header('token');
			$timestamp = $this->request->header('timestamp');
			$generate_token = sha1($action.$secret.$timestamp);
		//	if($generate_token!=$token){
		//		$this->response = array('status'=>400,'message'=>'Invalid Request');
		//		$this->__send_response();
		//	}else{
				$this->Auth->allow($this->request->action);
		//	}
		}

   
		if(isset($this->request->query['page']))
		{
			$this->page_number = $this->request->query['page']; 
		}
		if(isset($this->request->query['limit']))
		{
			$this->limit = $this->request->query['limit']; 
		}
		if(isset($this->request->query['show']) && $this->request->query['show'] == 'all')
		{
			$this->limit = MAX_PAGINATION_LIMIT;
		}
		$this->paginate_array = array(
			'limit' => $this->limit,
			'page' => $this->page_number
		);
    }


    function _check_login()
    {
		//$user=$this->User->findByIdAndStatus($this->userId,'Active');
       $user=$this->User->findById($this->userId);
       return $user;
    } 


    function user_signup()
    {
		$data = $this->data;
		$this->loadModel('DeviceToken');
		$this->loadModel('StripeAccount');
		$this->loadModel('User');
		if(!empty($data))
		{
			if(isset($data['email']) && $data['email']!="")
			{
				$email = $data['email'];
				if(isset($data['existing_email']) && !empty($data['existing_email']))
				{
					$chkexisting = $this->User->find('all',array('conditions'=>array('User.other_email'=>$data['existing_email'])));
					if($chkexisting)
					{
						$this->response = array('status'=>400,'message'=>"Email ".$data['existing_email']." Already linked to other account");
						$this->__send_response();
					}
					else
					{
						$this->request->data['User']['other_email'] = $data['existing_email'];
					}
				}
				else
				{
					$chkemail = $this->User->findByEmail($data['email']);
					if($chkemail)
					{
						$this->response = array('status'=>400,'message'=>'Email-Id already exists');
						$this->__send_response();
					}
				}
				$this->request->data['User']['email'] = $data['email'];
			}
			else
			{
				$this->response = array('status'=>400,'message'=>'email-id required');
				$this->__send_response();
			}

			if(isset($this->data['name']) && !empty($this->data['name']))
			{
				$this->request->data['User']['firstname'] = $data['name'];
			}
			if(isset($_FILES['profile_pic']))
    		{
				if(!empty($_FILES['profile_pic']['name'])){
					$picName = $_FILES['profile_pic']['name'];
					if(move_uploaded_file($_FILES["profile_pic"]["tmp_name"], WWW_ROOT . 'img/userImages/'.$picName))
					{
						$this->request->data['User']['profile_pic'] = $picName;
						$profile_pic = SITE_URL.'img/userImages/'.$picName;
					}
				}
				
			}

			if(isset($this->data['last_name']) && !empty($this->data['last_name']))
			{
				$this->request->data['User']['lastname'] = $data['last_name'];
			}

			if(isset($this->data['user_type']) && !empty($this->data['user_type']))
			{
				$this->request->data['User']['user_role'] = $data['user_type'];
				if($this->data['user_type']==2)
				{
					if(isset($this->data['profile_name']) && !empty($this->data['profile_name']))
					{
						$this->request->data['User']['profile_name'] = $this->data['profile_name'];
					}
					else
					{
						$this->response = array('status'=>400,'message'=>'please provide profile name');
						$this->__send_response();
					}
				
					$this->request->data['User']['language'] = 'english';
				}
				/*
				else
				{
					$this->request->data['User']['status'] = 'Active';
				}
				*/
			}
			else
			{
				$this->response = array('status'=>200,'message'=>'Please provide user type');
				$this->__send_response();

			}

			if(isset($data['password']) && $data['password']!="")
			{
				$pass = $data['password'];
				$this->request->data['User']['password'] = AuthComponent::password($pass);
			}
			else
			{
				$this->response = array('status'=>400,'message'=>'password required');
				$this->__send_response();
			}

			if($this->request->data['User']!="")
			{

				$this->User->create();
				$this->request->data['User']['user_role'] = $data['user_type'];
				if($rs = $this->User->save($this->request->data))
				{

					if($data['user_type']==3)
					{
							$acc_response = $this->Stripe->create_account($data['email']);
			    			if($acc_response['error']==400)
			    			{
			    				$this->response = $acc_response;
			    				$this->__send_response();
			    			}

			    			$acc_id = $acc_response['id'];
			    			$accountArr = array(
			    				'StripeAccount'   => array(
			    					'reader_id'   => $rs['User']['id'],
			    					'account_id'  => $acc_id,
			    					'created'     => date('Y-m-d H:i:s')
			    				)
			    			);
			    			$this->StripeAccount->save($accountArr,false);
					}
					
					$this->loadModel('EmailTemplate');
                    $id = $this->encryptor('encrypt',$rs['User']['id']); 
                    $link = SITE_URL."users/activate_account/".$id;
                    $to = $rs['User']['email'];
                    $this->loadModel('EmailTemplate');
                    $template = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.title'=>'account_activation')));
       
                    if($template)
                    { 
                        $arrFind=array('{link}','{name}');
                        $arrReplace=array($link,$data['name']);
                        $from = $template['EmailTemplate']['from_email'];
                        $subject = $template['EmailTemplate']['subject'];
                        $content=str_replace($arrFind, $arrReplace,$template['EmailTemplate']['content']);
                    }
                        
                    $result = $this->sendEmail($to,$from,$subject,$content); 
					if(isset($data['existing_email']) && !empty($data['existing_email']))
                    {
						$userdata = $this->User->findByEmail($data['existing_email']);
						if($userdata)
						{
							$this->User->id = $userdata['User']['id'];
							$this->User->saveField('other_email',$data['email']);
						}
					}

					$getuser = $this->User->findById($rs['User']['id']);
					if(!empty($getuser['User']['other_email']))
					{
						$other_acc = $this->User->find('first',array('conditions'=>['User.user_role !='=>$userType,
						    														'User.email'=>$getuser['User']['other_email'],
						    														],
						    												 		'fields' => ['id','user_role','email','firstname','lastname','profile_name']
						    													));
						if($other_acc)
						{
						    $other_account = [
						    			'id' => $other_acc['User']['id'],
						    			'user_role' => $other_acc['User']['user_role'],
						    			'email' => $other_acc['User']['email'],
						    			'firstname' => $other_acc['User']['firstname'],
						    			'lastname' => $other_acc['User']['lastname'],
						    			'profile_name' => $other_acc['User']['profile_name']
						    		];
						}
					}

					$userArr = array
					(
						'user_id'  		=>$getuser['User']['id'],
						'name'     		=>$getuser['User']['firstname'],
						'last_name'		=>$getuser['User']['lastname'],
						'profile_name'	=>$getuser['User']['profile_name'],
						'email'			=>$getuser['User']['email'],
						'user_type'		=>$getuser['User']['user_role'],
						'created'		=>date('d-m-Y H:i:s')
					);

					if(isset($other_account))
				    {
				        $userArr['other_account'] = $other_account;
				    }
				    else
				    {
				        $userArr['other_account'] = (object) array();
				    }

					$this->response =  array( 'status'=>200, 'data'=> $userArr, 'message'=>'User successfully registered.');
				}
				else
				{
					$this->response =  array('status'=>400,'message'=>'User registration could not be done, please try again later');
				}		
			}
		}
		else
		{
			$this->response =  array('status'=>400,'message'=>'Please fill details');
		}

		$this->__send_response();
	}


    /*Method: login
	* Description: App user authentication
	* Created By:  Pooja chaudhary on 20-july-2017
	*/
    function user_login()
    {

		$data=$this->data;
		if(!empty($data))
		{		
			if(isset($data['user_type']) && !empty($data['user_type']))
			{
				$userType = $data['user_type'];
				$user = $this->User->find('first',array('conditions'=>['User.email'=>$data['email'],
																	   'User.password'=>AuthComponent::password($data['password']),
																	   'User.user_role'=>$userType]
													));
				if(count($user)>0)
				{
					if(isset($data['existing_email']) && !empty($data['existing_email']))
					{
						$chkexisting = $this->User->find('all',array('conditions'=>array('User.other_email'=>$data['existing_email'])));
						if($chkexisting)
						{
							$this->response = array('status'=>400,'message'=>"Email ".$data['existing_email']." Already linked to other account");
							$this->__send_response();
						}
						else
						{
							$this->User->id = $user['User']['id'];
							$this->User->saveField('other_email',$data['existing_email']);
							$otheruser = $this->User->find('first',array('conditions'=>['User.email'=>$data['existing_email'],'User.user_role !='=>$userType]));
							if($otheruser)
							{
								$this->User->id = $otheruser['User']['id'];
								$this->User->saveField('other_email',$data['email']);
							}
						}
					}
				   
				   	$user = $this->User->findById($user['User']['id']);
				   	
				   	if($user['User']['status']=='Active')// || ($user['User']['user_role']==3) && $user['User']['status']!='deactivated')
				   	{
					    	$userId=$user['User']['id']; 
					    	$this->save_token($userId);
				            $this->User->id=$userId;
				            $sessionId=$this->generateRandomString(25);
				            $this->User->save(array('last_login_date' => date("Y-m-d H:i:s"),'signedIn'=>'1','session_id'=>$sessionId));	
				
						    if(!empty($user['User']['other_email']))
						    {
						    	$other_acc = $this->User->find('first',array('conditions'=>['User.user_role !='=>$userType,
						    																'User.email'=>$user['User']['other_email'],
						    																//'User.status'=>'Active'
						    																],
						    												 				'fields' => ['id','user_role','email','firstname','lastname','profile_name']
						    															));
						    	if($other_acc)
						    	{
						    		$other_account = [
						    			'id' => $other_acc['User']['id'],
						    			'user_role' => $other_acc['User']['user_role'],
						    			'email' => $other_acc['User']['email'],
						    			'firstname' => $other_acc['User']['firstname'],
						    			'lastname' => $other_acc['User']['lastname'],
						    			'profile_name' => $other_acc['User']['profile_name']
						    		];
						    	}
						    }                
							$this->loadModel('ReaderCard');
    						$total_cards = $this->ReaderCard->find('count',array('conditions'=>array('ReaderCard.reader_id'=>$user['User']['id'])));
				            $userArr=array(

								'user_id'		=>$user['User']['id'],
								'user_type' 	=>$user['User']['user_role'],
								'name'			=>$user['User']['firstname'],
								'last_name'		=>$user['User']['lastname'],
								'profile_name' 	=>$user['User']['profile_name'],
								'email'			=>$user['User']['email'],
								'status'		=>$user['User']['status'],
								'modified'		=>$user['User']['modified'],
								'session_id'	=>$sessionId,
								'city'			=>$user['User']['city'],
								'total_cards' => $total_cards
							
				           );

				            if(isset($other_account))
				            {
				           		$userArr['other_account'] = $other_account;
				            }
				            else
				            {
				            	$userArr['other_account'] = (object) array();
				            }
							$this->save_token($userArr['user_id']);
					    $this->response =  array( 'status'=>200,'message'=>'User successfully logged in.','data'=> $userArr);
						
					}
					elseif($user['User']['status']=="Inactive")
					{
						$this->response = array('status'=>400,'message'=>'Your account is not activated yet please activate your account to login.','user_status'=>'Inactive'); 
					}
					elseif($user['User']['status']=='deactivated')
					{
						$this->response = array('status'=>400,'message'=>'Sorry! this account has been deactivated.','user_status'=>'Deactivated'); 
					}
				}
				else
				{
		              $this->response = array('status'=>400,'message'=>'Invalid email or password'); 
		        }
		    }
		    else
			{
			    $this->response =  array('status'=>400,'message'=>'Please select in which account you want to login'); 
		    }
    	}
		else
		{
		    $this->response =  array('status'=>400,'message'=>'Invalid email or password'); 
	    }
        $this->__send_response();
    }

    /*Method: changePassword
	* Description: User can update their password
	* Created By:  Pooja chaudhary on 20-July-2017*/

	function switch_account()
	{
		$this->loadModel('User');
		$data = $this->data;

		if(isset($data['email']) && !empty($data['email'])){
			$email = $data['email'];
			$user = $this->User->find('first',array('conditions'=>['User.email'=>$data['email'],
																   'User.user_role'=>$data['user_type']]
													));
			if(count($user)>0)
			{
				if($user['User']['status']=='Active')// || ($user['User']['user_role']==3) && !in_array($user['User']['status'],array('deactivated','Inactive')))
				{
				   		$userId=$user['User']['id']; 
					    $this->save_token($userId);
				        $this->User->id=$userId;
				        $sessionId=$this->generateRandomString(25);
				        $this->User->save(array('last_login_date' => date("Y-m-d H:i:s"),'signedIn'=>'1','session_id'=>$sessionId));	
				
						if(!empty($user['User']['other_email']))
						{
						    $other_acc = $this->User->find('first',array('conditions'=>['User.user_role !='=>$data['user_type'],
						    															'User.email'=>$user['User']['other_email'],
						    															//'User.status'=>'Active'
						    															],
						    												 			'fields' => ['id','user_role','email','firstname','lastname','profile_name']
						    														));
						    if($other_acc)
						    {
						    	$other_account = [
						    		'id' => $other_acc['User']['id'],
						    		'user_role' => $other_acc['User']['user_role'],
						    		'email' => $other_acc['User']['email'],
						    		'firstname' => $other_acc['User']['firstname'],
						    		'lastname' => $other_acc['User']['lastname'],
						    		'profile_name' => $other_acc['User']['profile_name']
						    	];
						    }
						}                

				        $userArr=array(

								'user_id'		=>$user['User']['id'],
								'user_type' 	=>$user['User']['user_role'],
								'name'			=>$user['User']['firstname'],
								'last_name'		=>$user['User']['lastname'],
								'profile_name' 	=>$user['User']['profile_name'],
								'email'			=>$user['User']['email'],
								'status'		=>$user['User']['status'],
								'modified'		=>$user['User']['modified'],
								'session_id'	=>$sessionId,
								'city'			=>$user['User']['city'],
							
				           );

				        if(isset($other_account))
				        {
				           	$userArr['other_account'] = $other_account;
				        }
				        else
				        {
				            $userArr['other_account'] = (object) array();
				        }

					$this->response =  array( 'status'=>200,'message'=>'User successfully logged in.','data'=> $userArr);
				}
				elseif($user['User']['status']=="Inactive")
				{
					$this->response = array('status'=>400,'message'=>'Your account is not activated yet please activate your account to login.','user_status'=>'Inactive'); 
				}
				elseif($user['User']['status']=='deactivated')
				{
					$this->response = array('status'=>400,'message'=>'Sorry! this account has been deactivated.','user_status'=>'Deactivated'); 
				}
			}
			else
			{
		        $this->response = array('status'=>400,'message'=>'Invalid email'); 
		    }

		}else
		{
			$this->response =  array('status'=>400,'message'=>'Please provide email'); 
		}
		$this->__send_response();
	}



    /*Method: changePassword
	* Description: User can update their password
	* Created By:  Pooja chaudhary on 20-July-2017*/


	function change_password()
	{
            $data=$this->data;
            if(!empty($data)){

            	if(isset($data['user_id']) && !empty($data['user_id'])){
            		$userId = $data['user_id'];
            	}else{
            		$this->response = array('status'=>400,'message'=>'Please provide user id');
            		$this->__send_response();
            	}

            	if(isset($data['old_password']) && !empty($data['old_password'])){
            		$old_password = $data['old_password'];
            	}else{
            		$this->response = array('status'=>400,'message'=>'Please provide old password');
            		$this->__send_response();
            	}

            	if(isset($data['new_password']) && !empty($data['new_password'])){
            		$new_password = $data['new_password'];
            	}else{
            		$this->response = array('status'=>400,'message'=>'Please provide new password');
            		$this->__send_response();
            	}


                $userData=$this->User->findById($userId);
                if(AuthComponent::password($data['old_password'])!=$userData['User']['password']){
                     $this->response=array('status'=>400,'message'=>'Invalid current password'); 
                }else{
                    $this->User->id=$userId;
		    		$this->request->data['User']['password'] = AuthComponent::password($new_password);
		    		$this->User->save($this->request->data);
                    $this->response=array('status'=>200,'message'=>'Password updated');	
                }
            }else{
                   $this->response=array('status'=>400,'message'=>'Invalid data'); 
            }
            $this->__send_response();
    
    }

/*Method: checkEmail
* Description: to check an email availability
* Created By:  Pooja chaudhary on 22-Aug-2017
*/

function checkEmail(){
	$data = $this->data;
	if(!empty($data['email'])){
		$this->loadModel('User');
		$email = $data['email'];
		$chkmail = $this->User->findByEmail($email);
		if($chkmail){
			$this->response = array('status'=>200,'message'=>'Email id already exists');
		}else{
			$this->response = array('status'=>200,'message'=>'Email id does not exists');
		}
	}else{
		$this->response = array('status'=>400,'message'=>'Please provide email id');
	}
	$this->__send_response();
}


/*Method: forgotPassword
* Description: User can reset their password
* Created By:  Pooja chaudhary on 20-july-2017
*/

function forgot_password()
{
		$data = $this->data;
		if($data['email'])
		{
			$userData=$this->User->findByEmail($data['email']);
		    if(!empty($userData))
		    {
		        $newPwd=$this->generateRandomString(6);
			 	

					/*******Send Email******/
                $email = $userData['User']['email'];
                $name = $userData['User']['firstname'];
                $password=$newPwd;
	    		$from = 'info@tapesquad.com';
        		$to = $email;
                $subject = 'Forgot Password';
	     		$content = "Hello $name,<br /><br />
				Your password has reset.<br />
				<strong>Your new password is</strong>: $password<br />
				Thanks<br /><br />";

					if($this->sendEmail($to,$from,$subject,$content))
					{
						$this->User->id=$userData['User']['id'];
		         		$this->User->save(array('password'=>$this->Auth->password($newPwd)));
						$this->response =array('status'=>200,'message'=>'New password has been sent to your email');
				    }
				    else
				    {
						$this->response =array('status'=>400,'message'=>'Password could not be changed, please try again later.');
				    }
			}
			else
			{
			    $this->response=array('status'=>400,'message'=>'Invalid access');  
			}
		}
		else
		{
		    $this->response=array('status'=>400,'message'=>'Invalid access'); 
		}
		$this->__send_response();
    }


    /*Method: fetchTermsConditions
	* Description: this method is used to fetch terms and conditions
	* Created By:  Pooja chaudhary on 24-july-2017
	*/
	function fetchTermsConditions()
	{
		$this->loadModel('Content');
		$content = $this->Content->find('first',array('conditions'=>array('Content.title'=>'terms and conditions')));
		if($content){
			$this->response = array('status'=>200,'message'=>'success','data'=>$content);
		}else{
			$this->response = array('status'=>200,'message'=>'No data found');
		}
		$this->__send_response();
	}


    /*Method: completeRederProfile
	* Description: User will complete his details
	* Created By:  Pooja chaudhary on 24-july-2017
	*/
	function completeReaderProfile(){

    	$data = $this->data;
    	if($data && !empty($data))
    	{
    		$this->loadModel('User');
			if(isset($data['user_id']) && !empty($data['user_id']))
    		{
    			$id =  $data['user_id'];
    			$this->User->id = $id;
    		}

    		if(isset($data['lat']) && !empty($data['lat']))
    		{
    			$this->request->data['User']['latitude'] =  $data['lat'];
    		}

    		if(isset($data['long']) && !empty($data['long'])){
    			$this->request->data['User']['longitude'] =  $data['long'];
    		}

    		if(isset($data['city']) && !empty($data['city']))
    		{
    			$this->request->data['User']['city'] =  $data['city'];
    		}

    		if(isset($data['location']) && !empty($data['location']))
    		{
    			$this->request->data['User']['location'] =  $data['location'];
    		}

    		if(isset($data['gender']) && !empty($data['gender']))
    		{
    			$this->request->data['User']['gender'] =  $data['gender'];
    		}

    		if(isset($data['language']) && !empty($data['language']))
    		{
    			$this->request->data['User']['language'] =  $data['language'];
    		}

    		if(isset($data['quick_bio']) && !empty($data['quick_bio']))
    		{
    			$this->request->data['User']['quick_bio'] =  $data['quick_bio'];
    		}

    		if(isset($data['quote']) && !empty($data['quote']))
    		{
    			$this->request->data['User']['quote'] =  $data['quote'];
    		}

    		if(isset($data['profile_pic']) && !empty($data['profile_pic']))
    		{
    			$this->request->data['User']['profile_pic'] =  $data['profile_pic'];
    		}

			if(isset($_FILES['profile_pic']))
    		{
				if(!empty($_FILES['profile_pic']['name']))
				{
					$picName = $_FILES['profile_pic']['name'];
					if(move_uploaded_file($_FILES["profile_pic"]["tmp_name"], WWW_ROOT . 'img/userImages/'.$picName))
					{
						$this->request->data['User']['profile_pic']  = $picName;
						$profile_pic = SITE_URL.'img/userImages/'.$picName;
					}
					else
					{
						$this->response = array('status'=>400,'message'=>'profile pic could not be uploaded, please try again later');
						$this->__send_response();
					}
				}
			}

			if(isset($_FILES['govt_id_pic']))
    		{
				if(!empty($_FILES['govt_id_pic']['name']))
				{
					$govtPic = $_FILES['govt_id_pic']['name'];
					if(move_uploaded_file($_FILES["govt_id_pic"]["tmp_name"], WWW_ROOT . 'img/userImages/'.$govtPic))
					{
						$this->request->data['User']['govt_id_pic']  = $govtPic;
						$govt_pic = SITE_URL.'img/userImages/'.$govtPic;
					}
					else
					{
						$this->response = array('status'=>400,'message'=>'Govt id pic could not uploaded, please try again later');
						$this->__send_response();
					}
				}
			}

			if(isset($data['actor_reel']) && !empty($data['actor_reel']))
			{
    			$this->request->data['User']['actor_reel'] =  $data['actor_reel'];
    		}

    		if(isset($data['complete_profile']) && !empty($data['complete_profile']))
    		{
    			$this->request->data['User']['complete_profile'] =  $data['complete_profile'];
    		}

    		if($rs = $this->User->save($this->request->data['User']))
    		{
    			
    			$data = $rs['User'];
    			if(isset($profile_pic) && !empty($profile_pic))
    			{
    				$data['profile_pic'] = $profile_pic;
    			}
    			if(isset($govt_pic) && !empty($govt_pic))
    			{
    				$data['govt_id_pic'] = $govt_pic;
    			}

    			$this->response = array('status'=>200,'message'=>'success','data'=>$data);
    		}
    		else
    		{
    			$this->response = array('status'=>400,'message'=>'Profile could not completed , please try again');
    		}
		}
		else
		{
    		$this->response = array('status'=>400,'message'=>'DInt get any data, please find some data');
    	}
		return $this->__send_response();
    }


    /*Method: updateNewUserKey
	* Description: this method is used to update Is_new the user details
	* Created By:  Pooja chaudhary on 28-july-2017
	*/
	function updateNewUserKey()
	{

		$data = $this->data;
		if(isset($data['user_id']) && !empty($data['user_id']))
		{
			$this->User->id = $data['user_id'];
			if($this->User->saveField('is_new','2'))
			{
				$this->response = array('status'=>200,'message'=>'success');
			}
			else
			{
				$this->response = array('status'=>400,'message'=>'failed');
			}
		}
		else
		{
			$this->response = array('status'=>400,'message'=>'Please provide user id');
		}
		$this->__send_response();
	}


    /*Method: fetchReaderDetails
	* Description: this method is used to fetch the user details
	* Created By:  Pooja chaudhary on 24-july-2017
	*/
    function fetchReaderDetails(){

    	if(isset($this->data['user_id']) && !empty($this->data['user_id']))
    	{
    		$userId = $this->userId;
    		$user_id = $this->data['user_id'];
    		$this->loadModel('User');
    		$this->loadModel('Friend');
    		$this->User->id = $user_id;
    		if(!$this->User->exists())
    		{
    			$this->response = array('status'=>400,'message'=>'Invali user id');
    			$this->__send_response();
    		}
    		else
    		{
				$this->loadModel('Review');
    			$this->User->recursive = 2;
    			$this->Review->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'login_id','fields'=>array('firstname','lastname','profile_pic')))));
    			$this->User->bindModel(array('hasMany'=>array('Review'=>array('class'=>'Review','foreignKey'=>'user_id'))));
    			$user = $this->User->findById($user_id);
    			if(!empty($user))
    			{
    				unset($user['User']['password']);

    				$user['User']['language'] = explode(',',$user['User']['language']);

    				if(!empty($user['User']['tags']))
    				{
    					$user['User']['tags'] = explode(',',$user['User']['tags']);
    				}
    				else
    				{
    					$user['User']['tags'] = array();
    				}

    				if(!empty($user['User']['profile_pic']))
    				{
    					$user['User']['profile_pic'] = SITE_URL.'img/userImages/'.$user['User']['profile_pic'];
					}

					if(!empty($user['User']['govt_id_pic']))
					{
    					$user['User']['govt_id_pic'] = SITE_URL.'img/userImages/'.$user['User']['govt_id_pic'];
					}

					$ids = array($userId,$user['User']['id']);
	    		    $chkfriend = $this->Friend->find('first',array('conditions'=>array('Friend.user_id IN'=>$ids,'Friend.friend_id IN'=>$ids,'Friend.status !='=>0)));
	    			if($chkfriend){
		    			$friend_status = '1';
		    		}else{
		    			$friend_status = '0';
		    		}
		    		$user['User']['friend_status'] = $friend_status;


					if(!empty($user['User']['other_email']))
					{
						$other_acc = $this->User->find('first',array('conditions'=>['User.user_role !='=>$user['User']['user_role'],
						    														'User.email'=>$user['User']['other_email'],
						    														],
						    												 		'fields' => ['id','user_role','email','firstname','lastname','profile_name']
						    													));
						if($other_acc)
						{
						    $other_account = [
						    			'id' => $other_acc['User']['id'],
						    			'user_role' => $other_acc['User']['user_role'],
						    			'email' => $other_acc['User']['email'],
						    			'firstname' => $other_acc['User']['firstname'],
						    			'lastname' => $other_acc['User']['lastname'],
						    			'profile_name' => $other_acc['User']['profile_name']
						    		];

						}
					}

					if(isset($other_account))
				    {
				        $user['User']['other_account'] = $other_account;
				    }
				    else
				    {
				        $user['User']['other_account'] = (object) array();
				    }
    			
					$data = $user['User'];
    				if(isset($user['Review']) && !empty($user['Review']))
    				{
    					$size = sizeof($user['Review']);
    					$score = 0;
    					foreach($user['Review'] as $review):

			    			$score = $score + $review['Review']['rating_score'];
			    			$rating_score = ceil(($score)/($size));
			    			
							if(!empty($review['User']['profile_pic']))
							{
    							$profile_pic = SITE_URL.'img/userImages/'.$review['User']['profile_pic'];
    						}
    						else
    						{
    							$profile_pic = SITE_URL.'img/userImages/1503917394614.jpeg';
    						}

    						$review['profile_pic'] = $profile_pic;
    						$review['username'] = $review['User']['firstname'].' '.$review['User']['lastname'];
    				
							unset($review['User']);
    						$reviews[] = $review;
    					endforeach;

    					$data['reviews'] = $reviews;
    				}else
    				{
						$data['reviews'] = array();
    				}
    				
    				$this->response = array('status'=>200,'message'=>'success','data'=>$data);
    			}
    			else
    			{
    				$data = array();
    				$this->response = array('status'=>200,'message'=>'success','data'=>$data);
    			}
			}
		}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide User id');
    	}

    	$this->__send_response();
    }


    function updateRederUi()
    {	

    	$data = $this->data;
    	if(isset($data['reader_id']) && !empty($data['reader_id'])){
    		$reader_id = $data['reader_id'];
    		$this->loadModel('User');
    		$this->User->id = $reader_id;
    		if(!$this->User->exists()){
    			$this->response = array('status'=>400,'message'=>'Invalid User');
    			$this->__send_response();
    		}else{
    			$user = $this->User->findById($reader_id);
    			$left = array();
    			$complete = ['Register'];

    			if(!empty($user['User']['latitude']) && !empty($user['User']['longitude'])){
    				$complete[] = 'Location';
    			}else{
    				$left[] = 'Location';
    			}

    			/*if(!empty($user['User']['quick_bio']))
    			{
    				$complete[] = 'Quick bio';
    			}
    			else
    			{
    				$left[] = 'Quick bio';
    			}
    			*/

    			if(!empty($user['User']['language']))
    			{
    				$complete[] = 'Language';
    			}
    			else
    			{
    				$left[] = 'Language';
    			}

    			if(!empty($user['User']['gender']))
    			{
    				$complete[] = 'Gender';
    			}
    			else
    			{
    				$left[] = 'Gender';
    			}

				if(!empty($user['User']['profile_pic']))
				{
    				$complete[] = 'Profile pic';
    			}
    			else
    			{
    				$left[] = 'Profile pic';
    			}

    			if(!empty($user['User']['govt_id_pic']))
    			{
    				$complete[] = 'Government ID Verification';
    			}
    			else
    			{
    				$left[] = 'Government ID Verification';
    			}

    			if(!empty($user['User']['actor_reel']))
    			{
    				$complete[] = 'Actors Reel';
    			}
    			else
    			{
    				$left[] = 'Actors Reel';
    			}

    			$leftSize = sizeof($left);
    			if($leftSize==0)
    			{
    				$this->User->saveField('complete_profile','Yes');
    			} 

    			$resultArr['left'] = $left;
    			$resultArr['complete'] = $complete; 
    			$this->response = array('status'=>200,'message'=>'success','data'=>$resultArr);

    		}
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide reader id');
    	}

    	$this->__send_response();
    }


    function editReaderProfile()
    {

		$data = $this->data;
    	if($data && !empty($data))
    	{
    		$this->loadModel('User');

    		if(isset($data['user_id']) && !empty($data['user_id'])){
    			$id =  $data['user_id'];
    			$this->User->id = $id;
    		}
    		else
    		{
    			$this->response = array('status'=>400,'message'=>'Please provide user id');
    			$this->__send_response();
    		}

    		if(isset($data['email']) && !empty($data['email']))
    		{
    			$this->request->data['User']['email'] =  $data['email'];
    		}

    		if(isset($data['firstname']) && !empty($data['firstname']))
    		{
    			$this->request->data['User']['firstname'] =  $data['firstname'];
    		}

    		if(isset($data['lastname']) && !empty($data['lastname']))
    		{
    			$this->request->data['User']['lastname'] =  $data['lastname'];
    		}

    		if(isset($data['profile_name']) && !empty($data['profile_name']))
    		{
    			$this->request->data['User']['profile_name'] =  $data['profile_name'];
    		}

    		if(isset($data['phone_no']) && !empty($data['phone_no']))
    		{
    			$this->request->data['User']['phone_no'] =  $data['phone_no'];
    		}

    		if(isset($data['city']) && !empty($data['city']))
    		{
    			$this->request->data['User']['city'] =  $data['city'];
    		}

    		if(isset($data['gender']) && !empty($data['gender']))
    		{
    			$this->request->data['User']['gender'] =  $data['gender'];
    		}

    		if(isset($data['tags']) && !empty($data['tags']))
    		{
    			$this->request->data['User']['tags'] =  $data['tags'];
    		}

    		if(isset($data['language']) && !empty($data['language']))
    		{
    			$this->request->data['User']['language'] =  $data['language'];
    		}

    		if(isset($data['tape']) && !empty($data['tape']))
    		{
    			$this->request->data['User']['tape'] =  $data['tape'];
    		}

			if(isset($data['lat']) && !empty($data['lat']))
			{
    			$this->request->data['User']['latitude'] =  $data['lat'];
    		}

    		if(isset($data['long']) && !empty($data['long']))
    		{
    			$this->request->data['User']['longitude'] =  $data['long'];
    		}

			if(isset($data['location']) && !empty($data['location']))
			{
    			$this->request->data['User']['location'] =  $data['location'];
    		}

    		if(isset($data['quick_bio']) && !empty($data['quick_bio']))
    		{
    			$this->request->data['User']['quick_bio'] =  $data['quick_bio'];
    		}

    		if(isset($data['quote']) && !empty($data['quote']))
    		{
    			$this->request->data['User']['quote'] =  $data['quote'];
    		}

    		if(isset($data['profile_pic']) && !empty($data['profile_pic']))
    		{
    			$this->request->data['User']['profile_pic'] =  $data['profile_pic'];
    		}

			if(isset($_FILES['profile_pic']))
    		{
				if(!empty($_FILES['profile_pic']['name'])){
					$picName = $_FILES['profile_pic']['name'];
					if(move_uploaded_file($_FILES["profile_pic"]["tmp_name"], WWW_ROOT . 'img/userImages/'.$picName))
					{
						$this->request->data['User']['profile_pic'] = $picName;
						$profile_pic = SITE_URL.'img/userImages/'.$picName;
					}else{
						$this->response = array('status'=>400,'message'=>'profile pic could not be uploaded, please try again later');
						$this->__send_response();
					}
				}
			}

			if(isset($_FILES['govt_id_pic']))
    		{
				if(!empty($_FILES['govt_id_pic']['name'])){
					$govtPic = $_FILES['govt_id_pic']['name'];
					if(move_uploaded_file($_FILES["govt_id_pic"]["tmp_name"], WWW_ROOT . 'img/userImages/'.$govtPic))
					{
						$this->request->data['User']['govt_id_pic']  = $govtPic;
						$govt_pic = SITE_URL.'img/userImages/'.$govtPic;
					}else{
						$this->response = array('status'=>400,'message'=>'Govt id pic could not be uploaded, please try again later');
						$this->__send_response();
					}
				}
			}

			if(isset($data['actor_reel']) && !empty($data['actor_reel'])){
    			$this->request->data['User']['actor_reel'] =  $data['actor_reel'];
    		}

    		if(isset($data['complete_profile']) && !empty($data['complete_profile'])){
    			$this->request->data['User']['complete_profile'] =  $data['complete_profile'];
    		}

    		if($rs = $this->User->save($this->request->data['User'])){
    			
    			$data = $user = $this->User->findById($id);
    			if(isset($profile_pic) && !empty($profile_pic)){
    				$data['profile_pic'] = $profile_pic;
    			}
    			if(isset($govt_pic) && !empty($govt_pic)){
    				$data['govt_id_pic'] = $govt_pic;
    			}
				$this->response = array('status'=>200,'message'=>'success','data'=>$data);

    		}else{
    			$this->response = array('status'=>400,'message'=>'Profile could not updated , please try again');
    		}
					
		}else{
    		$this->response = array('status'=>400,'message'=>'Dint get any data, please send some data');
    	}

    	return $this->__send_response();

    }

    /*
    * Method: manageReaderSlots
	* Description: this method is used to manage slots for readers
	* Created By:  Pooja chaudhary on 15-December-2017
	*/
	function manageReaderSlots()
	{
		$data = $this->data;
		$this->loadModel('ReaderSlot');
		$reader_id = $this->userId;
		if(isset($data['slotsArray']) && !empty($data['slotsArray'])){
		
			$data['slotsArray'] =  stripslashes($data['slotsArray'] = str_replace('\n','',($data['slotsArray']))); 
			$slotsArray = json_decode($data['slotsArray']);
			$slotArr = array();
			foreach ($slotsArray as $slot){
				$apoint_date = date('Y-m-d',strtotime($slot->date));
				$chkdata = $this->ReaderSlot->find('first',array('conditions'=>array('ReaderSlot.apoint_date'=>$apoint_date)));
				if($chkdata){
					$this->ReaderSlot->id = $chkdata['ReaderSlot']['id'];
					$this->ReaderSlot->delete();
				}
				$slotArr[] = array(
					'ReaderSlot' => array(
						'reader_id' => $reader_id,
						'apoint_date' => $apoint_date,
						'slots' => $slot->slots
						)
					);
			}
			
			if($this->ReaderSlot->saveAll($slotArr))
			{
				$this->response = array('status'=>200,'message'=>'success','data'=>$slotArr);
			}
			else
			{
				$this->response = array('status'=>400,'message'=>'Server Error');
			}
			
		}
		else
		{
			$this->response = array('status'=>400,'message'=>'Please provide data');
		}
		$this->__send_response();
	}


	function getReaderSlots()
	{
		$this->loadModel('ReaderSlot');
		$data = $this->data;
		if(isset($data['reader_id']) && !empty($data['reader_id']))
		{
			$reader_id = $data['reader_id'];
			$today = date('Y-m-d');
			$tomorrow = date('Y-m-d',strtotime('+1 day'));
			$getslots = $this->ReaderSlot->find('all',array('conditions'=>array('ReaderSlot.reader_id'=>$reader_id,'ReaderSlot.apoint_date IN'=>array($today,$tomorrow)),
															'fields'=>array('apoint_date','slots')));
			if($getslots)
			{
				$resArr = array();
				foreach($getslots as $slots):
					$resArr[] = $slots['ReaderSlot'];
				endforeach;
				$this->response = array('status'=>200,'message'=>'success','data'=>$resArr);
			}
			else
			{
				$this->response = array('status'=>200,'message'=>'Sorry! No slots found');
			}
		}
		else
		{
			$this->response = array('status'=>400,'message'=>'Please provide reader id');
		}
		$this->__send_response();
	}


    /*Method: fetch_readers
	* Description: this method is used to submit feedback from reader
	* Created By:  Pooja chaudhary on 31-july-2017
	*/
    function fetchReaders()
    {
    	$data = $this->data;
    	$this->loadModel('User');
    	$this->loadModel('StripeAccount');
    	$currentuser = $this->User->find('first',['conditions'=>['User.id'=>$this->request->header('userId')]]);
    	$currentemail = $currentuser['User']['email'];
    	//echo $this->Auth->user('email');exit;
    	if(isset($data['language']) && !empty($data['language']))
    	{
    		//$language = explode(',',$data['language']);
    		$language = $data['language'];
    	}

    	if(isset($data['gender']) && !empty($data['gender']) && $data['gender']!=3)
    	{
    		$gender = $data['gender'];
    	}

    	if(isset($data['online']) && $data['online']!='')
    	{
    		$online = $data['online'];
    	}

		if(isset($data['lat']) && $data['lat']!='' && isset($data['long']) && $data['long']!='')
		{
    		$latitude = $data['lat'];
    		$longitude = $data['long'];
    		$radius = 70;
			$lng_min = $longitude - $radius/abs(cos(deg2rad($latitude))*69);
			$lng_max = $longitude + $radius/abs(cos(deg2rad($latitude))*69);
			$lat_min = $latitude - ($radius/69);
			$lat_max = $latitude + ($radius/69);
		}

    	
    	if(isset($language) && isset($gender) && isset($online) && isset($latitude))
    	{
			$conditions = array('User.latitude >='=>$lat_min,'User.latitude <='=>$lat_max,'User.longitude >='=>$lng_min,'User.longitude <='=>$lng_max,'User.is_online'=>$online,'User.language LIKE'=>'%'.$language.'%','User.gender'=>$gender,'User.status'=>'Active');
    	}
    	elseif(isset($gender) && isset($online) && isset($latitude))
    	{
			$conditions = array('User.latitude >='=>$lat_min,'User.latitude <='=>$lat_max,'User.longitude >='=>$lng_min,'User.longitude <='=>$lng_max,'User.is_online'=>$online,'User.gender'=>$gender,'User.status'=>'Active');
		}
    	elseif(isset($language) && isset($online) && isset($latitude))
    	{
			$conditions = array('User.latitude >='=>$lat_min,'User.latitude <='=>$lat_max,'User.longitude >='=>$lng_min,'User.longitude <='=>$lng_max,'User.is_online'=>$online,'User.language LIKE'=>'%'.$language.'%','User.status'=>'Active');
		}
    	elseif(isset($language) && isset($gender) && isset($latitude))
    	{
			$conditions = array('User.latitude >='=>$lat_min,'User.latitude <='=>$lat_max,'User.longitude >='=>$lng_min,'User.longitude <='=>$lng_max,'User.gender'=>$gender,'User.language LIKE'=>'%'.$language.'%','User.status'=>'Active');
		}
    	elseif(isset($language) && isset($online) && isset($gender))
    	{
			$conditions = array('User.is_online'=>$online,'User.language LIKE'=>'%'.$language.'%','User.gender'=>$gender,'User.status'=>'Active');
		}
    	elseif(isset($language) && isset($gender))
    	{
			$conditions = array('User.language LIKE'=>'%'.$language.'%','User.gender'=>$gender,'User.status'=>'Active');
    	}
    	elseif(isset($language) && isset($latitude))
    	{
			$conditions = array('User.latitude >='=>$lat_min,'User.latitude <='=>$lat_max,'User.longitude >='=>$lng_min,'User.longitude <='=>$lng_max,'User.language LIKE'=>'%'.$language.'%','User.status'=>'Active');
		}
    	elseif(isset($language) && isset($online))
    	{
			$conditions = array('User.is_online'=>$online,'User.language LIKE'=>'%'.$language.'%','User.status'=>'Active');
		}
    	elseif(isset($online) && isset($gender))
    	{
			$conditions = array('User.gender'=>$gender,'User.is_online'=>$online,'User.status'=>'Active');
		}
    	elseif(isset($online) && isset($latitude))
    	{
			$conditions = array('User.latitude >='=>$lat_min,'User.latitude <='=>$lat_max,'User.longitude >='=>$lng_min,'User.longitude <='=>$lng_max,'User.is_online'=>$online,'User.status'=>'Active');
		}
    	elseif(isset($gender) && isset($latitude))
    	{
			$conditions = array('User.latitude >='=>$lat_min,'User.latitude <='=>$lat_max,'User.longitude >='=>$lng_min,'User.longitude <='=>$lng_max,'User.gender'=>$gender,'User.status'=>'Active');
		}
    	elseif(isset($language))
    	{
			$conditions = array('User.language LIKE'=>'%'.$language.'%','User.status'=>'Active');
		}
    	elseif(isset($gender))
    	{
			$conditions = array('User.gender'=>$gender,'User.status'=>'Active');
		}
    	elseif(isset($online))
    	{
			$conditions = array('User.is_online'=>$online,'User.status'=>'Active');
		}
    	elseif(isset($latitude))
    	{
			$conditions = array('User.latitude >='=>$lat_min,'User.latitude <='=>$lat_max,'User.longitude >='=>$lng_min,'User.longitude <='=>$lng_max,'User.status'=>'Active');
    	}else{
    	
    		$conditions = array('User.status'=>'Active','User.govt_id_status'=>'Approved');
    	
    	}
    	$c1 = ['OR'=>['User.other_email !='=>$currentemail,'User.other_email is NULL']];
    	$conditions = array_merge($conditions , $c1);
    	//print "<pre>";print_r($conditions);exit;
    	$this->loadModel('Review');
    	$this->User->recursive = 2;
    	$this->Review->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'login_id','fields'=>array('firstname','lastname','profile_pic','profile_name')))));
    	$this->User->bindModel(array('hasMany'=>array('Review'=>array('class'=>'Review','foreignKey'=>'user_id'))));
    	$userdata = $this->User->find('all',array('conditions'=>array('User.user_role'=>3,$conditions)));
		//print "<pre>";print_r($userdata);exit;
    	if(!empty($userdata))
    	{
    		
    		foreach ($userdata as $user)
	    	{
	    		$account = $this->StripeAccount->findByReaderId($user['User']['id']);
	    		if($account)
	    		{
					$score = 0;
					$ratings = $this->Review->find('all',array('conditions'=>array('Review.user_id'=>$user['User']['id'])));
		    		if($ratings)
		    		{
						$size = count($ratings);
						foreach($ratings as $rating)
		    			{
		    				$score = $score + $rating['Review']['rating_score'];
		    			}
						$rating_score = $score/$size;
		    			$user['User']['rating_score'] = round($rating_score,2);
		    			$this->User->id = $user['User']['id'];
		    			$this->User->saveField('rating_score',$rating_score);
		    			$user['User']['score'] = round($rating_score,2);
		    		}
		    		else
		    		{
		    			$user['User']['score'] = 0;
		    		}

		    		if(!empty($user['User']['tags']))
		    		{
		    			$user['User']['tags'] = explode(',',$user['User']['tags']);
		    		}
		    		else
		    		{
		    			$user['User']['tags'] = array();
		    		}

		    		if(!empty($user['User']['language']))
		    		{
		    			$user['User']['language'] = explode(',',$user['User']['language']);
		    		}
		    		else
		    		{
		    			$user['User']['language'] = array();
		    		}
		    		
		    	
		    		if(isset($user['Review']) && !empty($user['Review']))
		    		{
		    			$counter = 1;
		    			
		    			foreach($user['Review'] as $review):

		    				if(!empty($review['User']['profile_pic']))
		    				{
		    					$profile_pic = SITE_URL.'img/userImages/'.$review['User']['profile_pic'];
		    				}
		    				else
		    				{
		    					$profile_pic = SITE_URL.'img/userImages/1503917394614.jpeg';
		    				}

		    				$review['profile_pic'] = $profile_pic;
		    				$review['username'] = $review['User']['firstname'].' '.$review['User']['lastname'];
		    				$review['profile_name'] = $review['User']['profile_name'];
		    				
							unset($review['User']);
		    				$reviews[] = $review;
		    				$counter++;

		    			endforeach;
		    			$user['User']['reviews'] = $reviews;
		    		}
		    		else
		    		{
		    			$user['User']['reviews'] = array();
		    		}
					unset($user['Review']);
		    		$userArr[] = $user['User'];
		    	}
	    	}
	    		
				if(isset($userArr))
				{
		    		foreach($userArr as $user):
		    			if(!empty($user['profile_pic']))
		    			{
		    				$user['profile_pic'] = SITE_URL.'img/userImages/'.$user['profile_pic'];
		    			}
		    			if(!empty($user['govt_id_pic']))
		    			{
		    				$user['govt_id_pic'] = SITE_URL.'img/userImages/'.$user['govt_id_pic'];
		    			}
		    			$responseArr[] = $user;

		    		endforeach;

		    		$direction = 'desc';
		    		usort($responseArr, create_function('$a, $b', '
				        $a = $a["score"];
				        $b = $b["score"];

				        if ($a == $b) return 0;
						return ($a ' . ($direction == 'desc' ? '>' : '<') .' $b) ? -1 : 1;
				    ')); 
				   // echo '<pre>'; print_r($responseArr); 
		    		$this->response = array('status'=>200,'message'=>'Success','data'=>$responseArr);
		    	}
		    	else
		    	{
		    		$userArr = array();
		    		$this->response = array('status'=>200,'message'=>'No reader found','data'=> (object) $userArr);
		    	}
	    	
	    }
	    else
	    {
	    	$userArr = array();
		    $this->response = array('status'=>200,'message'=>'No reader found','data'=> (object) $userArr);
	    }
    	
    	$this->__send_response();
    }



    function sortBy($field, $array, $direction)
	{
	    usort($array, create_function('$a, $b', '
	        $a = $a["' . $field . '"];
	        $b = $b["' . $field . '"];

	        if ($a == $b) return 0;
			return ($a ' . ($direction == 'desc' ? '>' : '<') .' $b) ? -1 : 1;
	    '));

	    return true;
	}

    /*Method: submit_feedback
	* Description: this method is used to submit feedback from reader
	* Created By:  Pooja chaudhary on 31-july-2017
	*/
    function submit_feedback()
    {

    	$data = $this->data;
    	if(!empty($data))
    	{
    		$this->loadModel('Feedback');
    		if(isset($data['reader_id']) && !empty($data['reader_id']))
    		{
    			$this->request->data['Feedback']['user_id'] = $data['reader_id'];
    		}
    		else
    		{
    			$this->response = array('status'=>400,'message'=>'please provide reader_id');
    			$this->__send_response();
    		}

    		if(isset($data['feedback']) && !empty($data['feedback']))
    		{
    			$this->request->data['Feedback'] = $data['feedback'];
    		}
    		else
    		{
    			$this->response = array('status'=>400,'message'=>'please provide reader_id');
    			$this->__send_response();
    		}

    		if($this->Feedback->save($this->request->data['Feedback']))
    		{
    			$this->response = array('status'=>200,'message'=>'success');
    		}
    		else
    		{
    			$this->response = array('status'=>400,'message'=>'failed');
    		}
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'please provide data');
    	}
    	$this->__send_response();
    }


    /*Method: get_faqs
	* Description: this method is used to fetch faqs
	* Created By:  Pooja chaudhary on 3-August-2017
	*/
    function get_faqs()
    {
		$data = $this->data;
    	if(isset($data['user_role']) && !empty($data['user_role']))
    	{
    		$user_role = $data['user_role'];
    		$this->loadModel('FaqTopic');
	    	$this->loadModel('Faq');
	    	$this->FaqTopic->bindModel(array('hasMany'=>array('Faq'=>array('class'=>'Faq','foreignKey'=>'topic_id'))));
	    	$this->paginate = array('conditions'=>array('FaqTopic.user_role IN'=>array($user_role,5)),'order'=>'FaqTopic.id DESC');
	    	$faqtopics = $this->paginate('FaqTopic');
	    	
	    	if($faqtopics)
	    	{
	    		foreach($faqtopics as $faqtopic):
	    			$faqdata = array();
	    			foreach($faqtopic['Faq'] as $faq):
		    			$faqdata[] = array
			    			(
			    				'id' => $faq['id'],
			    				'question' => $faq['question'],
			    				'answer' => $faq['answer'],
			    				'created' => date('d-m-Y',strtotime($faq['created'])),
			    				'modified' => date('d-m-Y',strtotime($faq['modified']))
		    				);
	    			endforeach;
	    			$faqArr[$faqtopic['FaqTopic']['topic']] = $faqdata;
	    			
	    		endforeach;
	    		$faqArray = $faqArr;
	    		$this->response = array('status'=>200,'message'=>'Success','data'=>$faqArray);
	    	}
	    	else
	    	{
	    		$this->response = array('status'=>200,'message'=>'success','data'=>array());
	    	}
		}
   		else
   		{
   			$this->response = array('status'=>200,'message'=>'please provide user role');
   		}
    	
    	$this->__send_response();
    }

    /*Method: fetch_languages
	* Description: this method is used to fetch languages
	* Created By:  Pooja chaudhary on 3-August-2017
	*/
    function fetch_languages()
    {
    	$this->loadModel('Language');
    	$languages = $this->Language->find('all',array('fields'=>array('id','name')));
    	if($languages)
    	{
    		foreach($languages as $language):
    			$languagedata[] = array(
    				'id' => $language['Language']['id'],
    				'name' => $language['Language']['name']
    				);
    		endforeach;
    		$this->response = array('status'=>200,'message'=>'success','data'=>$languagedata);
    	}
    	else
    	{
    		$this->response = array('status'=>200,'message'=>'success','data'=>array());
    	}
    	$this->__send_response();
    }


    /*Method: update_push
	* Description: this method is used to update push settings
	* Created By:  Pooja chaudhary on 3-August-2017
	*/
    function update_push()
    {
		$this->loadModel('User');
    	$data = $this->data;
    	if($data && !empty($data))
    	{
			if(isset($data['user_id']) && $data['user_id']!="")
    		{
    			$id = $data['user_id'];
    			$this->User->id = $id;
    		}
    		else
    		{
    			$this->response = array('status'=>400,'message'=>'Please provide user id');
    			$this->__send_response();
    		}

    		if(isset($data['push_msg']) && $data['push_msg']!="")
    		{
    			$this->request->data['User']['push_msg'] = $data['push_msg'];
    		}

    		if(isset($data['push_update']) && $data['push_update']!="")
    		{
    			$this->request->data['User']['push_update'] = $data['push_update'];
    		}

    		if(isset($data['push_recommend']) && $data['push_recommend']!="")
    		{
    			$this->request->data['User']['push_recommend'] = $data['push_recommend'];
    		}

    		if(isset($data['push_other']) && $data['push_other']!="")
    		{
    			$this->request->data['User']['push_other'] = $data['push_other'];
    		}

    		if(isset($data['txt_msg']) && $data['txt_msg']!="")
    		{
    			$this->request->data['User']['txt_msg'] = $data['txt_msg'];
    		}

    		if(isset($data['txt_update']) && $data['txt_update']!="")
    		{
    			$this->request->data['User']['txt_update'] = $data['txt_update'];
    		}

    		if(isset($data['txt_other']) && $data['txt_other']!="")
    		{
    			$this->request->data['User']['txt_other'] = $data['txt_other'];
    		}

    		if($this->User->save($this->request->data['User']))
    		{
    			$this->response = array('status'=>200,'message'=>'success');
    		}
    		else
    		{
    			$this->response = array('status'=>400,'message'=>'failed');
    		}
		}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'No data found , please provide data');
    	}
    	$this->__send_response();
    }


    /* This api is used to check promo code */
    public function checkPromo()
    {
    	$this->loadModel('User');
    	$this->loadModel('Project');
    	$data = $this->data;
    	$userId = $this->userId;
    	if(isset($data['promocode']) && !empty($data['promocode']))
    	{

    		if(isset($data['readerId']) && !empty($data['readerId']))
    		{
    			$readerId = $data['readerId'];
    			$reader = $this->User->findById($readerId);
    		}
    		else
    		{
    			$this->response = ['status'=>400,'message'=>'Please provide readerId'];
    			$this->__send_response();
    		}

    		$promocode = $data['promocode'];
    		$chkpromo = $this->User->findByPromoCode($promocode);
    		
    		if(!$chkpromo)
    		{
    			$this->response = ['status'=>400,'message'=>'Promocode not available'];
    			$this->__send_response();
    		}
    		else
			{
				$findpromo = $this->Project->find('all',array('conditions'=>['Project.client_id'=>$userId,'Project.promo_code'=>$promocode,'NOT' => array('Project.status IN' => array(2,3,5,7,8))]));
				
				if($findpromo)
				{
					$this->response = ['status'=>400,'message'=>'You have already redeemed this coupon!'];
					$this->__send_response();
				}
				else
				{
					$chkpr = $this->Project->find('all',array('conditions'=>['Project.promo_code !='=>'','Project.client_id'=>$userId,'NOT' => array('Project.status' => array(2,3,5,7,8))]));
					if($chkpr)
					{
						$this->response = ['status'=>400,'message'=>'You have already used a coupon'];
						$this->__send_response();
					}
					else
					{
		    			if(($chkpromo['User']['promo_count']<5) && ($reader['User']['reward_noti_count']<5))
		    			{
		    				$this->response = ['status'=>200,'message'=>'success'];
		    			}
		    			else
		    			{
		    				$this->response = ['status'=>400,'message'=>'Promocode not available'];
		    			}
		    		}
	    		}
    		}
    	}
    	else
    	{
    		$this->response = ['status'=>400,'message'=>'Please provide promocode'];
    	}
    	$this->__send_response();
    }

	
	/*
    * Method: scheduleTape
	* Description: this method is used to create a project or schedule a tape
	* Created By:  Pooja chaudhary on 29-Aug-2017
	*/
    function scheduleTape()
    {

    	$this->loadModel('DeviceToken');
    	$this->loadModel('Project');
    	$this->loadModel('StripeAccount');
    	$this->loadModel('UserCard');
    	$this->loadModel('User');
		$data = $this->data;
    	
    	if(isset($data['reader_id']) && $data['reader_id']!='')
    	{
    		$reader_id = $data['reader_id'];
    		$this->request->data['Project']['reader_id'] = $data['reader_id'];
    		$this->request->data['Project']['created'] = date('Y-m-d H:i:s');
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide reader id');
		}

    	if(isset($data['client_id']) && $data['client_id']!='')
    	{
    		$client_id = $data['client_id'];
    		$customer = $this->UserCard->findByUserId($client_id);
    		if(!$customer)
    		{
    			$this->response = array('status'=>'400','message'=>'Please add your credit card details');
    			$this->__send_response();
    		}
    		$this->request->data['Project']['client_id'] = $data['client_id'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide client id');
    	}

    	if(isset($data['tape_type']) && $data['tape_type']!='')
    	{
    		$tape_type = $data['tape_type'];
    		$this->request->data['Project']['tape_type'] = $data['tape_type'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide tape type');
    	}

    	if(isset($data['schedule_time']) && $data['schedule_time']!='')
    	{
    		$scheduledate = date('Y-m-d H:i:s',strtotime($data['schedule_time']));
    		$chkproject = $this->Project->find('first',array('conditions'=>array('Project.schedule_time'=>$scheduledate)));
    		if($chkProject)
    		{
    			$this->response = array('status'=>400,'message'=>'Sorry! this slot is not available, please schedule for other slot');
    			$this->__send_response();
    		}
    		else
    		{
    			$this->request->data['Project']['schedule_time'] = date('Y-m-d H:i:s',strtotime($data['schedule_time']));
    		}
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide schedule time');
    	}

    	if(isset($data['promo_code']) && !empty($data['promo_code']))
    	{
    		$this->request->data['Project']['promo_code'] = $data['promo_code'];
    	}

    	if(isset($data['project_name']) && !empty($data['project_name']))
    	{
    		$this->request->data['Project']['project_name'] = $data['project_name'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide project name');
    	}

    	if(isset($data['script']) && !empty($data['script']))
    	{
    		$this->request->data['Project']['script'] = $data['script'];
    	}

		if(isset($_FILES['script_file']) && !empty($_FILES['script_file']['name']))
    	{
	    	$file_name = $_FILES['script_file']['name'];
	    	if(move_uploaded_file($_FILES['script_file']['tmp_name'], WWW_ROOT.'img/scriptImages/'.$_FILES['script_file']['name']))
	    	{
	    		$this->request->data['Project']['script_file'] = $file_name;
	    		$this->request->data['Project']['script_path'] = 'img/scriptImages/'.$file_name;
	    	}
	    }

    	if(isset($data['call_duration']) && $data['call_duration']!='')
    	{
    		$this->request->data['Project']['duration'] = $data['call_duration'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide call duration');
    	}

    	if(isset($data['amount']) && $data['amount']!='')
    	{
			$this->request->data['Project']['amount'] = $data['amount'];
			$this->request->data['Project']['app_fees'] = 5;
			$this->request->data['Project']['reader_amount'] = $data['amount'] - 5;
    		$amount = $data['amount'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide reader id');
    	}

    	if(isset($data['token']) && $data['token']!='')
    	{
    		$token = $data['token'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide token');
    	}

    	if(isset($data['type']) && !empty($data['type']))
    	{
    		$type = $data['type'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide type of schedule tape');
    	}
    	if($scheduledate)
    	{
			$this->request->data['Project']['schedule_time_utc'] = $this->timezoneconvertToUtc( $scheduledate,$this->request->header('timezone') );
	    	$timeslot = date('h:i a',strtotime($scheduledate));
	    	$schedule_date = date('m-d-Y',strtotime($scheduledate));
    	}
    	else
    	{
    		$this->request->data['Project']['schedule_time_utc'] = date('Y-m-d H:i:s');
    		$scheduledate = date('Y-m-d H:i:s',strtotime($this->request->data['Project']['schedule_time_utc']));
    		$timeslot = date('h:i a',strtotime($scheduledate));
	    	$schedule_date = date('m-d-Y',strtotime($scheduledate));
    	}

    	
    	$this->request->data['Project']['timezone'] = $this->request->header('timezone');
    	
    	if($rs = $this->Project->save($this->request->data['Project']))
    	{

			if(isset($data['promo_code']))
			{
				$chkpromo = $this->User->findByPromoCode($promocode);
				$promo_count = $chkpromo['User']['promo_count'];
				$this->User->id = $chkpromo['User']['id'];
				$this->User->saveField('promo_count',$promo_count+1);
			}

			$get_reader = $this->User->findById($reader_id);
			$readername = $get_reader['User']['firstname'];
			$to = $get_reader['User']['email'];
			$get_client = $this->User->findById($client_id);
			$clientname = $get_client['User']['firstname'];
			
			//noti code start here //
			$this->sendNotification($client_id,$reader_id,'Tape Request',array('scheduledate'=>$scheduledate,'timeslot'=>$timeslot,'tape_type'=>$tape_type));
			//noti code ends here //

			$arrFind = array('{readername}','{clientname}','{scheduledate}','{timeslot}');
			$arrReplace = array($readername,$clientname,$schedule_date,$timeslot);
			$title = 'schedule_tape';
			$result = $this->sendmail($title,$arrFind,$arrReplace,$to);
			$this->response = array('status'=>200,'message'=>'success');

    	}
    	else
    	{
			$this->response = array('status'=>400,'message'=>'failed');
    	}

    	$this->__send_response();
    }

    /*Method: joinTape
    * Description: This method is used to update tape in progress
    * Created By: Pooja chaudhary on 26-December-2017
    */
    public function joinTape()
    {
    	$this->loadModel('Project');
    	$userId = $this->userId;

    	$user = $this->User->findById($userId);
    	
		$data = $this->data;
    	if(isset($data['project_id']) && !empty($data['project_id']))
    	{
    		$project_id = $data['project_id'];
    		$project = $this->Project->findById($project_id);
    		if($user['User']['user_role']==2)
	    	{
	    		$userrole = 'client';
	    		$receiver_id = $project['Project']['reader_id'];
	    	}
	    	elseif($user['User']['user_role']==3)
	    	{
	    		$userrole = 'reader';
	    		$receiver_id = $project['Project']['client_id'];
	    	}
	    
    		$this->Project->id = $project_id;
    		if($this->Project->saveField('status',4))
    		{

    			//noti code start here //
    			$this->sendNotification($userId,$receiver_id,'In Progress');
				//noti code ends here //
    			$this->response = array('status'=>200,'message'=>'success');
    		}
    		else
    		{
    			$this->response = array('status'=>400,'message'=>'Database Error');
    		}

    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide project id');
    	}
    	$this->__send_response();
    }


    /*
    * Method: fetchProjects
    * Description: This method is used to fetch all projects
    * Created By: Pooja chaudhary on 5-september-2017
    */
    public function fetchProjects()
    {
		//Configure::write('debug',true);
    	$data = $this->data;
	    $this->loadModel('Project');	
	    $this->loadModel('User');
	    $this->loadModel('Review');
	    date_default_timezone_set('UTC');
	    $now = date('Y-m-d H:i:s');
	    //$now = $this->timezoneconvertToUtc($now , $this->user_timezone);
	    if(isset($data['type']) && !empty($data['type']))
	    {
	    	$type = $data['type'];
	    	$cond = array();
	    	
		    if($type=='pending')
		    {
		    	$cond['AND'] = array("Project.status"=>2,'Project.schedule_time_utc >'=>$now);
		    }
		    elseif($type=='confirmed')
		    {
		    	$cond['AND'] = array('Project.status'=>1);
		    }
		    elseif($type=='progress')
		    {
		    	$cond['AND'] = array('Project.status'=>4);
		    }
		    elseif($type=='completed')
		    {
		    	$cond['AND'] = array('Project.status'=>6);
		    }
	    
	    }
	    else
	    {
		    $cond['AND'] = array("Project.status"=>2);
		}
	    
		
	    if(isset($data['client_id']) && !empty($data['client_id']))
	    {
	    		$client_id = $data['client_id'];
	    		$this->Project->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'reader_id','fields'=>array('id','firstname','lastname','profile_pic')))));
	    		$getProjects = $this->Project->find('all',array('conditions'=>array('Project.client_id'=>$client_id,$cond),'order'=>'Project.id DESC'));
		    		if($getProjects)
		    		{
		    			$projectsArr = array();
		    			foreach($getProjects as $proj):
		    				$schedule_time = $this->timezoneconvert($proj['Project']['schedule_time'], $proj['Project']['timezone'] , $this->user_timezone);
		    				$proj['Project']['schedule_time'] = $schedule_time;
		    				if($type=='confirmed' || $type=='progress')
		    				{
		    					$duration = $proj['Project']['duration'];
		    					$tapeEndTime = date('Y-m-d H:i:s',strtotime('+'.$duration.' minutes',strtotime($proj['Project']['schedule_time_utc'])));
		    					$comparetime = date('Y-m-d H:i:s');
		    					if($comparetime < $tapeEndTime && $proj['Project']['call_status']!=2)
		    					{

		    						$projectsArr[] = $proj;
		    					}
		    				}
		    				else
		    				{
		    					$projectsArr[] = $proj;
		    				}
		    			endforeach;
		    			$projects = array();
		    			foreach($projectsArr as $pro):
		    					if(!empty($pro['Project']['reader_video'])){
		    						$pro['Project']['reader_video'] = SITE_URL.$pro['Project']['reader_video'];
		    					}
		    					if(!empty($pro['Project']['script_path'])){
		    						$pro['Project']['script_path'] = SITE_URL.$pro['Project']['script_path'];
		    					}
		    					if(!empty($pro['Project']['client_video'])){
		    						$pro['Project']['client_video'] = SITE_URL.$pro['Project']['client_video'];
		    					}
			    				
			    				if(!empty($pro['User']['profile_pic'])){
			    					$pro['User']['profile_pic'] = SITE_URL.'img/userImages/'.$pro['User']['profile_pic'];
								}

								$reviews = $this->Review->find('all',array('conditions'=>array('Review.project_id'=>$pro['Project']['id'])));
			    				if($reviews)
			    				{
			    					$reviewsArr = array();
			    					foreach ($reviews as $value){
			    						$reviewedBy = $this->User->find('first',array('conditions'=>array('User.id'=>$value['Review']['login_id']),'fields'=>array('id','firstname','lastname','profile_pic')));
			    						if(!empty($reviewedBy['User']['profile_pic'])){
					    					$reviewedBy['User']['profile_pic'] = SITE_URL.'img/userImages/'.$reviewedBy['User']['profile_pic'];
										}
										$value['Review']['reviewedBy'] = $reviewedBy['User'];

			    						$reviewedTo = $this->User->find('first',array('conditions'=>array('User.id'=>$value['Review']['user_id']),'fields'=>array('id','firstname','lastname','profile_pic')));
			    						if(!empty($reviewedTo['User']['profile_pic'])){
					    					$reviewedTo['User']['profile_pic'] = SITE_URL.'img/userImages/'.$reviewedTo['User']['profile_pic'];
										}
										$value['Review']['reviewedTo'] = $reviewedTo['User'];

			    						$reviewsArr[] = $value['Review'];
			    					}
			    					$pro['Project']['reviews'] = $reviewsArr;
			    				}
			    				else
			    				{
		    						$pro['Project']['reviews'] = array();
		    					}

								$ratings = $this->Review->find('all',array('conditions'=>array('Review.user_id'=>$pro['User']['id'])));
								$final_score = 0;
								$score = 0;
								if($ratings)
								{

									foreach($ratings as $rating):
										$score = $score+$rating['Review']['rating_score'];
									endforeach;
									$final_score = floor($score/sizeOf($ratings));
								}
								$pro['User']['rating_score'] = $final_score;
			    				$pro['Project']['User'] = $pro['User'];
			   					$projects[] = $pro['Project'];
		    			endforeach;
		    			if($projects)
		    			{
		    				$response['Projects'] = $projects;
		    			}
		    			else
		    			{
		    				$response['Projects'] = array();
		    			}
		    			$this->response = array('status'=>200,'message'=>'success','data'=>$response);
		    		}
		    		else
		    		{
		    			$this->response = array('status'=>200,'message'=>'success','data'=>array());
		    		}


	    }
	    elseif(isset($data['reader_id']) && !empty($data['reader_id']))
	    {

		    		$reader_id = $data['reader_id'];
		    		$this->Project->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'client_id','fields'=>array('id','firstname','lastname','profile_pic')))));
		    		$getProjects = $this->Project->find('all',array('conditions'=>array('Project.reader_id'=>$reader_id,$cond),'order'=>'Project.id DESC'));
		    		print "<pre>";print_r($getProjects);exit;
		    		if($getProjects)
		    		{
		    			$projectsArr = array();
		    			foreach($getProjects as $proj):
		    				$schedule_time = $this->timezoneconvert($proj['Project']['schedule_time'], $proj['Project']['timezone'] , $this->user_timezone);
		    				$proj['Project']['schedule_time'] = $schedule_time;

		    				if($type=='confirmed' || $type=='progress')
		    				{
		    					$duration = $proj['Project']['duration'];
		    					$ptime = $proj['Project']['schedule_time_utc'];
		    					$tapeEndTime = date('Y-m-d H:i:s',strtotime('+'.$duration.' minutes',strtotime($ptime)));
		    					$comparetime = date('Y-m-d H:i:s');
		    					
		    					if($comparetime < $tapeEndTime){
		    						$projectsArr[] = $proj;
		    					}
		    				}
		    				else
		    				{
		    					$projectsArr[] = $proj;
		    				}
		    			endforeach;
		    			$projects = array();
		    			foreach($projectsArr as $pro):
		    				if(!empty($pro['Project']['script_path']))
		    				{
		    						$pro['Project']['script_path'] = SITE_URL.$pro['Project']['script_path'];
		    				}
		    				if(!empty($pro['Project']['reader_video']))
		    				{
		    					$pro['Project']['reader_video'] = SITE_URL.$pro['Project']['reader_video'];
		    				}
		    				if(!empty($pro['Project']['client_video']))
		    				{
		    					$pro['Project']['client_video'] = SITE_URL.$pro['Project']['client_video'];
		    				}
		    				
							if(!empty($pro['User']['profile_pic']))
							{
		    					$pro['User']['profile_pic'] = SITE_URL.'img/userImages/'.$pro['User']['profile_pic'];
							}

							$reviews = $this->Review->find('all',array('conditions'=>array('Review.project_id'=>$pro['Project']['id'])));
			    				if($reviews)
			    				{
			    					$reviewsArr = array();
			    					foreach ($reviews as $value)
			    					{
			    						$reviewedBy = $this->User->find('first',array('conditions'=>array('User.id'=>$value['Review']['login_id']),'fields'=>array('id','firstname','lastname','profile_pic')));
			    						if(!empty($reviewedBy['User']['profile_pic']))
			    						{
					    					$reviewedBy['User']['profile_pic'] = SITE_URL.'img/userImages/'.$reviewedBy['User']['profile_pic'];
										}
										$value['Review']['reviewedBy'] = $reviewedBy['User'];

			    						$reviewedTo = $this->User->find('first',array('conditions'=>array('User.id'=>$value['Review']['user_id']),'fields'=>array('id','firstname','lastname','profile_pic')));
			    						if(!empty($reviewedTo['User']['profile_pic']))
			    						{
					    					$reviewedTo['User']['profile_pic'] = SITE_URL.'img/userImages/'.$reviewedTo['User']['profile_pic'];
										}
										$value['Review']['reviewedTo'] = $reviewedTo['User'];

			    						$reviewsArr[] = $value['Review'];
			    					}
			    					$pro['Project']['reviews'] = $reviewsArr;
			    				}
			    				else
			    				{
		    						$pro['Project']['reviews'] = array();
		    					}

							$ratings = $this->Review->find('all',array('conditions'=>array('Review.user_id'=>$pro['User']['id'])));
							$final_score = 0;
							$score = 0;
							if($ratings)
							{
								foreach($ratings as $rating):
									$score = $score+$rating['Review']['rating_score'];
								endforeach;
								$final_score = floor($score/sizeOf($ratings));
							}
							$pro['User']['rating_score'] = $final_score;
		    				$pro['Project']['User'] = $pro['User'];
		    				$projects[] = $pro['Project'];
		    			endforeach;

		    			if($projects)
		    			{
		    				$response['Projects'] = $projects;
		    			}
		    			else
		    			{
		    				$response['Projects'] = array();
		    			}
		    			$this->response = array('status'=>200,'message'=>'success','data'=>$response);

		    		}
		    		else
		    		{
		    			$this->response = array('status'=>200,'message'=>'success','data'=>array());
		    		}
		}
		else
		{
		    $this->response = array('status'=>200,'message'=>'Please provide data');
		}
		
	    $this->__send_response();
	}


	function projectHistory(){

		$data = $this->data;
	    $this->loadModel('Project');	
	    $this->loadModel('User');
	    $this->loadModel('Review');
	    $this->loadModel('Dispute');
	    date_default_timezone_set($this->user_timezone);
	    $now = date('Y-m-d H:i:s');

	    if(isset($data['client_id']) && !empty($data['client_id'])){
	    		$client_id = $data['client_id'];
	    		//$this->Project->bindModel(array('belongsTo'=>array('Dispute'=>array('class'=>'Dispute','foreignKey'=>'id','fields'=>array('id','project_id','status')))));
	    		$this->Project->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'reader_id','fields'=>array('id','firstname','lastname','profile_pic')))));
	    		$getProjects = $this->Project->find('all',array('conditions'=>array('Project.client_id'=>$client_id),'order'=>'Project.id DESC'));
		    		if($getProjects)
		    		{
		    			$projectsArr = array();
		    			foreach($getProjects as $proj):
		    				
		    					$duration = $proj['Project']['duration']+2;
		    					$tapeEndTime = date('Y-m-d H:i:s',strtotime('+'.$duration.' minutes',strtotime($proj['Project']['schedule_time'])));

		    					$call_status = $proj['Project']['call_status'];
		    					$status = $proj['Project']['status'];
		    					if(($now > $tapeEndTime) || $call_status==2 || in_array($status, array(3,5,6,7,8))){
		    						$projectsArr[] = $proj;
		    					}
		    				
		    			endforeach;
		    			
		    			foreach($projectsArr as $pro):

		    					if($pro['Project']['is_dispute']==1)
		    					{
		    						$dispute = $this->Dispute->find('first',array('conditions'=>['Dispute.project_id'=>$pro['Project']['id']]));
		    						if($dispute)
		    						{
		    							if($dispute['Dispute']['status']==1)
		    							{
		    								$dispute_status = 'Open';
		    							}
		    							elseif($dispute['Dispute']['status']==2)
		    							{
		    								$dispute_status = 'In Process';
		    							}
		    							elseif($dispute['Dispute']['status']==3)
		    							{
		    								$dispute_status = 'Resolved';
		    							}
		    							elseif($dispute['Dispute']['status']==4)
		    							{
		    								$dispute_status = 'Refunded';
		    							}
		    							elseif($dispute['Dispute']['status']==5)
		    							{
		    								$dispute_status = 'Declined';
		    							}

		    							$pro['Project']['dispute_status'] = $dispute_status;
		    						}
		    						else
		    						{
		    							$pro['Project']['dispute_status'] = 1;
		    						}
		    					}
		    					else{
		    						$pro['Project']['dispute_status'] = '';
		    					}

		    					if(!empty($pro['Project']['reader_video']))
		    					{
		    						$pro['Project']['reader_video'] = SITE_URL.$pro['Project']['reader_video'];
		    					}
		    					if(!empty($pro['Project']['script_path']))
		    					{
		    						$pro['Project']['script_path'] = SITE_URL.$pro['Project']['script_path'];
		    					}
		    					if(!empty($pro['Project']['client_video']))
		    					{
		    						$pro['Project']['client_video'] = SITE_URL.$pro['Project']['client_video'];
		    					}
			    				$reviews = $this->Review->find('all',array('conditions'=>array('Review.project_id'=>$pro['Project']['id'])));
			    				if($reviews)
			    				{
			    					$reviewsArr = array();
			    					foreach ($reviews as $value)
			    					{
			    						$reviewedBy = $this->User->find('first',array('conditions'=>array('User.id'=>$value['Review']['login_id']),'fields'=>array('id','firstname','lastname','profile_pic')));
			    						if(!empty($reviewedBy['User']['profile_pic']))
			    						{
					    					$reviewedBy['User']['profile_pic'] = SITE_URL.'img/userImages/'.$reviewedBy['User']['profile_pic'];
										}
										$value['Review']['reviewedBy'] = $reviewedBy['User'];

			    						$reviewedTo = $this->User->find('first',array('conditions'=>array('User.id'=>$value['Review']['user_id']),'fields'=>array('id','firstname','lastname','profile_pic')));
			    						if(!empty($reviewedTo['User']['profile_pic']))
			    						{
					    					$reviewedTo['User']['profile_pic'] = SITE_URL.'img/userImages/'.$reviewedTo['User']['profile_pic'];
										}
										$value['Review']['reviewedTo'] = $reviewedTo['User'];

			    						$reviewsArr[] = $value['Review'];
			    					}
			    					$pro['Project']['reviews'] = $reviewsArr;
			    				}
			    				else
			    				{
		    						$pro['Project']['reviews'] = array();
		    					}
			    				if(!empty($pro['User']['profile_pic']))
			    				{
			    					$pro['User']['profile_pic'] = SITE_URL.'img/userImages/'.$pro['User']['profile_pic'];
								}

								$ratings = $this->Review->find('all',array('conditions'=>array('Review.user_id'=>$pro['User']['id'])));
								$final_score = 0;
								$score = 0;
								if($ratings)
								{

									foreach($ratings as $rating):
										$score = $score+$rating['Review']['rating_score'];
									endforeach;
									$final_score = floor($score/sizeOf($ratings));
								}
								$pro['User']['rating_score'] = $final_score;
			    				$pro['Project']['User'] = $pro['User'];
			    				$projects[] = $pro['Project'];
		    			endforeach;

		    			if($projects)
		    			{
		    				$response['Projects'] = $projects;
		    			}
		    			else
		    			{
		    				$response['Projects'] = array();
		    			}
		    			$this->response = array('status'=>200,'message'=>'success','data'=>$response);
		    		}else
		    		{
		    			$this->response = array('status'=>200,'message'=>'success','data'=>array());
		    		}
		}
	    elseif(isset($data['reader_id']) && !empty($data['reader_id']))
	    {
				$reader_id = $data['reader_id'];
		    	$this->Project->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'client_id','fields'=>array('id','firstname','lastname','profile_pic')))));
		    	$getProjects = $this->Project->find('all',array('conditions'=>array('Project.reader_id'=>$reader_id),'order'=>'Project.id DESC'));
		    	if($getProjects){

		    		$projectsArr = array();
		    		foreach($getProjects as $proj):
		    				
		    			$duration = $proj['Project']['duration']+2;
		    			$tapeEndTime = date('Y-m-d H:i:s',strtotime('+'.$duration.' minutes',strtotime($proj['Project']['schedule_time'])));
		    			$call_status = $proj['Project']['call_status'];
		    			$status = $proj['Project']['status'];
		    			if(($now > $tapeEndTime) || ($call_status==2) || (in_array($status, array(3,5,6,7,8)))){
		    				$projectsArr[] = $proj;
		    			}

		    		endforeach;
		    			
		    		foreach($projectsArr as $pro):

		    			if($pro['Project']['is_dispute']==1)
		    				{
		    					$dispute = $this->Dispute->find('first',array('conditions'=>['Dispute.project_id'=>$pro['Project']['id']]));
		    					if($dispute)
		    					{
		    						if($dispute['Dispute']['status']==1)
		    						{
		    							$dispute_status = 'Open';
		    						}
		    						elseif($dispute['Dispute']['status']==2)
		    						{
		    							$dispute_status = 'In Process';
		    						}
		    						elseif($dispute['Dispute']['status']==3)
		    						{
		    							$dispute_status = 'Resolved';
		    						}
		    						elseif($dispute['Dispute']['status']==4)
		    						{
		    							$dispute_status = 'Refunded';
		    						}
		    						elseif($dispute['Dispute']['status']==5)
		    						{
		    							$dispute_status = 'Declined';
		    						}
		    						$pro['Project']['dispute_status'] = $dispute_status;
		    					}
		    					else
		    					{
		    						$pro['Project']['dispute_status'] = 1;
		    					}
		    				}
		    				else{
		    						$pro['Project']['dispute_status'] = '';
		    				}

		    				if(!empty($pro['Project']['script_path'])){
		    						$pro['Project']['script_path'] = SITE_URL.$pro['Project']['script_path'];
		    				}
		    				if(!empty($pro['Project']['reader_video'])){
		    					$pro['Project']['reader_video'] = SITE_URL.$pro['Project']['reader_video'];
		    				}
		    				if(!empty($pro['Project']['client_video'])){
		    					$pro['Project']['client_video'] = SITE_URL.$pro['Project']['client_video'];
		    				}

		    				$reviews = $this->Review->find('all',array('conditions'=>array('Review.project_id'=>$pro['Project']['id'])));
		    				if($reviews)
		    				{
		    					$reviewsArr = array();
		    					foreach ($reviews as $value)
		    					{
		    						$reviewedBy = $this->User->find('first',array('conditions'=>array('User.id'=>$value['Review']['login_id']),'fields'=>array('id','firstname','lastname','profile_pic')));
		    						if(!empty($reviewedBy['User']['profile_pic']))
		    						{
				    					$reviewedBy['User']['profile_pic'] = SITE_URL.'img/userImages/'.$reviewedBy['User']['profile_pic'];
									}
									$value['Review']['reviewedBy'] = $reviewedBy['User'];
									$reviewedTo = $this->User->find('first',array('conditions'=>array('User.id'=>$value['Review']['user_id']),'fields'=>array('id','firstname','lastname','profile_pic')));
		    						if(!empty($reviewedTo['User']['profile_pic']))
		    						{
				    					$reviewedTo['User']['profile_pic'] = SITE_URL.'img/userImages/'.$reviewedTo['User']['profile_pic'];
									}
									$value['Review']['reviewedTo'] = $reviewedTo['User'];
									$reviewsArr[] = $value['Review'];
		    					}
		    					$pro['Project']['reviews'] = $reviewsArr;
		    				}
		    				else
		    				{
		    					$pro['Project']['reviews'] = array();
		    				}
		    				if(!empty($pro['User']['profile_pic']))
		    				{
		    					$pro['User']['profile_pic'] = SITE_URL.'img/userImages/'.$pro['User']['profile_pic'];
							}

							$ratings = $this->Review->find('all',array('conditions'=>array('Review.user_id'=>$pro['User']['id'])));
							$final_score = 0;
							$score = 0;
							if($ratings)
							{
								foreach($ratings as $rating):
									$score = $score+$rating['Review']['rating_score'];
								endforeach;
								$final_score = floor($score/sizeOf($ratings));
							}
							$pro['User']['rating_score'] = $final_score;
		    				$pro['Project']['User'] = $pro['User'];
		    				$projects[] = $pro['Project'];

		    			endforeach;

		    			if($projects)
		    			{
		    				$response['Projects'] = $projects;
		    			}
		    			else
		    			{
		    				$response['Projects'] = array();
		    			}
		    			$this->response = array('status'=>200,'message'=>'success','data'=>$response);
					}
		    		else
		    		{
		    			$this->response = array('status'=>200,'message'=>'success','data'=>array());
		    		}
		}
		else
		{
		    $this->response = array('status'=>200,'message'=>'Please provide data');
		}
	    $this->__send_response();

	}


    /*Method: updateProjectData
    * Description: this method is used to update project data
    * Created By: Pooja chaudhary on 5-september-2017
    */
    function updateProjectData()
    {
    	$this->loadModel('Project');
    	$this->loadModel('DeviceToken');
    	$userId = $this->userId;
    	$data = $this->data;
    	if(isset($data['reader_id']) && !empty($data['reader_id']))
    	{
    		$reader_id = $data['reader_id'];
		}else
		{
    		$this->response = array('status'=>400,'message'=>'Please provide client id');
    	}

    	if(isset($data['project_id']) && !empty($data['project_id']))
    	{
    		$project_id = $data['project_id'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide project id');
    	}

    	if(isset($data['script']) && !empty($data['script']))
    	{
    		$this->request->data['Project']['script'] = $data['script'];
    	}

    	if(isset($data['project_name']) && !empty($data['project_name']))
    	{
    		$this->request->data['Project']['project_name'] = $data['project_name'];
    	}

    	if(isset($data['sendTo_reader']) && !empty($data['sendTo_reader']))
    	{
    		$this->request->data['Project']['sendTo_reader'] = $data['sendTo_reader'];
    	}

    	if(isset($_FILES['script_file']) && !empty($_FILES['script_file']['name']))
    	{
	    	$file_name = $_FILES['script_file']['name'];
	    	if(move_uploaded_file($_FILES['script_file']['tmp_name'], WWW_ROOT.'img/scriptImages/'.$_FILES['script_file']['name']))
	    	{
	    		$this->request->data['Project']['script_file'] = $file_name;
	    		$this->request->data['Project']['script_path'] = 'img/scriptImages/'.$file_name;
	    	}
	    }

    	$this->Project->id = $project_id;
    	if($this->Project->save($this->request->data))
    	{
    		$project = $this->Project->findById($project_id);
    		$timeslot = date('H:i',strtotime($project['Project']['schedule_time']));
    		$scheduledate = date('m-d-Y',strtotime($project['Project']['schedule_time']));
    		//noti code start here //
			$this->sendNotification($userId,$reader_id,'Tape Updated',array('scheduledate'=>$scheduledate,'timeslot'=>$timeslot));
			//noti code ends here //

			$this->response = array('status'=>200,'message'=>'success');
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'failed');
    	}

    	$this->__send_response();
    }

   /* Method: updateCallStatus
    * Description: This method is used to update tape call status
    * Created By: Pooja chaudhary on 4-January-2018
    */
    function updateCallStatus()
    {
		//Configure::write('debug',true);
    	$this->loadModel('Project');
    	$data = $this->data;
    	if(isset($data['project_id']) && !empty($data['project_id']))
    	{
			$project_id = $data['project_id'];
			if($data['status'] == -1)
			{
				$project = $this->Project->find('first',array('conditions'=>array('Project.id'=>$project_id),'fields'=>array('call_status','call_start_time')));
				$newcall = $this->Project->find('first',array('conditions'=>array('Project.id'=>$project_id),'fields'=>array('reader_id','client_id','tape_type','project_name','script','script_file','duration','amount','token')));
				$newcall['schedule_time'] = date('Y-m-d H:i:s');
				$this->response = array('status'=>200,'message'=>'success','data'=>$project['Project'],'newcall'=>$newcall);
				$this->__send_response();
				exit;
			}
			else if($data['status'] == -2)
			{
				$project = $this->Project->find('first',array('conditions'=>array('Project.id'=>$project_id),'fields'=>array('call_status','call_start_time')));
				$this->response = array('status'=>200,'message'=>'success','data'=>$project['Project']);
				$this->__send_response();
				exit;
			}
			else{
			$project = $this->Project->find('first',array('conditions'=>array('Project.id'=>$project_id),'fields'=>array('call_status','call_start_time')));
			if(isset($data['call_by']) && !empty($data['call_by']))
			{
				$call_by = $data['call_by'];
			}else
			{
				$this->response = array('status'=>200,'message'=>'Please provide call by value');
				$this->__send_response();
			}
    		$status = $data['status'];
    		
    		$this->Project->id = $project_id;
    		if($this->Project->saveField('call_status',$status))
    		{
    			if($status==2)
    			{
    				$project = $this->Project->findById($project_id);
    				if($call_by=='actor')
    				{
    					//noti code start here //
						$this->sendNotification($project['Project']['client_id'],$project['Project']['reader_id'],'Call End');
						//noti code ends here //
    				}
    				elseif($call_by=='reader')
    				{
    					//noti code start here //
						$this->sendNotification($project['Project']['reader_id'],$project['Project']['client_id'],'Call End');
						//noti code ends here //
    				}	
    				$this->Project->id = $project_id;
    				$this->Project->saveField('status',6);
    			}
    			elseif($status==1 && $project['Project']['call_start_time']=='')
    			{
    				$project = $this->Project->findById($project_id);
    				if($call_by=='actor')
    				{
    					$this->Project->id = $project_id;
    				    $this->Project->saveField('call_joinby_actor',1);
    				    if($project['Project']['call_joinby_reader']==1)
    				    {
    				    	$this->Project->id = $project_id;
    				    	$this->Project->saveField('call_start_time',$data['call_start_time']);
    				    }
    					//noti code start here //
						$this->sendNotification($project['Project']['client_id'],$project['Project']['reader_id'],'CallStart');
						//noti code ends here //
    				}
    				elseif($call_by=='reader')
    				{
    					$this->Project->id = $project_id;
    				    $this->Project->saveField('call_joinby_reader',1);
    				    if($project['Project']['call_joinby_actor']==1)
    				    {
    				    	$this->Project->id = $project_id;
    				    	$this->Project->saveField('call_start_time',$data['call_start_time']);
    				    }
    					//noti code start here //
						$this->sendNotification($project['Project']['reader_id'],$project['Project']['client_id'],'CallStart');
						//noti code ends here //
    				}	
				}

    			$project = $this->Project->find('first',array('conditions'=>array('Project.id'=>$project_id),'fields'=>array('call_status','call_start_time')));
    			$this->response = array('status'=>200,'message'=>'success','data'=>$project['Project']);

    		}
    		else
    		{
    			$this->response = array('status'=>400,'message'=>'Server Error');
			}
		}
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'please provide project id');
    	}
    	$this->__send_response();
    }


   /* Method: withdraw_project
    * Description: This method is used to delete tape request before reader accept the request
    * Created By: Pooja chaudhary on 5-December-2017
    */
    function withdraw_project()
    {

    	$this->loadModel('Project');
    	$data = $this->data;
    	if(isset($data['project_id']) && !empty($data['project_id']))
    	{
    		$project_id = $data['project_id'];

    		$this->Project->id = $project_id;
    		if($this->Project->delete())
    		{
    			$this->response = array('status'=>200,'message'=>'project deleted successfully');
    		}
    		else
    		{
    			$this->response = array('status'=>400,'message'=>'project could not deleted');
    		}
    		
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide project id');
    		
    	}
    	$this->__send_response();
    }


   /* Method: acceptDeclineTape
    * Description: This method is used to update tape request status
    * Created By: Pooja chaudhary on 6-september-2017
    */
    function acceptDeclineTape()
    {
		$this->loadModel('Project');
    	$this->loadModel('StripeAccount');
    	$this->loadModel('ReaderCard');
    	$this->loadModel('UserCard');
    	$this->loadModel('User');
    	$data = $this->data;
    	$userId = $this->userId;
    	if(isset($data['project_id']) && !empty($data['project_id']))
    	{
    		$project_id = $data['project_id'];
    		$chkproject = $this->Project->findById($project_id);
    		$client_id = $chkproject['Project']['client_id'];
    		$amount = $chkproject['Project']['amount'];
    		if(!$chkproject)
    		{
    			$this->response = array('status'=>400,'message'=>'Invalid project id');
    			$this->__send_response();
    			exit;
    		}
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide project id');
    		$this->__send_response();
    		exit;
    	}

		if(isset($data['status']) && !empty($data['status']))
    	{
    		$status = $data['status'];
    		$this->loadModel('StripeCharge');
			$account = $this->StripeAccount->findByReaderId($userId);
			if(!$account)
			{
				$acc_id =  $this->createReaderStripeAccount($userId);
			}
			else
			{
				$acc_id = $account['StripeAccount']['account_id'];
			}
			
			$readerCard = $this->ReaderCard->findByReaderId($userId);
			if(!$readerCard)
			{
				$this->response = array('status'=>400,'message'=>'Please add your debit card details');
				$this->__send_response();
			}

			$customer = $this->UserCard->findByUserId($client_id);
			$customer_id = $customer['UserCard']['customer_id'];
			$readerid = $chkproject['Project']['reader_id'];
    		$reader = $this->User->findById($readerid);
    		$promocount = $reader['User']['promo_count'];
    		if($status == 1)
    		{
    			
    			$duration = $chkproject['Project']['duration'];
    			if($chkproject['Project']['promo_code']!='')
    			{
    				$promocount = $reader['User']['promo_count']+1;
    				if($duration==30){
    					$tape_fee = 3;
	    			}elseif($duration==60){
	    				$tape_fee =8;
	    			}
    			}
    			else
    			{
    				if($duration==30){
	    				$tape_fee = 5;
	    			}elseif($duration==60){
	    				$tape_fee = 10;
	    			}
    			}
    		// echo $amount;	
    		 	$app_fee = $tape_fee*100;
    		  	$transfer_amount = ($amount*100)-$app_fee;
    		 	
    		 	if($acc_id && $customer_id)
    		 	{
		   			$chargedata = array();
		    		$chargedata['amount'] =  $amount*100;
		    		$chargedata['app_fees'] = $app_fee;
		    		//$chargedata['reader_amount'] = $transfer_amount;
		    		$chargedata['acc_id'] = $acc_id;
		    		$chargedata['customer_id'] = $customer_id;
		    		//echo '<pre>'; print_r($chargedata); die;
		    		$charge = $this->Stripe->create_charge($chargedata);
		    		
		    		if($charge['status']==400)
					{
						$this->response = $charge;
						$this->__send_response();

					}
					else
					{

						$charge_id = $charge['id'];
						$stripedata = array(
							'StripeCharge' => array(
								
								'project_id' => $project_id,
								'charge_id' => $charge_id,
								'amount' => $amount,
								'app_fee' => $app_fee/100,
								'reader_amount' => $transfer_amount/100,
								'on_behalf_of' => $charge['on_behalf_of'],
								'status' => $charge['status'],
								'transfer_group' => $charge['transfer_group'],
								'response' => json_encode($charge),
								'created' => date('Y-m-d H:i:s'),
							)
						);

						$this->StripeCharge->create();
						$this->StripeCharge->save($stripedata);
					}
					$type = 'TapeRequest Accepted';
				}
				else
				{
					$this->response = array('status'=>200,'message'=>'Please check your billing preferences');
					$this->__send_response();
				} 
    		}
    		else
    		{
    				$type = 'TapeRequest Declined';
			}

    		$this->Project->id = $project_id;
			if($this->Project->saveField('status',$status))
			{
				$this->User->id = $readerid;
				$this->User->saveField('promo_count',$promocount);
				//noti code start here //
				$this->sendNotification($userId,$client_id,$type);
				//noti code ends here //
				$this->response = array('status'=>200,'message'=>'success');
			
			}else
			{
				$this->response = array('status'=>400,'message'=>'Server error');
			}

    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide status');
    		$this->__send_response();
    		exit;
    	}

    	$this->__send_response();
    }


   /* Method: addCard
    * Description: this is used to add cards
    * Created By: Pooja chaudhary on 1-september-2017
    */
	function addCard()
	{
    	
    	$this->loadModel('UserCard');
    	$this->loadModel('User');
    	$this->loadModel('StripeAccount');
    	$data = $this->data;
    	if(isset($data['user_id']) && !empty($data['user_id']))
    	{
    		$user_id = $data['user_id'];
    		$getuser = $this->User->findById($user_id);
    		$user_role = $getuser['User']['user_role'];
    		$user_email = $getuser['User']['email'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide user id');
    		$this->__send_response();
    	}

    	if(isset($data['card_token']) && !empty($data['card_token']))
    	{
    		$token = $data['card_token'];
    	}else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide card token');
    		$this->__send_response();
    	}

		$chk_customer = $this->UserCard->findByUserId($user_id);
    	if($chk_customer)
    	{
    		$customer_id = $chk_customer['UserCard']['customer_id'];
    		try{

	    		\Stripe\Stripe::setApiKey("sk_test_xpYOfExQDR85H8mA7OXx083M");
				$customer = \Stripe\Customer::retrieve($customer_id);
				$card_response = $customer->sources->create(array("source" => $token));
				$response = $card_response->__toArray();
				
					$customerArr = array(
			    		'UserCard' => array(
			    			'user_id' => $user_id,
			    			'card_token' => $token,
			    			'customer_id' => $chk_customer['UserCard']['customer_id'],
			    			'card_id'=> $response['id'],
			    			'acc_bal'=> $chk_customer['UserCard']['acc_bal'],
			    			'default_source' => $chk_customer['UserCard']['default_source'],
			    			'description' => $chk_customer['UserCard']['description'],
			    			'brand' => $response['brand'],
			    			'country' => $response['country'],
			    			'cvc_check' => $response['cvc_check'],
			    			'last4' => $response['last4'],
			    			'exp_month' => $response['exp_month'],
			    			'exp_year' => $response['exp_year'],
			    			'created'=> date('Y-m-d H:i:s')
						)
	    			); 
	    		
				if($customerArr)
				{
		    		if($rs = $this->UserCard->save($customerArr))
		    		{
		    			$this->response = array('status'=>200,'message'=>'Card added successfully');
		    		}
		    		else
		    		{
						$this->response = array('status'=>400,'message'=>'Card could not saved, please try again');
		    		}
		    	}
		    	else
		    	{
		    		$this->response = array('status'=>400,'message'=>'Please provide data');
		    	}

			}catch(Exception $e)
			{
				$token_error = $e->getMessage();
				$this->response = array('status'=>400,'message'=>$token_error);

			}
			
    	}
    	else
    	{


				$res = $this->Stripe->create_customer($token,$user_id);
		    	if(isset($res['id']))
		    	{
		    	
		    		$customerdata = json_decode($res->getlastResponse()->body);
		    		$card =  $customerdata->sources->data[0];
		    		$customerArr = array(
			    		'UserCard' 			=> array(
			    			'user_id' 		=> $user_id,
			    			'card_token' 	=> $token,
			    			'customer_id' 	=> $customerdata->id,
			    			'card_id'		=> $card->id,
			    			'acc_bal'		=> $customerdata->account_balance,
			    			'default_source'=> $customerdata->default_source,
			    			'description' 	=> $customerdata->description,
			    			'brand' 		=> $card->brand,
			    			'country' 		=> $card->country,
			    			'cvc_check' 	=> $card->cvc_check,
			    			'last4' 		=> $card->last4,
			    			'exp_month' 	=> $card->exp_month,
			    			'exp_year' 		=> $card->exp_year,
			    			'default' 		=> '1',
			    			'created'		=> date('Y-m-d H:i:s')
						)
		    		); 
		    		
					if($customerArr)
					{

			    		if($rs = $this->UserCard->save($customerArr))
			    		{
			    			$this->response = array('status'=>200,'message'=>'Card added successfully');
			    		}
			    		else
			    		{
			    			$this->response = array('status'=>400,'message'=>'Card could not saved, please try again');
			    		}
			    	}
			    	else
			    	{
			    		$this->response = array('status'=>400,'message'=>'Please provide data');
			    	}
		    	}
		    	else
		    	{
		    		$this->response = array('status'=>400,'message'=>'Invalid token error');
		    	}
	    }
    	
    	$this->__send_response();
    }

    function addReaderCard(){

    	
    	$data = $this->data;
    	$this->loadModel('StripeAccount');
    	$this->loadModel('User');
    	if(isset($data['reader_id']) && !empty($data['reader_id']))
    	{
    		$reader_id = $data['reader_id'];
    		if(isset($data['token']) && !empty($data['token']))
    		{
    		
				$token = $data['token'];

				$getAccount = $this->StripeAccount->findByReaderId($reader_id);
    			if($getAccount)
    			{
	    			$acc_id = $getAccount['StripeAccount']['account_id'];
	    		}
	    		else
	    		{
	    			$getuser = $this->User->findById($reader_id);
	    			if($getuser)
	    			{
	    				$email = $getuser['User']['email'];
	    				$acc_response = $this->Stripe->create_account($email);
		    				if($acc_response['status']==400){
				    				$this->response = $acc_response;
				    				$this->__send_response();
				    		}
			    			$acc_id = $acc_response['id'];
			    				$accountArr = array(
			    					'StripeAccount'   => array(
			    						'reader_id'   => $getuser['User']['id'],
			    						'account_id'  => $acc_id,
			    						'created'     => date('Y-m-d H:i:s')
			    						)
			    					);
			    			$this->StripeAccount->save($accountArr,false);
	    			}
	    			else
	    			{
	    				$this->response = array('status'=>400,'message'=>'Invalid reader');
	    			}
	    		}

					$res = $this->Stripe->update_account($acc_id,$token);
					if($res['status']==400){
						$this->response = array('status'=>400,'message'=>$res['message']);
						$this->__send_response();
					}
					
					$this->loadModel('ReaderCard');
					$updateArr = array(
	    				'ReaderCard' => array(
	    					'reader_id' => $reader_id,
	    					'card_token' => $token,
	    					'account_id' => $acc_id,
	    					'card_id' => $res['id'],
	    					'card_holder_name' => $res['name'],
	    					'brand' => $res['brand'],
	    					'last4' => $res['last4'],
	    					'currency' => $res['currency'],
	    					'country' => $res['country'],
	    					'exp_month' => $res['exp_month'],
	    					'exp_year' => $res['exp_year'],
	    					'card_type' => $res['funding'],
	    					'status' => $res['status']
	    					)
	    				);

	    			
	    			if($this->ReaderCard->save($updateArr))
	    			{
	    				$this->response = array('status'=>200,'message'=>'Success! your card added successfully');
	    			}
	    			else
	    			{
	    				$this->response = array('status'=>400,'message'=>'Sorry! Card could not add, please try again');
	    			}
				}
				else
	    		{
	    			$this->response = array('status'=>400,'message'=>'Please provide token');
	    		}
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide reader id');
    	}
		$this->__send_response();
    }

   

    /*Method: fetchCards
    * Description: this method is used to fetch user's card
    * Created By: Pooja chaudhary on 21-september-2017
    */
    function fetchCards(){
    	$this->loadModel('UserCard');
    	$data = $this->data;
    	if(isset($data['user_id']) && !empty($data['user_id'])){
    		$user_id = $data['user_id'];
    		$cards = $this->UserCard->find('all',array('conditions'=>array('UserCard.user_id'=>$user_id)));
    		if($cards){
    			foreach($cards as $card):
    				$cardArr[] = array(
    					'token' => $card['UserCard']['card_token'],
    					'customer_id' => $card['UserCard']['customer_id'],
    					'card_id' => $card['UserCard']['card_id'],
    					'card_name' => $card['UserCard']['brand'],
    					'country' => $card['UserCard']['country'],
    					'last4'	=> $card['UserCard']['last4'],
    					'exp_month' => $card['UserCard']['exp_month'],
    					'exp_year' => $card['UserCard']['exp_year'],
					);
    			endforeach;
    			$this->response = array('status'=>200,'message'=>'success','data'=>$cardArr);
    		}else{
    			$this->response = array('status'=>200,'message'=>'No cards found!','data'=>array());
    		}
    	}else{
    		$this->response =array('status'=>400,'message'=>'Please provide User id');
    	}
    	$this->__send_response();
    }


   /* Method: fetchReaderCards
    * Description: this method is used to fetch reader's card
    * Created By: Pooja chaudhary on 05-February-2018
    */
    function fetchReaderCards(){

		//Configure::write('debug',true);
    	$this->loadModel('ReaderCard');
    	$data = $this->data;
    	if(isset($data['reader_id']) && !empty($data['reader_id'])){
    		$reader_id = $data['reader_id'];
    		$cards = $this->ReaderCard->find('all',array('conditions'=>array('ReaderCard.reader_id'=>$reader_id)));
    		if($cards){
    			foreach($cards as $card):
    				$cardArr[] = array(
    					'card_id'	=> $card['ReaderCard']['card_id'],
    					'card_name' => $card['ReaderCard']['brand'],
    					'country' => $card['ReaderCard']['country'],
    					'last4'	=> $card['ReaderCard']['last4'],
    					'exp_month' => $card['ReaderCard']['exp_month'],
    					'exp_year' => $card['ReaderCard']['exp_year'],
					);
    			endforeach;
    			$this->response = array('status'=>200,'message'=>'success','data'=>$cardArr);
    		}else{
    			$this->response = array('status'=>200,'message'=>'No cards found!','data'=>array());
    		}
    	}else{
    		$this->response =array('status'=>400,'message'=>'Please provide Reader id');
    	}
    	$this->__send_response();
    }


   /* Method: deleteCard
    * Description: this method is used to delete user's card
    * Created By: Pooja chaudhary on 21-september-2017
    */
    function deleteCard(){
    	$this->loadModel('UserCard');
    	$userId = $this->userId;
    	$data = $this->data;
    	if(isset($data['user_id']) && !empty($data['user_id'])){
    		$user_id = $data['user_id'];
    		if($userId!=$user_id){
    			$this->response = array('status'=>400,'message'=>'Sorry! you dont have permission to delete card of other users');
    			$this->__send_response();
    		}
    	}else{
    		$this->response = array('status'=>400,'message'=>'Please provide user id');
    		$this->__send_response();
    	}

    	if(isset($data['card_id']) && !empty($data['card_id'])){
    		$card_id = $data['card_id'];
    	}else{
    		$this->response = array('status'=>400,'message'=>'Please provide card id');
    		$this->__send_response();
    	}

    	if(isset($data['customer_id']) && !empty($data['customer_id'])){
    		$customer_id = $data['customer_id'];
    	}else{
    		$this->response = array('status'=>400,'message'=>'Please provide customer id');
    		$this->__send_response();
    	}

    	$response = $this->Stripe->delete_card($customer_id,$card_id);
    	$res = $response->__toArray(); 
		if($res['deleted']==1){
			$getcard = $this->UserCard->findByCardId($card_id);
			if($this->UserCard->delete($getcard['UserCard']['id'])){
				$this->response = array('status'=>200,'message'=>'Card deleted successfully');
			}else{
				$this->response = array('status'=>400,'message'=>'Card could not deleted, please try again');
			}
		}else{
			$this->response = array('status'=>400,'message'=>'Card could not deleted, please try again');
		}
		$this->__send_response();
    }


    /*Method: deleteReaderCard
    * Description: this method is used to delete reader's card
    * Created By: Pooja chaudhary on 05-February-2018
    */
    function deleteReaderCard(){

    	$this->loadModel('ReaderCard');
    	
    	$userId = $this->userId;
    	$data = $this->data;
    	if(isset($data['reader_id']) && !empty($data['reader_id'])){
    		$reader_id = $data['reader_id'];
    		if($userId!=$reader_id){
    			$this->response = array('status'=>400,'message'=>'Sorry! you dont have permission to delete card of other users');
    			$this->__send_response();
    		}
    	}else{
    		$this->response = array('status'=>400,'message'=>'Please provide reader id');
    		$this->__send_response();
    	}

    	if(isset($data['card_id']) && !empty($data['card_id']))
    	{
    		$card_id = $data['card_id'];
    	}else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide card id');
    		$this->__send_response();
    	}

    	$carddata = $this->ReaderCard->find('first',array('conditions'=>array('ReaderCard.card_id'=>$card_id),'fields'=>array('account_id','id')));
    	$acc_id = $carddata['ReaderCard']['account_id'];
    	$res = $this->Stripe->delete_account_card($acc_id,$card_id);
    	
		if($res['deleted']==1)
		{
		
			if($this->ReaderCard->delete($carddata['ReaderCard']['id']))
			{
				$this->response = array('status'=>200,'message'=>'Card deleted successfully');
			}
			else
			{
				$this->response = array('status'=>400,'message'=>'Card could not deleted, please try again');
			}
		}
		else
		{
			$this->response = array('status'=>400,'message'=>'Card could not deleted, please try again');
		}
		$this->__send_response();
    }


    /*Method: defaultCard
    * Description: this method is used to set default card
    * Created By: Pooja chaudhary on 04-october-2017
    */
    function defaultCard(){

    	$this->loadModel('UserCard');
    	$data = $this->data;
    	if(isset($data['token']) && $data['token']!=''){
    		$token = $data['token']; 
    	}else{
    		$this->response = array('status'=>400,'message'=>'Please provide token');
    		$this->__send_response();
    	}

    	if(isset($data['user_id']) && !empty($data['user_id'])){
    		$user_id = $data['user_id'];
    	}else{
    		$this->response = array('status'=>400,'message'=>'Please provide user id');
    		$this->__send_response();
    	}

    	$chkdefault = $this->UserCard->find('first',array('conditions'=>array('UserCard.user_id'=>$user_id,'UserCard.card_token'=>$token,'UserCard.default_card'=>1)));
    	if($chkdefault){
			$this->response = array('status'=>200,'message'=>'This card is already set as deafult card');
		}else{

			if($this->UserCard->updateAll(array('UserCard.default_card'=>0),array('UserCard.user_id'=>$user_id))){
				$getcard = $this->UserCard->find('first',array('conditions'=>array('UserCard.user_id'=>$user_id,'UserCard.card_token'=>$token)));
				if($getcard){
					$id =  $getcard['UserCard']['id'];
					$this->UserCard->id = $id;
					//if($this->UserCard->saveField('default_card',"1")){
					if($this->UserCard->updateAll(array('default_card' => 1),array('id' => $id))){
						$this->response = array('status'=>200,'message'=>'success');
					}else{
						$this->response = array('status'=>400,'message'=>'Sorry! could not set default card');
					}
				}
			}
		}
		$this->__send_response();
    }

    /*Method: create_post
    * Description: this method is used to create post
    * Created By: Pooja chaudhary on 09-october-2017
    */
    function create_post(){
    	$this->loadModel('Post');
    	$data = $this->data;

    	if(isset($data['user_id']) && !empty($data['user_id'])){
    		$user_id = $data['user_id'];
    	}else{
    		$this->response = array('status'=>400,'message'=>'Please provide user id');
    		$this->__send_response();
    	}

    	if(isset($data['post_detail']) && !empty($data['post_detail'])){
    		$post_detail = $data['post_detail'];
    	}else{
    		$post_detail = '';
    	}

    	if(isset($data['url_link']) && !empty($data['url_link'])){
    		$url_link = $data['url_link'];
    	}else{
    		$url_link = '';
    	}

    	if(isset($data['full_videolink']) && !empty($data['full_videolink'])){
    		$full_videolink = $data['full_videolink'];
    	}else{
    		$full_videolink = '';
    	}


    	if(isset($_FILES['thumb_name']) && !empty($_FILES['thumb_name']['name'])){
    		$file_name = $_FILES['thumb_name']['name'];
    		if(move_uploaded_file($_FILES['thumb_name']['tmp_name'], WWW_ROOT.'img/postImages/'.$_FILES['thumb_name']['name'])){
    			$thumb_name = $_FILES['thumb_name']['name'];
    			$thumb_path = 'img/postImages/'.$thumb_name;
    		}
    	}else{
    		$thumb_name = '';
    		$thumb_path = '';
    	}

    	if(isset($_FILES['media_file']) && !empty($_FILES['media_file']['name'])){
    		$file_name = $_FILES['media_file']['name'];
    		if(move_uploaded_file($_FILES['media_file']['tmp_name'], WWW_ROOT.'img/postImages/'.$_FILES['media_file']['name'])){
    			$file_name = $_FILES['media_file']['name'];
    			$full_path = 'img/postImages/'.$file_name;
    		}
    	}else{
    		$file_name = '';
    		$full_path = '';
    	}

    	$dataArr = array(
    		'user_id' => $user_id,
    		'post_detail' => $post_detail,
    		'url_link' => $url_link,
    		'full_videolink' => $full_videolink,
    		'file_name' => $file_name,
    		'full_path' => $full_path,
    		'thumb_name' => $thumb_name,
    		'thumb_path' => $thumb_path,
    		'created' => date('Y-m-d H:i:s')
    		);

    	if($this->Post->save($dataArr)){
    		$this->response = array('status'=>200,'message'=>'Post created successfully');
    	}else{
    		$this->response = array('status'=>400,'message'=>'Post could not created');
    	}

    	$this->__send_response();
    }


    /*Method: updatePost
    * Description: this method is used to edit post
    * Created By: Pooja chaudhary on 15-December-2017
    */
    function updatePost(){
    	$this->loadModel('Post');
    	$data = $this->data;

    	if(isset($data['post_id']) && !empty($data['post_id']))
    	{
    		$post_id = $data['post_id'];
    	
    		$chkpost = $this->Post->findById($post_id);

    		if($chkpost){
    			$this->Post->id = $post_id;
    		}else{
				$this->response = array('status'=>400,'message'=>'Invalid post id');
    			$this->__send_response();
    		}

			if(isset($data['post_detail']) && !empty($data['post_detail'])){
	    		$this->request->data['post_detail'] = $data['post_detail'];
	    	}

	    	if(isset($data['url_link']) && !empty($data['url_link'])){
	    		$this->request->data['url_link'] = $data['url_link'];
	    	}

	    	if(isset($data['full_videolink']) && !empty($data['full_videolink'])){
	    		$this->request->data['full_videolink'] = $data['full_videolink'];
	    	}

	    	if(isset($_FILES['thumb_name']) && !empty($_FILES['thumb_name']['name'])){
	    		$file_name = $_FILES['thumb_name']['name'];
	    		if(move_uploaded_file($_FILES['thumb_name']['tmp_name'], WWW_ROOT.'img/postImages/'.$_FILES['thumb_name']['name'])){
	    			$this->request->data['thumb_name'] = $_FILES['thumb_name']['name'];
	    			$this->request->data['thumb_path'] = 'img/postImages/'.$thumb_name;
	    		}
	    	}

	    	if(isset($_FILES['media_file']) && !empty($_FILES['media_file']['name'])){
	    		$file_name = $_FILES['media_file']['name'];
	    		if(move_uploaded_file($_FILES['media_file']['tmp_name'], WWW_ROOT.'img/postImages/'.$_FILES['media_file']['name'])){
	    			$this->request->data['file_name'] = $_FILES['media_file']['name'];
	    			$this->request->data['full_path'] = 'img/postImages/'.$file_name;
	    		}
	    	}

	    	if($this->Post->save($this->request->data)){
	    		$rs = $this->Post->findById($post_id);
	    		$this->response = array('status'=>200,'message'=>'Post updated successfully','data'=>$rs['Post']);
	    	}else{
	    		$this->response = array('status'=>400,'message'=>'Post could not be updated, please try again');
	    	}

	    }else{
    		$this->response = array('status'=>400,'message'=>'Please provide post id');
    	}

    	$this->__send_response();
    }

    /*
    * Method: deletePost
    * Description: this method is used to delete post
    * Created By: Pooja chaudhary on 15-December-2017
    */
     function deletePost(){
     	$data = $this->data;
     	$this->loadModel('Post');

     	if(isset($data['post_id']) && !empty($data['post_id'])){

     		$chkpost = $this->Post->findById($data['post_id']);
     		if($chkpost){
     			$this->Post->id = $data['post_id'];

     			if($this->Post->delete()){
     				$this->response = array('status'=>200,'message'=>'Post deleted successfully');
     			}else{
     				$this->response = array('status'=>400,'message'=>'Server Error');
     			}
     		}else{
     			$this->response = array('status'=>400,'message'=>'Invalid post id');
     		}
     	}else{
     		$this->response = array('status'=>400,'message'=>'Please provide post id');
     	}
     	$this->__send_response();
     }

    /*Method: like_post
    * Description: this method is used to create post
    * Created By: Pooja chaudhary on 09-october-2017
    */
    function like_post()
    {
    	$this->loadModel('Post');
    	$this->loadModel('PostLike');
    	$data = $this->data;
    	$userId = $this->userId;

    	if(isset($data['user_id']) && !empty($data['user_id']))
    	{
    		$user_id = $data['user_id'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide user id');
    		$this->__send_response();
    	}

    	if(isset($data['post_id']) && !empty($data['post_id']))
    	{
    		$post_id = $data['post_id'];
    		$getpost = $this->Post->findById($post_id);
    		$receiver_id = $getpost['Post']['user_id'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide post id');
    		$this->__send_response();
    	}

    	if(isset($data['status']) && $data['status']!='')
    	{
    		$status = $data['status'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide status');
    		$this->__send_response();
    	}

    	if($status==1)
    	{
			$getLike = $this->PostLike->find('first',array('conditions'=>array('PostLike.user_id'=>$user_id,'PostLike.post_id'=>$post_id)));
	    	if($getLike)
	    	{
	    		$id = $getLike['PostLike']['id'];
	    		$this->PostLike->id = $id;
	    		if($this->PostLike->saveField('status',$status))
	    		{
	    			$this->response = array('status'=>200,'message'=>'success');
	    		}
	    		else
	    		{
			    	$this->response = array('status'=>400,'message'=>'failed');
			    }
			}
			else
			{
				$dataArr = array(
		    		'user_id' => $user_id,
		    		'post_id' => $post_id,
		    		'status' => $status,
		    		'created' => date('Y-m-d H:i:s')
	    		);

		    	if($this->PostLike->save($dataArr))
		    	{
		    		//noti code start here //
					$this->sendNotification($userId,$receiver_id,'Liked Post');
					//noti code ends here //
		    		$this->response = array('status'=>200,'message'=>'success');
		    	}
		    	else
		    	{
		    		$this->response = array('status'=>400,'message'=>'failed');
		    	}

			}

	    }
	    elseif($status==0)
	    {
	    	$getLike = $this->PostLike->find('first',array('conditions'=>array('PostLike.user_id'=>$user_id,'PostLike.post_id'=>$post_id)));
	    	if($getLike)
	    	{
	    		$id = $getLike['PostLike']['id'];
	    		$this->PostLike->id = $id;
	    		if($this->PostLike->saveField('status',$status))
	    		{
	    			$this->response = array('status'=>200,'message'=>'success');
	    		}
	    		else
	    		{
			    	$this->response = array('status'=>400,'message'=>'failed');
			    }
			}
			else
			{
				$dataArr = array(
		    		'user_id' => $user_id,
		    		'post_id' => $post_id,
		    		'status' => $status,
		    		'created' => date('Y-m-d H:i:s')
	    		);

		    	if($this->PostLike->save($dataArr))
		    	{
		    		$this->response = array('status'=>200,'message'=>'success');
		    	}
		    	else
		    	{
		    		$this->response = array('status'=>400,'message'=>'failed');
		    	}

			}
	    }

    	$this->__send_response();
    }

    /*Method: comment_post
    * Description: this method is used to create post
    * Created By: Pooja chaudhary on 09-october-2017
    */
    function comment_post()
    {
    	
    	$this->loadModel('PostComment');
    	$this->loadModel('Post');
    	$data = $this->data;
    	$userId = $this->userId;

    	if(isset($data['user_id']) && !empty($data['user_id']))
    	{
    		$user_id = $data['user_id'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide user id');
    		$this->__send_response();
    	}

    	if(isset($data['post_id']) && !empty($data['post_id']))
    	{
    		$post_id = $data['post_id'];
    		$getpost = $this->Post->findById($post_id);
    		$receiver_id = $getpost['Post']['user_id'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide post id');
    		$this->__send_response();
    	}

    	if(isset($data['comment']) && $data['comment']!='')
    	{
    		$comment = $data['comment'];
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide comment text');
    		$this->__send_response();
    	}

  			$dataArr = array(
	    		'user_id' => $user_id,
	    		'post_id' => $post_id,
	    		'comment' => $comment
	    	);

	    	if($this->PostComment->save($dataArr))
	    	{
	    		//noti code start here //
	    		//$notidata = array('post_id'=>$post_id,'comment'=>$comment);
				$this->sendNotification($userId,$friend_id,'Commented',$notidata);
				//noti code ends here //
	    		$this->response = array('status'=>200,'message'=>'success');
	    	}
	    	else
	    	{
	    		$this->response = array('status'=>400,'message'=>'failed');
	    	}

	    $this->__send_response();
    }


    /*Method: share_post
    * Description: this method is used to send notiication for sharing post
    * Created By: Pooja chaudhary on 22-January-2018
    */
    function share_post(){
    	
    	$this->loadModel('Post');
    	$data = $this->data;
    	$sender_id = $this->userId;

    	if(isset($data['post_id']) && !empty($data['post_id'])){
    		$post_id = $data['post_id'];
    		$getpost = $this->Post->findById($post_id);
    		$receiver_id = $getpost['Post']['user_id'];
    	}else{
    		$this->response = array('status'=>400,'message'=>'Please provide post id');
    		$this->__send_response();
    	}

    	//noti code start here //
	    //$notidata = array('post_id'=>$post_id);
		$this->sendNotification($sender_id,$receiver_id,'Share Post',$notidata);
		//noti code ends here //
	    $this->response = array('status'=>200,'message'=>'success');
	    	
		$this->__send_response();
    }

    
	/*Method: fetch_post
    * Description: this method is used to fetch all post
    * Created By: Pooja chaudhary on 09-october-2017
    */
    function fetch_post()
    {

    	$this->loadModel('Post');
    	$this->loadModel('PostLike');
    	$this->loadModel('PostComment');
    	$this->loadModel('ReportPost');
    	$this->loadModel('Friend');
    	$userId = $this->userId;

    	$data = $this->data;
    	if(isset($data['type']) && $data['type']=='all')
    	{
    		$reportedPost = $this->ReportPost->find('list',array('conditions'=>array('ReportPost.user_id'=>$userId),'fields'=>array('id','post_id')));
	    	$this->PostComment->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'user_id','fields'=>array('id','firstname',
	    		'lastname','profile_pic','city')))));
	    	$this->Post->bindModel(array('hasMany'=>array('PostComment'=>array('class'=>'PostComment','foreignKey'=>'post_id'))));
	    	$this->Post->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'user_id'))));
	    	$this->Post->recursive = 2;
	    	//$posts = $this->Post->find('all',array('conditions'=>array('NOT' => array('Post.id' => $reportedPost)),'order'=>'Post.id DESC'));
	    	$posts = $this->Post->find('all',array('conditions'=>['Post.status'=>'Active'],'order'=>'Post.id DESC'));
    	}
    	else
    	{
			$get_friends_list = $this->Friend->find('all',array('conditions'=>array('OR'=>array('Friend.user_id'=>$userId,'Friend.friend_id'=>$userId),'Friend.status'=>2),'fields'=>array('id','user_id','friend_id')));
    		if($get_friends_list)
    		{
				foreach($get_friends_list as $list):
					if($list['Friend']['user_id']==$userId)
    				{
    					$f_ids[] = $list['Friend']['friend_id'];
    				}
    				else
    				{
    					$f_ids[] = $list['Friend']['user_id'];
    				}
    				$f_ids[] = $userId;
    			endforeach;

    			$f_ids = array_unique($f_ids);
    			
    		}

    		$reportedPost = $this->ReportPost->find('list',array('conditions'=>array('ReportPost.user_id'=>$userId),'fields'=>array('id','post_id')));
	    	$this->PostComment->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'user_id','fields'=>array('id','firstname',
	    		'lastname','profile_pic','city','profile_name')))));
	    	$this->Post->bindModel(array('hasMany'=>array('PostComment'=>array('class'=>'PostComment','foreignKey'=>'post_id'))));
	    	$this->Post->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'user_id'))));
	    	$this->Post->recursive = 2;
	    	if(isset($f_ids) && !empty($f_ids))
	    	{
	    		$posts = $this->Post->find('all',array('conditions'=>array('Post.user_id IN'=>$f_ids),'order'=>'Post.id DESC'));
	    		//$posts = $this->Post->find('all',array('conditions'=>array('NOT' => array('Post.id' => $reportedPost),'Post.user_id IN'=>$f_ids),'order'=>'Post.id DESC'));
	    	}
	    	else
	    	{
	    		$posts = $this->Post->find('all',array('conditions'=>array('Post.user_id' => $userId),'order'=>'Post.id DESC'));
	    	}
    	}

		if($posts)
    	{
    		
    		$comments = array();
    		$likes = array();
	    	foreach($posts as $post)
	    	{
				$ids = array($userId,$post['User']['id']);
	    		$chkfriend = $this->Friend->find('first',array('conditions'=>array('Friend.user_id IN'=>$ids,'Friend.friend_id IN'=>$ids,'Friend.status !='=>0)));
	    		if($chkfriend)
	    		{
	    			$friend_status = '1';
	    		}
	    		else
	    		{
	    			$friend_status = '0';
	    		}

	    		$post['Post']['friend_status'] = $friend_status;
				if(!empty($post['Post']['full_path']))
	    		{
	    			$post['Post']['full_path'] = SITE_URL.$post['Post']['full_path'];
	    		}
	    		elseif(!empty($post['Post']['full_videolink']))
	    		{
	    			$post['Post']['full_path'] = SITE_URL.$post['Post']['full_videolink'];
	    		}
	    		else
	    		{
	    			$post['Post']['full_path'] = '';
	    		}
	    		if(!empty($post['Post']['thumb_path']))
	    		{
	    			$post['Post']['thumb_path'] = SITE_URL.$post['Post']['thumb_path'];
	    		}
	    		else
	    		{
	    			$post['Post']['thumb_path'] = '';
	    		}
	    		if(!empty($post['Post']['url_link']))
	    		{
	    			$post['Post']['url_link'] = $post['Post']['url_link'];
	    		}
	    		else
	    		{
	    			$post['Post']['url_link'] = '';
	    		}

	    		$chklike = $this->PostLike->find('first',array('conditions'=>array('PostLike.user_id'=>$userId,'PostLike.post_id'=>$post['Post']['id'],'status'=>1)));
	    		if($chklike)
	    		{
	    			$like_status = '1';
	    		}
	    		else
	    		{
	    			$like_status = '0';
	    		}

	    		$post['Post']['like_status'] = $like_status;
	    	    $post['Post']['username'] = $post['User']['firstname'].''.$post['User']['lastname'];
	    	    $post['Post']['profile_name'] = $post['User']['profile_name'];
	    		$post['Post']['city'] = $post['User']['city'];
	    		$post['Post']['user_image'] = SITE_URL.'img/userImages/'.$post['User']['profile_pic'];

	    		/* ---- post comment data start -----*/

	    		if(!empty($post['PostComment']))
	    		{

	    			$count_comment = sizeof($post['PostComment']);
	    			foreach ($post['PostComment'] as $comment)
	    			{
	    				if(!empty($comment['User']['profile_pic']))
	    				{
	    					$profile_pic = SITE_URL.'img/userImages'.$comment['User']['profile_pic'];
	    				}
	    				else
	    				{
	    					$profile_pic = '';
	    				}

	    				$comments[] = array(
	    					'id' => $comment['id'],
	    					'user_id' => $comment['user_id'],
	    					'post_id' => $comment['post_id'],
	    					'comment' => $comment['comment'],
	    					'user_name' => $comment['User']['firstname'].' '.$comment['User']['lastname'],
	    					'profile_name' => $comment['User']['profile_name'],
	    					'profile_pic' => $profile_pic,
	    					'created' => $comment['created']
	    					);
	    			}
	    			$post['Post']['comments'] = $comments;
	    			$post['Post']['total_comments'] = $count_comment;
				}
	    		else
	    		{
					$post['Post']['comments'] = array();
	    			$post['Post']['total_comments'] = 0;

	    		}

	    		/* ---- post comment data end -----*/


	    		/* ---- post like data start -----*/

	    		$likes = array();
	    		$this->PostLike->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'user_id','fields'=>array('id','firstname','lastname','profile_pic','city','profile_name')))));
	    		$getlikes = $this->PostLike->find('all',array('conditions'=>array('PostLike.post_id'=>$post['Post']['id'],'PostLike.status'=>1)));
	    		if($getlikes)
	    		{
	    				//$likes = array();
	    				foreach($getlikes as $getlike):
		    				if(!empty($getlike['User']['profile_pic']))
		    				{
		    					$profile_pic = SITE_URL.'img/userImages/'.$getlike['User']['profile_pic'];
		    				}
		    				else
		    				{
		    					$profile_pic = '';
		    				}

		    				$likes[] = array(
		    					'id' => $getlike['PostLike']['id'],
		    					'user_id' => $getlike['PostLike']['user_id'],
		    					'post_id' => $getlike['PostLike']['post_id'],
		    					'user_name' => $getlike['User']['firstname'].' '.$getlike['User']['lastname'],
		    					'profile_name' => $getlike['User']['profile_name'],
		    					'city' => $getlike['User']['city'],
		    					'profile_pic' => $profile_pic,
		    					'created' => $getlike['PostLike']['created']
		    					);
	    				endforeach;
	    		}

	    		if($likes)
	    		{
	    			$count_likes = sizeof($likes);
	    			$post['Post']['likes'] = $likes;
	    			$post['Post']['total_likes'] = $count_likes;
	    		}
	    		else
	    		{
	    			$post['Post']['likes'] = array();
	    			$post['Post']['total_likes'] = 0;
	    		}

	    		/* ---- post like data end-----*/

	    		$responseArr[] = $post['Post'];
	    	}

			if($responseArr)
			{
	    		$this->response = array('status'=>200,'message'=>'success','data'=>$responseArr);
	    	}
	    	else
	    	{
	    		$this->response = array('status'=>400,'message'=>'Post could not created','data'=> (object) array());
	    	}
	    }
	    else
	    {
	    	$this->response = array('status'=>400,'message'=>'No post found','data'=> (object) array());
	    }
    	$this->__send_response();
    }


    /*
    * Method: fetch_comments
    * Description: this method is used to fetch comment for particular post
    * Created By: Pooja chaudhary on 14-november-2017
    */
    function fetch_comments()
    {
    	$this->loadModel('Post');
    	$this->loadModel('PostComment');

    	$data = $this->data;
    	if(isset($data['post_id']) && !empty($data['post_id']))
    	{
    		$post_id = $data['post_id'];
    		$this->PostComment->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'user_id'))));
    		$comments = $this->PostComment->find('all',array('conditions'=>array('PostComment.post_id'=>$post_id)));
    		/* ---- post comment data start-----*/
	    		if(!empty($comments))
	    		{
	    		
	    			foreach ($comments as $comment) 
	    			{
	    				if(!empty($comment['User']['profile_pic']))
	    				{
	    					$profile_pic = SITE_URL.'img/userImages/'.$comment['User']['profile_pic'];
	    				}
	    				else
	    				{
	    					$profile_pic = '';
	    				}

	    				$commentsArr[] = array(
	    					'id' => $comment['PostComment']['id'],
	    					'user_id' => $comment['PostComment']['user_id'],
	    					'post_id' => $comment['PostComment']['post_id'],
	    					'comment' => $comment['PostComment']['comment'],
	    					'user_name' => $comment['User']['firstname'].' '.$comment['User']['lastname'],
	    					'profile_name' => $comment['User']['profile_name'],
	    					'profile_pic' => $profile_pic,
	    					'created' => $comment['PostComment']['created']
	    					);
	    			}
	    			$post['Post']['comments'] = $comments;
	    			
	    		}
	    		else
	    		{
	    			$commentsArr = array();
	    		}
	    		/* ---- post comment data end-----*/

	    	$this->response = array('status'=>200,'message'=>'success','data'=>$commentsArr);

    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'please provide post id');
    	}
    	$this->__send_response();
    }


    /*Method: fetch_likess
    * Description: this method is used to fetch likes for particular post
    * Created By: Pooja chaudhary on 14-november-2017
    */
    function fetch_likes()
    {
    	$this->loadModel('Friend');
    	$this->loadModel('Post');
    	$this->loadModel('PostLike');
    	$userId = $this->userId;
    	$data = $this->data;
    	if(isset($data['post_id']) && !empty($data['post_id']))
    	{
    		$post_id = $data['post_id'];
    		$this->PostLike->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'user_id'))));
    		$likes = $this->PostLike->find('all',array('conditions'=>array('PostLike.post_id'=>$post_id,'PostLike.status'=>1)));
    		/* ---- post comment data start-----*/
	    		if(!empty($likes))
	    		{
	    		
	    			foreach ($likes as $like)
	    			{
	    				$ids = array($userId,$like['User']['id']);
	    				$chkfriend = $this->Friend->find('first',array('conditions'=>array('Friend.user_id IN'=>$ids,'Friend.friend_id IN'=>$ids,'Friend.status !='=>0)));
	    				if($chkfriend)
	    				{
	    					$friend_status = '1';
	    				}
	    				else
	    				{
	    					$friend_status = '0';
	    				}

	    				if(!empty($like['User']['profile_pic'])){
	    					$profile_pic = SITE_URL.'img/userImages/'.$like['User']['profile_pic'];
	    				}
	    				else
	    				{
	    					$profile_pic = '';
	    				}

	    				$likesArr[] = array(
	    					'id' => $like['PostLike']['id'],
	    					'user_id' => $like['PostLike']['user_id'],
	    					'post_id' => $like['PostLike']['post_id'],
	    					'user_name' => $like['User']['firstname'].' '.$like['User']['lastname'],
	    					'profile_name' => $like['User']['profile_name'],
	    					'city' => $like['User']['city'],
	    					'profile_pic' => $profile_pic,
	    					'friend_status' => $friend_status,
	    					'created' => $like['PostLike']['created']
	    					);
	    			}
	    			
	    		}
	    		else
	    		{
	    			$likesArr = array();
	    		}
	    		/* ---- post comment data end-----*/

	    	$this->response = array('status'=>200,'message'=>'success','data'=>$likesArr);
		}
		else
		{
    		$this->response = array('status'=>400,'message'=>'please provide post id');
    	}
    	$this->__send_response();
    }

   /* Method: report_post
    * Description: this method is used to report a post
    * Created By: Pooja chaudhary on 15-november-2017
    */
    public function report_post()
    {

    	$this->loadModel('ReportPost');
    	$data = $this->data;
    	$userId = $this->userId;
    	if(isset($data['post_id']) && !empty($data['post_id']))
    	{
    		$post_id = $data['post_id'];
    		$chkreport = $this->ReportPost->find('first',array('conditions'=>array('ReportPost.post_id'=>$post_id,'ReportPost.user_id'=>$userId)));
    		if($chkreport)
    		{
    			$this->ReportPost->delete($chkreport['ReportPost']['id']);
    		}
    		$reportdata = array(
    			'ReportPost' => array(
    					'user_id' => $userId,
    					'post_id' => $post_id,
    					'reason' => $data['reason'],
    					'report_status' => 1
    				)
    			);

    		if($this->ReportPost->save($reportdata))
    		{
    			$this->response = array('status'=>200,'message'=>'success');
    		}
    		else
    		{
    			$this->response = array('status'=>400,'message'=>'Server error');
    		}
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide post id');
    	}

    	$this->__send_response();
    }

    /*Method: send_friend_request
    * Description: this method is used to send friend requests
    * Created By: Pooja chaudhary on 10-october-2017
    */
    public function send_friend_request()
	{
		$this->loadModel('Friend');
		$userId = $this->userId; 
		$data = $this->data;
		if(isset($data['friend_id']) && !empty($data['friend_id']))
		{
			$friend_id = $data['friend_id'];
		}
		else
		{
			$this->response = array('status'=>400,'message'=>'Please provide Friend ID','data'=>(object) array());
			$this->__send_response();
		}

		if($userId==$friend_id)
		{
			$this->response = array('status'=>400,'message'=>'Oops! you can not send friend request to yourself');
			$this->__send_response();	
		}
		$check = $this->Friend->find('all',array('conditions'=>array('Friend.friend_id IN'=>array($friend_id,$userId),'Friend.user_id IN'=>array($userId,$friend_id),'Friend.status IN'=>array(1,2,3))));
		if($check)
		{

			$this->response = array('status'=>400,'message'=>'Friend request alredy sent');
			$this->__send_response();
		}
		else
		{
			$this->Friend->deleteAll(array('Friend.friend_id IN'=>array($friend_id,$userId),'Friend.user_id IN'=>array($userId,$friend_id)));
		}

			$friend = $this->Friend->create();
			$data['Friend']['user_id'] = $userId;
			$data['Friend']['friend_id'] = $friend_id;
			$data['Friend']['created'] = date('Y-m-d H:i:s');
			$data['Friend']['status'] = 1;
			if($this->Friend->save($data))
			{
				//noti code start here //
				$this->sendNotification($userId,$friend_id,'Friend Request');
				//noti code ends here //
				$this->response = array('status'=>200,'message'=>'Friend Request Sent.','data'=>$data);
			}
			else
			{
				$this->response = array('status'=>400,'message'=>'Database Error','data'=>$data);
			}
		
		$this->__send_response();
	}


	/*Method: get_friend_requests
    * Description: this methosd is used to fetch friend requests list
    * Created By: Pooja chaudhary on 10-october-2017
    */
	public function get_friend_requests()
	{
		$this->loadModel('Friend');
		$userId = $this->userId; 
		$data = $this->data;
	   
		$check = $this->Friend->find('all',array('conditions'=>array('Friend.friend_id'=>$userId,'Friend.status'=>1),'contain'=>array('User')));
		
		$friends = [];
		$i = 0;
		foreach($check as $f)
		{
			
			if($f['User']['profile_pic'])
			{
				$friends[$i]['profile_pic']  = SITE_URL.'img/userImages/'.$f['User']['profile_pic'];
			}
			else
			{
				$friends[$i]['profile_pic']  = '';
			}
			$friends[$i]['name'] = ucfirst($f['User']['firstname'].' '.$f['User']['lastname']);
			$friends[$i]['date'] = date('Y-m-d',strtotime($f['Friend']['created']));
			$friends[$i]['request_id'] = $f['Friend']['id'];
			$friends[$i]['user_id'] = $f['User']['id'];
			$i++;
		}
			
		$this->response = array('status'=>200,'message'=>'Success','data'=>$friends);
		$this->__send_response();
	}

	/*Method: get_friends
    * Description: this methosd is used to fetch friend list
    * Created By: Pooja chaudhary on 10-october-2017
    */
	public function get_friends()
	{
		$this->loadModel('Friend');
		$this->loadModel('User');
		$userId = $this->userId; 
		$data = $this->data;
	    $conditions['or'] =  array('Friend.friend_id'=>$userId,'Friend.user_id'=>$userId);
		$check = $this->Friend->find('all',array('conditions'=>array('Friend.status'=>2,$conditions)));
		
		//echo '<pre>';print_r($check);
		$friends = [];
		$i = 0;
		foreach($check as $chk)
		{
			if($chk['Friend']['friend_id']==$userId)
			{
				$f = $this->User->findById($chk['Friend']['user_id']);
			}else
			{
				$f = $this->User->findById($chk['Friend']['friend_id']);
			}
			if($f['User']['profile_pic'])
			{
				$friends[$i]['profile_pic']  = SITE_URL.'img/userImages/'.$f['User']['profile_pic'];
			}
			else
			{
				$friends[$i]['profile_pic']  = '';
			}
			$friends[$i]['name'] = ucfirst($f['User']['firstname'].' '.$f['User']['lastname']);
			$friends[$i]['date'] = date('Y-m-d',strtotime($chk['Friend']['created']));
			$friends[$i]['user_id'] = $f['User']['id'];
			$i++;
		}
			
		$this->response = array('status'=>200,'message'=>'Success','data'=>$friends);
			
		
		$this->__send_response();
	}

	/*Method: update_friend_request
    * Description: this methosd is used to update friend request
    * Created By: Pooja chaudhary on 10-october-2017
    */
	public function update_friend_request()
	{
		$this->loadModel('Friend');
		$userId = $this->userId; 
		$data = $this->data;
	   
		if(isset($data['request_id']) && !empty($data['request_id']))
		{
			$request_id = $data['request_id'];
		}else
		{
			$this->response = array('status'=>400,'message'=>'Please provide Request ID');
			$this->__send_response();
		}

		if(isset($data['status']) && !empty($data['status']))
		{
			$status = $data['status'];

		}else
		{
			$this->response = array('status'=>400,'message'=>'Please provide Status');
			$this->__send_response();
		}


		$check = $this->Friend->find('first',array('conditions'=>array('Friend.id'=>$request_id)));
		if($check)
		{
			
			$data['Friend']['id'] = $check['Friend']['id'];
			$data['Friend']['status'] = $data['status'];
			if($this->Friend->save($data,false))
			{
				if($status==2)
				{
					//noti code start here //
					$this->sendNotification($userId,$client_id,'Friend Request Accepted');
					//noti code ends here //
				}
				$this->response = array('status'=>200,'message'=>'Success','data'=>$data);
			}
			else
			{
				$this->response = array('status'=>400,'message'=>'Database Error','data'=>$data);
			}
		}
		else
		{
			$this->response = array('status'=>400,'message'=>'Request not found.');
		}
		$this->__send_response();
	}

   /* Method: remove_friend
    * Description: this methosd is used to remove friend from friend list
    * Created By: Pooja chaudhary on 10-october-2017
    */
    function remove_friend()
    {

    	$this->loadModel('Friend');
    	$data = $this->data;
    	$userId = $this->userId;

    	if(isset($data['friend_id']) && !empty($data['friend_id']))
    	{
    		$friend_id = $data['friend_id'];
    	}else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide friend Id');
			$this->__send_response();
    	}

    	$cond['OR'] = array(array('Friend.user_id'=>$userId,'Friend.friend_id'=>$friend_id),array('Friend.friend_id'=>$userId,'Friend.user_id'=>$friend_id));
		$friends =  $this->Friend->find('first',array('conditions'=>array($cond)));
		        
		        if($friends)
		        {
		        	$this->Friend->id = $friends['Friend']['id'];
		        	
		        	if($this->Friend->saveField('status',0))
		        	{
		        		$this->response = array('status'=>200,'message'=>'success');
		        	}else
		        	{
		        		$this->response = array('status'=>400,'message'=>'Database Error');
		        	}
		        }else
		        {
		        	$this->response = array('status'=>400,'message'=>'Invalid friend id');
		        }
    	

    	$this->__send_response();

    }

    /*
    * Method: getSession
	* Description: this method is used to get Session from tokbox
	* Created By:  Pooja chaudhary on 10-july-2017
	*/
	function getSession()
	{
		//Configure::write('debug',true);
		$tokbox_api_key = TOKBOX_API_KEY;
		$tokbox_secret_key = TOKBOX_SECRET_KEY;

		$data = $this->data;
		$userId = $this->userId;
		$getuser = $this->User->findById($userId);
		if($getuser){
			if($getuser['User']['user_role']=='2')
			{
				$userrole = 'actor';
			}else
			{
				$userrole = 'reader';
			}
		}
		$this->loadModel('Project');
		if(isset($data['project_id']) && !empty($data['project_id']))
		{
			$project_id = $data['project_id'];
		
			$chkSession = $this->Project->findById($project_id);
			if($chkSession)
			{
				if(!empty($chkSession['Project']['session_id']))
				{

					$this->response = array(
						'status'=>200,
						'message'=>'success',
						'api_key'=> $tokbox_api_key,
						'secret_key'=> $tokbox_secret_key,
						'sessionId'=>$chkSession['Project']['session_id'],
						'token'=>$chkSession['Project']['token']
					);
				}
				else
				{

					$opentok = new \OpenTok\OpenTok($tokbox_api_key,$tokbox_secret_key);
					//$session = $opentok->createSession(array('mediaMode' => MediaMode::ROUTED));
					// An automatically archived session:
					$sessionOptions = array(
					    'archiveMode' => \OpenTok\ArchiveMode::MANUAL,
					    'mediaMode' => \OpenTok\MediaMode::ROUTED
					);
					$session = $opentok->createSession($sessionOptions); 
					$token = $session->generateToken(array(
						    //'role'       => Role::MODERATOR,
						    'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
						    'data'       => $userrole
							));
					// Store this sessionId in the database for later use
					$sessionId = $session->getSessionId();

					if(!empty($sessionId))
					{
						$session_id = $sessionId;
						$this->Project->id = $project_id;
						$this->Project->saveField('session_id',$session_id);
						$this->Project->saveField('token',$token);
						$this->response = array(
							'status'=>200,
							'message'=>'success',
							'api_key'=> $tokbox_api_key,
							'secret_key'=> $tokbox_secret_key,
							'sessionId'=>$session_id,
							'token'=>$token
							
						);

					}else
					{
						$this->response = array('status'=>400,'message'=>'Session could not created');
					}
				}
			}else
			{
				$this->response = array('status'=>400,'message'=>'Please provide valid project id');
			}
		}else
		{
			$this->response = array('status'=>400,'message'=>'Please provide project id');
		}
		$this->__send_response();
	}


	/*=========== Method to generate a token for a session ================*/
	function getToken()
	{
		$this->loadModel('Project');
		$this->loadModel('User');
		$data = $this->data;
		$userId = $this->userId;
		$getuser = $this->User->findById($userId);
		if($getsuer)
		{
			if($getuser['User']['user_role']=='2')
			{
				$userrole = 'actor';
			}else
			{
				$userrole = 'reader';
			}
		}
		if(isset($data['project_id']) && !empty($data['project_id']) && isset($data['sessionId']) && !empty($data['sessionId']))
		{
			$sessionId = $data['sessionId'];
			$project_id = $data['project_id'];

			$chkproject = $this->Project->findBySessionId($sessionId);
			if($chkproject)
			{
				$tokbox_api_key = TOKBOX_API_KEY;
				$tokbox_secret_key = TOKBOX_SECRET_KEY;
				$opentok = new \OpenTok\OpenTok($tokbox_api_key,$tokbox_secret_key);
				//$token = $opentok->generateToken($sessionId);
				//Set some options in a token
				$token = $sessionId->generateToken(array(
				    'role'       => Role::MODERATOR,
				    'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
				    'data'       => $userrole
				));

				$this->Project->id = $chkproject['Project']['id'];
				$this->Project->saveField('token',$token);
				$this->response = array(
					'status'=>200,
					'message'=>'success',
					'api_key'=> $tokbox_api_key,
					'secret_key'=> $tokbox_secret_key,
					'sessionId'=>$sessionId,
					'token'=>$token,
				);
			}else
			{
				$this->response = array('status'=>400,'message'=>'Invalid session id');
			}
		}else
		{
			$this->response = array('status'=>400,'message'=>'Please provide valid data');
		}
		$this->__send_response();
	}


	/*=========== Method to start an archieve ================*/
	function startArchieve()
	{
		
		$this->loadModel('Archieve');
		$this->loadModel('Project');
		$data = $this->data;
		if(isset($data['sessionId']) && !empty($data['sessionId']))
		{
			$sessionId = $data['sessionId'];
			$chkSession = $this->Project->findBySessionId($sessionId);
			if($chkSession)
			{
				$project_id = $chkSession['Project']['id'];
			}else
			{
				$this->response = array('status'=>400,'message'=>'Invalid session Id');
				$this->__send_response();
			}
			
			if(isset($data['scene_no']) && $data['scene_no']!='')
			{
				$scene_no = $data['scene_no'];
			}else
			{
				$this->response = array('status'=>400,'message'=>'Please provide scene_no');
			}
			if(isset($data['take_no']) && !empty($data['take_no']))
			{
				$take_no = $data['take_no'];
			}else
			{
				$this->response = array('status'=>400,'message'=>'Please provide scene_no');
			}

			if(isset($data['stream_id']) && !empty($data['stream_id']))
			{
				$stream_id = $data['stream_id'];
				$file_name = $stream_id.'.webm';
			}else
			{
				$this->response = array('status'=>400,'message'=>'Please provide stream id');
				$this->__send_response();
			}

			if(isset($_FILES['screen_shot']) && !empty($_FILES['screen_shot']['name']))
			{
    				$dir = WWW_ROOT.'uploads/screenshots/'.$project_id;
    				if(!is_dir($dir))
    				{
    					mkdir($dir,0777);
    				}
    				if(move_uploaded_file($_FILES['screen_shot']['tmp_name'], $dir.'/'.$_FILES['screen_shot']['name']))
    				{
    					$screenshot = 'uploads/screenshots/'.$project_id.'/'.$_FILES['screen_shot']['name'];

    				}else
    				{
    					$this->response = array('status'=>400,'message'=>'Sorry Screenshot could not be uploded');
    					$this->__send_response();
    				}
    		}

			$project_name = $chkSession['Project']['project_name'];
			$tokbox_api_key = TOKBOX_API_KEY;
			$tokbox_secret_key = TOKBOX_SECRET_KEY;
			try
			{
				$opentok = new \OpenTok\OpenTok($tokbox_api_key,$tokbox_secret_key);
					
				$archiveOptions = array(
						'name' => 'Archive for '.$project_name,     	// default: null
						'hasAudio' => true,                       	    // default: true
						'hasVideo' => true,                       	    // default: true
						'outputMode' => \OpenTok\OutputMode::INDIVIDUAL   // default: OutputMode::COMPOSED/INDIVIDUAL
				);
					
					$archive = $opentok->startArchive($sessionId, $archiveOptions);
					//Store this archiveId in the database for later use
					$archiveId = $archive->id; 
					if($archiveId){
						$this->Archieve->create();
						$archieve = array(
							'Archieve' => array(
								'stream_id'   => $stream_id,
								'scene_no'	  => $scene_no,
								'take_no'	  => $take_no,
								'project_id'  => $project_id,
								'session_id'  => $sessionId,
								'archieve_id' => $archiveId,
								'screen_shot'  => $screenshot,
								'file_name'	  => $file_name
								)	
							);
						$this->Archieve->Save($archieve);
						$this->response = array('status'=>200,'message'=>'success','data'=>$archiveId);
					}else
					{
						$this->response = array('status'=>400,'message'=>'failed');
					}
				}

				catch(\OpenTok\Exception\ArchiveDomainException $e)
				{
					$this->response = array('status'=>400,'message'=>'Something went wrong. Please try again.');
				}
				
			
		}else
		{
			$this->response = array('status'=>400,'message'=>'Please provide session id');
		}

		$this->__send_response();

	}

	/*=========== Method to stop an archieve ================*/
	function stopArchieve()
	{

		$data = $this->data;
		if(isset($data['archiveId']) && !empty($data['archiveId']))
		{
			$archiveId = $data['archiveId'];
			$tokbox_api_key = TOKBOX_API_KEY;
			$tokbox_secret_key = TOKBOX_SECRET_KEY;
			
			$opentok = new \OpenTok\OpenTok($tokbox_api_key,$tokbox_secret_key);
			$archive = $opentok->getArchive($archiveId); //die;
		
			if($archive->stop())
			{
				$this->response = array('status'=>200,'message'=>'success');
			}else
			{
				$this->response = array('status'=>400,'message'=>'error');
			}
		}else
		{
			$this->response = array('status'=>400,'message'=>'please provide archive id');
		}
		$this->__send_response();

	}

	function getArchieve(){

		$data = $this->data;
		if(isset($data['archiveId']) && !empty($data['archiveId']))
		{
			$archiveId = $data['archiveId'];
			$tokbox_api_key = TOKBOX_API_KEY;
			$tokbox_secret_key = TOKBOX_SECRET_KEY;
			
			$opentok = new \OpenTok\OpenTok($tokbox_api_key,$tokbox_secret_key);
			$archive = $opentok->getArchive($archiveId); //die;
			print_r($archive);
			echo $archive;
			$this->response = array('status'=>200,'message'=>'success','data'=>$archive);
			
		}else{
			$this->response = array('status'=>400,'message'=>'please provide archive id');
		}
		$this->__send_response();

	}

/*
	public function getSess(){archieve
		$path = ;
		$result = $this->generarchieve
		$this->response = arraarchievess','data'=>json_decode($result));
		$this->__send_response();	
	}
*/
	
	/*=========== Method to update archive info from opentok ================*/
	public function archive_info(){

			$data = $this->data;

			if(isset($data['archiveId']) && !empty($data['archiveId'])){
				$archiveId = $data['archiveId'];
			}else{
				$this->response = array('status'=>400,'message'=>'please provide archive id');
			}
			if(isset($data['project_id']) && !empty($data['project_id'])){
				$project_id = $data['project_id'];
			}else{
				$this->response = array('status'=>400,'message'=>'please provide project id');
			}
			
			$this->loadModel('Archieve');
    			    
					$path = 'https://api.opentok.com/v2/project/'.TOKBOX_PROJECT_API_KEY.'/archive/'.$archiveId;
					
		    		$result = $this->restApi($path);
		            if($result){
					  /*  $result = json_decode($result);
					    //print "<pre>";print_r($result);exit;
					    $desFolder = WWW_ROOT.'uploads/archives/'.$project_id.'/';
	    				if(!is_dir($desFolder)){
	    					mkdir($desFolder,0777);
	    				}
					    $url = $result->url;
						$time = time();
                        $fileName = 'archive_'.$time.'.mp4';
                        $filePath = $desFolder.$fileName;
						$fullpath = SITE_URL.'uploads/archives/'.$project_id.'/'.$fileName;
						$c = file_get_contents($url);
						//$c = $this->restApi1($url);
						file_put_contents($filePath,$c);
						//exit;

                        $get_data = $this->Archieve->findByArchieveId($archiveId);

                        $this->Archieve->id = $get_data['Archieve']['id'];
                        $this->request->data['Archieve']['name'] 		= $result->name;
                        $this->request->data['Archieve']['size']    	= $result->size;
                        $this->request->data['Archieve']['mode'] 		= $result->outputMode;
                        $this->request->data['Archieve']['reason'] 		= $result->reason;
                        $this->request->data['Archieve']['filepath'] 	= 'uploads/archives/'.$project_id.'/'.$fileName;
                        $this->request->data['Archieve']['duration'] 	= $result->duration;
                        $this->request->data['Archieve']['has_audio'] 	= $result->hasAudio;
                        $this->request->data['Archieve']['has_video'] 	= $result->hasVideo;
                        $this->request->data['Archieve']['updated_by'] 	= 'app';
                        $this->request->data['Archieve']['modified'] 	= date('Y-m-d H:i:s');

                        if( $this->Archieve->save($this->request->data)){
                        	$this->response = array('status'=>200,'message'=>'success');
                        }else{
                        	$this->response = array('status'=>400,'message'=>'Server Error');
						}*/
						$this->response = array('status'=>200,'message'=>'success');
                        
					}else{
						$this->response = array('status'=>400,'message'=>'Could not get any info for this archive');	
					}
				
		    $this->__send_response();
	}

	function deleteArchieve(){

		$data = $this->data;
		$archiveId = $data['archiveId'];
		$tokbox_api_key = TOKBOX_API_KEY;
		$tokbox_secret_key = TOKBOX_SECRET_KEY;
		$opentok = new \OpenTok\OpenTok($tokbox_api_key,$tokbox_secret_key);
		$archive = $opentok->getArchive($archiveId);
		$status = $archive->delete();
		$this->response = array('status'=>200,'message'=>$status);
		$this->__send_response();

	}

/*	public function list_archive(){

		$data = $this->data;
		$sessionId = $data['sessionId'];
		$tokbox_api_key = TOKBOX_API_KEY;
		$tokbox_secret_key = TOKBOX_SECRET_KEY;
		$opentok = new \OpenTok\OpenTok($tokbox_api_key,$tokbox_secret_key);
		$archiveList = $opentok->listArchives();
		$archives = $archiveList->getItems();
		$this->response = array('status'=>200,'message'=>'success','data'=>$archiveList);
		$this->__send_response();
	}


	public function get_archive_from_session(){

    		$data = $this->data;
    		$this->loadModel('Screenshot');
    		$this->loadModel('Project');
    		if(isset($data['sessionId'])){
	    		$sessionId = $data['sessionId'];
	    		$chkSession = $this->Project->findBySessionId($sessionId);
	    		if($chkSession){

	    			$project_id = $chkSession['Project']['id'];
					$path = 'https://api.opentok.com/v2/project/46029332/archive?sessionId='.$sessionId;
		    		$result = $this->restApi($path);
		            if($result) {
					    $res['recordings'] = json_decode($result);

					    foreach($res['recordings']->items as $item):
					    	
					    $url = $item->url;
						$time = time();
                        $desFolder =WWW_ROOT.DS. 'files/archives/';
                        $fileName = 'archive_'.$time.'.mp4';
                        $filePath = $desFolder.$fileName;
                        file_put_contents($filePath,file_get_contents($url));

						endforeach;
					}
		            $scrnShts = $this->Screenshot->find('all',array('conditions'=>array('Screenshot.project_id'=>$project_id)));
		            $screenArr = array();
		            if($scrnShts){

		            	foreach ($scrnShts as $value) {
		            		$value['Screenshot']['image'] = SITE_URL.$value['Screenshot']['image'];
		            		$screenArr[] = $value['Screenshot'];
		            	}
		            }
		            $res['screenshots'] = $screenArr;
		            $this->response = array('status'=>200,'message'=>'success','data'=>$res);
		        	
		        }else{
		        	$this->response = array('status'=>400,'message'=>'Please provide a valid session id');	
		        }
	        }else{
	        	$this->response = array('status'=>400,'message'=>'please provide session id');
	        }

	        $this->__send_response();
        }
    */

    /*=========== Method to save screenshots ================*/
    public function saveScreenshot(){

    		$this->loadModel('Screenshot');
    		$data = $this->data;
    		if(isset($data['project_id']) && !empty($data['project_id']))
    		{
    			$project_id = $data['project_id'];
    			$chkscreen = $this->Screenshot->findByProjectId($project_id);
    			if($chkscreen){
    				$this->Screenshot->id = $chkscreen['Screenshot']['id'];
    				$this->Screenshot->delete();
    			}
    			if(isset($_FILES['image']) && !empty($_FILES['image']['name']))
    			{
    				$dir = WWW_ROOT.'uploads/screenshots/'.$project_id;
    				if(!is_dir($dir))
    				{
    					mkdir($dir,0777);
    				}
    				if(move_uploaded_file($_FILES['image']['tmp_name'], $dir.'/'.$_FILES['image']['name'])){
    					$filename = 'uploads/screenshots/'.$project_id.'/'.$_FILES['image']['name'];

    						$screendata = array(
    							'Screenshot' => array(
	    							'project_id' => $project_id,
	    							'image' => $filename
    							)
    						);
    						
    						if($this->Screenshot->save($screendata)){
    							$this->response = array('status'=>200,'message'=>'success');
    						}else{
    							$this->response = array('status'=>400,'message'=>'failed');
    						}
    				}else{
    					$this->response = array('status'=>400,'message'=>'sorry, the file could not be uploaded');
    				}
    			}else{
    				$this->response = array('status'=>400,'message'=>'Please provide image');
    			}
    		}else{
    			$this->response = array('status'=>400,'message'=>'Please provide project id');
    		}
    		$this->__send_response();
    }


    /*=========== Method to get list of recordings ================*/
    public function getArchivesAndScreenshots(){

    	$data = $this->data;
    	$this->loadModel('Project');
    	$this->loadModel('Archieve');
    	$this->loadModel('Screenshot');
    	if(isset($data['sessionId']) && !empty($data['sessionId'])){
    		$sessionId = $data['sessionId'];
    		$archieves = $this->Archieve->find('all',array('conditions'=>array('Archieve.session_id'=>$sessionId),
    													   'fields'=>array('id','scene_no'),
    													   'group' => array('Archieve.scene_no')));

    		if($archieves)
    		{
    			//echo '<pre>'; print_r($archieves); die;
    			$scenes = array();
    			foreach($archieves as $arch):
    				if(!empty($arch['Archieve']['final_video']))
    				{
    					$arch['Archieve']['filepath'] = SITE_URL.'uploads/archives/'.$arch['Archieve']['project_id'].'/'.$arch['Archieve']['final_video'];
    				}
    				else if(!empty($arch['Archieve']['filepath']))
    				{
    					$arch['Archieve']['filepath'] = SITE_URL.$arch['Archieve']['filepath'];
    				}else
    				{
    					$arch['Archieve']['filepath'] = '';
    				}
    				if(!empty($arch['Archieve']['screen_shot']))
    				{
    					$arch['Archieve']['screen_shot'] = SITE_URL.$arch['Archieve']['screen_shot'];
    				}else
    				{
    					$arch['Archieve']['screen_shot'] = '';
					}
					//print "<pre>";print_r($arch);exit;
    				$project_id = $arch['Archieve']['project_id'];
    				$takes = $this->Archieve->find('all',array('conditions'=>array('Archieve.session_id'=>$sessionId,'Archieve.scene_no'=>$arch['Archieve']['scene_no'])));
    				if($takes)
    				{
    					//$archArr = array();
		    			foreach($takes as $take):
			    				if(!empty($take['Archieve']['final_video']))
			    				{
			    					$take['Archieve']['filepath'] = SITE_URL.'uploads/archives/'.$take['Archieve']['project_id'].'/'.$take['Archieve']['final_video'];
			    				}
		    					else if(!empty($take['Archieve']['filepath']))
		    					{
			    					$take['Archieve']['filepath'] = SITE_URL.$take['Archieve']['filepath'];
			    				}
			    				else
			    				{
			    					$take['Archieve']['filepath'] = '';
			    				}
			    				if(!empty($take['Archieve']['screen_shot']))
			    				{
			    					$take['Archieve']['screen_shot'] = SITE_URL.$take['Archieve']['screen_shot'];
			    				}
			    				else
			    				{
			    					$take['Archieve']['screen_shot'] = '';
			    				}
								$archArr[]  = $arch['Archieve'];
								$take['Archieve']['recording_status'] = $take['Archieve']['is_callback'];
			    				$scenes['scene_'.$arch['Archieve']['scene_no']][] = $take['Archieve'];
		    			endforeach;
		    			
		    		}else
		    		{
    					$scenes['scene_'.$arch['Archieve']['scene_no']] = array();
    				}
					
		    	endforeach;

    				$res['scenes'] = $scenes;
    				//echo '<pre>'; print_r($res); die;
    				$getscreen = $this->Screenshot->findByProjectId($project_id);
		    		if($getscreen)
		    		{
		    			$res['screenshot']['image'] = SITE_URL.$getscreen['Screenshot']['image'];
		    		}else
		    		{
		    			$res['screenshot'] = array();
		    		}
		    	//echo '<pre>'; print_r($res); die;
    			$this->response = array('status'=>200,'message'=>'success','data'=>$res);
    		}
    		else
    		{	
    			$this->response = array('status'=>200,'message'=>'success','data'=>array());
    		}
    	}
    	else
    	{
    		$this->response = array('status'=>400,'message'=>'Please provide session id');
    	}
    	$this->__send_response();
    }


    /*=========== Method to save single merged recording ================*/

    function saveMergedVideo()
    {
    	$this->loadModel('Project');
    	$data = $this->data;
    	if(isset($data['project_id']) && !empty($data['project_id'])){
    		$project_id = $data['project_id'];
    		$chkProject = $this->Project->findById($project_id);
    		if($chkProject){

    			if(isset($_FILES['reader_video']['name']) && !empty($_FILES['reader_video']['name'])){
    				$dir = WWW_ROOT.'uploads/fullvideos/'.$project_id;
    				if(!is_dir($dir)){
    					mkdir($dir,0777);
    				}
    				if(move_uploaded_file($_FILES['reader_video']['tmp_name'], $dir.'/'.$_FILES['reader_video']['name'])){

    					$filename = 'uploads/fullvideos/'.$project_id.'/'.$_FILES['reader_video']['name'];
    					$this->Project->id = $project_id;
    					if($this->Project->saveField('reader_video',$filename)){

    						$tapename = $chkProject['Project']['project_name'];
    						$getClient = $this->User->findById($chkProject['Project']['client_id']);
    						$getReader = $this->User->findById($chkProject['Project']['reader_id']);
    						$link = SITE_URL.$filename;

							$readername = $getReader['User']['firstname'];
    						$clientname = $getClient['User']['firstname'];
							$toreader = $getReader['User']['email'];
    						$toclient = $getClient['User']['email'];

    						// $toreader = 'gaganxicom@gmail.com';
    						// $toclient = 'gaganxicom@gmail.com';

    			// 			$arrFind = array('{user}','{username}','{tapename}','{link}');
							// $arrReplace = array($readername,$clientname,$tapename,$link);
							// $title = 'merged_video';
							// $result = $this->sendmail($title,$arrFind,$arrReplace,$toreader);

    						$arrFind = array('{user}','{username}','{tapename}','{link}');
							$arrReplace = array($clientname,$readername,$tapename,$link);
							$title = 'merged_video';
							$result = $this->sendmail($title,$arrFind,$arrReplace,$toclient);
    						
							$this->response = array('status'=>200,'message'=>'success');
    					}
    					else
    					{
    						$this->response = array('status'=>400,'message'=>'failed');
    					}
					}else
					{
    					$this->response = array('status'=>400,'message'=>'sorry, the file could not be uploaded');
    				}
    			}elseif(isset($_FILES['client_video']['name']) && !empty($_FILES['client_video']['name'])){

    				$dir = WWW_ROOT.'uploads/fullvideos/'.$project_id;
    				if(!is_dir($dir)){
    					mkdir($dir,0777);
    				}
    				if(move_uploaded_file($_FILES['client_video']['tmp_name'], $dir.'/'.$_FILES['client_video']['name'])){

    					$filename = 'uploads/fullvideos/'.$project_id.'/'.$_FILES['client_video']['name'];
    					$this->Project->id = $project_id;
    					if($this->Project->saveField('client_video',$filename)){

    						$tapename = $chkProject['Project']['project_name'];
    						$getClient = $this->User->findById($chkProject['Project']['client_id']);
    						$getReader = $this->User->findById($chkProject['Project']['reader_id']);
    						$link = SITE_URL.$filename;

							$readername = $getReader['User']['firstname'];
    						$clientname = $getClient['User']['firstname'];
							$toreader = $getReader['User']['email'];
    						$toclient = $getClient['User']['email'];

    						// $toreader = 'gaganxicom@gmail.com';
    						// $toclient = 'gaganxicom@gmail.com';

    			// 			$arrFind = array('{user}','{username}','{tapename}','{link}');
							// $arrReplace = array($readername,$clientname,$tapename,$link);
							// $title = 'merged_video';
							// $result = $this->sendmail($title,$arrFind,$arrReplace,$toreader);

    						$arrFind = array('{user}','{username}','{tapename}','{link}');
							$arrReplace = array($clientname,$readername,$tapename,$link);
							$title = 'merged_video';
							$result = $this->sendmail($title,$arrFind,$arrReplace,$toclient);

							
							$this->response = array('status'=>200,'message'=>'success');
    					}else{
    						$this->response = array('status'=>400,'message'=>'failed');
    					}
					}else{
    					$this->response = array('status'=>400,'message'=>'sorry, the file could not be uploaded');
    				}

    			}else{
    				$this->response = array('status'=>400,'message'=>'Please provide video file');
    			}
    		}else{
    			$this->response = array('status'=>400,'message'=>'Invalid project id');
    		}

    	}else{
    		$this->response = array('status'=>400,'message'=>'Please provide project id');
    	}
    	$this->__send_response();
    }


	function generateRandomString($length = 10) {
		    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < $length; $i++){
		            $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    return $randomString;
	}


	/*
	* Method: save_token
	* Description: this function is responsible for updating device token
	* Created By:  Pooja chaudhary on 20-july-2017
	*/

	function save_token($userId)
	{
				
			    $this->loadModel('DeviceToken');
			    $this->loadModel('User');
			
			    $getuser = $this->User->findById($userId);
			  
			    $user_role_id = $getuser['User']['user_role'];
			   	$delete = $this->DeviceToken->deleteAll(array('DeviceToken.user_id'=>$userId),false);
			    

				if($this->deviceToken && $delete){
					$this->DeviceToken->create();
					$tokenExists=array('DeviceToken'=>array(
					                   		'device_token'=>$this->deviceToken,
					                   		'user_id'=>$userId,
					                        'user_role_id'=>$user_role_id,
					                        'created'=>date('Y-m-d H:i:s')
					                        
				     ));

					$this->DeviceToken->save($tokenExists,false);
			    }else{
			    	$this->response = array('status'=>400,'message'=>'Please provide Device Token in header');
			    	$this->__send_response();
			    }
			return;
    }

    /*
	* Method: rateUser
	* Description: this function is responsible for rating user
	* Created By:  Pooja chaudhary on 06-dec-2017
	*/
    function rateUser(){

    	$this->loadModel('Review');
    	$this->loadModel('User');
    	$login_id = $this->userId;
    	$data = $this->data;
    	if(isset($data['user_id']) && !empty($data['user_id'])){
    		$user_id = $data['user_id'];
    		$this->request->data['Review']['user_id'] = $user_id;
    		$this->request->data['Review']['login_id'] = $login_id;

    		if(isset($data['project_id']) && !empty($data['project_id'])){
    			$this->request->data['Review']['project_id'] = $data['project_id'];
    		}
    		if(isset($data['private_message']) && !empty($data['private_message'])){
    			$this->request->data['Review']['private_msg'] = $data['private_message'];
    		}
    		if(isset($data['public_message']) && !empty($data['public_message'])){
    			$this->request->data['Review']['public_msg'] = $data['public_message'];
    		}
    		if(isset($data['rating_score']) && !empty($data['rating_score'])){
    			$this->request->data['Review']['rating_score'] = $data['rating_score'];
    		}
    		//else{
    		//	$this->response = array('status'=>400,'message'=>'Please provide rate score');
    		//	$this->__send_response();
    		//}
    		if(isset($data['tags']) && !empty($data['tags'])){
    			$this->request->data['Review']['tags'] = $data['tags'];
    		}

    		$this->request->data['Review']['created'] = date('Y-m-d H:i:s');

    		if($this->Review->save($this->request->data)){
    			$this->response = array('status'=>200,'message'=>'success');

    		}else{
    			$this->response = array('status'=>400,'message'=>'server error');
    		}
    		
		}else{
    		$this->response = array('status'=>400,'message'=>'Please provide user id');
    	}
    	$this->__send_response();
    }


    /* ======  CHAT API STARTS HERE ====== */

	function getChatList()
	{
		$this->loadModel('Chat');
		$this->loadModel('User');
		$userId = $this->userId;
		$chatlist = $this->Chat->find('all',array('conditions'=>array('OR'=>array('Chat.sender_id'=>$userId,'Chat.receiver_id'=>$userId)),'group'=>array('Chat.chat_id'),'fields'=>array('MAX(Chat.id) as id','MAX(Chat.created) as maxdate'),'order'=>array('Chat.id'=> 'DESC')));
		
		if($chatlist)
		{
			$chat = array();
			foreach($chatlist as $c):
				$chatdata = $this->Chat->findById($c['0']['id']);
				
				if($chatdata['Chat']['sender_id']==$userId)
				{
					$user_id = $chatdata['Chat']['receiver_id'];
				}
				else
				{
					$user_id = $chatdata['Chat']['sender_id'];
				}
				
				$getuser = $this->User->findById($user_id);
				if(!empty($getuser['User']['profile_pic']))
				{
					$picture = SITE_URL.'img/userImages/'.$getuser['User']['profile_pic'];
				}
				else
				{
					$picture = '';
				}
				$chatdata['Chat']['user_id'] = $user_id;
				$chatdata['Chat']['profile_name'] = $getuser['User']['profile_name'];
				$chatdata['Chat']['name'] = $getuser['User']['firstname'];
				$chatdata['Chat']['user_image'] = $picture;
				$chatdata['Chat']['is_online'] = $getuser['User']['is_online'];
				$chat[] = $chatdata['Chat'];
				
			endforeach;
			
			
			usort($chat, function ($item1, $item2) {
			    if ($item1['id'] == $item2['id']) return 0;
			    return $item1['id'] > $item2['id'] ? -1 : 1;
			});
			
			$this->response = array('status'=>200,'message'=>'success','data'=>$chat);
		}
		else
		{
			$this->response = array('status'=>200,'message'=>'No chat found','data'=>array());
		}
		$this->__send_response();
	}

	


	/* Method: sendMessage
    *  Description: This method is use to send messages
    *  Created by: Pooja Chaudhary on 15-June-2017
    */

    function sendMessage(){

        $this->loadModel('Chat');
        $this->loadModel('User');
        $this->loadModel('DeviceToken');
        $data = $this->data;
        if($data)
        {
            if(isset($data['receiver_id']) && !empty($data['receiver_id'])){
                $receiver_id = $data['receiver_id'];
                $this->request->data['Chat']['receiver_id'] = $receiver_id;
            }else{
                $this->response = array('status'=>400,'message'=>'Please provide receiver id');
                $this->__send_response();
            }

            if(isset($_FILES['attachment']) && $_FILES['attachment']['name']!==''){
                      
                if(move_uploaded_file($_FILES['attachment']['tmp_name'], WWW_ROOT.'uploads/attachments/'.$_FILES['attachment']['name']))
                {
                        $this->request->data['Chat']['attachment'] = SITE_URL.'uploads/attachments/'.$_FILES['attachment']['name'];
                }else{
                        $this->response = array('status'=>400,'message'=>'media file could not sent, please try again later');
                        $this->__send_response();
                }
            }
            if(isset($data['message']) && !empty($data['message'])){
                		$this->request->data['Chat']['message'] = $data['message'];
            }

            if(isset($_FILES['attachment']) && $_FILES['attachment']['name']!=='' && isset($data['message']) && !empty($data['message'])){
            	$this->request->data['Chat']['isMediaMessage'] = 2;
            }elseif(isset($_FILES['attachment']) && $_FILES['attachment']['name']!==''){
            	$this->request->data['Chat']['isMediaMessage'] = 1;
            	$this->request->data['Chat']['message'] = '';
            }elseif(isset($data['message']) && !empty($data['message'])){
            	$this->request->data['Chat']['isMediaMessage'] = 0;
            	$this->request->data['Chat']['attachment'] = '';
            }
                
            $userId = $this->userId;
            $getSender = $this->User->findById($userId);
            $chkRcver = $this->User->findById($receiver_id);
            
			if($chkRcver)
            {
                $sender_id = $this->userId;
                $chatlist_id = $sender_id+$receiver_id;
                $stringParts = str_split($chatlist_id);
                sort($stringParts);
                $chat_id = implode('', $stringParts);
                $this->request->data['Chat']['chat_id'] = $chat_id;
                $this->request->data['Chat']['sender_id'] = $sender_id;
               
               if($rs = $this->Chat->save($this->request->data)){
                	//noti code start here //
					$this->sendNotification($userId,$receiver_id,'New message',array());
					//noti code ends here //
                	$this->response = array('status'=>200,'message'=>'success','data'=>$rs['Chat']);
                }else{
                	$this->response = array('status'=>400,'message'=>'Server Error');
                }

                
            }else{
            	 $this->response = array('status'=>400,'message'=>'Please provide valid receiver id');
            }
                
            
        }else{
            $this->response = array('status'=>400,'message'=>'Please provide data');
        }
        $this->__send_response();
    }


    function getAllMessages(){

    	$this->loadModel('User');
    	$this->loadModel('Chat');
    	$userId = $this->userId;
    	$data = $this->data;
    	if(isset($data['receiver_id']) && !empty($data['receiver_id'])){
    		$receiver_id = $data['receiver_id'];
			$messages = $this->Chat->find('all',array('conditions'=>array(
																'OR'=>array(
																	array('Chat.sender_id'=>$userId,'Chat.receiver_id'=>$receiver_id),
																	array('Chat.sender_id'=>$receiver_id,'Chat.receiver_id'=>$userId)
		                                                        )),
    															//'order' => array('Chat.created'=>'ASC')
																)); 
			if($messages){
				
				$chat = array();
				foreach($messages as $msg):
					$sender = $this->User->findById($msg['Chat']['sender_id']);
					if(!empty($sender['User']['profile_pic']))
					{
						$picture = SITE_URL.'img/userImages/'.$sender['User']['profile_pic'];
					}
					else
					{
						$picture = '';
					}

					$msg['Chat']['sender_name'] = $sender['User']['firstname'].$sender['User']['lastname'];
					$msg['Chat']['user_image'] = $picture;
					$chat[] = $msg['Chat'];

				endforeach;

				$this->response = array('status'=>200,'message'=>'success','data'=>$chat);
			}else{
				$this->response = array('status'=>200,'message'=>'No message found','data'=>array());
			}
    	}else{
    		$this->response = array('status'=>400,'message'=>'Please provide receiver id');
    	}
    	$this->__send_response();
    }
    

	/* ======  CHAT API ENDS HERE ====== */

	public function createReaderStripeAccount($id=null){

		$this->loadModel('User');
		$user = $this->User->findById($id);

		$acc_response = $this->Stripe->create_account($user['User']['email']);
		if($acc_response['status']==400){
			    $this->response = $acc_response;
			    $this->__send_response();
		}
			$acc_id = $acc_response['id'];
			$accountArr = array(
			    'StripeAccount'   => array(
			    	'reader_id'   => $user['User']['id'],
			    	'account_id'  => $acc_id,
			    	'created'     => date('Y-m-d H:i:s')
			    )
			);
		$this->StripeAccount->save($accountArr,false);
		return $acc_id;

	}

	function logout(){
         
			$userId = $this->userId;
			$this->loadModel('DeviceToken');
			$delete = $this->DeviceToken->deleteAll(array('DeviceToken.user_id'=>$userId),false);
			$this->Auth->logout();
            $this->response = array(
                            'status' => 200,
                            'message' => 'logged out',
					);
			
            $this->__send_response();
    }


	function encryptor($action, $string) {
	    $output = false;

	    $encrypt_method = "AES-256-CBC";
	    //pls set your unique hashing key
	    $secret_key = 'muni';
	    $secret_iv = 'muni123';

	    //hash
	    $key = hash('sha256', $secret_key);

	    //iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	    $iv = substr(hash('sha256', $secret_iv), 0, 16);

	    //do the encyption given text/string/number
	    if( $action == 'encrypt' ) {
		$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
		$output = base64_encode($output);
	    }
	    else if( $action == 'decrypt' ){
	    //decrypt the given text/string/number
		$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	    }

	    return $output;
	}


	/*Method: sendmail
    * Description: this is a common method to send mails
    * Created By: Pooja chaudhary on 1-september-2017
    */
	function sendmail($title,$arrfind,$arrReplace,$to)
	{

    	$this->loadModel('EmailTemplate');
        $template = $this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.title'=>$title)));
       	if($template)
       	{ 
            $arrFind= $arrfind;
            $arrReplace= $arrReplace;
            $from = $template['EmailTemplate']['from_email'];
            $subject = $template['EmailTemplate']['subject'];
            $content=str_replace($arrFind, $arrReplace,$template['EmailTemplate']['content']);
        }
        $result = $this->sendEmail($to,$from,$subject,$content); 
        return $result;
    }


    
	/* Method: refundAmount
	*  Description: for refunding amount
	*  Created By: Pooja Chaudhary
	*/
    function refundAmount()
    {
    	$this->loadModel('StripeCharge');
    	$this->loadModel('Refund');
    	$this->loadModel('Project');
    	$this->loadModel('Archieve');
    	$data = $this->data;
    	if(isset($data['user_id']) && !empty($data['user_id']))
    	{
    		$user_id = $data['user_id'];
			if(isset($data['project_id']) && !empty($data['project_id']))
    		{
    			$project_id = $data['project_id'];
    			$project = $this->Project->findById($project_id);
    			if(!empty($project['Project']['promo_code']))
				{
					$reader = $this->User->findByPromoCode($project['Project']['promo_code']);
					$readerId = $reader['User']['id'];
					$promo_count = $reader['User']['promo_count']-1;
					
				}
			}
    		else
    		{
    			$this->response = array('status'=>400,'message'=>'Please provide project id');
    			$this->__send_response();
    		}

    		if(isset($data['refund_initiated_by']) && !empty($data['refund_initiated_by']))
    		{
    			$refund_initiated_by = $data['refund_initiated_by'];
    		}
    		else
    		{
    			$this->response = array('status'=>400,'message'=>'Please provide refund_initiated_by value');
    			$this->__send_response();
    		}

    		$stripedata = $this->StripeCharge->find('first',array('conditions'=>array('StripeCharge.project_id'=>$project_id)));
    		if($stripedata)
    		{
    			$charge_id = $stripedata['StripeCharge']['charge_id'];
    			$result = $this->Stripe->create_refund($charge_id);
    			if($result['status']==400)
    			{
					
    				$this->response = ['status'=>400,'message'=>$result['message']];
    				$this->__send_response();
    			}
    			else
    			{
					$this->StripeCharge->id = $stripedata['StripeCharge']['id'];
		    		$this->StripeCharge->saveField('charge_status','refunded');
					$refundArr = array(
    					'Refund' => array(
    						'project_id' => $project_id,
    						'refund_id'	 => $result['id'],
    						'charge_id'	 => $result['charge'],
    						'amount'	 => $result['amount']/100,
    						'status'	 => $result['status'],
    						'currency'	 => $result['currency'],
    						'receipt_no' => $result['receipt_no'],
    						'failed_reason' => $result['reason']
    						)
    					);

    				if($this->Refund->save($refundArr))
    				{

    					if($project['Project']['call_status']==1 || $project['Project']['call_status']==2)
    					{
	    					if(!empty($project['Project']['promo_code']))
							{
										$reader = $this->User->findByPromoCode($project['Project']['promo_code']);
										$readerId = $reader['User']['id'];
										$promo_count = $reader['User']['promo_count']-1;
										$this->User->id = $readerId;
										$this->User->saveField('promo_count',$promo_count);

								        $this->User->id = $readerId;
								        $this->User->saveField('promo_count',$promo_count);
							}
	    				}
    					
    					if($refund_initiated_by=='actor')
	    				{
	    					
	    					$this->Archieve->deleteAll(array('Archieve.project_id'=>$project_id));
	    					//noti code start here //
							$this->sendNotification($project['Project']['client_id'],$project['Project']['reader_id'],'Call End');
							//noti code ends here //
	    				}
	    				elseif($refund_initiated_by=='reader')
	    				{
	    					//noti code start here //
							$this->sendNotification($project['Project']['reader_id'],$project['Project']['client_id'],'Call End');
							//noti code ends here //
	    				}	

    					$this->Project->updateAll(array('Project.refund_initiated_by'=>"'$refund_initiated_by'",'Project.status'=>'8','Project.refund_request'=>'1'),array('Project.id'=>$project_id));
    					$this->response = array('status'=>200,'message'=>'success');
    				}
    				else
    				{
    					$this->response = array('status'=>400,'message'=>'Server Error');
    				}
    			}
    		}
    		else
    		{
    			$this->response = array('status'=>400,'message'=>'Sorry! No charge id found for this project, so you are not eligible for any refund process.');
    		}
		}
		else
		{
    		$this->response = array('status'=>400,'message'=>'Please provide user id');
    	}
    	$this->__send_response();	
	}



	public function raiseDispute()
	{

		$this->loadModel('Dispute');
		$data = $this->data;
		$userId = $this->userId;

		if(isset($data['project_id']) && !empty($data['project_id']))
		{
			$projectId = $data['project_id'];
			$this->request->data['Dispute']['project_id'] = $data['project_id'];
			$this->request->data['Dispute']['user_id'] = $userId;
			$chkdispute = $this->Dispute->find('first',array('conditions'=>array('Dispute.project_id'=>$data['project_id'])));
			if($chkdispute)
			{
				$this->response = array('status'=>400,'message'=>'Sorry! you have already raised dispute for this project');
				$this->__send_response();
			}
		}
		else
		{
			$this->response = array('status'=>400,'message'=>'Please provide project id');
			$this->__send_response();
		}

		if(isset($data['message']) && !empty($data['message']))
		{
			$this->request->data['Dispute']['message'] = $data['message'];
		}
		else
		{
			$this->response = array('status'=>400,'message'=>'Please provide message');
			$this->__send_response();
		}

		if(isset($_FILES['image1']))
    	{
				if(!empty($_FILES['image1']['name']))
				{
					$image1 = $_FILES['image1']['name'];
					if(move_uploaded_file($_FILES["image1"]["tmp_name"], WWW_ROOT . 'uploads/disputes/'.$image1))
					{
						$this->request->data['Dispute']['image1']  = 'uploads/disputes/'.$image1;
						
					}
					else
					{
						$this->response = array('status'=>400,'message'=>'Image1 could not be uploaded, please try again later');
						$this->__send_response();
					}
				}
		}

		if(isset($_FILES['image2']))
    	{
				if(!empty($_FILES['image2']['name']))
				{
					$image2 = $_FILES['image2']['name'];
					if(move_uploaded_file($_FILES["image2"]["tmp_name"], WWW_ROOT . 'uploads/disputes/'.$picName))
					{
						$this->request->data['Dispute']['image2']  = 'uploads/disputes/'.$image2;
						
					}
					else
					{
						$this->response = array('status'=>400,'message'=>'Image2 could not be uploaded, please try again later');
						$this->__send_response();
					}
				}
		}

		if(isset($_FILES['image3']))
    	{
				if(!empty($_FILES['image3']['name']))
				{
					$image3 = $_FILES['image3']['name'];
					if(move_uploaded_file($_FILES["image3"]["tmp_name"], WWW_ROOT . 'uploads/disputes/'.$picName))
					{
						$this->request->data['Dispute']['image3']  = 'uploads/disputes/'.$picName;
						
					}
					else
					{
						$this->response = array('status'=>400,'message'=>'Image3 could not be uploaded, please try again later');
						$this->__send_response();
					}
				}
		}

		if($this->Dispute->save($this->request->data))
		{

			$this->loadModel('Project');
			$this->Project->id = $data['project_id'];
			$this->Project->saveField('is_dispute',1);
			$this->response = array('status'=>200,'message'=>'Success');

		}
		else
		{
			$this->response = array('status'=>400,'message'=>'Server Error! dispute could not submitted, please try again');
			$this->__send_response();
		}

		$this->__send_response();
	}


	public function clientPosts()
	{

		$this->loadModel('Post');
    	$this->loadModel('PostLike');
    	$this->loadModel('PostComment');
    	
    	$data = $this->data;
    	if(isset($data['client_id']) && !empty($data['client_id']))
    	{
    		$client_id = $data['client_id'];
			$this->PostComment->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'user_id','fields'=>array('id','firstname',
	    		'lastname','profile_pic','city')))));
	    	$this->Post->bindModel(array('hasMany'=>array('PostComment'=>array('class'=>'PostComment','foreignKey'=>'post_id'))));
	    	$this->Post->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'user_id'))));
	    	$this->Post->recursive = 2;
	    	$posts = $this->Post->find('all',array('conditions'=>array('Post.user_id'=>$client_id),'order'=>'Post.id DESC'));

	    	if($posts)
	    	{
	    		
	    		$comments = array();
	    		$likes = array();
		    	foreach($posts as $post)
		    	{

		    		if(!empty($post['Post']['full_path']))
		    		{
		    			$post['Post']['full_path'] = SITE_URL.$post['Post']['full_path'];
		    		}else
		    		{
		    			$post['Post']['full_path'] = '';
		    		}
		    		if(!empty($post['Post']['thumb_path']))
		    		{
		    			$post['Post']['thumb_path'] = SITE_URL.$post['Post']['thumb_path'];
		    		}else
		    		{
		    			$post['Post']['thumb_path'] = '';
		    		}
		    		if(!empty($post['Post']['url_link']))
		    		{
		    			$post['Post']['url_link'] = $post['Post']['url_link'];
		    		}
		    		elseif(!empty($post['Post']['full_videolink']))
		    		{
		    			$post['Post']['url_link'] = SITE_URL.$post['Post']['full_videolink'];
		    		}else
		    		{
		    			$post['Post']['url_link'] = '';
		    		}

		    		$chklike = $this->PostLike->find('first',array('conditions'=>array('PostLike.user_id'=>$userId,'PostLike.post_id'=>$post['Post']['id'],'status'=>1)));
		    		if($chklike)
		    		{
		    			$like_status = '1';
		    		}
		    		else
		    		{
		    			$like_status = '0';
		    		}

		    		$post['Post']['like_status'] = $like_status;
		    	    $post['Post']['username'] = $post['User']['firstname'].''.$post['User']['lastname'];
		    	    $post['Post']['profile_name'] = $post['User']['profile_name'];
		    		$post['Post']['city'] = $post['User']['city'];
		    		$post['Post']['user_image'] = SITE_URL.'img/userImages/'.$post['User']['profile_pic'];

		    		/* ---- post comment data start -----*/

		    		if(!empty($post['PostComment']))
		    		{

		    			$count_comment = sizeof($post['PostComment']);
		    			foreach ($post['PostComment'] as $comment){

		    				if(!empty($comment['User']['profile_pic']))
		    				{
		    					$profile_pic = SITE_URL.'img/userImages'.$comment['User']['profile_pic'];
		    				}
		    				else
		    				{
		    					$profile_pic = '';
		    				}

		    				$comments[] = array(
		    					'id' => $comment['id'],
		    					'user_id' => $comment['user_id'],
		    					'post_id' => $comment['post_id'],
		    					'comment' => $comment['comment'],
		    					'user_name' => $comment['User']['firstname'].' '.$comment['User']['lastname'],
		    					'profile_name' => $comment['User']['profile_name'],
		    					'profile_pic' => $profile_pic,
		    					'created' => $comment['created']
		    					);
		    			}
		    			$post['Post']['comments'] = $comments;
		    			$post['Post']['total_comments'] = $count_comment;

		    		}
		    		else
		    		{

		    			$post['Post']['comments'] = array();
		    			$post['Post']['total_comments'] = 0;

		    		}

		    		/* ---- post comment data end -----*/


		    		/* ---- post like data start -----*/

		    		$likes = array();
		    		$this->PostLike->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'user_id','fields'=>array('id','firstname','lastname','profile_pic','city')))));
		    		$getlikes = $this->PostLike->find('all',array('conditions'=>array('PostLike.post_id'=>$post['Post']['id'],'PostLike.status'=>1)));
		    		if($getlikes)
		    		{
		    				//$likes = array();
		    				foreach($getlikes as $getlike):
			    				if(!empty($getlike['User']['profile_pic'])){
			    					$profile_pic = SITE_URL.'img/userImages/'.$getlike['User']['profile_pic'];
			    				}else{
			    					$profile_pic = '';
			    				}

			    				$likes[] = array(
			    					'id' => $getlike['PostLike']['id'],
			    					'user_id' => $getlike['PostLike']['user_id'],
			    					'post_id' => $getlike['PostLike']['post_id'],
			    					'user_name' => $getlike['User']['firstname'].' '.$like['User']['lastname'],
			    					'profile_name' => $getlike['User']['profile_name'],
			    					'city' => $getlike['User']['city'],
			    					'profile_pic' => $profile_pic,
			    					'created' => $getlike['PostLike']['created']
			    					);
		    				endforeach;
		    		}

		    		if($likes)
		    		{
		    			$count_likes = sizeof($likes);
		    			$post['Post']['likes'] = $likes;
		    			$post['Post']['total_likes'] = $count_likes;
		    		}
		    		else
		    		{
		    			$post['Post']['likes'] = array();
		    			$post['Post']['total_likes'] = 0;
		    		}

		    		/* ---- post like data end-----*/

		    		$responseArr[] = $post['Post'];
		    	}

				if($responseArr)
				{
		    		$this->response = array('status'=>200,'message'=>'success','data'=>$responseArr);
		    	}
		    	else
		    	{
		    		$this->response = array('status'=>400,'message'=>'Post could not created');
		    	}
		    }
		    else
		    {
		    	$this->response = array('status'=>400,'message'=>'No post found');
		    }
		}
		else
		{
			$this->response = array('status'=>400,'message'=>'Please provide client id');

		}
    	$this->__send_response();

	}


	public function invoices(){

		$this->loadModel('Payment');
		$this->loadModel('Project');
		$this->loadModel('User');

		$userId = $this->userId;
		$user = $this->User->findById($userId);
		$this->Payment->bindModel(array('belongsTo'=>array('Project'=>array('class'=>'Project','foreignKey'=>'project_id'))));
		$payments = $this->Payment->find('all',array('conditions'=>array('OR'=>['Payment.client_id'=>$userId,'Payment.reader_id'=>$userId]),'order'=>array('Payment.id'=>'DESC')));	

		if($payments)
		{
			$invoices = array();
			foreach ($payments as $payment)
			{
				$reader = $this->User->find('first',array('conditions'=>array('User.id'=>$payment['Payment']['reader_id']),'fields'=>array('id','firstname','lastname')));
				$client = $this->User->find('first',array('conditions'=>array('User.id'=>$payment['Payment']['client_id']),'fields'=>array('id','firstname','lastname')));
				$invoices[] = array(
					'project_name' => $payment['Project']['project_name'],
					'reader_name'  => $reader['User']['firstname'],
					'client_name' =>  $client['User']['firstname'],
					'session_length' => $payment['Project']['duration'],
					'amount'	=> $payment['Project']['amount'],
					'date'		=> $payment['Payment']['created'],

					);
			}

			if($invoices)
			{
				$this->response = array('status'=>200,'message'=>'success','data'=>$invoices);
			}
			else
			{
				$this->response = array('status'=>200,'message'=>'No invoice found');
			}
		}
		else
		{
			$this->response = array('status'=>200,'message'=>'No invoice found');
		}
		$this->__send_response();
		
	}

	public function notificationList(){

		$this->loadModel('NotiData');
		$userId = $this->userId;

		$notifications  = $this->NotiData->find('all',array('conditions'=>array('NotiData.user_id'=>$userId),'order'=>'NotiData.id DESC'));
		if($notifications){
			$this->response = array('status'=>200,'message'=>'success','data'=>$notifications);
		}else{
			$this->response = array('status'=>200,'message'=>'no data found','data'=>array());
		}
		$this->__send_response();
	}

	public function userStatus(){

		$this->loadModel('User');
		$data = $this->data;

		if(isset($data['status']) && $data['status']!=''){
			$status = $data['status'];
			
				$this->User->id = $this->userId;
				if($this->User->saveField('is_online',$status)){
					$this->response = array('status'=>200,'message'=>'success');
				}else{
					$this->response = array('status'=>400,'message'=>'Server Error');
				}
			
		}else{
			$this->response = array('status'=>400,'message'=>'Please provide status');
		}
		$this->__send_response();
	}


	/* This api is used to search users with their profile name */
	public function searchUsers(){

		$this->loadModel('Users');
		$this->loadModel('Friend');
		$userId = $this->userId;
		$data = $this->data;
		if(isset($data['searchKey']) && !empty($data['searchKey'])){
			$searchKey = $data['searchKey'];
			$users = $this->User->find('all',array('conditions'=>array('User.profile_name LIKE'=>'%'.$searchKey. '%','User.status'=>'Active'),'fields'=>array('id','firstname','lastname','profile_name','profile_pic','email','city','location','language','gender','rating_score')));
			
			if($users){
				$userArr = array();
				foreach($users as $user):
					unset($user['User']['password']);
					$ids = array($userId,$user['User']['id']);
	    		    $chkfriend = $this->Friend->find('first',array('conditions'=>array('Friend.user_id IN'=>$ids,'Friend.friend_id IN'=>$ids,'Friend.status !='=>0)));
	    			if($chkfriend){
		    			$friend_status = '1';
		    		}else{
		    			$friend_status = '0';
		    		}
		    		$user['User']['friend_status'] = $friend_status;

		    		if(!empty($user['User']['profile_pic'])){
    					$user['User']['profile_pic'] = SITE_URL.'img/userImages/'.$user['User']['profile_pic'];
					}

					if(!empty($user['User']['govt_id_pic'])){
    					$user['User']['govt_id_pic'] = SITE_URL.'img/userImages/'.$user['User']['govt_id_pic'];
					}

		    		$userArr[] = $user;
				endforeach;
				$this->response = array('status'=>200,'message'=>'success','data'=>$userArr);
			}else{
				$this->response = array('status'=>200,'message'=>'No data found');
			}

		}else{
			$this->response = array('status'=>400,'message'=>'Please provide searchKey');
		}
		$this->__send_response();
	}

	/* This api is used to deactivate account */
	function deactivateAccount()
	{

		$data = $this->data;
		$this->loadModel('User');
		$userId = $this->userId;

		$chkuser = $this->User->find('first',array('conditions'=>['User.id'=>$userId]));
		if($chkuser)
		{
			$username = $chkuser['User']['firstname'].''.$user['User']['lastname'];
			if($chkuser['User']['user_role']==2)
			{
				$role = 'Actor';
			}
			else{
				$role = 'Reader';
			}
			$this->User->id = $userId;
			if($this->User->saveField('status','deactivated'))
			{

				$this->loadModel('AdminNotification');
				$adminnoti['user_id'] = $userId;
				$adminnoti['message'] = '<a href="'.SITE_URL.'users/view_user/'.$userId.'">'.ucfirst($username).' '.'('.$role.')'.' '.'has deleted his account.</a>';
				$this->AdminNotification->save($adminnoti);
				$this->response = ['status'=>200,'message'=>'success'];
			}
			else
			{
				$this->response = ['status'=>400,'message'=>'Server Error'];
			}

		}
		else
		{
			$this->response = ['status'=>400,'message'=>'Invalid user id'];
		}
		$this->__send_response();
	}


	public function ExtendTapeResponseFromActor()
	{
		$this->loadModel('Project');
		$this->loadModel('User');

		$data = $this->data;
		
		$project_id = $data['project_id'];
		$project = $this->Project->findById($project_id);
		$readerId = $project['Project']['reader_id'];
		$clientId = $project['Project']['client_id'];
		$duration = $project['Project']['duration'];
		$schedule_time = $project['Project']['schedule_time'];
		$schedule_end_time = date('Y-m-d H:i:s',strtotime("+".$duration." minutes", strtotime($project['Project']['schedule_time'])));
		
		$chkProject = $this->Project->find('all',array('conditions'=>array('Project.schedule_time'=>$schedule_end_time,'Project.reader_id'=>$readerId)));
		if($chkProject)
		{
			$this->response = array('status'=>200,'message'=>'Sorry! slots are not available for extending the tape');
			$this->__send_response();
		}
		else
		{
			//noti code start here //
			$this->sendNotification(1,$readerId,'Tape Extend Request',array('project_id'=>$project['Project']['id'],'reader_id'=>$readerId,'client_id'=>$clientId,
											 'tape_type'=>$project['Project']['tape_type']));
			//noti code ends here //

			$this->response = array('status'=>200,'message'=>'Notification sent to actor');
			$this->__send_response();
		}
	}

	public function ExtendTapeResponseFromReader()
	{
		$this->loadModel('Project');
		$this->loadModel('User');
		$this->loadModel('StripeCharge');
		$this->loadModel('StripeAccount');
		$this->loadModel('UserCard');
		$data = $this->data;
		
		$project_id = $data['project_id'];
		$project = $this->Project->findById($project_id);
		$promo = $project['Project']['promo_code'];
		$readerId = $project['Project']['reader_id'];
		$clientId = $project['Project']['client_id'];
		$duration = $project['Project']['duration'];
		$schedule_time = $project['Project']['schedule_time'];
		$schedule_end_time = date('Y-m-d H:i:s',strtotime("+".$duration." minutes", strtotime($project['Project']['schedule_time'])));
		$account = $this->StripeAccount->findByReaderId($readerId);
		if(!$account)
		{
			$acc_id =  $this->createReaderStripeAccount($userId);
		}
		else
		{
			$acc_id = $account['StripeAccount']['account_id'];
		}
		$customer = $this->UserCard->findByUserId($clientId);
		$customer_id = $customer['UserCard']['customer_id'];
		
		$chkcharge = $this->StripeCharge->find('first',array('conditions'=>array('StripeCharge.project_id'=>$project_id),'fields'=>array('id','charge_id')));
		if($chkcharge)
		{
			/*$charge_id = $chkcharge['StripeCharge']['charge_id'];
			$result = $this->Stripe->create_refund($charge_id);
			$this->StripeCharge->delete($chkcharge['StripeCharge']['id']);*/
		}
		//$duration = $project['Project']['duration']+30;  // extend 30 min call
		$duration = $project['Project']['duration']+15; // extend 10 min call
		//$amount   = ($duration/30) * 20;
		$amount   = $project['Project']['amount']+15;
		$extendedAmount = 15 ;

		if($project['Project']['promo_code']!='')
    	{
    		$fee = (25/100)*$amount;
	    	$tape_fee = $fee-2;
		}
    	else
    	{
			//$tape_fee = (25/100)*$amount;
			$tape_fee = 5;
    	}
    		
    	$app_fee = $tape_fee*100;
    	$transfer_amount = ($amount*100)-$app_fee;
    		 	
    	if($acc_id && $customer_id)
    	{
		   	$chargedata = array();
		    $chargedata['amount'] =  $extendedAmount*100;
		    $chargedata['app_fees'] = $app_fee;
		    $chargedata['acc_id'] = $acc_id;
		    $chargedata['customer_id'] = $customer_id;
		    $charge = $this->Stripe->create_charge($chargedata);
			
			if($charge['status']==400)
			{
				$this->response = $charge;
				$this->__send_response();
			}
			else
			{
				$charge_id = $charge['id'];
				$stripedata = array(
				'StripeCharge' => array
						(
							'project_id' => $project_id,
							'charge_id' => $charge_id,
							'amount' => $amount,
							'app_fee' => $app_fee/100,
							'reader_amount' => $transfer_amount/100,
							'on_behalf_of' => $charge['on_behalf_of'],
							'status' => $charge['status'],
							'transfer_group' => $charge['transfer_group'],
							'response' => json_encode($charge),
							'created' => date('Y-m-d H:i:s'),
						)
					);
			}
			$projectdata = array(
							'Project' => array(
								'duration' => $duration,
								'amount'   => $amount,
								'reader_amount'   => $project['Project']['reader_amount'] + ($extendedAmount - $app_fee),
								'app_fees'   => $project['Project']['app_fees'] + $app_fee,
								'status'   => 4,
								'modified' => date('Y-m-d H:i:s')
								)
			);

			$this->Project->id = $project_id;
			$this->StripeCharge->create();
			if($this->Project->save($projectdata) && $this->StripeCharge->save($stripedata))
			{
				$this->StripeCharge->delete($chkcharge['StripeCharge']['id']);
				$this->sendNotification($project['Project']['reader_id'],$project['Project']['client_id'],'CallExtend',['project_id'=>$project['Project']['id']]);
				$this->response = array('status'=>200,'message'=>'Your call is extended for 15 min');
				$this->__send_response();
			}
			else
			{
				$this->response = array('status'=>400,'message'=>'Server Error!');
				$this->__send_response();
			}
		}
		else
		{
			$this->response = array('status'=>200,'message'=>'Please check your billing preferences');
			$this->__send_response();
		} 
	}

	public function readerRevenue()
	{
		$this->loadModel('Project');
		$this->loadModel('User');

		$data = $this->data;
		$readerId = $this->userId;
		$this->Project->bindModel(array('belongsTo'=>array('StripeCharges'=>array('class'=>'StripeCharges','foreignKey'=>'id','fields'=>array('reader_amount')))));
		$project = $this->Project->find('first',['conditions'=>['Project.reader_id'=>$readerId],'fields'=>['SUM(Project.duration) as total_duration','SUM(StripeCharges.reader_amount) as total_revenue','id'],'group'=>['Project.reader_id']]);
		$this->response = array('status'=>200,'data'=>['total_duration'=>$project[0]['total_duration'].' min','total_revenue'=>'$'.$project[0]['total_revenue']]);
		$this->__send_response();
		
	}

	// promo Information
	public function getPromoInfo()
	{
		$this->loadModel('Project');
		$this->loadModel('User');
		
		
		$this->loadModel('UserCard');
		$data = $this->data;
		if($data['reader_id'])
		{
			
		$user = $this->User->find('first',['conditions'=>['id'=>$data['reader_id']]]);
		$promocode = $user['User']['promo_code'];
		if($promocode)
		{
			$this->Project->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'client_id','fields'=>array('id','firstname','lastname','profile_pic')))));
			$project = $this->Project->find('all',['conditions'=>['Project.promo_code'=>$promocode],'fields'=>['Project.created','User.firstname','User.lastname','User.profile_pic']]);
			$res = [];
			foreach($project as $p)
			{
				$pic = ($p['User']['profile_pic']) ? SITE_URL.'img/userImages/'.$p['User']['profile_pic'] : "" ;
				$res[] = ['created'=>date('m-d-y',strtotime($p['Project']['created'])),'firstname'=>$p['User']['firstname'],
				'lastname'=>$p['User']['lastname'],
				'profile_pic'=>$pic];
				
				
			}
			
			
			$this->response = array('status'=>200,'data'=>$res);
			$this->__send_response();
		}
		else{
			$this->response = array('status'=>200,'message'=>'User does not have promocode.');
			$this->__send_response();
		}
	}
	else{
		$this->response = array('status'=>200,'message'=>'Please provide reader_id');
			$this->__send_response();
	}
	}



	public function timezoneconvert($date, $from , $to = 'UTC')
    {
		date_default_timezone_set($from);
       	$datetime = new \DateTime($date);
        $datetime->format('Y-m-d H:i:s');
        $timeEurope = new \DateTimeZone($to);
        $datetime->setTimezone($timeEurope);
        //date_default_timezone_set('UTC');
        return $pickup_date = $datetime->format('Y-m-d H:i:s');
    	
    }

    public function timezoneconvertToUtc($date, $from)
    {
		date_default_timezone_set($from);
       	$datetime = new \DateTime($date);
        $datetime->format('Y-m-d H:i:s');
        $timeEurope = new \DateTimeZone('UTC');
        $datetime->setTimezone($timeEurope);
        //date_default_timezone_set('UTC');
        return $pickup_date = $datetime->format('Y-m-d H:i:s');
    	
    }
	    	
}
