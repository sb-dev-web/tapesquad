<?php 


App::uses('AppController', 'Controller');
//require_once(ROOT.'/vendors'.DS.'ffmpeg'.DS.'autoload.php');
class UsersController extends AppController{

	public $name = 'users';
	public $components = array('Auth','RequestHandler','Paginator','Core','Session','Stripe');
	public $helpers = array('Html','Session','Form','Js');
	public $uses = array('User','Project','Review','Faq','FaqTopic','Refund','Payment','StripeToken','Post','PostComment','PostLike','ReportPost');


	public function beforeFIlter()
	{
		parent::beforeFilter();
		$this->Auth->allow(array('activate_account','login','ajax_update_userstatus','chkRequestStatus','chkRequestCallStatus','archive_info','unzipfile','chkcard','convertvideo','makePayment','forgot_password'));
	}


	function converToTz($time="",$toTz='',$fromTz='')
    {   
        // timezone by php friendly values
        $date = new DateTime($time, new DateTimeZone($fromTz));
        $date->setTimezone(new DateTimeZone($toTz));
        $time= $date->format('Y-m-d H:i:s');
        return $time;
    }


	public function activate_account($id=null){
		$this->layout = false;
		
		if($id){
			$user_id = $this->encryptor('decrypt',$id); //die;
			if($this->User->updateAll(array('User.status'=>"'Active'"),array('User.id'=>$user_id))){
				$this->Session->setFlash(__('Your account has been activated successfully, please login',true),'default',array('class'=>'success_message alert alert-success'));
			}else{
				$this->Session->setFlash(__('Your account could not activated, please try again later',true),'default',array('class'=>'error_message alert alert-error'));
			}
		}else{
			$this->Session->setFlash(__('Your account could not activated, please try again later',true),'default',array('class'=>'error_message alert alert-error'));
		}
	}


	/* Method: Admin login
	*  Description: sdmin login method
	*  created: By Pooja chaudhary on 1-june-2017
	*/

	public function login()
	{
		ini_set('post_max_size',0);
		ini_set('upload_max_filesize','500M');
		
		$this->layout = false;
		if($this->Session->check('Auth.User'))
		{
                	$this->redirect(array('action' => 'dashboard'));      
        }
		
		if($this->request->is('post'))
		{
			$user = $this->User->find('first',array('conditions'=>array('User.email'=>$this->request->data['User']['email'])));
			$pass = $user['User']['password'];
			$newpass = AuthComponent::password($this->request->data['User']['password']); 
			if(!empty($user))
			{
				if($user['User']['user_role']==1)
				{
					
					 $this->request->data['Login']['username'] = $this->request->data['User']['email'];
					 $this->request->data['Login']['password'] = $this->request->data['User']['password'];
					 
					 if($this->Auth->login())
					 {			 
					 	
					 	/*--update access logs--*/
					 	$this->User->validation=null;
					 	$arrData=array('User'=>
							 array('last_login_ip'=>$this->request->clientIp(),
							       'last_login_date'=>date('Y-m-d H:i:s')
						));
				
						$this->User->id=$user['User']['id'];
						$this->User->save($arrData,false);
						/*--update access logs--*/	
						
						$this->redirect(array('controller'=>'users','action' => 'dashboard'));	
						
					 }
					 else
					 {
					 	$this->Session->setFlash(__('Invalid username or password, please try again',true),'default',array('class'=>'alert alert-danger'));
					 }
				}
				else
				{
					$this->Session->setFlash(__('Unauthorised user, try again',true),'default',array('class'=>'alert alert-danger'));
				}
			}
			else
			{
				$this->Session->setFlash(__('Invalid username, try again',true),'default',array('class'=>'alert alert-danger'));
			}
		}
		
	}

	function forgot_password()
	{
		$this->layout = false;
		if(isset($this->request->data['User']['email']))
		{
	
		    $userData=$this->User->findByEmail($data['email']);
		    if(!empty($userData))
		    {
		         $newPwd=$this->generateRandomString(6);
			 	 $this->User->id=$userData['User']['id'];
		         $this->User->save(array('password'=>$this->Auth->password($newPwd)));

		   		/*******Send Email******/
		                     $email = $userData['User']['email'];
		                     $name = $userData['User']['firstname'];
		                     $password=$newPwd;
				    		 $from = 'info@tapesquad.com';
		            		 $to = $email;
		                     $subject = 'Forgot Password';
				     		 $content = "Hello $name,<br /><br />
							 Your password has reset.<br />
							 <strong>Your new password is</strong>: $password<br />
							 Thanks<br /><br />";

		                  
				    if($this->sendEmail($to,$from,$subject,$content))
				    {
						$this->Session->setFlash(__('New password has been sent to your email',true),'default',array('class'=>'alert alert-danger'));
				    }
				    else
				    {
				      	$this->Session->setFlash(__('Password could not be changed, please try again later.',true),'default',array('class'=>'alert alert-danger'));
					}
			}
			else
			{
			 		$this->Session->setFlash(__('Invalid email id',true),'default',array('class'=>'alert alert-danger'));
			}
		}
		 
    }



    public function logout()
    {
	    $this->Auth->logout();
        $this->redirect($this->Auth->logout());
	}


	public function chkstripe()
	{
		$this->redirect(array('action'=>'pay_now'));
	}

	public function pay_now()
	{
		$this->layout = false;
	}

	public function payments()
	{
		echo '<pre>'; print_r($this->request->data); die;
	}


	public function dashboard()
	{
		$this->set('title_for_layout','Dashboard');
		$total_revenue = $this->Payment->find('all',['fields'=>['SUM(Payment.amount) as total_revenue']]); 
		$this->set('total_revenue',$total_revenue[0][0]['total_revenue']);
		$tape_income = $this->Payment->find('all',['fields'=>['SUM(Payment.app_fee) as tape_income']]); 
		$this->set('tape_income',$tape_income[0][0]['tape_income']);
		$new_users = $this->User->find('count',array('conditions'=> array('DATE_FORMAT(User.created,"%m") = "'.date("m").'"','User.user_role'=>2)));
		$total_users = $this->User->find('count',array('conditions'=>array('User.user_role'=>2)));
		$new_readers = $this->User->find('count',array('conditions'=> array('DATE_FORMAT(User.created,"%m") = "'.date("m").'"','User.user_role'=>3)));
		$total_readers = $this->User->find('count',array('conditions'=>array('User.user_role'=>3)));
		$total_project_requests = $this->Project->find('count');
		$pending_projects = $this->Project->find('count',array('conditions'=>array('Project.status'=>2)));
		$confirmed_projects = $this->Project->find('count',array('conditions'=>array('Project.status'=>1)));
		$completed_projects = $this->Project->find('count',array('conditions'=>array('Project.status'=>6)));
		$success_rate = round((($completed_projects*100)/$total_project_requests),1);
		$usersPerMonth = $this->User->find('all',array('conditions'=>array('User.user_role'=>2),
									   'fields'=>array('MONTH(created) AS month',"COUNT('id') as total_users"),
									   'group'=>array('month'),
								        'order'=>'created ASC',
									));

		$months = $this->User->find('all',array(
									   'fields'=>array('MONTH(created) AS month'),
									   'group'=>array('month'),
								       'order'=>'created ASC',
									));

		$readersPerMonth = $this->User->find('all',array('conditions'=>array('User.user_role'=>3),
									   'fields'=>array('MONTH(created) AS month',"COUNT('id') as total_users"),
									   'group'=>array('month'),
								       'order'=>'created ASC',
									));

		if($months)
		{
			foreach($months as $month):
					if($month['0']['month']==1){
					   $month['0']['month'] = 'January';
					}
					if($month['0']['month']==2){
					   $month['0']['month'] = 'February';
					}
					if($month['0']['month']==3){
					   $month['0']['month'] = 'March';
					}
					if($month['0']['month']==4){
					   $month['0']['month'] = 'April';
					}
					if($month['0']['month']==5){
					   $month['0']['month'] = 'May';
					}
					if($month['0']['month']==6){
					   $month['0']['month'] = 'June';
					}
					if($month['0']['month']==7){
					   $month['0']['month'] = 'July';
					}
					if($month['0']['month']==8)
					{
					   $month['0']['month'] = 'August';
					}
					if($month['0']['month']==9)
					{
					   $month['0']['month'] = 'September';
					}
					if($month['0']['month']==10)
					{
					   $month['0']['month'] = 'October';
					}
					if($month['0']['month']==11)
					{
					   $month['0']['month'] = 'November';
					}
					if($month['0']['month']==12)
					{
					   $month['0']['month'] = 'December';
					}
					
					$monthArr[] = $month['0']['month'];
			endforeach;

			if(isset($monthArr) && $monthArr!="")
			{
				$this->set('months',$monthArr);
			}

		}

		if($readersPerMonth)
		{
			foreach($readersPerMonth as $reader):
				$readers[] = $reader['0']['total_users'];
			endforeach;

			if(isset($readers) && $readers!="")
			{
				$this->set('readers',$readers);
			}
		}

		if($usersPerMonth)
		{
			foreach($usersPerMonth as $user):
				$users[] = $user['0']['total_users'];
			endforeach;

			if(isset($users) && $users!="")
			{
				$this->set('users',$users);
			}
		}


		/* -------  Projects bar chart data ---------- */

		$pro_months = $this->Project->find('all',array(
									   'fields'=>array('MONTH(created) AS month'),
									   'group'=>array('month'),
								       'order'=>'created ASC',
									));

		$project_requests = $this->Project->find('all',array(
									   'fields'=>array('MONTH(created) AS month',"COUNT('id') as total_requests"),
									   'group'=>array('month'),
								       'order'=>'created ASC',
									));

		if($pro_months)
		{
			foreach($pro_months as $monthdata):
					if($monthdata['0']['month']==1){
						$monthdata['0']['month'] = 'January';
					}
					if($monthdata['0']['month']==2){
						$monthdata['0']['month'] = 'February';
					}
					if($monthdata['0']['month']==3){
						$monthdata['0']['month'] = 'March';
					}
					if($monthdata['0']['month']==4){
						$monthdata['0']['month'] = 'April';
					}
					if($monthdata['0']['month']==5){
						$monthdata['0']['month'] = 'May';
					}
					if($monthdata['0']['month']==6){
						$monthdata['0']['month'] = 'June';
					}
					if($monthdata['0']['month']==7){
						$monthdata['0']['month'] = 'July';
					}
					if($monthdata['0']['month']==8){
						$monthdata['0']['month'] = 'August';
					}
					if($monthdata['0']['month']==9){
						$monthdata['0']['month'] = 'September';
					}
					if($monthdata['0']['month']==10){
						$monthdata['0']['month'] = 'October';
					}
					if($monthdata['0']['month']==11){
						$monthdata['0']['month'] = 'November';
					}
					if($monthdata['0']['month']==12){
						$monthdata['0']['month'] = 'December';
					}
					
					$Pro_monthArr[] = $monthdata['0']['month'];
			endforeach;

			if(isset($Pro_monthArr) && $Pro_monthArr!="")
			{
				$this->set('monthsArr',$Pro_monthArr);
			}

		}

		if($project_requests)
		{
			foreach($project_requests as $project):
				$ProjectRequests[] = $project['0']['total_requests'];
			endforeach;

			if(isset($ProjectRequests) && $ProjectRequests!="")
			{
				$this->set('ProjectRequests',$ProjectRequests);
			}
		}

		/* -------  Projects bar chart data ends here ------------*/

		//echo '<pre>'; print_r($Pro_monthArr); print_r($readers); print_r($users);

		$this->set(compact('new_users', 'total_users', 'new_readers', 'total_readers','total_project_requests','pending_projects','completed_projects','success_rate'));
		
	}

	public function manage_users()
	{

		$this->loadModel('Post');
		$this->User->bindModel(array('hasMany'=>array('Post'=>array('class'=>'Post','foreignKey'=>'user_id','fields'=>array('COUNT(id) as total_posts')))));
		if(isset($this->request->query['dateStart']) && !empty($this->request->query['dateStart']) && isset($this->request->query['dateEnd']) && !empty($this->request->query['dateEnd']))
		{
		            $startDate = date('Y-m-d H:i:s',strtotime($this->request->query['dateStart']));
		            $endDate = date('Y-m-d H:i:s',strtotime($this->request->query['dateEnd']));
		            $this->set('startDate',date('m/d/Y',strtotime($startDate)));
		            $this->set('endDate',date('m/d/Y',strtotime($endDate)));
		            $users = $this->User->find('all',array('conditions'=>array('User.created >='=>$startDate,'User.created <='=>$endDate,'User.user_role '=>2)));
		}
		else{
			$users = $this->User->find('all',array('conditions'=>array('User.user_role '=>2)));
		}
		if($users)
		{
			$userArr = array();
			foreach($users as $user){
				if(isset($user['Post'][0])){
					$user['User']['total_posts'] = $user['Post'][0]['Post'][0]['total_posts'];
				}else{
					$user['User']['total_posts'] = 0;
				}
				unset($user['Post']);
				$userArr[] = $user;
			}
			$this->set('users',$userArr);
		}

	}

	public function manage_userposts($id=null)
	{
		$this->User->id = $id;
		if(!$this->User->exists())
		{
			$this->Session->setFlash(__('Invalid user',true),'default',array('class'=>'alert alert-error'));
			$this->render($this->referer());
		}
		else
		{

			$this->Post->bindModel(array('hasMany'=>array('PostComment'=>array('class'=>'PostComment','foreignKey'=>'post_id','fields'=>'COUNT(id) as total_comments'))));
			$this->User->bindModel(array('hasMany'=>array('Post'=>array('class'=>'Post','foreignKey'=>'user_id'))));
			$this->User->recursive = 2;
			$user = $this->User->findById($id);
			$postArr = array();
			if(!empty($user['Post']))
			{
				foreach($user['Post'] as $post):
					if(!empty($post['full_path']))
					{
		    			$post['full_path'] = SITE_URL.$post['full_path'];
		    		}else
		    		{
		    			$post['full_path'] = '';
		    		}
		    		if(!empty($post['thumb_path']))
		    		{
		    			$post['thumb_path'] = SITE_URL.$post['thumb_path'];
		    		}else
		    		{
		    			$post['thumb_path'] = '';
		    		}

		    		if(!empty($post['url_link']))
		    		{
		    			$post['url_link'] = $post['url_link'];
		    		}
		    		elseif(!empty($post['full_videolink']))
		    		{
		    			$post['url_link'] = SITE_URL.$post['full_videolink'];
		    		}
		    		else
		    		{
		    			$post['url_link'] = '';
		    		}

					if(isset($post['PostComment'][0]))
					{
						$post['total_comments'] = $post['PostComment'][0]['PostComment'][0]['total_comments'];
					}
					else
					{
						$post['total_comments'] = 0;
					}
					unset($post['PostComment']);
					$post['total_likes'] = $this->PostLike->find('count',array('conditions'=>array('PostLike.post_id'=>$post['id'],'PostLike.status'=>1)));
					$chkreport = $this->ReportPost->find('first',array('conditions'=>array('ReportPost.post_id'=>$post['id'],'ReportPost.report_status'=>1)));
					if($chkreport)
					{
						$post['is_reported'] = 'Yes';
						$post['reason'] = $chkreport['ReportPost']['reason'];
					}
					else
					{
						$post['is_reported'] = 'No';
					}
					$postArr[] = $post;
				endforeach;
				
			}
			$this->set('posts',$postArr);
			$this->set('user',$user['User']);
		}
	}


	public function manage_posts()
	{
		$this->Post->bindModel(array('hasMany'=>array('PostComment'=>array('class'=>'PostComment','foreignKey'=>'post_id','fields'=>'COUNT(id) as total_comments'))));
		$this->Post->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'user_id'))));
		$this->Post->recursive = 2;
		$posts = $this->Post->find('all');
			
		$postArr = array();
		if(!empty($posts))
		{
			foreach($posts as $post):

				if(!empty($post['Post']['full_path']))
				{
		    		$post['Post']['full_path'] = SITE_URL.$post['Post']['full_path'];
		    	}
		    	else
		    	{
		    		$post['Post']['full_path'] = '';
		    	}

		    	if(!empty($post['Post']['url_link']))
		    	{
	    			$post['Post']['url_link'] = $post['Post']['url_link'];
	    		}
	    		elseif(!empty($post['Post']['full_videolink']))
	    		{
	    			$post['Post']['url_link'] = SITE_URL.$post['Post']['full_videolink'];
	    		}
	    		else
	    		{
	    			$post['Post']['url_link'] = '';
	    		}

				if(isset($post['PostComment'][0]))
				{
					$post['Post']['total_comments'] = $post['PostComment'][0]['PostComment'][0]['total_comments'];
				}
				else
				{
					$post['Post']['total_comments'] = 0;
				}

				unset($post['PostComment']);
				$post['Post']['total_likes'] = $this->PostLike->find('count',array('conditions'=>array('PostLike.post_id'=>$post['Post']['id'],'PostLike.status'=>1)));
				$chkreport = $this->ReportPost->find('first',array('conditions'=>array('ReportPost.post_id'=>$post['Post']['id'],'ReportPost.report_status'=>1)));
				if($chkreport)
				{
					$post['Post']['is_reported'] = 'Yes';
					$post['Post']['reason'] = $chkreport['ReportPost']['reason'];
				}
				else
				{
					$post['Post']['is_reported'] = 'No';
				}
				$postArr[] = $post;

			endforeach;
		}
		$this->set('posts',$postArr);
	}


	public function chkcard()
	{
		\Stripe\Stripe::setApiKey("sk_test_xpYOfExQDR85H8mA7OXx083M");
		$customer = \Stripe\Customer::retrieve('cus_BZDTzU3fxJojv1');
		echo '<pre>'; print_r($customer); die;
	}


	public function manage_readers()
	{
		if(isset($this->request->query['dateStart']) && !empty($this->request->query['dateStart']) && isset($this->request->query['dateEnd']) && !empty($this->request->query['dateEnd']))
		{
		            $startDate = date('Y-m-d H:i:s',strtotime($this->request->query['dateStart']));
		            $endDate = date('Y-m-d H:i:s',strtotime($this->request->query['dateEnd']));
		            $this->set('startDate',date('m/d/Y',strtotime($startDate)));
		            $this->set('endDate',date('m/d/Y',strtotime($endDate)));
		            $users = $this->User->find('all',array('conditions'=>array('User.created >='=>$startDate,'User.created <='=>$endDate,'User.user_role '=>3)));
		}
		else{
			$users = $this->User->find('all',array('conditions'=>array('User.user_role '=>3)));
		}

		if($users)
		{
			$this->set('users',$users);
		}
	}

	public function manage_rewards()
	{
		$users = $this->User->find('all',array('conditions'=>array('User.user_role '=>3,'User.reward_noti_count'=>5)));
		if($users)
		{
			//echo '<pre>'; print_r($users);
			$userArr = array();
			foreach($users as $user):
				$userId = $user['User']['id'];
				$promo = $user['User']['promo_code'];
				$chkprojects = $this->Project->find('all',array('conditions'=>array('Project.reader_id'=>$userId,'Project.status'=>6,'Project.call_status'=>2,
																					'Project.payment_status'=>1,'Project.is_dispute'=>0,'Project.promo_code'=>$promo)));
				if($chkprojects)
				{
					//$size = sizeof($chkprojects);
					//if($size==5)
					//{
						$user['Projects'] = $chkprojects;
						$userArr[] = $user;
					//}
				}
			endforeach;
			//echo '<pre>'; print_r($userArr);
			$this->set('users',$userArr);
		}
		//die;
	}

	public function projects_list($id=null)
	{
		$this->loadModel('Project');
		$this->loadModel('User');
		if($id)
		{
			$reader = $this->User->findById($id);
			if($reader)
			{
				$promo = $reader['User']['promo_code'];

				$projects = $this->Project->find('all',array('conditions'=>array('Project.reader_id'=>$id,'Project.status'=>6,'Project.call_status'=>2,
																					'Project.payment_status'=>1,'Project.is_dispute'=>0,'Project.promo_code'=>$promo),
															'fields'=>['project_name','reader_id','client_id','amount','duration','schedule_time','status']));
				if($projects)
				{
					$projectArr = [];
					foreach($projects as $project):
						
						$user = $this->User->find('first',array('conditions'=>array('User.id'=>$project['Project']['client_id']),'fields'=>['id','firstname','lastname','email']));

						$project['User'] = $user['User'];
						$projectArr[] = $project;

					endforeach;
					$this->set('projects',$projectArr);
					//echo '<pre>'; print_r($projectArr);
				}

			}
			else
			{
				$this->Session->setFlash(__('Invalid User',true),'default',array('class'=>'alert alert-danger'));
				$this->redirect($this->referer());
			}
		}
	}

	public function change_status($id=null,$status=null)
	{
		if($id)
		{
			$this->User->id = $id;
			$this->User->saveField('status',$status);
			$this->redirect('view_user/'.$id);
		}
	}

	public function ajax_update_userstatus()
	{
		$this->layout = 'ajax';
		$data = $this->request->data;
		$this->User->id = $data['user_id'];
		$id = $data['user_id'];
		if(isset($data['govtid_status']))
		{
			$govtid_status = $data['govtid_status'];

			if($this->User->updateAll(array('User.status'=>"'".$data['status']."'",'User.govt_id_status'=>"'".$govtid_status."'"),array('User.id'=>$id)))
			{
				if($govtid_status=='Approved')
				{
					$user = $this->User->findById($id);
					$firstname = ucfirst($user['User']['firstname']);
					$first = substr($firstname,0,1);
					$lastname = ucfirst($user['User']['lastname']);
					$last = substr($lastname,0,1);
					$promocode = $first.$last.'TAPE10';
					$this->User->id = $id;
					$this->User->saveField('promo_code',$promocode);
				}
				echo 'success';
			}
			else
			{
				echo 'User status could not updated';
			}
		}
		else
		{
			if($this->User->updateAll(array('User.status'=>"'".$data['status']."'"),array('User.id'=>$id)))
			{
				echo 'success';
			}
			else
			{
				echo 'User status could not updated';
			}
		}
		die;
	}

	public function ajax_update_rewardstatus()
	{
		$this->layout = 'ajax';
		$data = $this->request->data;
		$this->User->id = $data['user_id'];
		$id = $data['user_id'];
		if($this->User->updateAll(array('User.reward_status'=>"'".$data['status']."'"),array('User.id'=>$id)))
			{
				echo 'success';
			}
			else
			{
				echo 'Status could not updated';
			}
		
		die;
	}

	public function ajax_update_poststatus()
	{
		$this->layout = 'ajax';
		$this->loadModel('Post');
		$data = $this->request->data;
		$this->Post->id = $data['post_id'];
		$id = $data['post_id'];
		if($this->Post->updateAll(array('Post.status'=>"'".$data['status']."'"),array('Post.id'=>$id)))
			{
				echo 'success';
			}
			else
			{
				echo 'Status could not updated';
			}
		
		die;
	}

	
	public function view_user($id=null)
	{
		if($id)
		{
			$this->User->id = $id;
			if(!$this->User->exists())
			{
				$this->Session->setFlash(__('Invalid User',true),'default',array('class'=>'alert alert-danger'));
				$this->render('manage_users');
			}
			else
			{
				$this->loadModel('AdminNotification');
				$this->AdminNotification->updateAll(array('status'=>1),array('AdminNotification.user_id'=>$id));
				$user = $this->User->findById($id);
				if($user)
				{
					$this->set('user',$user);
				}
				if(isset($this->request->query['dateStart']) && !empty($this->request->query['dateStart']) && isset($this->request->query['dateEnd']) && !empty($this->request->query['dateEnd']))
				{
		            $startDate = date('Y-m-d H:i:s',strtotime($this->request->query['dateStart']));
		            $endDate = date('Y-m-d H:i:s',strtotime($this->request->query['dateEnd']));
		            $this->set('startDate',date('m/d/Y',strtotime($startDate)));
		            $this->set('endDate',date('m/d/Y',strtotime($endDate)));
		            
		            $this->Project->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'reader_id'))));
					$projects = $this->Project->find('all',array('conditions'=>array('Project.created >='=>$startDate,'Project.created <='=>$endDate,'Project.client_id'=>$id),'order'=>'Project.id DESC'));
		        }
		        else
		        {
		        	$this->Project->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'reader_id'))));
					$projects = $this->Project->find('all',array('conditions'=>array('Project.client_id'=>$id),'order'=>'Project.id DESC'));
		        }
				$this->set('projects',$projects);
			}
		}
	}

	public function view_reader($id=null)
	{
		$this->loadModel('User');
		if($id)
		{
			$this->User->id = $id;
			if(!$this->User->exists())
			{
				$this->Session->setFlash(__('Invalid User',true),'default',array('class'=>'alert alert-danger'));
				$this->render('manage_readers');
			}
			else
			{
				$user = $this->User->findById($id);
				if($user)
				{
					$this->set('user',$user);
				}
				if(isset($this->request->query['dateStart']) && !empty($this->request->query['dateStart']) && isset($this->request->query['dateEnd']) && !empty($this->request->query['dateEnd']))
				{
		            $startDate = date('Y-m-d H:i:s',strtotime($this->request->query['dateStart']));
		            $endDate = date('Y-m-d H:i:s',strtotime($this->request->query['dateEnd']));
		            $this->set('startDate',date('m/d/Y',strtotime($startDate)));
		            $this->set('endDate',date('m/d/Y',strtotime($endDate)));
		            
		            $this->Project->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'client_id'))));
					$projects = $this->Project->find('all',array('conditions'=>array('Project.created >='=>$startDate,'Project.created <='=>$endDate,'Project.reader_id'=>$id),'order'=>'Project.id DESC'));
		        }
		        else
		        {
		        	$this->Project->bindModel(array('belongsTo'=>array('User'=>array('class'=>'User','foreignKey'=>'client_id'))));
					$projects = $this->Project->find('all',array('conditions'=>array('Project.reader_id'=>$id),'order'=>'Project.id DESC'));
		        }

		        $this->set('projects',$projects);
			}
			
		}
	}

	public function delete_user($id=null)
	{
		
		if($id)
		{
			$this->User->id = $id;
			if(!$this->User->exists())
			{
				$this->Session->setFlash(__('User not found, Invalid Id',true),'default',array('class'=>'alert alert-danger'));
				$this->redirect($this->referer());
			}
			else
			{
				if($this->User->delete($id))
				{
					$this->Session->setFlash(__('Record deleted successfully',true),'default',array('class'=>'alert alert-success'));
					$this->redirect($this->referer());
				}
				else
				{
					$this->Session->setFlash(__('Record could not deleted, please try again later',true),'default',array('class'=>'alert alert-danger'));
					$this->redirect($this->referer());
				}
			}
		}
	}


	public function manage_projects()
	{
		$projects = $this->Project->find('all');
		if($projects)
		{
			foreach ($projects as $project) 
			{
				$reader = $this->User->find('first',array('conditions'=>array('User.id'=>$project['Project']['reader_id']),'fields'=>array('id','firstname','lastname','email')));
				$client = $this->User->find('first',array('conditions'=>array('User.id'=>$project['Project']['client_id']),'fields'=>array('id','firstname','lastname','email')));
				$project['Reader'] = $reader['User'];
				$project['Client'] = $client['User'];

				$projectArr[] = $project;
			}

			$this->set('projects',$projectArr);
		}
	}


	public function add_faq($topicid=null)
	{
		$this->loadModel('Faq');
		$this->loadModel('FaqTopic');
		if(!empty($this->request->data))
		{
			//echo '<pre>'; print_r($this->request->data);
			if($topicid)
			{
				$this->FaqTopic->id = $topicid;
				if(!$this->FaqTopic->exists())
				{
					$this->Session->setFlash(__('Invalid topic id',true),'default',array('class'=>'alert alert-danger'));
					$this->redirect('manage_faq_topics');	
				}
				else
				{
					$this->request->data['Faq']['topic_id'] = $topicid;
					$this->request->data['Faq']['created'] = date('Y-m-d');
					if($this->Faq->save($this->request->data))
					{
						$this->Session->setFlash(__('Question added successfully',true),'default',array('class'=>'alert alert-success'));
						$this->redirect('manage_faqs/'.$topicid);	
					}
					else
					{
						$this->Session->setFlash(__('Question could not added',true),'default',array('class'=>'alert alert-danger'));
						$this->redirect('manage_faqs/'.$topicid);	
					}
				}
			}
		}

		$this->set('title','Add');
	
	}


	public function edit_faq($id=null)
	{
		$this->loadModel('Faq');
		
		if(!empty($this->request->data))
		{
			if($id)
			{
				$this->Faq->id = $id;
				if(!$this->Faq->exists())
				{
					$this->Session->setFlash(__('Invalid question id',true),'default',array('class'=>'alert alert-danger'));
					$this->redirect('manage_reader_faqs');	
				}
				else
				{
					$this->request->data['Faq']['modified'] = date('Y-m-d');
					$topicid = $this->request->data['Faq']['topic_id'];
					if($this->Faq->save($this->request->data))
					{
						$this->Session->setFlash(__('Question updated successfully',true),'default',array('class'=>'alert alert-success'));
						$this->redirect('manage_faqs/'.$topicid);
					}
					else
					{
						$this->Session->setFlash(__('Question could not updated',true),'default',array('class'=>'alert alert-danger'));
						$this->redirect('manage_faqs/'.$topicid);
					}
				}
			}
		}
		$this->request->data = $this->Faq->findById($id);
		$this->set('title','Edit');
		$this->render('add_faq');
	}


	public function add_faq_topic($id=null)
	{

		$this->loadModel('FaqTopic');
		if(!empty($this->request->data))
		{
			//echo '<pre>'; print_r($this->request->data); die;
			if($id){
				$this->FaqTopic->id = $id;
				if(!$this->FaqTopic->exists())
				{
					$this->Session->setFlash(__('Invalid topic id',true),'default',array('class'=>'alert alert-danger'));
					$this->redirect('manage_faq_topics');	
				}
				else
				{
					$this->request->data['FaqTopic']['modified'] = date('Y-m-d');
					if($this->FaqTopic->save($this->request->data))
					{
						$this->Session->setFlash(__('FaqTopic updated successfully',true),'default',array('class'=>'alert alert-success'));
						$this->redirect('manage_faq_topics');	
					}
					else
					{
						$this->Session->setFlash(__('FaqTopic could not updated',true),'default',array('class'=>'alert alert-danger'));
						$this->redirect('manage_faq_topics');	
					}
				}
			}
			else
			{
				$this->request->data['FaqTopic']['created'] = date('Y-m-d H:i:s');
				$user_role = $this->request->data['FaqTopic']['user_role'];
				$topic = $this->request->data['FaqTopic']['topic'];
				$chktopic = $this->FaqTopic->find('first',array('conditions'=>array('FaqTopic.user_role'=>$user_role,'FaqTopic.topic'=>$topic)));
				if($chktopic)
				{
					$this->Session->setFlash(__('Topic already exists',true),'default',array('class'=>'alert alert-danger'));
					$this->redirect('add_faq_topic');	
				}
				$this->FaqTopic->create();
				if($this->FaqTopic->save($this->request->data))
				{
					$this->Session->setFlash(__('FaqTopic submitted successfully',true),'default',array('class'=>'alert alert-success'));
					$this->redirect('manage_faq_topics');	
				}
				else
				{
					$this->Session->setFlash(__('FaqTopic could not submitted, please try again later',true),'default',array('class'=>'alert alert-danger'));
					$this->redirect('manage_faq_topics');	
				}
			}
		}

		if($id)
		{
			$this->request->data = $this->FaqTopic->findById($id);
			$this->set('title','Edit');
		}
		else
		{
			$this->set('title','Add');
		}

	}

	public function manage_faqs($id)
	{
		if($id)
		{
			$this->loadModel('Faq');
			$faqs = $this->Faq->find('all',array('conditions'=>array('Faq.topic_id'=>$id)));
			if($faqs)
			{
				$this->set('faqs',$faqs);
			}
		}
	}

	public function manage_faq_topics()
	{

		$this->loadModel('FaqTopic');
		$faqs = $this->FaqTopic->find('all');
		if($faqs)
		{
			$this->set('faqs',$faqs);
		}
	}

	public function delete_faq_topic($id=null)
	{

		$this->loadModel('FaqTopic');
		if($id)
		{
			$this->FaqTopic->id = $id;
			if(!$this->FaqTopic->exists())
			{
				$this->Session->setFlash(__('Invalid topic id', true),'default',array('class'=>'alert alert-danger'));
				$this->redirect('manage_faq_topics');
			}
			else
			{
				
				if($this->FaqTopic->delete())
				{
					$this->Session->setFlash(__('Topic deleted successfully', true),'default',array('class'=>'alert alert-success'));
					$this->redirect('manage_faq_topics');
				}
				else
				{
					$this->Session->setFlash(__('Topic could not deleted', true),'default',array('class'=>'alert alert-danger'));
					$this->redirect('manage_faq_topics');
				}
			}
		}

	}

	public function delete_faq($id=null,$topicid=null)
	{

		$this->loadModel('Faq');
		if($id)
		{
			$this->Faq->id = $id;
			if(!$this->Faq->exists())
			{
				$this->Session->setFlash(__('Invalid question id', true),'default',array('class'=>'alert alert-danger'));
				$this->redirect('manage_faqs/'.$topicid);
			}
			else
			{
				
				if($this->Faq->delete())
				{
					$this->Session->setFlash(__('Question deleted successfully', true),'default',array('class'=>'alert alert-success'));
					$this->redirect('manage_faqs/'.$topicid);
				}
				else
				{
					$this->Session->setFlash(__('Question could not deleted', true),'default',array('class'=>'alert alert-danger'));
					$this->redirect('manage_faqs/'.$topicid);
				}
			}
		}

	}

	public function add_cms($id=null)
	{

		$this->loadModel('Content');
		if(!empty($this->request->data))
		{
			if($id)
			{
				$this->Content->id = $id;
				if(!$this->Content->exists())
				{
					$this->Session->setFlash(__('Invalid content id',true),'default',array('class'=>array('alert alert-danger')));
					$this->redirect('manage_cms');
				}
				else
				{
					$this->request->data['Content']['modified'] = date('Y-m-d H:i:s');
					if($this->Content->save($this->request->data))
					{
						$this->Session->setFlash(__('Cms content updated successfully',true),'default',array('alert alert-success'));
						$this->redirect('manage_cms');
					}
					else
					{
						$this->Session->setFlash(__('Content could not updated',true),'default',array('class'=>'alert alert-danger'));
						$this->redirect('manage_cms');
					}
				}
			}
			else
			{
				$this->Content->create();
				$this->request->data['Content']['created'] = date('Y-m-d H:i:s');
				if($this->Content->save($this->request->data))
				{
						$this->Session->setFlash(__('Cms content saved successfully',true),'default',array('alert alert-success'));
						$this->redirect('manage_cms');
				}
				else
				{
						$this->Session->setFlash(__('Content could not saved, please try again later',true),'default',array('class'=>'alert alert-danger'));
						$this->redirect('manage_cms');
				}
			}
		}
		if($id)
		{
			$this->set('title','Edit');
			$this->request->data = $this->Content->findById($id);
		}
		else
		{
			$this->set('title','Add');
		}	

	}

	public function manage_cms()
	{

		$this->loadModel('Content');
		$contents = $this->Content->find('all');
		if($contents)
		{
			$this->set('contents',$contents);
		}
	}

	public function delete_content($id=null)
	{

		$this->loadModel('Content');
		if($id)
		{
			$this->Content->id = $id;
			if(!$this->Content->exists())
			{
				$this->Session->setFlash(__('Invalid content id',true),'default',array('class'=>'alert alert-danger'));
				$this->redirect('manage_cms');
			}
			else
			{
				if($this->Content->delete($id))
				{
					$this->Session->setFlash(__('Record deleted successfully',true),'default',array('class'=>'alert alert-success'));
					$this->redirect('manage_cms');
				}
				else
				{
					$this->Session->setFlash(__('Record could not deleted , please try again',true),'default',array('class'=>'alert alert-danger'));
					$this->redirect('manage_cms');
				}
			}
		}

	}

	public function add_template($id=null)
	{

		$this->loadModel('EmailTemplate');
		if(!empty($this->request->data)){
			if($id)
			{
				$this->EmailTemplate->id = $id;
				if(!$this->EmailTemplate->exists())
				{
					$this->Session->setFlash(__('Invalid template id',true),'default',array('class'=>'alert alert-danger'));
					$this->redirect('manage_templates');
				}
				else
				{
					$this->request->data['EmailTemplate']['modified'] = date('Y-m-d H:i:s');
					if($this->EmailTemplate->save($this->request->data))
					{
						$this->Session->setFlash(__('Template updated successfully',true),'default',array('class'=>'alert alert-success'));
						$this->redirect('manage_templates');
					}
					else
					{
						$this->Session->setFlash(__('Template could not updated',true),'default',array('class'=>'alert alert-danger'));
						$this->redirect('manage_templates');
					}
				}
			}
			else
			{
				$this->request->data['EmailTemplate']['created'] = date('Y-m-d H:i:s');
				if($this->EmailTemplate->save($this->request->data))
				{
					$this->Session->setFlash(__('Template added successfully',true),'default',array('class'=>'alert alert-success'));
					$this->redirect('manage_templates');
				}
				else
				{
					$this->Session->setFlash(__('Template could not saved, please try again',true),'default',array('class'=>'alert alert-danger'));
					$this->redirect('manage_templates');
				}
			}
		}

		if($id)
		{
			$this->request->data = $this->EmailTemplate->findById($id);
			$this->set('title','Edit');
		}
		else
		{
			$this->set('title','Add');
		}

	}

	public function manage_templates()
	{

		$this->loadModel('EmailTemplate');
		$templates = $this->EmailTemplate->find('all');
		$this->set('templates',$templates);
	}

	public function reported_posts()
	{

		$this->loadModel('ReportPost');
		$this->loadModel('User');
		$this->loadModel('Post');
		$this->ReportPost->recursive = 2;

		$this->Post->bindModel(array('belongsTo'=>['User'=>['class'=>'User','foreignKey'=>'user_id','fields'=>['id','firstname','lastname','profile_name','email']]]));
		$this->ReportPost->bindModel(array('belongsTo'=>['Post'=>['class'=>'Post','foreignKey'=>'post_id']]));
		$this->ReportPost->bindModel(array('belongsTo'=>['User'=>['class'=>'User','foreignKey'=>'user_id','fields'=>['id','firstname','lastname','profile_name','email']]]));
		//$posts = $this->ReportPost->find('all',['group'=>['ReportPost.post_id'],'order'=>['ReportPost.id'=>'DESC']]);
		$posts = $this->ReportPost->find('all',['order'=>['ReportPost.id'=>'DESC']]);
		if(isset($posts))
		{
			//echo '<pre>'; print_r($posts);
			$this->set('posts',$posts);
		}
	}

	public function delete_template($id=null)
	{

		$this->loadModel('EmailTemplate');
		if($id)
		{
			$this->EmailTemplate->id = $id;
			if(!$this->EmailTemplate->exists())
			{
				$this->Session->setFlash(__('Invalid template id',true),'default',array('class'=>'alert alert-danger'));
				$this->redirect('manage_templates');
			}
			else
			{
				if($this->EmailTemplate->delete($id))
				{
					$this->Session->setFlash(__('Template deleted successfully',true),'default',array('class'=>'alert alert-success'));
					$this->redirect('manage_templates');
				}
				else
				{
					$this->Session->setFlash(__('Template could not deleted , please try again',true),'default',array('class'=>'alert alert-danger'));
					$this->redirect('manage_templates');
				}
			}
		}
	}

	public function manage_disputes()
	{

		$this->loadModel('Dispute');
		$this->loadModel('Project');
		$this->loadModel('User');
		$this->Dispute->bindModel(array('belongsTo'=>array('Project'=>array('class'=>'Project','foreignKey'=>'project_id','fields'=>array('id','project_name','schedule_time','reader_id','client_id')))));
		$disputes = $this->Dispute->find('all');
		if($disputes)
		{
			$disArr = array();
			foreach ($disputes as $dispute) 
			{
				$reader = $this->User->find('first',array('conditions'=>array('User.id'=>$dispute['Project']['reader_id']),'fields'=>array('id','firstname','lastname','email')));
				$client = $this->User->find('first',array('conditions'=>array('User.id'=>$dispute['Project']['client_id']),'fields'=>array('id','firstname','lastname','email')));
				$dispute['Reader'] = $reader['User'];
				$dispute['Client'] = $client['User'];
				$disArr[] = $dispute;
			}
			$this->set('disputes',$disArr);
		}
	}

	public function manage_payments()
	{

		$this->loadModel('Payment');
		$this->loadModel('Project');
		$this->loadModel('User');
		$total_revenue = $this->Payment->find('all',['fields'=>['SUM(Payment.amount) as total_revenue']]); 
		$this->set('total_revenue',$total_revenue[0][0]['total_revenue']);
		$tape_income = $this->Payment->find('all',['fields'=>['SUM(Payment.app_fee) as tape_income']]); 
		$this->set('tape_income',$tape_income[0][0]['tape_income']);
		$this->Payment->bindModel(array('belongsTo'=>array('Project'=>array('class'=>'Project','foreignKey'=>'project_id','fields'=>array('id','reader_id','client_id','project_name')))));
		$payments = $this->Payment->find('all',['order'=>['Payment.id'=>'DESC']]);
		if($payments)
		{
			//echo '<pre>'; print_r($payments); 
			$paymentArr = array();
			foreach($payments as $payment):

				$reader_id = $payment['Project']['reader_id'];
				$client_id = $payment['Project']['client_id'];
				$reader = $this->User->find('first',array('conditions'=>array('User.id'=>$reader_id),'fields'=>array('id','firstname','lastname')));
				$client = $this->User->find('first',array('conditions'=>array('User.id'=>$client_id),'fields'=>array('id','firstname','lastname')));
				$payment['Reader'] = $reader['User'];
				$payment['Client'] = $client['User'];
				$paymentArr[] = $payment;
			endforeach;
			$this->set('payments',$paymentArr);
		}
	}

	
	public function view_project($id=null)
	{
		$this->loadModel('Project');
		if($id)
		{
			$project = $this->Project->findById($id);
			$this->set('project',$project);
		}
	}

	public function delete_dispute($id=null)
	{
		
		$this->loadModel('Dispute');
		if($id)
		{
			$this->Dispute->id = $id;
			if(!$this->Dispute->exists())
			{
				$this->Session->setFlash(__('Dispute not found, Invalid Id',true),'default',array('class'=>'alert alert-danger'));
				$this->redirect($this->referer());
			}
			else
			{
				if($this->Dispute->delete($id))
				{
					$this->Session->setFlash(__('Record deleted successfully',true),'default',array('class'=>'alert alert-success'));
					$this->redirect($this->referer());
				}
				else
				{
					$this->Session->setFlash(__('Record could not deleted, please try again later',true),'default',array('class'=>'alert alert-danger'));
					$this->redirect($this->referer());
				}
			}
		}
	}

	
	public function ajax_update_disputestatus()
	{

		$this->layout = 'ajax';
		$this->loadModel('Dispute');
		$this->loadModel('Project');
		$this->loadModel('User');
		$data = $this->request->data;
		
		$id = $data['dispute_id'];
		if(isset($data['status']))
		{
			$status = $data['status'];
			$this->Dispute->id = $id;
			if($this->Dispute->saveField('status',$status))
			{
				if($status==4)
				{
					$dispute = $this->Dispute->findById($id);
					$project = $this->Project->findById($dispute['Dispute']['project_id']);
					$reader = $this->User->findById($project['Project']['reader_id']);
					$promo_count = $reader['User']['promo_count']-1;
					$this->User->id = $reader['User']['id'];
					$this->User->saveField('promo_count',$promo_count);
				}
				echo 'success';
			}
			else
			{
				echo 'Status could not updated';
			}
		}
		else
		{
			echo 'Please provide status';
		}
		die;
	}


	public function getPayment()
	{

		$this->layout = 'ajax';
		$this->loadModel('Dispute');
		$this->loadModel('Payment');
		$this->loadModel('StripeCharge');
		$data = $this->request->data;

		$id = $data['dispute_id'];
		$dispute = $this->Dispute->findById($id);
		if($dispute)
		{
			$project_id = $dispute['Dispute']['project_id'];
			$getproject = $this->Project->findById($project_id);
			$reader_id = $getproject['Project']['reader_id'];
			$client_id = $getproject['Project']['client_id'];
			$chargedata = $this->StripeCharge->find('first',array('conditions'=>array('StripeCharge.project_id'=>$project_id)));
			if($chargedata)
			{
						$charge_id = $chargedata['StripeCharge']['charge_id'];
						$result = $this->Stripe->charge_capture($charge_id);
						if($result['status']=='succeeded')
						{

							$this->Dispute->id = $id;
							$this->Dispute->saveField('status',5);
							$this->StripeCharge->id = $chargedata['StripeCharge']['id'];
							$this->StripeCharge->saveField('charge_status','captured');
							$this->Project->id = $project_id;
							$this->Project->saveField('payment_status',1);
							$invoice = '#invoice'.$id;

							$paymentdata = array(
								'Payment' => array(
									'project_id' 	=> $id,
									'reader_id'	 	=> $reader_id,
									'client_id'	 	=> $client_id,
									'charge_id'	 	=> $result['id'],
									'invoice'	 	=> $result['description'],
									'amount'	 	=> $chargedata['amount'],
									'reader_amount'	=> $chargedata['reader_amount'],
									'app_fee'	 	=> $chargedata['app_fee'],
									'bal_transaction' => $result['balance_transaction'],
									'payment_created' => date('Y-m-d H:i:s',strtotime($result['created'])),
									'status'		=>  $result['status'],
									'transfer'		=>  $result['transfer'],
									'response_data' => json_encode($result)
									)
								);

							if($this->Payment->save($paymentdata))
							{
								echo 'success';
							}
						}
						else
						{
							echo $result['message'];
						}
			}
			else
			{
				echo 'Charge id not found for this Project';
			}
		}
		else
		{
			echo 'Please provide valid id for dispute';
		}
		die;
	}


	public function makeRefund()
	{

		$this->layout = 'ajax';
		$this->loadModel('Dispute');
		$this->loadModel('Refund');
		$this->loadModel('StripeCharge');
		$data = $this->request->data;

		$id = $data['dispute_id'];
		$dispute = $this->Dispute->findById($id);
		if($dispute)
		{
			$project_id = $dispute['Dispute']['project_id'];
			$chargedata = $this->StripeCharge->find('first',array('conditions'=>array('StripeCharge.project_id'=>$project_id)));
			if($chargedata)
			{
						$charge_id = $chargedata['StripeCharge']['charge_id'];
		    			$result = $this->Stripe->create_refund($charge_id);
		    			if($result['status']==400)
		    			{
		    				echo $failed_reason = $result['message'];
		    			}
		    			else
		    			{

		    				$this->StripeCharge->id = $chargedata['StripeCharge']['id'];
		    				$this->StripeCharge->saveField('charge_status','refunded');
							$this->Dispute->id = $id;
							$this->Dispute->saveField('status',4);


		    				$refundArr = array(
		    					'Refund' => array(
		    						'project_id' => $id,
		    						'refund_id'	 => $result['id'],
		    						'charge_id'	 => $result['charge'],
		    						'amount'	 => $result['amount'],
		    						'status'	 => $result['status'],
		    						'currency'	 => $result['currency'],
		    						'receipt_no' => $result['receipt_no'],
		    						'failed_reason' => $result['reason']
		    						)
		    					);
		    				
							$this->Project->id = $id;
		    				if($this->Project->saveField('status',7))
							{
								echo 'success';
							}
		    			}
			}
			else
			{
				echo 'Charge id not found for this Project';
			}
			
		}
		else
		{
			echo 'Please provide valid id for dispute';
		}
		die;
	}

	/*========== Cron Job function to chk Request status ==============*/
	/* 1=confirmed,2=waiting,3=declined,4=progress,5=dissolve,6=completed,7=cancelled,8=Refunded */
	public function chkRequestStatus()
	{

			$this->layout = false;
			$nowRequests = $this->Project->find('all',array('conditions'=>array('Project.tape_type'=>2,'Project.status'=>2)));
			if($nowRequests)
			{
				foreach ($nowRequests as $request)
				{
			
					$now = date('Y-m-d H:i:s');
					$created = $request['Project']['created'];
					$diff=strtotime($now)-strtotime($created);
					$temp=$diff/86400;
					$days=floor($temp); $temp=24*($temp-$days);
					$hours=floor($temp); $temp=60*($temp-$hours);
					$minutes=floor($temp); $temp=60*($temp-$minutes);

					//total minutes
					echo $total_time = $hours+$minutes;
					
						if($total_time>=2)
						{
							$id = $request['Project']['id'];
							$this->Project->id = $id;
							if($this->Project->saveField('status',5))
							{
								echo 'success';
							}
						}
				}
			}

			$scheduleRequests = $this->Project->find('all',array('conditions'=>array('Project.tape_type'=>1,'Project.status'=>2)));
			if($scheduleRequests)
			{
				foreach ($scheduleRequests as $srq)
				{
			
					$now = date('Y-m-d H:i:s');
					$created = $srq['Project']['created'];
					$diff=strtotime($now)-strtotime($created);
					$temp=$diff/86400;
					$days=floor($temp); $temp=24*($temp-$days);
					$hours=floor($temp); $temp=60*($temp-$hours);
					$minutes=floor($temp); $temp=60*($temp-$minutes);

					//total minutes
					echo $total_time = $hours+$minutes;
						if($total_time>=22)
						{
							$id = $srq['Project']['id'];
							$this->Project->id = $id;
							if($this->Project->saveField('status',5))
							{
								echo 'success';
							}
						}
				}
			}

			die;
	}


	
	/*========== Cron Job function to chk call status =================*/

	public function chkRequestCallStatus()
	{

		//echo $data = date('Y-m-d H:i:s',strtotime("+30 minutes"));
		$this->loadModel('StripeCharge');
		$this->loadModel('Project');
		$this->loadModel('User');
		$this->layout = false;
		$today = date('Y-m-d H:i:s');
		$requests = $this->Project->find('all',array('conditions'=>array('Project.status IN'=>array(1,4),'Project.call_status'=>0,'Project.created'=>date('Y-m-d'))));
		if($requests)
		{
			foreach($requests as $request)
			{
				$id = $request['Project']['id'];
				$schedule_time = $request['Project']['schedule_time'];
				$duration = $request['Project']['duration'];
				$schedule_time = date('Y-m-d H:i:s',strtotime("+".$duration." minutes", strtotime($schedule_time)));
			 
				if($today>$schedule_time)
				{
					$chargedata = $this->StripeCharge->find('first',array('conditions'=>array('StripeCharge.project_id'=>$id)));
		    		if($chargedata)
		    		{
		    			$charge_id = $chargedata['StripeCharge']['charge_id'];
		    			$result = $this->Stripe->create_refund($charge_id);
		    			if($result['status']==400)
		    			{
		    				echo $failed_reason = $result['message'];
		    			}
		    			else
		    			{
							$this->StripeCharge->id = $chargedata['StripeCharge']['id'];
		    				$this->StripeCharge->saveField('charge_status','refunded');

		    				$refundArr = array(
		    					'Refund' => array(
		    						'project_id' => $id,
		    						'refund_id'	 => $result['id'],
		    						'charge_id'	 => $result['charge'],
		    						'amount'	 => $result['amount'],
		    						'status'	 => $result['status'],
		    						'currency'	 => $result['currency'],
		    						'receipt_no' => $result['receipt_no'],
		    						'failed_reason' => $result['reason']
		    						)
		    					);

		    				$payment_status = 'Refund success';
							$this->Project->id = $id;
		    				if($this->Project->saveField('status',8))
							{
								echo $payment_status;
							}
		    			}
		    		}
		    		else
		    		{
		    			$this->Project->id = $id;
		    			if($this->Project->saveField('status',7))
						{
							
							echo 'Refund successfully';
						}
		    		}
				}
			}
		}

		$requestdata = $this->Project->find('all',array('conditions'=>array('Project.status'=>4,'Project.call_status'=>1)));
		if($requestdata)
		{
			$this->loadModel('Refund');
			foreach ($requestdata as $rqst)
			{
				$id = $rqst['Project']['id'];
				$is_refund = $rqst['Project']['is_refund'];
				$schedule_time = $rqst['Project']['schedule_time'];
				$duration = $rqst['Project']['duration'];
				$schedule_time = date('Y-m-d H:i:s',strtotime("+".$duration." minutes", strtotime($schedule_time)));
				if($today>$schedule_time)
				{
					$this->Project->id = $id;
					if($is_refund==0)
					{
						if($this->Project->saveField('status',6))
						{
							echo 'status updated as completed';
						}
					}
					elseif($is_refund==1)
					{
						if($this->Project->saveField('status',8))
						{
							
							echo 'status updated as Refunded';
						}
					}

				}
			}
		}

		$completedRequests = $this->Project->find('all',array('conditions'=>array('Project.status'=>4,'Project.call_status'=>2)));
		if($completedRequests)
		{
			foreach ($completedRequests as $cmprqst)
			{
					$id = $cmprqst['Project']['id'];
					$this->Project->id = $id;
					if($this->Project->saveField('status',6))
					{
						echo 'status updated as completed';
					}
			}
		}

		die;
	}

	/*============ Cron job for updating archieve info =====================*/

	public function archive_info()
	{

			$this->loadModel('Archieve');
    		$archieves = $this->Archieve->find('all',array('conditions'=>array('OR'=>array('Archieve.file_type'=>'webm','Archieve.duration'=>0))));
    		if($archieves)
    		{
    			foreach ($archieves as $arch):
    				$archiveId = $arch['Archieve']['archieve_id'];
    				$project_id = $arch['Archieve']['project_id'];
	    			$path = 'https://api.opentok.com/v2/project/46069462/archive/'.$archiveId;
		    		$result = $this->restApi($path);

		            if($result)
		            {

					    $result = json_decode($result);
					   	$this->request->data['Archieve']['name'] 		= $result->name;
                        $this->request->data['Archieve']['size']    	= $result->size;
                        $this->request->data['Archieve']['mode'] 		= $result->outputMode;
                        $this->request->data['Archieve']['reason'] 		= $result->reason;
                        $this->request->data['Archieve']['duration'] 	= $result->duration;
                        $this->request->data['Archieve']['has_audio'] 	= $result->hasAudio;
                        $this->request->data['Archieve']['has_video'] 	= $result->hasVideo;
                        $this->request->data['Archieve']['updated_by'] 	= 'cron';
                        $this->request->data['Archieve']['modified'] 	= date('Y-m-d H:i:s');

					    $desFolder = WWW_ROOT.'uploads/archives/'.$project_id.'/';

	    				if(!is_dir($desFolder))
	    				{
	    					mkdir($desFolder,0777,true);
	    					chmod($desFolder,0777);
						}

						$url = $result->url;
						$time = time();
                        $fileName = 'archive_'.$time.'.zip';
                        $filePath = $desFolder.$fileName;

                        $zipdir = WWW_ROOT.'uploads/archives/'.$project_id.'/archive_'.$time;
                        $fullpath = 'uploads/archives/'.$project_id.'/archive_'.$time.'/';
                        file_put_contents($filePath,file_get_contents($url));
                        chmod($filePath,0777);
                        //echo '<pre>';
                        $this->unzip($filePath,$zipdir,false, true, true);
						$targetpath = $zipdir.'/'.$arch['Archieve']['file_name'];
                        $newfilename = 'client_recording'.$time.'.mp4';
                        $uploadpath = $desFolder.$newfilename;
                        
                        $result = exec("ffmpeg -fflags +genpts -i $targetpath -r 24 $uploadpath -loglevel error 2>&1",$videoOutput, $returnData); // uncomment this
                        //$result = exec("ffmpeg -i $targetpath -preset veryfast $uploadpath",$videoOutput, $returnData);
                        //echo '<pre>'; 
                        //print_r($videoOutput);
                        if($videoOutput)
                        {
                        	foreach ($videoOutput as $value) 
                        	{
                        		$this->log($value);
                        	}
                        } 
                        //echo $returnData;
                    	//if($returnData==1){
                    		$this->request->data['Archieve']['file_type'] 	= 'mp4';
                       		$this->request->data['Archieve']['filepath'] 	= 'uploads/archives/'.$project_id.DS.$newfilename;
                    	//}
                     
						
                    }
                    $this->Archieve->id = $arch['Archieve']['id'];
					$this->Archieve->save($this->request->data);
				endforeach;
			}
		   die;
	}


	public function unzip($file,$zdir)
	{

		    $zip=zip_open($file);
		    if(!$zip) {return("Unable to proccess file '{$file}'");}

		    $e='';
			while($zip_entry=zip_read($zip)) 
			{
		      
		       $zname=zip_entry_name($zip_entry);
			   if(!zip_entry_open($zip,$zip_entry,"r")) {$e.="Unable to proccess file '{$zname}'"; continue;}
		       if(!is_dir($zdir)) mkdir($zdir,0777,true);

		       $zip_fs=zip_entry_filesize($zip_entry);
		       if(empty($zip_fs)) continue;

		       $zz=zip_entry_read($zip_entry,$zip_fs);
			   $z=fopen($zdir.'/'.$zname,"w");
		       fwrite($z,$zz);
		       fclose($z);
		       zip_entry_close($zip_entry);

		    }
		    zip_close($zip);
			return($e);
	} 

	/*============ Cron job end here for updating archieve info =====================*/


	/*============ Cron job start here to capture payments from charge id ===========*/

	public function makePayment()
	{
		$this->loadModel('Payment');
		$this->loadModel('Project');
		$this->loadModel('StripeCharge');
		$now = date('Y-m-d H:i:s');
		$today = date('Y-m-d');
		$projects = $this->Project->find('all',array('conditions'=>array('Project.status'=>6,'Project.payment_status'=>0,'Project.is_dispute'=>0,'Project.refund_request'=>0)));
		
		if($projects)
		{
			//echo '<pre>'; print_r($projects);
			foreach($projects as $project):
				$id = $project['Project']['id'];
				$promocode = $project['Project']['promo_code'];
				$readerId = $project['Project']['reader_id'];
				$clientId = $project['Project']['client_id'];
				$tapetype = $project['Project']['tape_type'];
				if($tapetype==1){ $tape_type == 'Schedule Tape'; }else{ $tape_type == 'Tape Now'; }
				$schedule_time = $project['Project']['schedule_time'];
				$duration = $project['Project']['duration']+120;
				$schedule_time = date('Y-m-d H:i:s',strtotime("+".$duration." minutes", strtotime($schedule_time)));
				$scheduledate = date('Y-m-d',strtotime($project['Project']['schedule_time']));
				$timeslot = date('H:i',strtotime($project['project']['schedule_time']));
				if($now > $schedule_time)
				{
					$chargedata = $this->StripeCharge->find('first',array('conditions'=>array('StripeCharge.project_id'=>$project['Project']['id'])));
					if($chargedata)
					{
						$charge_id = $chargedata['StripeCharge']['charge_id'];
						$result = $this->Stripe->charge_capture($charge_id);
						if($result['status']=='succeeded')
						{
							
							$this->StripeCharge->id = $chargedata['StripeCharge']['id'];
							$this->StripeCharge->saveField('charge_status','captured');
							$this->Project->id = $id;
							$this->Project->saveField('payment_status',1);
							$invoice = '#invoice'.$id;
							$paymentdata = array(
								'Payment' => array(
									'project_id' => $id,
									'charge_id'	 => $result['id'],
									'reader_id'	 => $project['Project']['reader_id'],
									'client_id'	 => $project['Project']['client_id'],
									'invoice'	 => $result['description'],
									'amount'	 	=> $chargedata['StripeCharge']['amount'],
									'reader_amount'	=> $chargedata['StripeCharge']['reader_amount'],
									'app_fee'	 	=> $chargedata['StripeCharge']['app_fee'],
									'bal_transaction' => $result['balance_transaction'],
									'payment_created' => date('Y-m-d H:i:s',strtotime($result['created'])),
									'status'	=>  $result['status'],
									'transfer'	=> $result['transfer'],
									'response_data' => json_encode($result)
									)
								);

							if($this->Payment->save($paymentdata))
							{
								if(!empty($promocode))
								{
									$reader = $this->User->findById($readerId);
									$reader_promo = $reader['User']['promo_code'];
									$reward_noti_count = $reader['User']['reward_noti_count'];
									if($promocode==$reader_promo)
									{
										if($reward_noti_count<5)
										{
											$reward_count = $reward_noti_count+1;
											$this->User->id = $readerId;
											$this->User->saveField('reward_noti_count',$reward_count);
											//noti code start here //
											$this->sendNotification($clientId,$readerId,'Promocode Notification',array('scheduledate'=>$scheduledate,'timeslot'=>$timeslot,'tape_type'=>$tape_type,'reward_count'=>$reward_count,'promo_code'=>$promocode));
											//noti code ends here //

											if($reward_noti_count==5)
											{
												//noti code start here //
												$this->sendNotification($clientId,$readerId,'Reward Notification',array('scheduledate'=>$scheduledate,'timeslot'=>$timeslot,'tape_type'=>$tape_type,'reward_count'=>$reward_count,'promo_code'=>$promocode));
												//noti code ends here //
											}
										}
										
									}
								}
								echo 'Payment successfully done';
							}
						}
						else
						{
							echo  $result['message'];
						}
					}
				}
			endforeach;
		}
		die;
	}

	/*============ Cron job end here to capture payments from charge id ===========*/



	public function notifications()
	{
		$this->loadModel('AdminNotification');
		$notifications = $this->AdminNotification->find('all',array('order'=>['AdminNotification.id'=>'DESC']));
		if($notifications)
		{
			$this->set('notifications',$notifications);
		}
	}

	public function delete_noti($id=null)
	{
		$this->loadModel('AdminNotification');
		if($id)
		{
			$this->AdminNotification->id = $id;
			if(!$this->AdminNotification->exists())
			{
				$this->Session->setFlash(__('Record not found, Invalid Id',true),'default',array('class'=>'alert alert-danger'));
				$this->redirect($this->referer());
			}
			else
			{
				if($this->AdminNotification->delete($id))
				{
					$this->Session->setFlash(__('Record deleted successfully',true),'default',array('class'=>'alert alert-success'));
					$this->redirect($this->referer());
				}
				else
				{
					$this->Session->setFlash(__('Record could not deleted, please try again later',true),'default',array('class'=>'alert alert-danger'));
					$this->redirect($this->referer());
				}
			}
		}
	}

	public function convertvideoformat($targetfile=null,$uploadpath=null)
	{
		$targetfile = "/home/xicom/tapsquad/app/webroot/uploads/archives/17/archive_1519735444/9af558ec-c1ac-4dc7-8b66-1b3ce2336598.webm";
		$uploadpath = "/home/xicom/tapsquad/app/webroot/uploads/archives/17/recording1.mp4";
		//echo $ffmpeg = exec('ffmpeg -version', $output, $returnvalue);
		
		$convert = exec("ffmpeg -fflags +genpts -i $targetfile -r 24 $uploadpath",$videoOutput, $returnData);
		echo '<pre>';
		print_r($videoOutput);
		echo $returnData;
		//return $returnData;
		die;
	}


	public function encryptor($action, $string) 
	{
	    $output = false;

	    $encrypt_method = "AES-256-CBC";
	    //pls set your unique hashing key
	    $secret_key = 'muni';
	    $secret_iv = 'muni123';

	    // hash
	    $key = hash('sha256', $secret_key);

	    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	    $iv = substr(hash('sha256', $secret_iv), 0, 16);

	    //do the encyption given text/string/number
	    if( $action == 'encrypt' ) {
		$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
		$output = base64_encode($output);
	    }
	    else if( $action == 'decrypt' ){
	    	//decrypt the given text/string/number
		$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	    }

	    return $output;
	}

	
	

}

?>