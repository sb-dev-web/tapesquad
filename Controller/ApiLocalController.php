<?php 

/*pruuf ios controller
  Controller: Api
* Description: Web services 
* Created By: Pooja chaudhary on 14-Mar-2016 */


App::uses('AppController', 'Controller');
class ApiLocalController extends AppController {
        
        var $user_timezone = '';
	var $usertimestamp ='';
	public $components = array('Auth','Core','Session','Paginator');
	public $helpers=array('Form','Session','Paginate');
	var $allowedActions = array('signIn','forgotPassword','userRegistration');
    	public $uses=array('User','Login','Patient');
    	public $layout=false;
    	public $autoRender=false;
    	var $sid= '';
    	var $token='';
    	public $userId='';
        // For iOS
	public $deviceToken = '';
    	public $deviceType='';
    	public $sessionId='';


/**
 * @name Send Response
 * Purpose : This method return the response from any method when called
 * @author Pooja
 * @access private
 * @return the response data
 */
private function __send_response()
	{ 
            echo str_replace('<null>', '""', str_replace(':null',':""',json_encode($this->response))) ;
            exit;
	}
	public function beforeFilter()
	{ 
          
        $this->user_timezone = '';
		if($this->request->header('timezone'))
		{
			$this->user_timezone = $this->request->header('timezone');
		}
		else
		{
			$this->user_timezone = 'UTC';
		}
                $action=$this->action;
                $this->deviceToken=$this->request->header('Push_token');
                $this->deviceType=$this->request->header('device_type');
		header('Content-type: application/json');
		parent::beforeFilter(); 
		if(!in_array($this->request->action, $this->allowedActions))
		{ 
                     	if(!$this->Session->id()) 
			{ 
                           session_start();
			}
                      
			$this->timezone =  $this->request->header('timezone');
			$secret = SECRET_KEY_APP; 
			$token = 'pooja';//$this->request->header('token');
			//$token = $this->request->header('token'); 
			$emailId = $this->request->header('email');
			$timestamp = $this->request->header('timestamp');
                        $this->userId=$this->request->header('userId');
			$this->usertimestamp = $this->request->header('timestamp');
			$generate_token = 'pooja';
		        //$generate_token = sha1($action.$secret . $timestamp . $this->userId);
                        $this->sessionId=$this->request->header('sessionId');
                        
			//echo $generate_token;die;

			if($generate_token !=$token)
			{
                            $this->response = array(
                                                    'status' => 400,
                                                    'message' => 'Invalid Credentials',
                                            );
                            $this->__send_response();
			} else
			{ 
				$user = $this->_check_login();
                               // print_r($user); die;
				if(!empty($user))
				if($this->Auth->login($this->userId))
				{
                                    $this->Auth->allow();
					
				} 
				else
				{
                                    $this->Auth->logout();
                                    $this->response = array(
                                                    'status' => 203,
                                                    'message' => 'You have been logged out',
                                            );
                                          $this->__send_response();
				}
			}
		} else
		{
                   	$secret = SECRET_KEY_APP;
			$timestamp = $this->request->header('timestamp');
			$token = 'pooja';//$this->request->header('token');
		 	//$token = $this->request->header('token');
                      
			$generate_token = 'pooja';//sha1( $action .$secret . $timestamp); 
		        //$generate_token = sha1( $action .$secret . $timestamp); 
			//TODO Check Issue from Mobile
                        //  echo  $generate_token; die;
			
			if($generate_token != $token)
			{
				$this->response = array(
					'status' => 400,
					'message' => 'Invalid Credentials',
				);
				$this->__send_response();
			} else
			{
                               $this->Auth->allow($this->request->action);
			}
		}

		if(isset($this->request->query['page']))
		{
			$this->page_number = $this->request->query['page']; 
		}
		if(isset($this->request->query['limit']))
		{
			$this->limit = $this->request->query['limit']; 
		}
		if(isset($this->request->query['show']) && $this->request->query['show'] == 'all')
		{
			$this->limit = MAX_PAGINATION_LIMIT;
		}
		$this->paginate_array = array(
			'limit' => $this->limit,
			'page' => $this->page_number
		);
               
		
	   
	}
  function _check_login(){

       $user=$this->User->findByIdAndStatus($this->userId,'Active');
	//echo '<pre>'; print_r($user); die;
       return $user;
  } 
 
        
        
/*Method: registration
* Description: App user registration
* Created By:  Pooja chaudhary on 14-Mar-2016*/

	function userRegistration(){

		$data = $this->data;
		if(!empty($data)){

			if(isset($data['username']) && $data['username']!=""){
				$chkusername = $this->User->findByUsername($data['username']);
				if($chkusername){
					$this->response = array('status'=>400,'message'=>'Username aleady exists');
					$this->__send_response();
				}else{
					$this->request->data['User']['username'] = $data['username'];
				}
			}else{
				$this->response = array('status'=>400,'message'=>'Username required');
				$this->__send_response();
			}


			if(isset($data['email_id']) && $data['email_id']!=""){
				$chkemail = $this->User->findByEmailId($data['email_id']);
				if($chkemail){
					$this->response = array('status'=>400,'message'=>'Email-Id aleady exists');
					$this->__send_response();
				}else{
					$this->request->data['User']['email_id'] = $data['email_id'];
				}
				
			}else{
				$this->response = array('status'=>400,'message'=>'email-id required');
				$this->__send_response();
			}

			if(isset($data['password']) && $data['password']!=""){
				$pass = $data['password'];
				$this->request->data['User']['password'] = AuthComponent::password($pass);
				
			}else{
				$this->response = array('status'=>400,'message'=>'password required');
				$this->__send_response();
			}


			if(isset($data['dob']) && $data['dob']!=""){
				$this->request->data['User']['dob'] = date('Y-m-d',strtotime($data['dob']));
			}

			if($this->request->data['User']!=""){

				$this->User->create();
				$this->request->data['User']['user_role_id'] = 2;
				if($rs = $this->User->save($this->request->data)){

					    $id = $this->encryptor('encrypt',$rs['User']['id']); 
					    $to = 'pc31190@gmail.com';
					    $url = SITE_URL."users/activate_account/".$id; 
				            $subject = 'Account Activation Link';
				     	    $from = 'info@pruuf.com';
				     	    $content = "Hello,<br /><br />
							Your account has been created successfully please click on below link to activate your account.<br />
							<strong> $url</strong><br /><br />
							
							Thanks<br />
							Pruuf Admin";

					    $this->sendEmail($to,$from,$subject,$content);					


					 if(isset($rs['User']['dob'])){
						$dob = date('d-m-Y',strtotime($rs['User']['dob']));
					 }else{
						$dob = '';
					 }
					 $userArr=array(
						'user_id'=>$rs['User']['id'],
						'username'=>$rs['User']['username'],
						'email_id'=>$rs['User']['email_id'],
						'dob'=> $dob,
					 );

					 

		       		$this->response =  array( 'status'=>200, 'data'=> $userArr, 'message'=>'User successfully registered.' );

				}else{
					$this->response =  array('status'=>400,'message'=>'User registration could not be done, please try again later');
				}		

			}

		}else{

			$this->response =  array('status'=>400,'message'=>'Please fill details');

		}

		$this->__send_response();
	}



/*Method: login
* Description: App user authentication
* Created By:  Pooja chaudhary on 14-Mar-2016*/
        
        
    function signIn(){

	
         $data=$this->data;
	
         if(!empty($data)){					 
		
		$user=$this->User->findByEmailIdAndPassword($data['email_id'],AuthComponent::password($data['password']));
                if(count($user)>0)
		{
		    if($user['User']['status']=="Active")
			{
		            $userId=$user['User']['id'];
		            $this->User->id=$userId;
		            $sessionId=$this->generateRandomString(25);
		            $this->User->save(array('last_login_date' => date("Y-m-d H:i:s"),'signedIn'=>'1','session_id'=>$sessionId));	
		
			    if($user['User']['user_image']!=''){
			    	$user_image =   SITE_URL.'img/userImages/'.$user['User']['user_image'];
			    }else{
		                $user_image = '';
			    }                  

		            $userArr=array(

					'user_id'=>$user['User']['id'],
					'user_role_id' => $user['User']['user_role_id'],
					'username'=>$user['User']['username'],
					'name'=>$user['User']['name'],
					'email_id'=>$user['User']['email_id'],
					'user_image'=>$user_image,
					'status'=>$user['User']['status'],
					'modified'=>$user['User']['modified'],
					'session_id'=>$sessionId,
					'designation'=>$user['User']['designation'],
					'description'=>$user['User']['description'],
					'location'=>$user['User']['location'],
					'creditcard_type'=>$user['User']['creditcard_type'],
					'creditcard_number'=>$user['User']['creditcard_number'],
					'cardholder_name'=>$user['User']['cardholder_name'],
					'card_expiry'=>$user['User']['card_expiry'],
					'billing_address'=>$user['User']['billing_address'],
					'top_hashtags'=>$user['User']['top_hashtags'],
					'fb_link'=>$user['User']['fb_link'],
					'li_link'=>$user['User']['li_link'],
					'tw_link'=>$user['User']['tw_link'],
					'gp_link'=>$user['User']['gp_link'],
					'pi_link'=>$user['User']['pi_link'],
				
					
		           );

			        $this->response =  array( 'status'=>200, 'data'=> $userArr, 'message'=>'User successfully logged in.' );

			}elseif($user['User']['status']=="Inactive"){
				$this->response = array('status'=>400,'message'=>'Your account is not activated yet please activate your account to login.'); 

			}elseif($user['User']['status']=="Suspended"){
				$this->response = array('status'=>400,'message'=>'Your account is suspended, please contact admin.'); 

			}else{
			       $this->response = array('status'=>400,'message'=>'Your account is banned, you are not allowed to access this site.'); 
			}

                }else{
                       $this->response = array('status'=>400,'message'=>'Invalid email or password'); 
                }
         }
	else{
		       $this->response =  array('status'=>400,'message'=>'Invalid email or password'); 
	    }
         	$this->__send_response();
        }


	function generateRandomString($length = 10) {
		    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < $length; $i++) {
		            $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    return $randomString;
	    }
    
/*Method: forgotPassword
* Description: User and Expert can reset their password
* Created By:  Pooja chaudhary on 14-Mar-2016*/

    
	function forgotPassword(){
	
	 $data = $this->data;
	 if($data['email_id']){
	
            $userData=$this->User->findByEmailId($data['email_id']);
	    
            if(!empty($userData)){
                 $newPwd=$this->Core->generatePassword(8);
		 $this->User->id=$userData['User']['id'];
                 $this->User->saveField('password',$this->Auth->password($newPwd));

           		/*******Send Email******/
                             $email = $userData['User']['email_id'];
                             $name = $userData['User']['name'];
                             $password=$newPwd;
			     $from = 'info@pruuf.com';
                             $to = $email;
                             $subject = 'Forgot Password';
			     $content = "Hello $name,<br /><br />
						Your password has reset.<br />
						<strong>Your new password is</strong>: $password<br />
						Thanks<br /><br />";

                          
			      if($this->sendEmail($to,$from,$subject,$content)){
					$this->User->saveField('password_flag','1');
		              		$this->response =array('status'=>200,'message'=>'New password has been sent to your email');
			      }else{
					$this->response =array('status'=>400,'message'=>'Password could not be changed, please try again later.');
			      }
		 }else{
		    	$this->response=array('status'=>400,'message'=>'Invalid access');  
		 }
        }else{
            $this->response=array('status'=>400,'message'=>'Invalid access'); 
        }
            $this->__send_response();
        }


/*Method: changePassword
* Description: User and expert can update their password
* Created By:  Pooja chaudhary on 14-Mar-2016*/


	function changePassword(){
            $data=$this->data;
            if(!empty($data)){
                $userData=$this->User->findById($this->userId,array('password'));
                if(AuthComponent::password($data['oldPassword'])!=$userData['User']['password']){
                     $this->response=array('status'=>400,'message'=>'Invalid current password'); 
                }else{
                    $this->User->id=$this->userId;
		    $this->request->data['User']['password'] = AuthComponent::password($data['newPassword']);
		    $this->request->data['User']['password_flag'] = 0;
                    $this->User->save($this->request->data);
                    $this->response=array('status'=>200,'message'=>'Password updated');	
                }
            }else{
                   $this->response=array('status'=>400,'message'=>'Invalid access'); 
            }
             $this->__send_response();
        }


/*Method: getProfile
* Description: User and Expert can check their profile
* Created By:  Pooja chaudhary on 15-Mar-2016*/

	function getProfile(){

		$data = $this->data;
		if($data['user_id']){

			$user_id = $data['user_id'];
			$this->User->id = $user_id;
			if(!$this->User->exists()){
				$this->response = array('status'=>400,'message'=>'User not found');
			}else{
				
				 $user = $this->User->findById($user_id);
				                 

				    $userArr=array(

						'user_id'=>$user['User']['id'],
						'user_role_id'=>$user['User']['user_role_id'],
						'username'=>$user['User']['username'],
						'name'=>$user['User']['name'],
						'email_id'=>$user['User']['email_id'],
						'user_image'=>$user['User']['user_image'],
						'status'=>$user['User']['status'],
						'modified'=>$user['User']['modified'],
						'designation'=>$user['User']['designation'],
						'description'=>$user['User']['description'],
						'location'=>$user['User']['location'],
						'creditcard_type'=>$user['User']['creditcard_type'],
						'creditcard_number'=>$user['User']['creditcard_number'],
						'cardholder_name'=>$user['User']['cardholder_name'],
						'card_expiry'=>$user['User']['card_expiry'],
						'billing_address'=>$user['User']['billing_address'],
						'top_hashtags'=>$user['User']['top_hashtags'],
						'fb_link'=>$user['User']['fb_link'],
						'li_link'=>$user['User']['li_link'],
						'tw_link'=>$user['User']['tw_link'],
						'gp_link'=>$user['User']['gp_link'],
						'pi_link'=>$user['User']['pi_link'],
				
					
				   );

				       $this->response =  array('status'=>200, 'data'=> $userArr, 'message'=>'success' );
			}


		}else{
			$this->response = array('status'=>400,'message'=>'Invalid User Id');
		}

		$this->__send_response();
	}


/*Method: update Profile
* Description: User and expert can update their profile
* Created By:  Pooja chaudhary on 15-Mar-2016*/

	function updateProfile(){

		$data = $this->data;
		if(!empty($data)){
			
			if($data['id'] && $data['id']!=""){  
				$id=$data['id'];
				$this->User->id = $id;

					if(!$this->User->exists()){
						$this->response = array('status'=>400,'message'=>'Invalid User');
					}else{

						if(isset($_FILES['user_image'])){
						    
						    if(!empty($_FILES['user_image']['name'])){
						       $picName = $_FILES['user_image']['name'];
						       if(move_uploaded_file($_FILES["user_image"]["tmp_name"], WWW_ROOT . 'img/userImages/'.$picName))
							{
						   		$this->request->data['User']['user_image']  = SITE_URL.'img/userImages/'.$picName;
							}
						    }

				  		}
					
						if(isset($data['name']) && $data['name']!=""){
							$this->request->data['User']['name'] = $data['name'];
						}

						if(isset($data['username']) && $data['username']!=""){
							$this->request->data['User']['username'] = $data['username'];
						}

						if(isset($data['designation']) && $data['designation']!=""){
							$this->request->data['User']['designation'] = $data['designation'];
						}
		
						if(isset($data['location']) && $data['location']!=""){
							$this->request->data['User']['location'] = $data['location'];
						}

						if(isset($data['description']) && $data['description']!=""){
							$this->request->data['User']['description'] = $data['description'];
						}

						if(isset($data['creditcard_type']) && $data['creditcard_type']!=""){
							$this->request->data['User']['creditcard_type'] = $data['creditcard_type'];
						}

						if(isset($data['creditcard_number']) && $data['creditcard_number']!=""){
							$this->request->data['User']['creditcard_number'] = $data['creditcard_number'];
						}

						if(isset($data['cardholder_name']) && $data['cardholder_name']!=""){
							$this->request->data['User']['cardholder_name'] = $data['cardholder_name'];
						}

						if(isset($data['card_expiry']) && $data['card_expiry']!=""){
							$this->request->data['User']['card_expiry'] = $data['card_expiry'];
						}
		
						if(isset($data['billing_address']) && $data['billing_address']!=""){
							$this->request->data['User']['billing_address'] = $data['billing_address'];
						}

						if(isset($data['top_hashtags']) && $data['top_hashtags']!=""){
							$this->request->data['User']['top_hashtags'] = $data['top_hashtags'];
						}

						if(isset($data['fb_link']) && $data['fb_link']!=""){
							$this->request->data['User']['fb_link'] = $data['fb_link'];
						}

						if(isset($data['li_link']) && $data['li_link']!=""){
							$this->request->data['User']['li_link'] = $data['li_link'];
						}

						if(isset($data['tw_link']) && $data['tw_link']!=""){
							$this->request->data['User']['tw_link'] = $data['tw_link'];
						}

						if(isset($data['gp_link']) && $data['gp_link']!=""){
							$this->request->data['User']['gp_link'] = $data['gp_link'];
						}

						if(isset($data['pi_link']) && $data['pi_link']!=""){
							$this->request->data['User']['pi_link'] = $data['pi_link'];
						}

						$this->request->data['User']['modified'] = date('Y-m-d');

						if($this->request->data['User']!=""){
			
							if($this->User->save($this->request->data)){
							
								  $user = $this->User->findById($id);

								                

								    $userArr=array(

										'user_id'=>$user['User']['id'],
										'user_role_id'=>$user['User']['user_role_id'],
										'username'=>$user['User']['username'],
										'name'=>$user['User']['name'],
										'email_id'=>$user['User']['email_id'],
										'user_image'=>$user['User']['user_image'],
										'status'=>$user['User']['status'],
										'designation'=>$user['User']['designation'],
										'description'=>$user['User']['description'],
										'location'=>$user['User']['location'],
										'creditcard_type'=>$user['User']['creditcard_type'],
										'creditcard_number'=>$user['User']['creditcard_number'],
										'cardholder_name'=>$user['User']['cardholder_name'],
										'card_expiry'=>$user['User']['card_expiry'],
										'billing_address'=>$user['User']['billing_address'],
										'top_hashtags'=>$user['User']['top_hashtags'],
										'fb_link'=>$user['User']['fb_link'],
										'li_link'=>$user['User']['li_link'],
										'tw_link'=>$user['User']['tw_link'],
										'gp_link'=>$user['User']['gp_link'],
										'pi_link'=>$user['User']['pi_link'],
										'modified'=>$user['User']['modified']
				
					
								   );

				       				$this->response = array('status'=>200, 'data'=> $userArr, 'message'=>'User profile updated successfully');
						
							}else{
								$this->response = array('status'=>200, 'message'=>'Profile could not be updated, please try again later');
							}		

						}
					
					}
				
			}else{
				$this->response = array('status'=>'400','message'=>'Please provide user Id');
			}

		}else{
			$this->response = array('status'=>400,'message'=>'No data found');
		}

		$this->__send_response();
	}


/*Method: getExperts
* Description: experts listing
* Created By:  Pooja chaudhary on 16-Mar-2016*/

	function getExperts(){

		$experts = $this->User->find('all',array('conditions'=>array('User.status'=>'Active','User.user_role_id IN'=>array('3','4'))));
		if($experts){

			foreach($experts as $user):
			
				if($user['User']['user_image']!=''){
					$user_image =   SITE_URL.'img/userImages/'.$user['User']['user_image'];
				}else{
					$user_image = '';
				}                  

					$userArr[]=array(

						'user_id'=>$user['User']['id'],
						'user_role_id'=>$user['User']['user_role_id'],
						'username'=>$user['User']['username'],
						'name'=>$user['User']['name'],
						'email_id'=>$user['User']['email_id'],
						'user_image'=>$user['User']['user_image'],
						'status'=>$user['User']['status'],
						'designation'=>$user['User']['designation'],
						'description'=>$user['User']['description'],
						'location'=>$user['User']['location'],
						'top_hashtags'=>$user['User']['top_hashtags'],
						'fb_link'=>$user['User']['fb_link'],
						'li_link'=>$user['User']['li_link'],
						'tw_link'=>$user['User']['tw_link'],
						'gp_link'=>$user['User']['gp_link'],
						'pi_link'=>$user['User']['pi_link'],
						'modified'=>$user['User']['modified'],
				
					
					);

				    $this->response = array('status'=>200, 'data'=> $userArr, 'message'=>'success');	

			endforeach;

		}else{
			$this->response = array('status'=>200,'message'=>'No expert found');
		}
		$this->__send_response();
	}

/*Method: setExpertAvalibility
* Description: expert set his availability
* Created By:  Pooja chaudhary on 17-Mar-2016*/


	public function setExpertAvalibility(){

		$this->loadModel('ExpertAvailabilty');
		$data = $this->data;
		
		if($data['id']!="" && $data['available_dates']!=""){
			$id = $data['id'];
			$data['available_dates'] =  stripslashes($data['available_dates'] = str_replace('\n','',($data['available_dates']))); 
			$available_dates = json_decode($data['available_dates']);
			
			foreach($available_dates as $dates):
	
				$res_arr[] = array(

					'user_id' => $id,
					'avail_date' => date('Y-m-d',strtotime($dates->avail_date)),
					'time_slot' => $dates->time_slot,
					'added_date' => date('Y-m-d')

				);

			endforeach;

			if($res_arr!=""){
				if($this->ExpertAvailabilty->saveAll($res_arr)){

					$data_arr = $this->ExpertAvailabilty->find('all',array('conditions'=>array('ExpertAvailabilty.user_id'=>$id)));
					if($data_arr){
						foreach($data_arr as $arr){

							$res[] = array(
								
							    'id' => $arr['ExpertAvailabilty']['user_id'],
							    'date'=> date('d-m-Y',strtotime($arr['ExpertAvailabilty']['avail_date'])),
							    'time_slot' => $arr['ExpertAvailabilty']['time_slot'],

							);

						$this->response = array('status'=>200,'message'=>'success','data'=>$res);

						}
					}else{
						$this->response = array('status'=>400,'message'=>'No availability found for this expert.');
					}
				}else{
					$this->response = array('status'=>400,'message'=>'Availability could not set,please try again');			
				}
			}

		}else{

			$this->response  = array('status'=>400,'message'=>'Please select ');
		}
		$this->__send_response();
	}


/*Method: getExpertAvalibility
* Description: show expert availability to user
* Created By:  Pooja chaudhary on 17-Mar-2016*/


	public function getExpertAvalibility(){

		$this->loadModel('ExpertAvailabilty');
		$data = $this->data;
		if($data['id']!=""){
			$id = $data['id'];

			if(isset($data['avail_date']) && $data['avail_date']!=""){
				$date = date('Y-m-d',strtotime($data['avail_date']));
				$get_data = $this->ExpertAvailabilty->find('first',array('conditions'=>array('ExpertAvailabilty.user_id'=>$id,'ExpertAvailabilty.avail_date'=>$date)));
				if($get_data){

					$res = array(
								
						 'id' => $get_data['ExpertAvailabilty']['user_id'],
						 'date'=> date('d-m-Y',strtotime($get_data['ExpertAvailabilty']['avail_date'])),
						 'time_slot' => $get_data['ExpertAvailabilty']['time_slot'],

					);

					$this->response = array('status'=>200,'message'=>'success','data'=>$res);
					
				}else{

					$this->response = array('status'=>400,'message'=>'Experts availability not found');
				}
				


			}else{
				$get_data = $this->ExpertAvailabilty->find('all',array('conditions'=>array('ExpertAvailabilty.user_id'=>$id)));
				if($get_data){

					foreach($get_data as $arr){

								$res[] = array(
								
								    'id' => $arr['ExpertAvailabilty']['user_id'],
								    'date'=> date('d-m-Y',strtotime($arr['ExpertAvailabilty']['avail_date'])),
								    'time_slot' => $arr['ExpertAvailabilty']['time_slot'],

								);

							$this->response = array('status'=>200,'message'=>'success','data'=>$res);
					}
				}else{

					$this->response = array('status'=>400,'message'=>'Experts availability not found');
				}
			}

		}else{

			$this->response = array('status'=>400,'message'=>'Invalid user id');
		}	
		
		$this->__send_response();
	}


/*Method: bookExpert
* Description: User request for booking an expert 
* Created By:  Pooja chaudhary on 20-Mar-2016*/


	function bookExpert(){

		$this->loadModel('BookingRequest');
		$data = $this->data;

		if(isset($data['user_id']) && $data['user_id']!=""){
			$user_id = $data['user_id'];
			$this->request->data['BookingRequest']['user_id'] = $data['user_id'];
		}else{
			$this->response = array('status'=>400,'message'=>'please provide user Id');
			$this->__send_response();
		}

		if(isset($data['expert_id']) && $data['expert_id']!=""){
			$expert_id = $data['expert_id'];
			$this->request->data['BookingRequest']['expert_id'] = $data['expert_id'];
		}else{
			$this->response = array('status'=>400,'message'=>'please provide expert Id');
			$this->__send_response();
		}

		if(isset($data['apoint_date']) && $data['apoint_date']!=""){
			$apoint_date = date('Y-m-d',strtotime($data['apoint_date']));
			$this->request->data['BookingRequest']['apoint_date'] = date('Y-m-d',strtotime($data['apoint_date']));
		}else{
			$this->response = array('status'=>400,'message'=>'please provide apoint date');
			$this->__send_response();
		}

		if(isset($data['time_slot']) && $data['time_slot']!=""){
			$time_slot = $data['time_slot'];
			$this->request->data['BookingRequest']['time_slot'] = $data['time_slot'];
		}else{
			$this->response = array('status'=>400,'message'=>'please provide time slot');
			$this->__send_response();
		}

		if(isset($data['description']) && $data['description']!=""){
		
			$this->request->data['BookingRequest']['description'] = $data['description'];
		}

		$this->request->data['BookingRequest']['pruuf_bookingid'] = 'PRUUF'.rand(100,1000);
		$this->request->data['BookingRequest']['amount'] = 1;
		$this->request->data['BookingRequest']['added_date'] = date('Y-m-d');

		if(!empty($this->request->data)){

			$chkbooking = $this->BookingRequest->findByUserIdAndExpertIdAndApointDateAndTimeSlot($user_id,$expert_id,$apoint_date,$time_slot);

			if(!$chkbooking){			
				if($rs = $this->BookingRequest->save($this->request->data)){
		
					$this->response = array('status'=>200,'message'=>'success');
		
				}else{
					$this->response = array('status'=>400,'message'=>'request could not done, please try again later');
				}
			}else{
				$this->response = array('status'=>400,'message'=>'You already booked for this');
			}

		}
		
		$this->__send_response();

	}


/*Method: listBookings
* Description: List booking requests for user
* Created By:  Pooja chaudhary on 20-Mar-2016*/

	function listBookings(){

		$this->loadModel('BookingRequest');
		$data = $this->data;

		if(isset($data['id']) && $data['id']!=""){

			$id = $data['id'];
			$get_list = $this->BookingRequest->find('all',array('conditions'=>array('BookingRequest.user_id'=>$id),'order'=>array('BookingRequest.id DESC')));

			if($get_list){

				foreach($get_list as $list):

					$bookings[] = array(
						
						'user_id' => $list['BookingRequest']['user_id'],
						'expert_id' => $list['BookingRequest']['expert_id'],
						'apoint_date' => $list['BookingRequest']['apoint_date'],
						'time_slot' => $list['BookingRequest']['time_slot'],
						'description' => $list['BookingRequest']['description'],
						'status' => $list['BookingRequest']['status'],
						'created' => $list['BookingRequest']['added_date']

					);

				endforeach;

				$this->response = array('status'=>200,'message'=>'success','data'=>$bookings);

			}else{
				$this->response = array('status'=>400,'message'=>'No booking found');
			}

		}else{

			$this->response = array('status'=>'400','message'=>'Please provide a valid id');
		}
		$this->__send_response();
	}


/*Method: ListExpertBookings
* Description: List booking requests for experts
* Created By:  Pooja chaudhary on 20-Mar-2016*/

	function ListExpertBookings(){

		$this->loadModel('BookingRequest');
		$data = $this->data;

		if(isset($data['id']) && $data['id']!=""){

			$id = $data['id'];

			if(isset($data['apoint_date']) && $data['apoint_date']!="" && isset($data['time_slot']) && $data['time_slot']!=""){
				$apoint_date = date('Y-m-d',strtotime($data['apoint_date']));
				$time_slot = $data['time_slot'];
				$get_list = $this->BookingRequest->findByExpertIdAndApointDateAndTimeSlot($id,$apoint_date,$time_slot);

				if($get_list){
					
						$bookings = array(
						
							'user_id' => $get_list['BookingRequest']['user_id'],
							'expert_id' => $get_list['BookingRequest']['expert_id'],
							'apoint_date' => $get_list['BookingRequest']['apoint_date'],
							'time_slot' => $get_list['BookingRequest']['time_slot'],
							'description' => $get_list['BookingRequest']['description'],
							'status' => $get_list['BookingRequest']['status'],
							'created' => $get_list['BookingRequest']['added_date']

						);

					

					$this->response = array('status'=>200,'message'=>'success','data'=>$bookings);

				}else{
					$this->response = array('status'=>400,'message'=>'No booking found');
				}

				
			}elseif(isset($data['apoint_date']) && $data['apoint_date']!=''){

				$apoint_date = date('Y-m-d',strtotime($data['apoint_date']));
				$get_list = $this->BookingRequest->find('all',array('conditions'=>array('BookingRequest.expert_id'=>$id,'BookingRequest.apoint_date'=>$apoint_date),'order'=>array('BookingRequest.id DESC')));

				if($get_list){
					
					
					foreach($get_list as $list):

						$bookings[] = array(
						
							'user_id' => $list['BookingRequest']['user_id'],
							'expert_id' => $list['BookingRequest']['expert_id'],
							'apoint_date' => $list['BookingRequest']['apoint_date'],
							'time_slot' => $list['BookingRequest']['time_slot'],
							'description' => $list['BookingRequest']['description'],
							'status' => $list['BookingRequest']['status'],
							'created' => $list['BookingRequest']['added_date']

						);

					endforeach;

					$this->response = array('status'=>200,'message'=>'success','data'=>$bookings);

				}else{
					$this->response = array('status'=>400,'message'=>'No booking found');
				}


		       }else{

				$get_list = $this->BookingRequest->find('all',array('conditions'=>array('BookingRequest.expert_id'=>$id),'order'=>array('BookingRequest.id DESC')));

				if($get_list){

					foreach($get_list as $list):

						$bookings[] = array(
						
							'user_id' => $list['BookingRequest']['user_id'],
							'expert_id' => $list['BookingRequest']['expert_id'],
							'apoint_date' => $list['BookingRequest']['apoint_date'],
							'time_slot' => $list['BookingRequest']['time_slot'],
							'description' => $list['BookingRequest']['description'],
							'status' => $list['BookingRequest']['status'],
							'created' => $list['BookingRequest']['added_date']

						);

					endforeach;

					$this->response = array('status'=>200,'message'=>'success','data'=>$bookings);

				}else{
					$this->response = array('status'=>400,'message'=>'No booking found');
				}
			}

		}else{

			$this->response = array('status'=>'400','message'=>'Please provide a valid id');
		}
		$this->__send_response();
		
	}

	
/*Method: BookExpertRequest
* Description: Expert will accept or reject booking requests
* Created By:  Pooja chaudhary on 14-Mar-2016*/

	function BookExpertRequest(){

		$this->loadModel('BookingRequest');
		$data = $this->data; 
		if(isset($data['expert_id']) && $data['expert_id']!="" && isset($data['booking_id']) && $data['booking_id']!="" && isset($data['type']) && $data['type']!=""){

			$expert_id = $data['expert_id'];
			$booking_id = $data['booking_id'];
			$type = $data['type'];
			
			$booking_data = $this->BookingRequest->findById($booking_id);
			//echo '<pre>'; print_r($booking_data); die;
			$user_id = $booking_data['BookingRequest']['user_id'];
			$booking_uniqueid  = $booking_data['BookingRequest']['pruuf_bookingid'];
			$apoint_date = $booking_data['BookingRequest']['apoint_date'];
			$time_slot = $booking_data['BookingRequest']['time_slot'];

			$same_bookings = $this->BookingRequest->find('all',array('conditions'=>array('BookingRequest.apoint_date'=>$apoint_date,'BookingRequest.time_slot'=>$time_slot,'BookingRequest.expert_id'=>$expert_id)));

			$get_seeker = $this->User->findById($user_id);
				
				$payer_email = $get_seeker['User']['email_id'];
				$expiry = explode('/',$get_seeker['User']['card_expiry']);
				$exp_month = $expiry[0];
				$exp_year  = $expiry[1];

			if($type=="Accept"){
				$status = "Accepted";
			}else{
				$status = "Rejected";
			}

			if($status=='Accepted'){

					 $params = array(

						"testmode"   => "on",
						"private_live_key" => "sk_live_SQmWEJHklSxPy7dYgD4sfxKP",
						"public_live_key"  => "pk_live_DTIXxVftmsiU6ltSS3FG0wk0",
						"private_test_key" => "sk_test_xpYOfExQDR85H8mA7OXx083M",
						"public_test_key"  => "pk_test_UyPENM01NHIPpSbtLGsLvD2M"
					);
		
					if ($params['testmode'] == "on") {
						\Stripe\Stripe::setApiKey($params['private_test_key']);
						$pubkey = $params['public_test_key'];
					} else {
						\Stripe\Stripe::setApiKey($params['private_live_key']);
						$pubkey = $params['public_live_key'];
					}
				 
					try{
						$result = \Stripe\Token::create(
						    array(
							"card" => array(
							    "name" => 'test',//$get_seeker['User']['cardholder_name'],
							    "number" => 4242424242424242,//$get_seeker['User']['creditcard_number']
							    "exp_month" => '10',//$exp_month,
							    "exp_year" => '2019',//$exp_year,
							    "cvc" => 123 //$get_seeker['User']['card_cvc']
								)
						    	)
						);
	
						$token = $result['id']; 
						$amount = 1;
						$invoice = "Invoice#" .rand(100,10000);
						try{
							$charge = \Stripe\Charge::create(array(
							      "amount" => $amount*100,
							      "currency" => "usd",
							      "card" => $token,
							      "description" => $invoice 
							));

							$response = $charge->__toArray();
							//echo '<pre>'; print_r($response); 
			
							if($response['status']=='succeeded'){

								$modified = date('Y-m-d');
								if($this->BookingRequest->updateAll(array('BookingRequest.status'=>"'$status'",'BookingRequest.modified'=>"'$modified'"))){
									
									$payment_arr = array(
										'user_id' => $user_id,
										'expert_id'=> $expert_id,
										'booking_id' => $booking_id,
										'booking_uniqueid' => $booking_uniqueid,
										'payment_id' => $response['id'],
										'amount' => 	$amount,
										'payment_created' => $response['created'],
										'invoice' => $invoice,
										'status' => 'success',
										'response_data' => $charge,
										'created' => date('Y-m-d')		
									);

									$this->loadModel('Payment');
									$this->Payment->save($payment_arr,false);

									if($same_bookings){
				
										foreach($same_bookings as $dup_booking):

											$book_id = $dup_booking['BookingRequest']['id'];
											$this->BookingRequest->id = $book_id;
											$this->BookingRequest->saveField('status','Rejected');

										endforeach;

									}									

									$this->response = array('status'=>200,'message'=>'Booking request accepted successfully');			
								}else{
									$this->response = array('status'=>400,'message'=>'Booking request could not be rejected, please try again');
								}

							}else{
								$this->response = array('status'=>400,'message'=>'Booking failed due to failure of payment');
							}
			
						}catch(Exception $e){
							$charge_error = $e->getMessage();
							$this->response = array('status'=>400,'message'=>$charge_error); 
						}
			

				 	 }catch(Exception $e){
						$token_error =  $e->getMessage(); 
						$this->response = array('status'=>400,'message'=>$token_error);
				  	 }

			}else{
				
				
				$this->BookingRequest->id = $booking_id;
				$modified = date('Y-m-d');
				
				if($this->BookingRequest->updateAll(array('BookingRequest.status'=>"'$status'",'BookingRequest.modified'=>"'$modified'"))){
					
					$this->response = array('status'=>200,'message'=>'Booking request rejected successfully');			
				}else{
					$this->response = array('status'=>400,'message'=>'Booking request could not be rejected, please try again');
				}
			}

		}else{
			$this->response = array('status'=>400,'message'=>'Please provide all details');
		}

		$this->__send_response();

	}	


	function logout(){
         
            $this->Auth->logout();
            $this->response = array(
                            'status' => 200,
                            'message' => 'logged out',
                    );
            $this->__send_response();
       }


	function encryptor($action, $string) {
	    $output = false;

	    $encrypt_method = "AES-256-CBC";
	    //pls set your unique hashing key
	    $secret_key = 'muni';
	    $secret_iv = 'muni123';

	    // hash
	    $key = hash('sha256', $secret_key);

	    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	    $iv = substr(hash('sha256', $secret_iv), 0, 16);

	    //do the encyption given text/string/number
	    if( $action == 'encrypt' ) {
		$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
		$output = base64_encode($output);
	    }
	    else if( $action == 'decrypt' ){
	    	//decrypt the given text/string/number
		$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	    }

	    return $output;
	}

        
}
