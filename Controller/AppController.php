<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
public $components = array('Session','Email','Auth','Paginator','Stripe');

	function beforeFilter()
	{
  			$this->loadModel('User');
			$this->Auth->loginRedirect=array('controller'=>'users','action'=>'dashboard','admin' => false);
			$this->Auth->logoutRedirect=array('controller'=>'users','action'=>'login','admin' => false);
			$this->Auth->authError='Session expire!.Please login.';
			$this->Auth->authenticate = array(
			  				'Form' => array(
				        		'userModel' => 'User',
				        		'fields' => array('username' => 'email','password' => 'password')
							//'scope' => array('user_role_id' =>'1')
						)
					); 			
	       		$this->Auth->authorize = array('Controller');		       					
  	}
	
	function beforeRender()
	{
		$this->set('loggedIn', $this->Auth->loggedIn());
   		$this->set('loggedUser',$this->Auth->user());
   		//$this->_setErrorLayout();
	}

	function _setErrorLayout(){

		if ($this->name == 'CakeError')
		{
			$this->layout = 'error404';
		}
	}

	function isAuthorized($user=null)
	{
   		return true;		
	}

	
	public function captureCharge($project_id = null)
	{
		$this->loadModel('Payment');
		$this->loadModel('Project');
		$this->loadModel('StripeCharge');
		
		$now = date('Y-m-d H:i:s');
		$today = date('Y-m-d');
		
		$projects = $this->Project->find('all',array('conditions'=>array('Project.id'=>$project_id,'Project.status'=>6,'Project.payment_status'=>0,'Project.is_dispute'=>0,'Project.refund_request'=>0)));
		//print "<pre>";print_r($projects);exit;
		if($projects)
		{
			
			foreach($projects as $project):
				$id = $project['Project']['id'];
				$promocode = $project['Project']['promo_code'];
				$readerId = $project['Project']['reader_id'];
				$clientId = $project['Project']['client_id'];
				$tapetype = $project['Project']['tape_type'];
				if($tapetype==1){ $tapetype == 'Schedule Tape'; }else{ $tapetype == 'Tape Now'; }
				$schedule_time = $project['Project']['schedule_time'];
				$duration = $project['Project']['duration']+120;
				$schedule_time = date('Y-m-d H:i:s',strtotime("+".$duration." minutes", strtotime($schedule_time)));
				$scheduledate = date('Y-m-d',strtotime($project['Project']['schedule_time']));
				$timeslot = date('H:i',strtotime($project['Project']['schedule_time']));
				
					$chargedata = $this->StripeCharge->find('all',array('conditions'=>array('StripeCharge.project_id'=>$project['Project']['id'])));
					foreach($chargedata as $chargedata)
					{
						$charge_id = $chargedata['StripeCharge']['charge_id'];
						$result = $this->Stripe->charge_capture($charge_id);
						if($result['status']=='succeeded')
						{
							$this->StripeCharge->id = $chargedata['StripeCharge']['id'];
							$this->StripeCharge->saveField('charge_status','captured');
							$this->Project->id = $id;
							$this->Project->saveField('payment_status',1);
							$invoice = '#invoice'.$id;
							$paymentdata = array(
								'Payment' => array(
									'project_id' => $id,
									'charge_id'	 => $result['id'],
									'reader_id'	 => $project['Project']['reader_id'],
									'client_id'	 => $project['Project']['client_id'],
									'invoice'	 => $result['description'],
									'amount'	 	=> $chargedata['StripeCharge']['amount'],
									'reader_amount'	=> $chargedata['StripeCharge']['reader_amount'],
									'app_fee'	 	=> $chargedata['StripeCharge']['app_fee'],
									'bal_transaction' => $result['balance_transaction'],
									'payment_created' => date('Y-m-d H:i:s',strtotime($result['created'])),
									'status'	=>  $result['status'],
									'transfer'	=> $result['transfer'],
									'response_data' => json_encode($result)
									)
								);

							if($this->Payment->saveAll($paymentdata))
							{
								//echo json_encode($paymentdata);
								//echo "*****************************************";
								if(!empty($promocode))
								{
									$reader = $this->User->findById($readerId);
									$reader_promo = $reader['User']['promo_code'];
									$reward_noti_count = $reader['User']['reward_noti_count'];
									if($promocode==$reader_promo)
									{
										if($reward_noti_count<5)
										{
											$reward_count = $reward_noti_count+1;
											$this->User->id = $readerId;
											$this->User->saveField('reward_noti_count',$reward_count);
											//noti code start here //
											$this->sendNotification($clientId,$readerId,'Promocode Notification',array('scheduledate'=>$scheduledate,'timeslot'=>$timeslot,'tape_type'=>$tapetype,'reward_count'=>$reward_count,'promo_code'=>$promocode));
											//noti code ends here //

											if($reward_noti_count==5)
											{
												//noti code start here //
												$this->sendNotification($clientId,$readerId,'Reward Notification',array('scheduledate'=>$scheduledate,'timeslot'=>$timeslot,'tape_type'=>$tapetype,'reward_count'=>$reward_count,'promo_code'=>$promocode));
												//noti code ends here //
											}
										}
										
									}
								}
								echo 'Payment successfully done<br>';
							}
						}
						else
						{
							echo  $result['message'].'<br>';
						}
					}
				
			endforeach;
		}
		die;
	}


	public function createJwtToken()
	{
		$key = TOKBOX_SECRET_KEY;
		$header =   [
                        "alg" => "HS256",
  						"typ" => "JWT"
				    ];

				
		$headers_encoded = $this->base64url_encode(json_encode($header));
		$payload =  [      
					  // "iss"=> "46069462",
					  "iss" => TOKBOX_PROJECT_API_KEY,
					   "ist"=> "project",
					   "iat"=> time(),
					   "exp"=> strtotime('+2 day'),
					   "jti"=> "jwt_nonce"
				    ];

		
		$payload_encoded = $this->base64url_encode(json_encode($payload));
		$signature = hash_hmac('SHA256',$headers_encoded.'.'.$payload_encoded,$key,true);
		$signature_encoded = $this->base64url_encode($signature);
		$token = $headers_encoded.'.'.$payload_encoded.'.'.$signature_encoded;
		return $token;

	}

	public function base64url_encode($data)
	{ 
    	return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
	}

	public function restApi($path)
	{

			$json_web_token = $this->createJwtToken();
			$curl = curl_init();
			curl_setopt($curl,CURLOPT_HTTPHEADER,array('Accept:application/json','X-OPENTOK-AUTH:'.$json_web_token.''));
			curl_setopt($curl, CURLOPT_URL, $path); 
			//curl_setopt($curl, CURLOPT_GET, true); // Use POST // Setup post body
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
			//Execute request and read response
			$response = curl_exec($curl);
			//Check errors
			if($response) {
			  return $response; 
			}else{
			  $error = curl_error($curl). '(' .curl_errno($curl). ')';
			} 
			return;
			curl_close($curl);
	}

	public function restApi1($path)
	{

			$json_web_token = $this->createJwtToken();
			$curl = curl_init();
			//curl_setopt($curl,CURLOPT_HTTPHEADER,array('Accept:application/json','X-OPENTOK-AUTH:'.$json_web_token.''));
			curl_setopt($curl, CURLOPT_URL, $path); 
			//curl_setopt($curl, CURLOPT_GET, true); // Use POST // Setup post body
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
			//Execute request and read response
			$response = curl_exec($curl);
			//Check errors
			if($response) {
			  return $response; 
			}else{
			  $error = curl_error($curl). '(' .curl_errno($curl). ')';
			} 
			return;
			curl_close($curl);
	}

	public function generateSession($path=null)
	{
		  $json_web_token = $this->createJwtToken();
		  //$path = 'https://api.opentok.com/session/create';
		  $curl = curl_init();

		  curl_setopt_array($curl, array(
	      CURLOPT_URL => $path,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "archiveMode=manual&p2p.preference=disabled&location=14.141.175.106",
		  CURLOPT_HTTPHEADER => array(
		    "Accept: application/json",
		    "Cache-Control: no-cache",
		    "Content-Type: application/x-www-form-urlencoded",
		    //"Postman-Token: d93b7592-cd5d-43b3-ac12-ba6b25878450",
		    "X-OPENTOK-AUTH:".$json_web_token
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  $error =  "cURL Error #:" . $err;
		  return json_encode(array('error'=>$error));
		} else {
		  return $response;
		}
		return;
	}


	function sendNotification($sender_id,$receiver_id,$type,$data=array())
	{

		$this->loadModel('User');
		$this->loadModel('DeviceToken');
		if($sender_id==1)
		{
			$sender_name = 'TapeSquad';
			$sender_image = SITE_URL.'img/tapelogo.png';
		}
		else
		{
			$sender = $this->User->findById($sender_id);
			$sender_name = $sender['User']['firstname'];
			if(!empty($sender['User']['profile_pic']))
			{
				$sender_image = SITE_URL.'img/userImages/'.$sender['User']['profile_pic'];
			}
			else
			{
				$sender_image = '';
			}
		}
		
		$receiver = $this->User->findById($receiver_id);
		$receiver_name = $receiver['User']['firstname'];
		if(!empty($receiver['User']['profile_pic']))
		{
			$receiver_image = SITE_URL.'img/userImages/'.$receiver['User']['profile_pic'];
		}
		else
		{
			$receiver_image = '';
		}

		switch ($type)
		{
			case 'TapeRequest Accepted':
				$message = ucfirst($sender_name).' has accepted your taping request.';
				break;
			case 'TapeRequest Declined':
				$message = ucfirst($sender_name).' will not be avail. Continue searching our list of "Readers" ready to tape.';
				break;
			case 'Friend Request':
				$message = ucfirst($sender_name).' sent you a friend request.';
				break;
			case 'Friend Request Accepted':
				$message = ucfirst($sender_name).' accepted your friend request.'; 
				break;
			case 'Liked Post':
				$message = ucfirst($sender_name).' liked your post';
				break;
			case 'Commented':
				$comment = $data['comment'];
				$message = ucfirst($sender_name).' commented: '.$comment;
				break;
			case 'New message':
				$message = ucfirst($sender_name).' sent you a message.';
				break;
			case 'Tape Request':
				if($data['tape_type']==1)
				{
					$message = 'Hello '.ucfirst($receiver_name).' you have a scheduled tape with '.ucfirst($sender_name).' on '.$data['scheduledate'].' at '.$data['timeslot'];
				}
				else
				{
					$message = 'Hello '.ucfirst($receiver_name).' you have an instant scheduled tape with '.ucfirst($sender_name).'.';		
				}
				break;
			case 'Tape Updated':
				$message = 'Hello '.ucfirst($receiver_name).' your scheduled tape with '.ucfirst($sender_name).' on '.$data['scheduledate'].' at '.$data['timeslot'].' has been updated please check.';
				break;
			case 'Share Post':
				$message = ucfirst($sender_name).' shared your post.'; 
				break;
			case 'Call End':
				$message = ucfirst($sender_name).' has ended the call.';
				break;
			case 'In Progress':
				$message = 'Your tape status has been changed to In Progress.';
				break;
			case 'CallStart':
				$message = 'Your call with '.ucfirst($sender_name).' has been started.'; 
				break;
			case 'Extend Tape':
				$message = 'Your session time is completed. Would you like to continue this session? If so, you will be billed $20.';
				break;
			case 'Tape Extend Request':
				$message = 'Would you like to extend this tape with actor?';
				break;
			case 'Tape Download':
				$message = ucfirst($sender_name).' is in process of printing the sides.';
				break;
			default:
				$message = 'Invalid data';
				break;
		}


		$notidata = array('sender_id'=>$sender_id,'sender_name'=>$sender_name,'sender_image'=>$sender_image,'receiver_id'=>$receiver_id,'receiver_name'=>$receiver_name,'receiver_image'=>$receiver_image,'data'=>$data);
		$chktoken = $this->DeviceToken->findByUserId($receiver_id);
		if($chktoken)
		{
			$token = $chktoken['DeviceToken']['device_token'];
			$pushres = $this->__send_notification_to_ios($token,$message,$notidata,$type);
		}
		else
		{
			$retmsg = 'Token not found or invalid';
			$this->save_noti_data(json_encode($notidata),$retmsg,'Invalid or not found');
		}
		
		//save noti to db code starts here //
			$this->loadModel('NotiData');
			$notiArr = array(
				'NotiData' => array(
					'user_id' => $receiver_id,
					'sender_id' => $sender_id,
					'sender_name' => $sender_name,
					'sender_image'=> $sender_image,
					'message' => $message,
					'type' => $type,
					'noti_data' => json_encode($notidata)
					)
				);
			$this->NotiData->save($notiArr);
			return $retmsg;
		//save noti to db code ends here //
		
	}

	/* Method: push_notification
	* Description: this function is responsible for sending push notifications
	* Created By:  Pooja chaudhary on 19-January-2018
	*/

	function __send_notification_to_ios($device=null,$message=null,$data=array(),$type=null)
	{
			$mode = APNS_MODE;

			if($mode=='PRODUCTION')
			{
				$url = 'ssl://gateway.push.apple.com:2195';
				$cert = WWW_ROOT . 'pem/propem.pem';
			}
			else
			{
				$url = 'ssl://gateway.sandbox.push.apple.com:2195';
				$cert = WWW_ROOT . 'pem/devpem.pem';
			}
			
			
            $passPhrase = '';

			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $cert);
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passPhrase);

			$device_token = $device;//mb_convert_encoding($device, 'utf-8');
			$retmsg = "Device token : $device_token, length <" . strlen($device_token) . ">" . PHP_EOL;
			$fp = stream_socket_client($url,$err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			stream_set_blocking($fp, 0);
			stream_set_write_buffer($fp, 0);
			    
			if(!$fp)

			        $retmsg = "Failed to connect: $err $errstr" . PHP_EOL;
					$retmsg = 'Connected to APNS' . PHP_EOL;
					$bodydata['aps'] = array(
							'alert' =>$message,
							'badge' => 1,
							'sound' => 'default', 
							'type'  => $type,
							'data' => $data
						);
						

			$payload = json_encode($bodydata);

				if(strlen($device_token) != 64){
			        $retmsg = "Device token has invalid length";
			    }elseif(strlen($payload) < 10) {
			        $retmsg = 'Invalid payload size';
			    }else{

				    $msg = chr(0)
				            .pack('n', 32) //token length
				            .pack('H*', $device_token) //token
				            .pack('n', strlen($payload)) //length of payload
				            .$payload;


				    //Send it to the server
				    $result = fwrite($fp, $msg);

				    if (!$result)
				        $retmsg = 'Message not delivered' . PHP_EOL;
				    else{
				        $retmsg = "Sent {".strlen($msg)."} to server, received {".$result."}" . PHP_EOL;
						$response = fread($fp, 6);
				    }
				}
				fclose($fp);
				$this->save_noti_data($payload,$retmsg,$device_token);
	}


	function save_noti_data($request,$response,$token)
	{
		$this->loadModel('Notification');
		$notiarr = array
			(
				'Notification'=>array
				(
					'request' => $request,
					'response' => $response,
					'token'=> $token,
				)
			);
		$this->Notification->save($notiarr);
	}	


	function checkAppleErrorResponse($fp)
	{
			//byte1=always 8, byte2=StatusCode, bytes3,4,5,6=identifier(rowID). Should return nothing if OK.
            $apple_error_response = fread($fp, 6);
            //NOTE: Make sure you set stream_set_blocking($fp, 0) or else fread will pause your script and wait forever when there is no response to be sent.

            if ($apple_error_response)
            {
                 //unpack the error response (first byte 'command" should always be 8)
                 $error_response = unpack('Ccommand/Cstatus_code/Nidentifier', $apple_error_response);

                 if ($error_response['status_code'] == '0') {
                     $error_response['status_code'] = '0-No errors encountered';
                 } else if ($error_response['status_code'] == '1') {
                     $error_response['status_code'] = '1-Processing error';
                 } else if ($error_response['status_code'] == '2') {
                     $error_response['status_code'] = '2-Missing device token';
                 } else if ($error_response['status_code'] == '3') {
                     $error_response['status_code'] = '3-Missing topic';
                 } else if ($error_response['status_code'] == '4') {
                     $error_response['status_code'] = '4-Missing payload';
                 } else if ($error_response['status_code'] == '5') {
                     $error_response['status_code'] = '5-Invalid token size';
                 } else if ($error_response['status_code'] == '6') {
                     $error_response['status_code'] = '6-Invalid topic size';
                 } else if ($error_response['status_code'] == '7') {
                     $error_response['status_code'] = '7-Invalid payload size';
                 } else if ($error_response['status_code'] == '8') {
                     $error_response['status_code'] = '8-Invalid token';
                 } else if ($error_response['status_code'] == '255') {
                     $error_response['status_code'] = '255-None (unknown)';
                 } else {
                     $error_response['status_code'] = $error_response['status_code'] . '-Not listed';
                 }

                 echo '<br><b>+ + + + + + ERROR</b> Response Command:<b>' . $error_response['command'] . '</b>&nbsp;&nbsp;&nbsp;Identifier:<b>' . $error_response['identifier'] . '</b>&nbsp;&nbsp;&nbsp;Status:<b>' . $error_response['status_code'] . '</b><br>';
                 echo 'Identifier is the rowID (index) in the database that caused the problem, and Apple will disconnect you from server. To continue sending Push Notifications, just start at the next rowID after this Identifier.<br>';
                //echo 'hi'; die;
                 return true;
            }
            return false;
    }

	function sendEmail($to,$from,$subject,$content='')
	{
		try{
			$this->Email->smtpOptions = array(
					'port'=>'465',
					'timeout'=>'30',
					'host' => 'ssl://smtpout.secureserver.net',
					'username'=>'info@tapesquad.com',
					'password'=>'Tapesquad_123',
					//'client' => 'tapesquad',
					'ssl' => true
			);  
			$content .= "<br>To:".$to;
			$this->Email->from=$from;
			$this->Email->to=$to;
			$this->Email->bcc="rishi@xicom.biz";
			$this->Email->subject=$subject;
			$this->Email->sendAs='both';
			$this->Email->delivery = 'smtp';
			if($this->Email->send($content)){
				return true;
			}
		}catch(Exception $e){
			return $e->getMessage();
		} 
	  	return false;
	}

	public function convert_mp4_mp3( $project_id , $filename )
	{
		
			$mp4file = WWW_ROOT.'uploads/archives/'.$project_id.'/'.$filename;
			$audiofile = WWW_ROOT.'uploads/archives/'.$project_id.'/'.str_replace('.mp4','.mp3', $filename);
			$cmd = 'ffmpeg -i '.$mp4file.'  '.$audiofile;
			//$cmd = 'ffmpeg -i '.$mp4file.' -vn -ab 128k -ar 44100 -y '.$audiofile;
			
			$cmd = shell_exec($cmd);
			return str_replace('.mp4','.mp3', $filename);
	
	}

	public function merge_video_audio( $project_id, $video , $audio , $id )
	{
		$video = WWW_ROOT.'uploads/archives/'.$project_id.'/'.$video;
		$audio = WWW_ROOT.'uploads/archives/'.$project_id.'/'.$audio;
		$new_video_file = "Video_final_".$id.".mp4";
		$new_video = WWW_ROOT.'uploads/archives/'.$project_id.'/'.$new_video_file;
		$cmd = "ffmpeg  -i ".$video." -i ".$audio." -c:a aac -strict -2 ".$new_video;
		$cmd = shell_exec($cmd);
		return $new_video_file;
		

	}

	public function merge_audios( $project_id, $audio1 , $audio2 , $id )
	{
		$audio1 = WWW_ROOT.'uploads/archives/'.$project_id.'/'.$audio1;
		$audio2 = WWW_ROOT.'uploads/archives/'.$project_id.'/'.$audio2;
		$new_audio_filename = "Audio_final_".$id.".mp3";
		$new_audio = WWW_ROOT.'uploads/archives/'.$project_id.'/'.$new_audio_filename;
		$cmd = "ffmpeg -i ".$audio1." -i ".$audio2."  -filter_complex amix -c:a libmp3lame -q:a 3 -ac 2 ".$new_audio;
		//$cmd = "ffmpeg -i ".$audio1." -i ".$audio2."  -filter_complex amix=inputs=2:duration=first:dropout_transition=3 ".$new_audio;
		$cmd = shell_exec($cmd);
		return $new_audio_filename;
		//var_dump($cmd);


	}

	//ffmpeg -i audio.mp3 -i 1.mp3  -filter_complex amerge -c:a libmp3lame -q:a 4 audiofinal.mp3
}
