<?php

//use stripe;
require_once(ROOT.'/app/Vendor'.DS.'stripe'.DS.'init.php');

class StripeComponent extends Component
{

    public $STRIPE_KEY = 'sk_test_xpYOfExQDR85H8mA7OXx083M';
    
    public function create_charge($data)
    {
        
        try{
              $pay  = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
              $res = \Stripe\Charge::create(array(
                        "amount" => $data['amount'],
                        "currency" => "usd",
                        "customer" => $data['customer_id'], // obtained with Stripe.js
                        "description" => "TapeSquad Customer",
                        "application_fee" => $data['app_fees'],
                        "destination" => array(
                            //"amount"  => $data['reader_amount'],
                            "account" => $data['acc_id'],
                        ),
                        "capture" => false
                    )
              );

          $response = $res->__toArray();

        }catch(Exception $e){
            $token_error =  $e->getMessage(); 
            $response = array('status'=>400,'message'=>$token_error,'error_type'=>'token error');
        }

        return $response;

    }

    public function charge_capture($charge_id)
    {
        $pay = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        try{
            $ch  = \Stripe\Charge::retrieve($charge_id);
            $result = $ch->capture();
            return $result->__toArray();
        }catch(Exception $e){
            $error =  $e->getMessage(); 
            $data = json_encode(array('status'=>400,'message'=>$error));
        }
        

    }

    public function create_customer($token,$user_id)
    {
        try{
          $pay = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
          $data = \Stripe\Customer::create(array(
            "description" => "TapeSquad Customer",
            "source" => $token // obtained with Stripe.js
          ));

          
        }catch(Exception $e){
          $token_error =  $e->getMessage(); 
          $data = json_encode(array('status'=>400,'message'=>$token_error,'error_type'=>'token error'));
          
        }
       
       return $data;
        
    }

    public function delete_card($customer_id,$card_id)
    {
        $pay = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        $customer = \Stripe\Customer::retrieve($customer_id);
        $data = $customer->sources->retrieve($card_id)->delete();

        return $data;
    }

    public function get_customer($id)
    {
        $pay  = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        $data = \Stripe\Customer::retrieve($id);
        return $data;

    }

   
    public function update_customer($token,$customer_id)
    {
        $pay = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        $cu  = \Stripe\Customer::retrieve($customer_id);
        $cu->description = "Tapesquad Customer";
        $cu->source = $token; // obtained with Stripe.js
        $cu->save();
        return $cu;
    }

    public function delete_customer($customer_id)
    {
        $pay = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        $cu = \Stripe\Customer::retrieve($customer_id);
        $cu->delete();
    }

    public function delete_account_card($acc_id,$card_id)
    {
        $pay = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        try{
            $account = \Stripe\Account::retrieve("acct_19IFZrHTE30A0U0w");
            $result = $account->external_accounts->retrieve("card_1BsObtHTE30A0U0wv6NwSyzO")->delete();
            $response = $result->__toArray(true);
        }catch(Exception $e){

            $error = $e->getMessage();
            $response = array('status'=>400,'message'=>$error);
        }
        return $response;
    }

    public function create_account($email=null)
    {
      $pay = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        try{
                $account_res = \Stripe\Account::create(array(
                          //"managed" => false,
                          "country" => "US",
                          "email" => $email,
                          "type" => "custom",
                          //'managed' =>true,
                          //'transfer_schedule'=>array('delay_days'=>7,'interval'=>'daily'),
                        ));

                

        }catch(Exception $e){
            $error = $e->getMessage();
            $account_res = array('status'=>400,'message'=>$error);
        }

        return $account_res;

    }

    public function update_account($id=null,$token=null)
    {
        try{
        
            $pay     = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
            $account = \Stripe\Account::retrieve($id);
            $result = $account->external_accounts->create(array("external_account" => $token));
            $response = $result->__toArray();
            
        }catch(Exception $e){
            $error = $e->getMessage();
            $response = array('status'=>400,'message'=>$error);   
           
        }
        return $response;
    }

    public function verify_acc_details($data=[])
    {
        $pay     = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        $account = \Stripe\Account::retrieve($data['account_id']);
        $account->legal_entity->address->city = $data['city'];
        $account->legal_entity->address->state = $data['state'];
        $account->legal_entity->address->line1 = $data['line1'];
        $account->legal_entity->address->postal_code = $data['postal_code'];
        $account->legal_entity->dob->day = $data['dob_day'];
        $account->legal_entity->dob->month = $data['dob_month'];
        $account->legal_entity->dob->year = $data['dob_year'];
        $account->legal_entity->first_name = $data['first_name'];
        $account->legal_entity->last_name = $data['last_name'];
        $account->legal_entity->ssn_last_4 = $data['ssn_last_4'];
        $account->legal_entity->type = $data['type'];
        $account->legal_entity->personal_id_number = $data['personal_id_number'];
        $account->tos_acceptance->date = strtotime($data['tos_acceptance_date']);
        $account->tos_acceptance->ip = $data['tos_acceptance_ip'];
        return $account->save();
    }


    public function verify_stripe_account_details($data=[]){

        $pay     = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        $account = \Stripe\Account::retrieve($data['account_id']);
        $account->legal_entity->dob->day = 10;
        $account->legal_entity->dob->month = 01;
        $account->legal_entity->dob->year = 1986;
        $account->legal_entity->first_name = "Jenny";
        $account->legal_entity->last_name = "Rosen";
        $account->legal_entity->type = "individual";
        $response = $account->save();
        return $response->__toArray();
    }

    public function retrieve_account($id=null)
    {
        $pay     = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        $account = \Stripe\Account::retrieve($id);
        return $account;

    }

    public function create_card($data=[])
    {
        $pay  =  \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        $card =  \Stripe\Token::create(array(
                 "card" => array(
                    "number" => $data['number'],
                    "exp_month" => $data['exp_month'],
                    "exp_year" => $data['exp_year'],
                    "cvc" => $data['cvc'],
                    'currency'=>'usd'
                  )
                ));
        return $card;

    }

    public function create_bankaccount($data=[])
    {
        $pay  =  \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        $card =  \Stripe\Token::create(array(
                    "bank_account" => array(
                        "country" => 'US',
                        "currency" => 'usd',
                        "account_holder_name" => $data['account_holder_name'],
                        "account_holder_type" => $data['account_holder_type'],
                        "routing_number" => $data['routing_number'],
                        "account_number" => $data['account_number']
                    )
                ));
        return $card;
    }


    public function create_refund($charge_id=null)
    {
        $pay = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        try{
            $res = \Stripe\Refund::create(array
                        (
                            "charge" => $charge_id
                        ));
            $response = $res->__toArray(true);
        }catch(Stripe\Error\InvalidRequest $e){
            
            if($e->getTrace()[0]['args'][0])
            {
                $json = json_decode($e->getTrace()[0]['args'][0],true);
                if($json['error']['code'] == 'charge_already_refunded')
                {
                    $response = array('status'=>true ,'message'=>'Amount Refunded');
                }
                else{
                    $error = $e->getMessage();
                    $response = array('status'=>400,'message'=>$error,'error_type'=>'Refund Error');
                }
            }
            else{
                $error = $e->getMessage();
                $response = array('status'=>400,'message'=>$error,'error_type'=>'Refund Error');
            }
            //$error = $e->error();
            
        }
        return $response;
    }

    public function get_charge($charge_id=null)
    {
        $pay = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        $res = \Stripe\Charge::retrieve($charge_id);
        return $res;
    }

    public function get_appfees($fees_id=null)
    {
        $pay = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        $res = \Stripe\ApplicationFee::retrieve($fees_id);
        return $res;
    }

    public function create_transfer($id)
    {
        $pay = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
        $res =   \Stripe\Transfer::create(
              array(
                "amount" => '100',
                "currency" => "usd",
                "destination" => $id,
                'description'   =>'test1',
                'source_transaction'=>'ch_19fRUPK3LldMDxEIpGsHW3tQ',
                "transfer_schedule" => array('delay_days'=>2,'interval'=>'daily'),
                )
            );

        return $res;
    }

    public function ct($email)
    {
      $pay = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
      $res = \Stripe\Stripe::setApiKey($this->STRIPE_KEY);
             return   \Stripe\Account::create(array(
                  "managed" => false,
                  "country" => "US",
                  "email" => $email,
                  'managed' =>true,
                  'transfer_schedule'=>array('delay_days'=>2,'interval'=>'daily'),
                ));
       return $res;
    }


}
?>