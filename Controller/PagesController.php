<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @return void
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
public function beforeFIlter()
{
	parent::beforeFilter();
	$this->Auth->allow('opentokcallback','saveRecording','test','cronDeleteProject');
}

public function opentokcallback()
{
	Configure::write('debug',true);
	header('Content-type: application/json');
	ini_set('upload_max_filesize', '1224M');
	ini_set('max_execution_time', 0);
	ini_set('memory_limit', '2048M');	
	ini_set('output_buffering', 0);
	ini_set('implicit_flush', 1);	
	$this->autoRender = false;
	$this->loadModel('Callback');
	$this->loadModel('Archieve');
	
	if($jsonData = $this->request->input('json_decode'))
	{
		$this->Callback->create();
		$savedata['Callback']['content'] = json_encode($jsonData);
		$this->Callback->save($savedata);
		
		$archiveId = $jsonData->id;
		$get_data = $this->Archieve->find('all',['conditions'=>['archieve_id'=>$archiveId]]);
		
		if($jsonData->status == "stopped")
		{
			$this->captureCharge($get_data[0]['Archieve']['project_id']);
		}
		else if($jsonData->status == "available")
		{
		
				$path = 'https://api.opentok.com/v2/project/'.TOKBOX_PROJECT_API_KEY.'/archive/'.$get_data[0]['Archieve']['archieve_id'];
				$result = $this->restApi($path);
				if($result){
					$result = json_decode($result);
					//print "<pre>";print_r($result);exit;
					$desFolder = WWW_ROOT.'uploads/archives/'.$get_data[0]['Archieve']['project_id'].'/';
					if(!is_dir($desFolder)){
						mkdir($desFolder,0777);
					}
					$url = $result->url;
					$filePathZip = $desFolder.time().'.zip';
					$c = file_get_contents($url);
					file_put_contents($filePathZip,$c);
					$time = time();

					$mp4arr = [];
					$zip = new \ZipArchive;
						$res = $zip->open($filePathZip);
						//print_r($res);exit;
						$mergedata =  [];
						$mergedata['project_id'] = $get_data[0]['Archieve']['project_id'];
						if ($res === TRUE) {
						$zip->extractTo($desFolder);
						$zip->close();
						
							if ($dh = opendir($desFolder)){

							  	while (($file = readdir($dh)) !== false){
									//print "<pre>";print_r($file);
									$fileexp =  explode('.',$file );
									if($fileexp[1] == 'webm')
									{
										$webmfile = $desFolder.$file;
										$fname = "Archieve_".$get_data[0]['Archieve']['id'].'__'.$fileexp[0].'.mp4';
										$desf = $desFolder.$fname;
										$mp4arr[] = 'uploads/archives/'.$get_data[0]['Archieve']['project_id'].'/'.$fname;
										$cmd = 'ffmpeg -i '.$webmfile.' -c:a aac -strict -2 '.$desf;
										//$cmd = 'ffmpeg -i '.$webmfile.' -vcodec libx264 -acodec aac '.$desf;
										$cmd = shell_exec($cmd);
										var_dump($cmd);
										if($file == $get_data[0]['Archieve']['file_name'])
										{
											$mergedata['reader_video'] = $fname;
										}
										else
										{
											$mergedata['client_video'] = $fname;
										}

										$mergedata['id'] = $get_data[0]['Archieve']['id'];
									}

								}

							  closedir($dh);
							}
							
							 print "<pre>";print_r($mergedata);
							 $this->video_create( $mergedata['project_id'],$mergedata['client_video'],$mergedata['reader_video'],$mergedata['id'] );
						  
						// exec('ffmpeg -i "Spider-Man.webm" -qscale 0 "Spider-Man.mp4"');
					} else 
					{
							echo 'failed!';
					}

					if($mp4arr)
					{
			
					$i = 0;
					foreach($get_data as $get_data)
					{
						$this->Archieve->id = $get_data['Archieve']['id'];
						$this->request->data['Archieve']['id'] = $get_data['Archieve']['id'];
						$this->request->data['Archieve']['name'] 		= $result->name;
						$this->request->data['Archieve']['size']    	= $result->size;
						$this->request->data['Archieve']['mode'] 		= $result->outputMode;
						$this->request->data['Archieve']['reason'] 		= $result->reason;
						$fileexp =  explode('.',$get_data['Archieve']['file_name'] );
						$this->request->data['Archieve']['filepath'] 	= "uploads/archives/".$get_data['Archieve']['project_id']."/Archieve_".$get_data['Archieve']['id'].'__'.$fileexp[0].'.mp4';
						$this->request->data['Archieve']['final_video'] 	= 'Video_final_'.$get_data['Archieve']['id'].'.mp4';
						$this->request->data['Archieve']['duration'] 	= $result->duration;
						$this->request->data['Archieve']['has_audio'] 	= $result->hasAudio;
						$this->request->data['Archieve']['has_video'] 	= $result->hasVideo;
						$this->request->data['Archieve']['updated_by'] 	= 'mycron';
						$this->request->data['Archieve']['modified'] 	= date('Y-m-d H:i:s');
						$this->request->data['Archieve']['is_callback'] 	= 1;
						$this->Archieve->save($this->request->data);
						$i++;
					}
				}
				else{
					echo 'Could not get any info for this archive';	
				}
				$this->loadModel('DeviceToken');
				$this->loadModel('Project');
				//echo $get_data['project_id'];
				$p = $this->Project->findById($get_data['Archieve']['project_id']);
				//print_r($p);
				$chktoken = $this->DeviceToken->findByUserId($p['Project']['client_id']);
				if($chktoken)
				{
					$token = $chktoken['DeviceToken']['device_token'];
					$message = "Recording of project (".$p['Project']['project_name'].") is available";
					$type = "recording_available";
					$notidata = ['project_name'=>$p['Project']['project_name'],'project_id'=>$getdata['Archieve']['project_id'],'archieve_id'=>$getdata['Archieve']['archieve_id'],'session_id'=>$getdata['Archieve']['session_id']];
					$pushres = $this->__send_notification_to_ios($token,$message,$notidata,$type);
					print_r($pushres);
				}
				else
				{
					$retmsg = 'Token not found or invalid';
					$this->save_noti_data(json_encode($notidata),$retmsg,'Invalid or not found');
				}
				echo "success<br>";
		}
	}
	}
}

private function video_create($project_id , $client , $reader , $id)
{

	$this->autoRender = false;
	// echo $this->sendEmail('gaganxicom@gmail.com','info@tapesquad.com','Testing','hi testing');
	// exit;
	$mp31 = $this->convert_mp4_mp3($project_id,$client);
	$mp32 = $this->convert_mp4_mp3($project_id,$reader);
	$finalaudio = $this->merge_audios($project_id,$mp31,$mp32 , $id);
	
	$video_url = $this->merge_video_audio( $project_id,$reader , $finalaudio , $id);
	return $video_url;
}

//1. When an audition notification goes through - if accepted after more than two minutes the call doesn't allow the USER to accept it. Client: It should delete the request after 5 minutes if not accepted.
public function cronDeleteProject()
{
	$this->autoRender = false;
	Configure::write('debug',true);
	$this->loadModel('Project');
	date_default_timezone_set('UTC');
	$currenttime = date('Y-m-d H:i:s');
	$fivemintime = date('Y-m-d H:i:s',strtotime($currenttime.' +5 min'));
	//echo $currenttime.'/'.$fivemintime;
	$p = $this->Project->find('all',['conditions'=>
										[
											' TIMESTAMPDIFF (MINUTE, Project.schedule_time_utc , UTC_TIMESTAMP ) >= 5 ',
											' Project.schedule_time_utc != "0000-00-00 00:00:00" ',
											' Project.status = 2 ',
		]]);
	//print "<pre>";print_r($p);
	foreach($p as $p)
	{
		if($p['Project']['schedule_time_utc'] < $fivemintime)
		{
			$this->Project->delete($p['Project']['id']);
			echo "deleted:".$p['Project']['id'].'<br>';
		}
	}

}

}
