<?php
    use OpenTok\OpenTok;
    use OpenTok\MediaMode;
    use OpenTok\ArchiveMode;
    include('./vendor/autoload.php');
    include('./config.php');

    $opentok = new OpenTok($API_KEY, $API_SECRET);
  echo  $session = $opentok->createSession(array('mediaMode' => MediaMode::ROUTED));
    echo $session->getSessionId();

  
echo 'hello';
// Create a session that attempts to use peer-to-peer streaming:
echo $session = $opentok->createSession();

// A session that uses the OpenTok Media Router, which is required for archiving:
echo $session = $opentok->createSession(array( 'mediaMode' => MediaMode::ROUTED ));

// A session with a location hint:
echo $session = $opentok->createSession(array( 'location' => '12.34.56.78' ));

// An automatically archived session:
echo $sessionOptions = array(
    'archiveMode' => ArchiveMode::ALWAYS,
    'mediaMode' => MediaMode::ROUTED
);
echo $session = $opentok->createSession($sessionOptions);


// Store this sessionId in the database for later use
echo $sessionId = $session->getSessionId();
echo 'hi';
?>