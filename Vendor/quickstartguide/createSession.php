<?php


    require './vendor/autoload.php';
    require './config.php';

    use \OpenTok\OpenTok;
    use \OpenTok\MediaMode;
    use \OpenTok\ArchiveMode;
    
    //echo $API_KEY;
     
 $opentok = new OpenTok($API_KEY,$API_SECRET);
  
//echo 'hello';
// Create a session that attempts to use peer-to-peer streaming:
//echo $session = $opentok->createSession();

// A session that uses the OpenTok Media Router, which is required for archiving:
 $session = $opentok->createSession(array( 'mediaMode' => MediaMode::ROUTED ));

// A session with a location hint:
//echo $session = $opentok->createSession(array( 'location' => '12.34.56.78' ));

// An automatically archived session:
 $sessionOptions = array(
    'archiveMode' => ArchiveMode::ALWAYS,
    'mediaMode' => MediaMode::ROUTED
);
echo $session = $opentok->createSession($sessionOptions);


// Store this sessionId in the database for later use
 $sessionId = $session->getSessionId();

$token = $opentok->generateToken($sessionId);
// Generate a Token by calling the method on the Session (returned from createSession)
$token = $session->generateToken();

// Set some options in a token
$token = $session->generateToken(array(
'role' => Role::PUBLISHER,
'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
'data' => 'name=Eleo'
));
?>