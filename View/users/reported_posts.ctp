
<?php echo $this->Html->css(array('admin/Datatables/dataTables.bootstrap','admin/Datatables/buttons.bootstrap','admin/Datatables/fixedHeader.bootstrap','admin/Datatables/responsive.bootstrap')); 
      echo $this->Html->script(array('admin/Datatables/jquery.dataTables.min','admin/Datatables/dataTables.bootstrap.min','admin/Datatables/dataTables.buttons.min','admin/Datatables/buttons.bootstrap.min','admin/Datatables/buttons.flash.min','admin/Datatables/buttons.html5.min','admin/Datatables/buttons.print.min','admin/Datatables/dataTables.fixedHeader.min','admin/Datatables/dataTables.keyTable.min','admin/Datatables/dataTables.responsive.min','admin/Datatables/responsive.bootstrap','admin/Datatables/dataTables.scroller.min','admin/Datatables/jszip.min','admin/Datatables/pdfmake.min','admin/Datatables/vfs_fonts'));
?>
<style>
.modal{
    background: #fff none repeat scroll 0 0;
    bottom: 52% !important;
    display: none;
    height: 212px !important;
    left: 39% !important;
    top: 39% !important;
    outline: 0 none;
    overflow: hidden;
    position: fixed;
    width: 382px !important;
    z-index: 1050;
}
</style>
<!-- page content -->
      
   <div class="content-inner manageuserstap">
          <!-- Page Header-->
          <!--<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Tables</h2>
            </div>
          </header>-->
          <!-- Breadcrumb-->
          <ul class="breadcrumb">
            <div class="container-fluid">
              <li class="breadcrumb-item"><a href="<?php echo $this->webroot; ?>users/dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active">Manage Users</li>
            </div>
          </ul>
          <section class="tables">   
            <div class="container-fluid">
              <div class="row">
                
                <div class="col-lg-12">
                  <div class="card">
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Users</h3>
                    </div>
                    <div class="card-body user-view-body">
              <p><?php echo $this->Session->flash(); ?></p>
              <?php  ?>
              <div class="table-responsive">
                      <table id="datatable-buttons" class="table table-striped table-bordered" data-order="[[ 6, &quot;desc&quot; ]]">
                        <thead>
                        <tr>
                          <th>Reported By</th>
                          <th>Post Of</th>
                          <th>Post Detail</th>
                          <th>Media Link</th>
                          <th>Reason</th>
                          <th>Status</th>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Actions</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php if(isset($posts) && !empty($posts)){ foreach($posts as $post): ?>

                           <?php if(!empty($post['Post']['url_link']))
                                {
                                  $file_name = $post['Post']['url_link'];
                                }
                                elseif(!empty($post['Post']['full_videolink']))
                                {
                                  $file_name = $post['Post']['full_videolink'];
                                }
                                elseif(!empty($post['Post']['full_path']))
                                {
                                  $file_name = $post['Post']['full_path'];
                                }
                                else
                                {
                                  $file_name = '';
                                  
                                }
                          ?>

                        <tr>
                          <td width="100px"><?php if(!empty($post['User']['firstname']) && !empty($post['User']['lastname'])){ echo $post['User']['firstname'].' '.$post['User']['lastname']; }elseif(!empty($post['User']['firstname'])){ echo $post['User']['firstname']; }else{ echo 'N/A'; } ?>
                          </td>
                           <td width="100px"><?php if(!empty($post['Post']['User']['firstname']) && !empty($post['Post']['User']['lastname'])){ echo $post['Post']['User']['firstname'].' '.$post['Post']['User']['lastname']; }elseif(!empty($post['Post']['User']['firstname'])){ echo $post['Post']['User']['firstname']; }else{ echo 'N/A'; } ?>
                          </td>
                           <td width="100px"><?php if(!empty($post['Post']['post_detail'])){ echo $post['Post']['post_detail']; }else{ echo 'N/A'; } ?></td>
                          <td width="100px"><?php if(!empty($file_name)){  $filetype = explode('.',$file_name); if(!in_array($filetype[3],array('jpeg','jpg','gif'))){ ?>
                                  <a class="fancybox" data-fancybox-type="iframe" data-fancybox-group="grp" title="" href="<?php echo $this->webroot.$file_name; ?>">
                                  <?php //echo $this->webroot.$file_name; ?>Check here
                                  </a>
                                  <?php }else{ ?>
                                  <a class="fancybox" target="_blank" data-fancybox-type="image" data-fancybox-group="grp" title="" href="<?php echo $file_name; ?>">
                                  <?php //echo $this->webroot.$file_name; ?>check here
                                  </a>
                              <?php }}else{  echo 'N/A'; } ?></td>
                          <td width="100px"><?php echo $post['ReportPost']['reason']; ?></td>
                          <td width="100px"><?php echo $post['Post']['status']; ?></td>
                          <td width="50px"><?php echo date('m-d-Y',strtotime($post['ReportPost']['created'])); ?></td>
                          <td width="50px"><?php echo date('H:i:s',strtotime($post['ReportPost']['created'])); ?></td>
                          <td width="50px">

                            <a class="action-class btn btn-info btn-xs" title="Edit" href="javascript:void(0)" onclick="showModal('<?php echo $post['Post']['id']; ?>','<?php echo $post['Post']['status']; ?>')"><i class="fa fa-edit"></i>
                            </a>
                           
                          </td>
                      </tr>

                      <?php endforeach; }else{ ?>
                      <tr>
                        <td colspan="6">No Records found!</td>
                      </tr>
                      <?php } ?>
                    </tbody>
                      </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

             <!======Model code ======>
                    
                      <div class="modal post_modal" id="myModal">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4>Update Status</h4>
                          </div>
                          <div class="modal-body" style="height:88px;">
                            <label style="margin-left:35px;">Post status</label>
                            <select id="status_data" class="" style="width:150px;color:black;margin-left:45px;">
                              <option value="Active">Active</option>
                              <option value="Inactive">Inactive</option>
                            </select></br>

                          </div>

                          <div class="res_msg"></div>
                          <div class="modal-footer" style="padding:8px;">
                          <a href="javascript:void(0);" onclick="update_status()" class="btn btn-primary">Update</a>
                            <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>
                          </div>
                      </div>
          </section>
         
        </div>

<!-- /page content -->


    <!-- Datatables -->
   <style>
	.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
	border-bottom-right-radius: 0;
	border-top-right-radius: 0;
	display: none;
}
   </style>

    <script>
      $(document).ready(function() {
        
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 6, 'desc' ]],
          'columnDefs': [
            { orderable: true, //targets: [0] 
            }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });

    function get_confirm(){
        return confirm('Are you sure, you want to delete this record?');
    }

    function showModal(id,status){

      user_id = id;
      status = status;
      //govt_id_status = govt_id_status;
      //$('#status_data').val(status);
      //$('#govtid_status').val(govt_id_status);
      $('#myModal').modal();
    }

    function update_status(){

      $('.res_msg').html('');
      Id = user_id;
      status = $('#status_data').val();
     
      if(Id!="" && status!="")
      {

        $.ajax(
        {
    
          'url': '<?php echo $this->webroot; ?>users/ajax_update_poststatus',
          'type': 'POST',
          'dataType': 'text',
          'data': {post_id:Id,status:status},
          'success': function(data){
            //alert(data);
            if(data=='success')
            {
              $('#myModal').hide();
              swal("Success!", "status updated successfully!", "success");
           
              swal(
              { 
                  title: "Success!",
                  text: "status updated successfully!",
                  type: "success" 
              },
              function()
              {
                location.reload();
              });

            }else
            {
             
              noty({
                 text: data,
                 layout: 'bottomLeft',
                 closeWith: ['click', 'hover'],
                 type: 'error'
              });
            }
          }

        });
      }
      else
      {
         noty({
           text: 'Please select status',
           layout: 'bottomLeft',
           closeWith: ['click', 'hover'],
           type: 'error'
        });
      }
    }



    </script>
    <!-- /Datatables -->
