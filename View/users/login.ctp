<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TapeSquad Admin</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/material/bootstrap.min.css">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/material/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/material/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="<?php echo $this->webroot; ?>images/favicon.ico">
    <!-- Font Awesome CDN-->
    <!-- you can replace it by local Font Awesome-->
    <script src="https://use.fontawesome.com/99347ac47f.js"></script>
    <!-- Font Icons CSS-->
    <link rel="stylesheet" href="https://file.myfontastic.com/da58YPMQ7U5HY8Rb6UxkNf/icons.css">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <h1><img style="width: 450px;" src="<?php echo $this->webroot; ?>img/logo1.png"></h1>
                  </div>
                  <!--<p>Admin Panel</p>-->
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                  <?php echo $this->Form->create('User',array('url'=>array('controller'=>'users','action'=>'login'),'id'=>'login-form')); ?>
                  <!--<form id="login-form" method="post">-->
                    <div class="form-group">
                      <?php echo $this->Form->input('email',array('type'=>'text','class'=>'input-material','label'=>false,'div'=>false,'id'=>"login-username")); ?>
                      <label for="login-username" class="label-material">Email</label>
                    </div>
                    <div class="form-group">
                    <?php echo $this->Form->input('password',array('type'=>'password','class'=>'input-material','label'=>false,'div'=>false,'id'=>"login-password")); ?>
                    <label for="login-password" class="label-material">Password</label>
                    </div><!--<a id="login" href="index.html" class="btn btn-primary">Login</a>-->
                    <?php echo $this->Form->submit('Login',array('class'=>'btn btn-primary submit')); ?>
                    <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                <!--  </form>-->
                <?php $this->Form->end(); ?>
                <a style="margin-top:20px;" href="<?php echo $this->webroot; ?>users/forgot_password" class="forgot-pass">Forgot Password?</a>
                <?php echo $this->Session->flash(); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
        <!--<p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a></p>-->
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
      </div>
    </div>
   <?php 

    echo $this->Html->script(array('admin/jquery.min','material/tether.min','material/bootstrap','material/bootstrap.min','admin/fastclick','admin/gauge.min','admin/moment.min.js','admin/daterangepicker','admin/icheck.min','admin/froala_editor.min','jquery.validationEngine','jquery.validationEngine-en','admin/jquery.sparkline','admin/date','admin/sweetalert.min','admin/jquery.noty','ckeditor/ckeditor','material/front','material/jquery.validate.min','material/jquery.cookie'));

    ?>
  </body>
</html>