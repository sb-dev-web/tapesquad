
<?php echo $this->Html->css(array('admin/Datatables/dataTables.bootstrap','admin/Datatables/buttons.bootstrap','admin/Datatables/fixedHeader.bootstrap','admin/Datatables/responsive.bootstrap')); 
      echo $this->Html->script(array('admin/Datatables/jquery.dataTables.min','admin/Datatables/dataTables.bootstrap.min','admin/Datatables/dataTables.buttons.min','admin/Datatables/buttons.bootstrap.min','admin/Datatables/buttons.flash.min','admin/Datatables/buttons.html5.min','admin/Datatables/buttons.print.min','admin/Datatables/dataTables.fixedHeader.min','admin/Datatables/dataTables.keyTable.min','admin/Datatables/dataTables.responsive.min','admin/Datatables/responsive.bootstrap','admin/Datatables/dataTables.scroller.min','admin/Datatables/jszip.min','admin/Datatables/pdfmake.min','admin/Datatables/vfs_fonts'));
?>
<style>
.modal{
    background: #fff none repeat scroll 0 0;
    bottom: 52% !important;
    display: none;
    height: 181px !important;
    left: 39% !important;
    top: 39% !important;
    outline: 0 none;
    overflow: hidden;
    position: fixed;
     width: 382px !important;
    z-index: 1050;
}
</style>
<!-- page content -->
       <div class="right_col" style="min-height: 940px;" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
               
              </div>

             
            </div>

            <div class="clearfix"></div>

            <div class="row">
              
             <div class="x_panel">
                  <div class="x_title">
                     <h2>Email Templates<small>List</small></h2>
                     <a class="GoBack btn-info btn btn-sm" href="javascript:void(0)">Go Back</a>
                     <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   
		    <p><?php echo $this->Session->flash(); ?></p>
		    <?php if(isset($templates) && !empty($templates)){ ?>
		    
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          
                          <th>title</th>
                          <th>From Email</th>
                          <th>Subject</th>
                          <th>Status</th>
                  			  <th>Created</th>
                          <th>Actions</th>

			                  </tr>
                      </thead>


                      <tbody>
			                 <?php foreach($templates as $template): ?>
                        <tr>
                         
                          <td width="300px;"><?php if(!empty($template['EmailTemplate']['title'])){ echo $template['EmailTemplate']['title']; }else{ echo 'N/A'; } ?></td>
                          <td width="500px;"><?php if(!empty($template['EmailTemplate']['from_email'])){ echo $template['EmailTemplate']['from_email']; }else{ echo 'N/A'; } ?></td>
                          <td width="50px;"><?php echo $template['EmailTemplate']['subject']; ?></td>
                          <td width="50px;"><?php echo $template['EmailTemplate']['status']; ?></td>
                          <td width="50px;"><?php echo date('d-m-Y',strtotime($template['EmailTemplate']['created'])); ?></td>
			                    <td>
                          <a class="action-class btn btn-info btn-xs" title="Edit" href="<?php echo $this->webroot; ?>users/add_template/<?php echo $template['EmailTemplate']['id']; ?>"><i class="fa fa-edit"></i></a>
                  				<a class="action-class btn btn-danger btn-xs" title="Delete" onclick="return get_confirm();" href="<?php echo $this->webroot; ?>users/delete_template/<?php echo $template['EmailTemplate']['id']; ?>"><i class="fa fa-times"></i></a>
                  			  </td>
                  	  </tr>
			                <?php endforeach; ?>
                      
                      </tbody>
                      
		<?php } else { ?>
			<p>Sorry! No data found</p>
		<?php } ?>
                    </table>
                  </div>
                </div>
              </div>

                <!======Model code ======>
                    
                      <div class="modal post_modal" id="myModal">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h5>Update Status</h5>
                          </div>
                          <div class="modal-body" style="height:55px;">
                            
                            <select id="status_data" class="" style="width:150px;color:black;">
                              <option value="">Select</option>
                              <option value="Active">Active</option>
                              <option value="Inactive">Inactive</option>
                              
                            </select>
                          </div>

                          <div class="res_msg"></div>
                          <div class="modal-footer" style="padding:8px;">
                            <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>
                            <a href="javascript:void(0);" onclick="update_status()" class="btn btn-primary">Update</a>
                          </div>
                      </di
              
                </div>
              </div>
            </div>
          </div>
   </div>
        <!-- /page content -->


    <!-- Datatables -->

  <style>
  	.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
  	border-bottom-right-radius: 0;
  	border-top-right-radius: 0;
  	display: none;
    }
  </style>


    <script>
      $(document).ready(function() {

      $('a.GoBack').click(function(){
          parent.history.back();
          return false;
      });
      
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });

    function get_confirm(){
        return confirm('Are you sure, you want to delete this record?');
    }

    function showModal(id,status){

      content_id = id;
      status = status;
      $('#status_data').val(status);
      $('#myModal').modal();
    }

    function update_status(){

      $('.res_msg').html('');
      Id = content_id;
      status = $('#status_data').val();
      if(Id!="" && status!=""){

        $.ajax({
    
          'url': '<?php echo $this->webroot; ?>contents/ajax_update_contentstatus',
          'type': 'POST',
          'dataType': 'text',
          'data': {content_id:Id,status:status},
          'success': function(data){

            if(data=='success'){
              $('#myModal').hide();
              swal("Success!", "status updated successfully!", "success");
           
              swal({ 
                  title: "Success!",
                  text: "status updated successfully!",
                  type: "success" 
                },
                function(){
                 
                 location.reload();
              });
            }else{
             
              noty({
                 text: data,
                 layout: 'bottomLeft',
                 closeWith: ['click', 'hover'],
                 type: 'error'
              });
            }
          }

        });
      }else{
         noty({
           text: 'Please select status',
           layout: 'bottomLeft',
           closeWith: ['click', 'hover'],
           type: 'error'
        });
      }
    }

    </script>

    <!-- /Datatables -->
