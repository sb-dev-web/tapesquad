<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TapeSquad Admin</title>

    <!-- Bootstrap -->
    <link href="<?php echo $this->webroot; ?>css/admin/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo $this->webroot; ?>css/admin/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo $this->webroot; ?>css/admin/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo $this->webroot; ?>css/admin/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo $this->webroot; ?>css/admin/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <?php echo $this->Form->create('User',array('url'=>array('controller'=>'users','action'=>'login'))); ?>
              <h1>TapeSquad Admin Login</h1>
              <div>
               <?php echo $this->Form->input('email',array('type'=>'text','class'=>'form-control','placeholder'=>'Username','label'=>false)); ?>
              </div>
              <div>
               <?php echo $this->Form->input('password',array('type'=>'password','class'=>'form-control','placeholder'=>'Password','label'=>false)); ?>
              </div>
              <div>
               <?php echo $this->Form->submit('Log in',array('class'=>'btn btn-default submit')); ?>
                <a class="reset_pass" href="<?php echo $this->webroot; ?>users/reset_password">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

	<?php $this->Form->end(); ?>
	<?php echo $this->Session->flash(); ?>
          </section>
        </div>

     
      </div>
    </div>
  </body>
</html>
