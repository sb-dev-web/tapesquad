
<?php echo $this->Html->css(array('admin/Datatables/dataTables.bootstrap','admin/Datatables/buttons.bootstrap','admin/Datatables/fixedHeader.bootstrap','admin/Datatables/responsive.bootstrap')); 
      echo $this->Html->script(array('admin/Datatables/jquery.dataTables.min','admin/Datatables/dataTables.bootstrap.min','admin/Datatables/dataTables.buttons.min','admin/Datatables/buttons.bootstrap.min','admin/Datatables/buttons.flash.min','admin/Datatables/buttons.html5.min','admin/Datatables/buttons.print.min','admin/Datatables/dataTables.fixedHeader.min','admin/Datatables/dataTables.keyTable.min','admin/Datatables/dataTables.responsive.min','admin/Datatables/responsive.bootstrap','admin/Datatables/dataTables.scroller.min','admin/Datatables/jszip.min','admin/Datatables/pdfmake.min','admin/Datatables/vfs_fonts'));
?>
<style>
.fancybox > img {
    height: 46px;
    width: 46px;
}

.modal{
    background: #fff none repeat scroll 0 0;
    bottom: 52% !important;
    display: none;
    height: 212px !important;
    left: 39% !important;
    top: 39% !important;
    outline: 0 none;
    overflow: hidden;
    position: fixed;
    width: 382px !important;
    z-index: 1050;
}
</style>
<!-- page content -->
      
   <div class="content-inner manageuserstap">
          <!-- Page Header-->
          <!--<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Tables</h2>
            </div>
          </header>-->
          <!-- Breadcrumb-->

          <ul class="breadcrumb">
            <div class="container-fluid">
              <li class="breadcrumb-item"><a href="<?php echo $this->webroot; ?>users/dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active">Manage Disputes</li>
            </div>
          </ul>
          <section class="tables">   
            <div class="container-fluid">
              <div class="row">
                
                <div class="col-lg-12">
                  <div class="card">
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Disputes</h3>
                    </div>
                    <div class="card-body user-view-body">
              <p><?php echo $this->Session->flash(); ?></p>
              <?php  ?>
              <div class="table-responsive">
                      <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                          <th>Project Name</th>
                          <th>Reader Name</th>
                          <th>Client Name</th>
                          <th>Message</th>
                          <th>Images</th>
                          <th>Status</th>
                          <th>Payment Status</th>
                          <th>Created</th>
                          <th>Modified</th>
                          <th>Actions</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php if(isset($disputes) && !empty($disputes)){ foreach($disputes as $dispute): ?>
                        <tr>
                          <td width="100px"><?php if(!empty($dispute['Project']['project_name'])){ echo $dispute['Project']['project_name']; }else{ echo 'N/A'; } ?></td>
                          <td width="100px"><?php if(!empty($dispute['Reader']['firstname']) && !empty($dispute['Reader']['lastname'])){ echo $dispute['Reader']['firstname'].' '.$dispute['Reader']['lastname']; }elseif(!empty($dispute['Client']['firstname'])){ echo $dispute['Client']['firstname']; }else{ echo 'N/A'; } ?>
                          </td>
                          <td width="100px"><?php if(!empty($dispute['Client']['firstname']) && !empty($dispute['Client']['lastname'])){ echo $dispute['Client']['firstname'].' '.$dispute['Client']['lastname']; }elseif(!empty($dispute['Client']['firstname'])){ echo $dispute['Client']['firstname']; }else{ echo 'N/A'; } ?>
                          </td>
                          <td width="100px"><?php if(!empty($dispute['Dispute']['message'])){ echo $dispute['Dispute']['message']; }else{ echo 'N/A'; } ?></td>
                          <td class="disputeimages" width="200px"><?php if(!empty($dispute['Dispute']['image1'])){ ?>
                                <!--<img src = "<?php echo $this->webroot; ?><?php echo $dispute['Dispute']['image1']; ?>">-->
                                <a class="fancybox" data-fancybox-type="image" data-fancybox-group="grp" title="Dispute" href="<?php echo $this->webroot; ?><?php echo $dispute['Dispute']['image1']; ?>"><img src = "<?php echo $this->webroot; ?><?php echo $dispute['Dispute']['image1']; ?>"></a>
                                <?php }elseif(!empty($dispute['Dispute']['image2'])) { ?>
                                <a class="fancybox" data-fancybox-type="image" data-fancybox-group="grp" title="Dispute" href="<?php echo $this->webroot; ?><?php echo $dispute['Dispute']['image2']; ?>"><img src = "<?php echo $this->webroot; ?><?php echo $dispute['Dispute']['image2']; ?>"></a>
                                <?php }elseif(!empty($dispute['Dispute']['image3'])) { ?>
                                <a class="fancybox" data-fancybox-type="image" data-fancybox-group="grp" title="Dispute" href="<?php echo $this->webroot; ?><?php echo $dispute['Dispute']['image3']; ?>"><img src = "<?php echo $this->webroot; ?><?php echo $dispute['Dispute']['image3']; ?>"></a>
                                <?php } ?>
                          </td>
                         
                          
                          <td width="100px"><?php if($dispute['Dispute']['status']==1){ ?>
                                                    <span style="color:orange;"><?php echo 'Open'; ?></span>
                                            <?php } elseif($dispute['Dispute']['status']==2){ ?>
                                                    <span style="color:hsl(225, 70%, 30%);"><?php echo 'In Process'; ?></span>
                                            <?php } elseif($dispute['Dispute']['status']==3){ ?>
                                                    <span style="color:green;"><?php echo 'Resolved'; ?></span>
                                            <?php } elseif($dispute['Dispute']['status']==4){ ?>
                                                    <span style="color:green;"><?php echo 'Refunded'; ?></span>
                                            <?php } elseif($dispute['Dispute']['status']==5){ ?>
                                                    <span style="color:red;"><?php echo 'Declined'; ?></span>
                                            <?php } ?>
                          </td>

                          <td width="100px"><?php if(!empty($dispute['Dispute']['payment_status'])){ echo $dispute['Dispute']['payment_status']; }else{ echo 'N/A'; } ?></td>
                          <td width="100px"><?php echo date('d-m-Y',strtotime($dispute['Dispute']['created'])); ?></td>
                          <td width="100px"><?php echo date('d-m-Y',strtotime($dispute['Dispute']['modified'])); ?></td>
       
                          <td>
                            <a class="btn btn-info btn-xs" title="Edit" href="javascript:void(0)" onclick="showModal('<?php echo $dispute['Dispute']['id']; ?>','<?php echo $dispute['Dispute']['status']; ?>')"><i class="fa fa-edit"></i></a>
                            <?php if($dispute['Dispute']['status']==2){ ?>
                              <a class="btn btn-success btn-xs" title="Get payment" href="javascript:void(0)" onclick="getPayment('<?php echo $dispute['Dispute']['id'];?>','payment')"><i class="fa fa-cc-stripe"></i>Get Payment</a>
                              <a class="btn btn-success btn-xs" title="Get payment" href="javascript:void(0)" onclick="getPayment('<?php echo $dispute['Dispute']['id'];?>','refund')"><i class="fa fa-cc-stripe"></i>Refund Amount</a>
                            <?php } ?>
                            <a class="btn btn-danger btn-xs" title="Delete" onclick="return get_confirm();" href="<?php echo $this->webroot; ?>users/delete_dispute/<?php echo $dispute['Dispute']['id']; ?>"><i class="fa fa-times"></i>
                              </a>
                            
                            
                          </td>
                      </tr>

                      <?php endforeach; }else{ ?>
                      <tr>
                        <td>No Records found!</td>
                      </tr>
                      <?php } ?>
                    </tbody>
                      </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

             <!======Model code ======>
                    
                      <div class="modal post_modal" id="myModal">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4>Update Status</h4>
                          </div>
                          <div class="modal-body" style="height:88px;">
                            <label style="margin-left:35px;">Status</label>
                            <select id="status_data" class="" style="width:150px;color:black;margin-left:45px;">
                                <option value="">Select</option>
                                <option value="2">In Process</option>
                                <option value="3">Resolved</option>
                                <option value="4">Refunded</option>
                                <option value="5">Declined</option>
                            </select></br>

                          </div>

                          <div class="res_msg"></div>
                          <div class="modal-footer" style="padding:8px;">
                          <a href="javascript:void(0);" onclick="update_status()" class="btn btn-primary">Update</a>
                            <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>
                            
                          </div>
                      </div>
          </section>
         
        </div>

<!-- /page content -->


    <!-- Datatables -->
  <style>
	.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle)
  {
    	border-bottom-right-radius: 0;
    	border-top-right-radius: 0;
    	display: none;
  }
  </style>

    <script>
      $(document).ready(function(){
        
        $('#flashMessage').delay(5000).fadeOut('slow');
        $(".fancybox").fancybox({
          openEffect: 'none',
          closeEffect: 'none',
          nextEffect: 'none',
          prevEffect: 'none',
          padding: 0,
          margin: [20, 0, 20, 0]
        });

        var handleDataTableButtons = function(){
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });

    function get_confirm(){
        return confirm('Are you sure, you want to delete this record?');
    }

    function showModal(id,status){

      dispute_id = id;
      status = status;
      if(status==1){
        status = '';
      }
      $('#status_data').val(status);
      $('#myModal').modal();
    }

    function update_status(){

        $('.res_msg').html('');
        Id = dispute_id;
        status = $('#status_data').val();
       
        if(Id!="" && status!=""){

          $.ajax({
      
            'url': '<?php echo $this->webroot; ?>users/ajax_update_disputestatus',
            'type': 'POST',
            'dataType': 'text',
            'data': {dispute_id:Id,status:status},
            'success': function(data){
              //alert(data);
              if(data=='success'){
                $('#myModal').hide();
                swal("Success!", "status updated successfully!", "success");
             
                swal({ 
                    title: "Success!",
                    text: "status updated successfully!",
                    type: "success" 
                  },
                  function(){
                   
                   location.reload();
                });
              }else{
               
                noty({
                   text: data,
                   layout: 'bottomLeft',
                   closeWith: ['click', 'hover'],
                   type: 'error'
                });
              }
            }

          });
        }else{
           noty({
             text: 'Please select status',
             layout: 'bottomLeft',
             closeWith: ['click', 'hover'],
             type: 'error'
          });
        }
    }

    function getPayment(dispute_id,type){

        $('.res_msg').html('');
        if(type=='payment'){
          var url = '<?php echo $this->webroot; ?>users/getPayment';
        }else if(type=='refund'){  
          var url = '<?php echo $this->webroot; ?>users/makeRefund';
        }
        $.ajax({
      
            'url': url,
            'type': 'POST',
            'dataType': 'text',
            'data': {dispute_id:dispute_id,type:type},
            'success': function(data){
              
              if(data=='success'){

                swal("Success!", "Payment done successfully!", "success");
                swal({ 
                    title: "Success!",
                    text: "Payment done successfully!",
                    type: "success" 
                  });

              }else{
               
                noty({
                   text: data,
                   layout: 'bottomLeft',
                   closeWith: ['click', 'hover'],
                   type: 'error'
                });

              }
            }

          });
    }

  </script>
  <!-- /Datatables -->
