<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<?php echo $this->Html->css(array('admin/Datatables/dataTables.bootstrap','admin/Datatables/buttons.bootstrap','admin/Datatables/fixedHeader.bootstrap','admin/Datatables/responsive.bootstrap')); 
      echo $this->Html->script(array('admin/Datatables/jquery.dataTables.min','admin/Datatables/dataTables.bootstrap.min','admin/Datatables/dataTables.buttons.min','admin/Datatables/buttons.bootstrap.min','admin/Datatables/buttons.flash.min','admin/Datatables/buttons.html5.min','admin/Datatables/buttons.print.min','admin/Datatables/dataTables.fixedHeader.min','admin/Datatables/dataTables.keyTable.min','admin/Datatables/dataTables.responsive.min','admin/Datatables/responsive.bootstrap','admin/Datatables/dataTables.scroller.min','admin/Datatables/jszip.min','admin/Datatables/pdfmake.min','admin/Datatables/vfs_fonts'));
?>



<?php echo $this->Html->script(array('material/charts-home')); ?>

<style>
.modal{
    background: #fff none repeat scroll 0 0;
    bottom: 52% !important;
    display: none;
    height: 212px !important;
    left: 39% !important;
    top: 39% !important;
    outline: 0 none;
    overflow: hidden;
    position: fixed;
    width: 382px !important;
    z-index: 1050;
}
</style>
<!-- page content -->
     <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Dashboard</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
          <section class="dashboard-counts no-padding-bottom">
            <div class="container-fluid">
              <div class="row bg-white has-shadow">
                <!-- Item -->
                <div class="col-xl-3 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-violet"><i class="fa fa-usd"></i></div>
                    <div class="title"><span>Total<br>Revenue</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 25%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-violet"></div>
                      </div>
                    </div>
                    <div class="number"><strong><?php if($total_revenue==''){ echo '0';}else{ echo $total_revenue; } ?></strong></div>
                  </div>
                </div>
                <!-- Item -->

                <!-- Item -->
                <div class="col-xl-3 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-green"><i class="fa fa-usd"></i></div>
                    <div class="title"><span>Total<br>Income</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 25%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-green"></div>
                      </div>
                    </div>
                    <div class="number"><strong><?php if($tape_income==''){ echo '0';}else{ echo $tape_income; } ?></strong></div>
                  </div>
                </div>
                <!-- Item -->

                <div class="col-xl-3 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-red"><i class="icon-user"></i></div>
                    <div class="title"><span>Total<br>Users</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 25%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-red"></div>
                      </div>
                    </div>
                    <div class="number"><strong><?php echo $total_users; ?></strong></div>
                  </div>
                </div>
                
                <div class="col-xl-3 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-orange"><i class="icon-user"></i></div>
                    <div class="title"><span>Total<br>Readers</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 25%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-orange"></div>
                      </div>
                    </div>
                    <div class="number"><strong><?php echo $total_readers; ?></strong></div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
          <?= $this->element('calendar') ?>
          </section>
          <!-- Dashboard Header Section    -->
          <section class="dashboard-header">
            <div class="container-fluid">
              <div class="row">
                <!-- Statistics -->
                <div class="statistics col-lg-3 col-12">
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-red"><i class="fa fa-tasks"></i></div>
                    <div class="text"><strong><?php echo $total_project_requests; ?></strong><br><small>Project Requests</small></div>
                  </div>
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-orange"><i class="fa fa-clock-o"></i></div>
                    <div class="text"><strong><?php echo $pending_projects; ?></strong><br><small>Pending Requests</small></div>
                  </div>
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-green"><i class="fa fa-thumbs-up"></i></div>
                    <div class="text"><strong><?php echo $completed_projects; ?></strong><br><small>Completed Requests</small></div>
                  </div>
                </div>
                <!-- Line Chart            -->
                <div class="chart col-lg-6 col-12">
                  <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                    <canvas id="lineCahrt"></canvas>
                  </div>
                </div>
                <div class="chart col-lg-3 col-12">
                  <!-- Bar Chart   -->
                  <div class="bar-chart has-shadow bg-white">
                    <div class="title"><strong class="text-violet"><?php echo $total_project_requests; ?></strong><br><small>Project Requests</small></div>
                    <canvas id="barChartHome"></canvas>
                  </div>
                  <!-- Numbers-->
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-green"><i class="fa fa-line-chart"></i></div>
                    <div class="text"><strong><?php echo $success_rate; ?>%</strong><br><small>Success Rate</small></div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section class="dashboard-header">
            <div class="container-fluid">
              <div class="row">
                
                
                <!-- <div class="chart col-lg-6 col-12">
                  <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                    <canvas id="sessionchart"></canvas>
                  </div>
                </div> -->

               
                <div class="chart col-lg-12 col-12">
                  <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                    <canvas id="revenuechart" height="400px"></canvas>
                  </div>
                </div>
                
              </div>

             

            
</div> </section> <section class="dashboard-header">
            <div class="container-fluid">

              <div class="row">
                
                
                <div class="chart col-lg-6 col-12"><h6> Sessions Data of last 12 months  </h6>
                  <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                    
                    <canvas id="monthlysessionchart"></canvas>
                  </div>
                </div>

               
                <div class="chart col-lg-6 col-12"><h6> Total Posts of last 12 months  </h6>
                  <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                  
                    <canvas id="monthlypostchart"></canvas>
                  </div>
                </div>
                
              </div>
</div>

</section>

<section class="dashboard-header">
            <div class="container-fluid">

              <div class="row">
                
                
                <div class="chart col-lg-6 col-12"><h6> Sessions Data of last 10 Days  </h6>
                  <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                    
                    <canvas id="dailysessionchart"></canvas>
                  </div>
                </div>

               
                <div class="chart col-lg-6 col-12"><h6> Total Posts of last 10 Days  </h6>
                  <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                  
                    <canvas id="dailypostchart"></canvas>
                  </div>
                </div>
                
              </div>
</div>

</section>

          <section class="tables">   
            <div class="container-fluid">
              <div class="row">
                
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      
                      
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Readers idle for 3 months:  <span style="color:#fbb917; font-weight: bold;"><?php echo $totalIdealReaders; ?></span></h3>
                    </div>
                    <div class="card-body user-view-body ">
                    

              <p><?php echo $this->Session->flash(); ?></p>
              <?php  ?>
                    <div class="table-responsive">
                      <table id="datatable-buttons" class="table table-striped table-bordered" style="min-width: 1235px !important;" data-page-length="10" data-order="[[9, &quot;desc&quot; ]]">
                        <thead>
                        <tr>
                          
                          <th>Name</th>
                          <th>Promo Code</th>
                          <th>Email</th>
                          <th>Actor Account</th>
                          <th>Phone No</th>
                          <th>Govt ID Status</th>
                          <th>Reader status</th>
                          <th>Months</th>
                          <th>Status</th>
                          <th>Created</th>
                          <!--<th>Modified</th>-->
                          <th>Actions</th>

                        </tr>
                      </thead>

                      <tbody>
                        <?php if(isset($readersArr) && !empty($readersArr)){ foreach($readersArr as $user): ?>
                        <tr>
                         
                          <td width="150px"><?php if(!empty($user['User']['firstname']) && !empty($user['User']['lastname'])){ echo $user['User']['firstname'].' '.$user['User']['lastname']; }elseif(!empty($user['User']['firstname'])){ echo $user['User']['firstname']; }else{ echo 'N/A'; } ?>
                          </td>
                          <td width="100px"><?php if(!empty($user['User']['promo_code'])){ echo $user['User']['promo_code']; }else{ echo 'N/A'; } ?></td>
                          <td width="100px"><?php if(!empty($user['User']['email'])){ echo $user['User']['email']; }else{ echo 'N/A'; } ?></td>
                          <td width="100px"><?php if(!empty($user['User']['other_account'])){ echo $user['User']['other_account']; }else{ echo 'N/A'; } ?></td>
                          <td width="100px"><?php if(!empty($user['User']['phone_no'])){ echo $user['User']['phone_no']; }else{ echo 'N/A'; } ?></td>
                          <td width="100px"><?php echo $user['User']['govt_id_status']; ?></td>
                          <td width="100px"><?php if(!empty($user['Project'])){ echo '<button class="btn btn-success btn-sm">Not Idle</button>'; }else{ echo '<button class="btn btn-danger btn-sm">Idle</button>'; } ?></td>
                          <td width="50px"><?php echo $user['User']['ideal_month']; ?></td>
                          <td width="100px"><?php echo $user['User']['status']; ?></td>
                          <td width="100px"><?php echo date('m-d-Y',strtotime($user['User']['created'])); ?></td>
                          <!--<td width="100px"><?php echo date('m-d-Y',strtotime($user['User']['modified'])); ?></td>-->
       
                         <td width="200px">
                          <a class="action-class btn btn-success btn-xs" data-toogle="tooltip" title="View" data-original-title='view details' href="<?php echo $this->webroot; ?>users/view_reader/<?php echo $user['User']['id']; ?>"><i class="fa fa-eye"></i>
                          </a>
                         <a class="action-class btn btn-info btn-xs" title="Edit" href="javascript:void(0)" onclick="showModal('<?php echo $user['User']['id']; ?>','<?php echo $user['User']['status']; ?>','<?php echo $user['User']['govt_id_status']; ?>')"><i class="fa fa-edit"></i>
                            </a>
                          <a class="action-class btn btn-danger btn-xs" title="Delete" onclick="return get_confirm();" href="<?php echo $this->webroot; ?>users/delete_user/<?php echo $user['User']['id']; ?>"><i class="fa fa-times"></i></a>
                        </td>
                      </tr>

                      <?php endforeach; }else{ ?>
                      <tr>
                        <td>No Records found!</td>
                      </tr>
                      <?php } ?>
                    </tbody>
                      </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

             <!======Model code ======>
                    
                        <div class="modal post_modal" id="myModal">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4>Update Status</h4>
                          </div>
                          <div class="modal-body" style="height:88px;">
                            <label style="margin-left:35px;">User status</label>
                            <select id="status_data" class="" style="width:150px;color:black;margin-left:45px;">
                              <option value="">Select</option>
                              <option value="Active">Active</option>
                              <option value="Inactive">Inactive</option>
                              
                            </select></br>

                            <label style="margin-left:35px;">Govt Id status</label>
                            <select id="govtid_status" class="" style="width:150px;color:black;margin-left: 28px;">
                              <option value="">Select</option>
                              <option value="Pending">Pending</option>
                              <option value="Approved">Approved</option>
                              
                            </select>
                          </div>

                          <div class="res_msg"></div>
                          <div class="modal-footer" style="padding:8px;">
                          <a href="javascript:void(0);" onclick="update_status()" class="btn btn-primary">Update</a>
                            <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>
                            
                          </div>
                      </div>

          </section>
         
          
         
        </div>
        <!-- /page content -->

<script>
    var monthData = '<?php echo json_encode($months); ?>';
    var usersData = '<?php echo json_encode($usersdata); ?>';
    var readersData = '<?php echo json_encode($readersdata); ?>';
    var sixtysessionData = '<?php echo json_encode($sixties); ?>';
    var thirtysessionData = '<?php echo json_encode($thirties); ?>';
    var revenueData = '<?php echo json_encode($revenues); ?>';
    var monthsArr = '<?php echo json_encode($monthsArr); ?>';
    var projectRequests = '<?php echo json_encode($ProjectRequests); ?>';
    
    $(document).ready(function() {
     
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          //'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: true, targets: [7] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });

   
    // ------------------------------------------------------- //
    // Line Chart
    // ------------------------------------------------------ //
    var legendState = true;
    if ($(window).outerWidth() < 576) {
        legendState = false;
    }

    var LINECHART = $('#lineCahrt');
    
    var myLineChart = new Chart(LINECHART, {
        type: 'line',
        options: {
            scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    }
                }],
                yAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    }
                }]
            },
            legend: {
                display: legendState
            }
        },
        data: {
            labels:  JSON.parse('<?= json_encode($last12m) ?>'),
            datasets: [
                {
                    label: "Users",
                    fill: true,
                    lineTension: 0,
                    backgroundColor: "transparent",
                    borderColor: '#f15765',
                    pointBorderColor: '#da4c59',
                    pointHoverBackgroundColor: '#da4c59',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "#44c384",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBorderColor: "#fff",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: JSON.parse(usersData),
                    spanGaps: false
                },
                {
                    label: "Readers",
                    fill: true,
                    lineTension: 0,
                    backgroundColor: "transparent",
                    borderColor: "#54e69d",
                    pointHoverBackgroundColor: "#44c384",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "#44c384",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBorderColor: "#fff",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: JSON.parse(readersData),
                    spanGaps: false
                }
            ]
        }
    });

    


     // ------------------------------------------------------- //
    // Line Chart for Revenue 
    // ------------------------------------------------------ //
    var legendState = true;
    if ($(window).outerWidth() < 576) {
        legendState = false;
    }

    var revenuechart = $('#revenuechart');
    
    var myLineChart2 = new Chart(revenuechart, {
        type: 'line',
        options: {
            scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    }
                }],
                yAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    }
                }]
            },
            legend: {
                display: legendState
            }
        },
        data: {
            labels: JSON.parse('<?= json_encode($last12m) ?>'),
            datasets: [
                {
                    label: "Monthly Revenue",
                    fill: true,
                    lineTension: 0,
                    backgroundColor: "transparent",
                    borderColor: '#08a9ee',
                    pointBorderColor: '#084eee',
                    pointHoverBackgroundColor: '#084eee',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "#08a9ee",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBorderColor: "#fff",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: JSON.parse(revenueData),
                    spanGaps: false
                }
                
            ]
        }
    });




    // ------------------------------------------------------- //
    // session numbers for each MONTH: 30 60, 45 an 75 mins
    // ------------------------------------------------------ //
    var legendState = true;
    if ($(window).outerWidth() < 576) {
        legendState = false;
    }

    var sessionchart = $('#monthlysessionchart');
    
    var newLineChart1 = new Chart(sessionchart, {
        type: 'line',
        options: {
            scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    }
                }],
                yAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    }
                }]
            },
            legend: {
                display: legendState
            }
        },
        data: {
            labels: JSON.parse('<?= json_encode($last12m) ?>'),
            datasets: [
                {
                    label: "30 Mins Sessions",
                    fill: true,
                    lineTension: 0,
                    backgroundColor: "transparent",
                    borderColor: '#f15765',
                    pointBorderColor: '#da4c59',
                    pointHoverBackgroundColor: '#da4c59',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "#44c384",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBorderColor: "#fff",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: JSON.parse('<?= json_encode($data30) ?>'),
                    spanGaps: false
                },
                {
                    label: "60 Mins Sessions",
                    fill: true,
                    lineTension: 0,
                    backgroundColor: "transparent",
                    borderColor: "#0000FF",
                    pointHoverBackgroundColor: "#9400D3",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "#9400D3",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBorderColor: "#fff",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: JSON.parse('<?= json_encode($data60) ?>'),
                    spanGaps: false
                },
                {
                    label: "45 Mins Sessions",
                    fill: true,
                    lineTension: 0,
                    backgroundColor: "transparent",
                    borderColor: "#FBBF2D",
                    pointHoverBackgroundColor: "#9400D3",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "#9400D3",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBorderColor: "#fff",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: JSON.parse('<?= json_encode($data40) ?>'),
                    spanGaps: false
                },
                {
                    label: "75 Mins Sessions",
                    fill: true,
                    lineTension: 0,
                    backgroundColor: "transparent",
                    borderColor: "#00A65A",
                    pointHoverBackgroundColor: "#9400D3",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "#9400D3",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBorderColor: "#fff",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: JSON.parse('<?= json_encode($data70) ?>'),
                    spanGaps: false
                }
            ]
        }
    });


    // ------------------------------------------------------- //
    // session numbers for each MONTH: 30 60, 45 an 75 mins
    // ------------------------------------------------------ //
    var legendState = true;
    if ($(window).outerWidth() < 576) {
        legendState = false;
    }

    var sessionchart = $('#dailysessionchart');
    
    var newLineChart1 = new Chart(sessionchart, {
        type: 'line',
        options: {
            scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    }
                }],
                yAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    }
                }]
            },
            legend: {
                display: legendState
            }
        },
        data: {
            labels: JSON.parse('<?= json_encode($lastWeek) ?>'),
            datasets: [
                {
                    label: "30 Mins Sessions",
                    fill: true,
                    lineTension: 0,
                    backgroundColor: "transparent",
                    borderColor: '#f15765',
                    pointBorderColor: '#da4c59',
                    pointHoverBackgroundColor: '#da4c59',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "#44c384",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBorderColor: "#fff",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: JSON.parse('<?= json_encode($week30) ?>'),
                    spanGaps: false
                },
                {
                    label: "60 Mins Sessions",
                    fill: true,
                    lineTension: 0,
                    backgroundColor: "transparent",
                    borderColor: "#0000FF",
                    pointHoverBackgroundColor: "#9400D3",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "#9400D3",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBorderColor: "#fff",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: JSON.parse('<?= json_encode($week60) ?>'),
                    spanGaps: false
                },
                {
                    label: "45 Mins Sessions",
                    fill: true,
                    lineTension: 0,
                    backgroundColor: "transparent",
                    borderColor: "#FBBF2D",
                    pointHoverBackgroundColor: "#9400D3",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "#9400D3",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBorderColor: "#fff",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: JSON.parse('<?= json_encode($week40) ?>'),
                    spanGaps: false
                },
                {
                    label: "75 Mins Sessions",
                    fill: true,
                    lineTension: 0,
                    backgroundColor: "transparent",
                    borderColor: "#00A65A",
                    pointHoverBackgroundColor: "#9400D3",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "#9400D3",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBorderColor: "#fff",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: JSON.parse('<?= json_encode($week70) ?>'),
                    spanGaps: false
                }
            ]
        }
    });


    // ------------------------------------------------------- //
    // AVERAGE POSTS(from FEED) numbers for the MONTH.
    // ------------------------------------------------------ //
    var legendState = true;
    if ($(window).outerWidth() < 576) {
        legendState = false;
    }

    var sessionchart = $('#monthlypostchart');
    
    var newLineChart1 = new Chart(sessionchart, {
        type: 'line',
        options: {
            scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    }
                }],
                yAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    }
                }]
            },
            legend: {
                display: legendState
            }
        },
        data: {
            labels: JSON.parse('<?= json_encode($last12m) ?>'),
            datasets: [
                {
                    label: "No. Of  Posts",
                    fill: true,
                    lineTension: 0,
                    backgroundColor: "transparent",
                    borderColor: '#f15765',
                    pointBorderColor: '#da4c59',
                    pointHoverBackgroundColor: '#da4c59',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "#44c384",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBorderColor: "#fff",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: JSON.parse('<?= json_encode($posts) ?>'),
                    spanGaps: false
                },
                
            ]
        }
    });


 // ------------------------------------------------------- //
    // DAILY POSTS(from FEED) number. Posts can be: images, video or text (all)
    // ------------------------------------------------------ //
    var legendState = true;
    if ($(window).outerWidth() < 576) {
        legendState = false;
    }

    var sessionchart = $('#dailypostchart');
    
    var newLineChart1 = new Chart(sessionchart, {
        type: 'line',
        options: {
            scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    }
                }],
                yAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    }
                }]
            },
            legend: {
                display: legendState
            }
        },
        data: {
            labels: JSON.parse('<?= json_encode($lastWeek) ?>'),
            datasets: [
                {
                    label: "No. Of  Posts",
                    fill: true,
                    lineTension: 0,
                    backgroundColor: "transparent",
                    borderColor: '#f15765',
                    pointBorderColor: '#da4c59',
                    pointHoverBackgroundColor: '#da4c59',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "#44c384",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBorderColor: "#fff",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: JSON.parse('<?= json_encode($wposts) ?>'),
                    spanGaps: false
                },
                
            ]
        }
    });




    // ------------------------------------------------------- //
    // Bar Chart
    // ------------------------------------------------------ //
    var BARCHARTHOME = $('#barChartHome');
    var barChartHome = new Chart(BARCHARTHOME, {
        type: 'bar',
        options:
        {
            scales:
            {
                xAxes: [{
                    display: false
                }],
                yAxes: [{
                    display: false
                }],
            },
            legend: {
                display: false
            }
        },
        data: {
            labels: JSON.parse(monthsArr),//["January", "February", "March", "April", "May", "June", "July", "August", "September", "November", "December"],
            datasets: [
                {
                    label: "Requests",
                    backgroundColor: [
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)'
                    ],
                    borderColor: [
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)',
                        'rgb(121, 106, 238)'
                    ],
                    borderWidth: 1,
                    data: JSON.parse(projectRequests)//[35, 49, 55, 68, 81, 95, 85, 40, 30, 27, 22, 15]
                }
            ]
        }
    });

    function get_confirm(){
        return confirm('Are you sure, you want to delete this record?');
    }

    function showModal(id,status,govt_id_status){

      user_id = id;
      status = status;
      govt_id_status = govt_id_status;
      $('#status_data').val(status);
      $('#govtid_status').val(govt_id_status);
      $('#myModal').modal();
    }

   

    function update_status(){

      $('.res_msg').html('');
      Id = user_id;
      status = $('#status_data').val();
      govtid_status = $('#govtid_status').val();
      if(Id!="" && status!="" && govtid_status!=""){

        $.ajax({
    
          'url': '<?php echo $this->webroot; ?>users/ajax_update_userstatus',
          'type': 'POST',
          'dataType': 'text',
          'data': {user_id:Id,status:status,govtid_status:govtid_status},
          'success': function(data){
            //alert(data);
            if(data=='success'){
              $('#myModal').hide();
              swal("Success!", "status updated successfully!", "success");
           
              swal({ 
                  title: "Success!",
                  text: "status updated successfully!",
                  type: "success" 
                },
                function(){
                 
                 location.reload();
              });
            }else{
             
              noty({
                 text: data,
                 layout: 'bottomLeft',
                 closeWith: ['click', 'hover'],
                 type: 'error'
              });
            }
          }

        });
      }else{
         noty({
           text: 'Please select status',
           layout: 'bottomLeft',
           closeWith: ['click', 'hover'],
           type: 'error'
        });
      }
    }
     
    </script>