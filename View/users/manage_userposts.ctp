<?php echo $this->Html->css(array('admin/Datatables/dataTables.bootstrap','admin/Datatables/buttons.bootstrap','admin/Datatables/fixedHeader.bootstrap','admin/Datatables/responsive.bootstrap')); 
      echo $this->Html->script(array('admin/Datatables/jquery.dataTables.min','admin/Datatables/dataTables.bootstrap.min','admin/Datatables/dataTables.buttons.min','admin/Datatables/buttons.bootstrap.min','admin/Datatables/buttons.flash.min','admin/Datatables/buttons.html5.min','admin/Datatables/buttons.print.min','admin/Datatables/dataTables.fixedHeader.min','admin/Datatables/dataTables.keyTable.min','admin/Datatables/dataTables.responsive.min','admin/Datatables/responsive.bootstrap','admin/Datatables/dataTables.scroller.min','admin/Datatables/jszip.min','admin/Datatables/pdfmake.min','admin/Datatables/vfs_fonts'));
?>
<style>
.modal{
    background: #fff none repeat scroll 0 0;
    bottom: 52% !important;
    display: none;
    height: 212px !important;
    left: 39% !important;
    top: 39% !important;
    outline: 0 none;
    overflow: hidden;
    position: fixed;
    width: 382px !important;
    z-index: 1050;
}
</style>
<!-- page content -->
      
   <div class="content-inner manageuserstap">
          <!-- Page Header-->
          <!--<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Tables</h2>
            </div>
          </header>-->
          <!-- Breadcrumb-->
          <ul class="breadcrumb">
            <div class="container-fluid">
              <li class="breadcrumb-item"><a href="<?php echo $this->webroot; ?>users/dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active">Manage Users</li>
            </div>
          </ul>
          <section class="tables">   
            <div class="container-fluid">
              <div class="row">
                
                <div class="col-lg-12">
                  <div class="card">
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Posts</h3>
                    </div>
                    <div class="card-body user-view-body">
              <p><?php echo $this->Session->flash(); ?></p>
              <?php  ?>
              <div class="table-responsive">
                      <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                          
                          <th>Email</th>
                          <th>Description</th>
                          <th>Media Link</th>
                          <th>No of Likes</th>
                          <th>No of Comments</th>
                          <th>Is Reported</th>
                          <th>Created</th>
                          <th>Modified</th>
                          <!--<th>Actions</th>-->
                        </tr>
                      </thead>

                      <tbody>
                        <?php if(isset($posts) && !empty($posts)){ foreach($posts as $post): ?>
                        <tr>
                         
                            <td width="100px"><?php if(!empty($user['email'])){ echo $user['email']; }else{ echo 'N/A'; } ?></td>
                            <td width="100px"><?php if(!empty($post['post_detail'])){ echo $post['post_detail']; }else{ echo 'N/A'; } ?></td>
                            <td>
                              <?php if($post['Post']['full_path']!=''){ ?>
                                <a href="<?php echo $post['Post']['full_path']; ?>" target="blank"><?php echo $post['Post']['full_path']; ?></a>
                              <?php }elseif($post['Post']['url_link']!=''){?>
                                <a href="<?php echo $post['Post']['url_link']; ?>" target="blank"><?php echo $post['Post']['url_link']; ?></a>
                              <?php }else{ echo 'N/A'; } ?>
                            </td>
                            <td width="100px"><?php echo $post['total_likes']; ?></td>
                            <td width="100px"><?php echo $post['total_comments']; ?></td>
                            <td width="100px">
                            <?php if($post['is_reported']=='Yes'){ ?>
                              <a href='javascript:void(0)' data-toogle="tooltip" title="<?php echo $post['reason']; ?>"><?php echo $post['is_reported']; ?></a>
                            <?php }else{ echo $post['is_reported']; } ?></td>
                            <td width="100px"><?php echo date('d-m-Y',strtotime($post['created'])); ?></td>
                            <td width="100px"><?php echo date('d-m-Y',strtotime($post['modified'])); ?></td>
         
                            <!--<td width="100px">
                              <a class="action-class btn btn-success btn-xs" data-toogle="tooltip" title="View" data-original-title='view details' href="<?php echo $this->webroot; ?>users/view_user/<?php echo $user['User']['id']; ?>"><i class="fa fa-eye"></i>
                              </a>
                              <a class="action-class btn btn-info btn-xs" title="Edit" href="javascript:void(0)" onclick="showModal('<?php echo $user['User']['id']; ?>','<?php echo $user['User']['status']; ?>','<?php echo $user['User']['govt_id_status']; ?>')"><i class="fa fa-edit"></i>
                              </a>
                              <a class="action-class btn btn-danger btn-xs" title="Delete" onclick="return get_confirm();" href="<?php echo $this->webroot; ?>users/delete_user/<?php echo $user['User']['id']; ?>"><i class="fa fa-times"></i>
                              </a>
                            </td>-->

                        </tr>

                      <?php endforeach; }else{ ?>
                      <tr>
                        <td>No Records found!</td>
                      </tr>
                      <?php } ?>
                    </tbody>
                      </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

             <!======Model code ======>
                    
                      <div class="modal post_modal" id="myModal">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4>Update Status</h4>
                          </div>
                          <div class="modal-body" style="height:88px;">
                            <label style="margin-left:35px;">User status</label>
                            <select id="status_data" class="" style="width:150px;color:black;margin-left:45px;">
                              <option value="">Select</option>
                              <option value="Active">Active</option>
                              <option value="Inactive">Inactive</option>
                              
                            </select></br>

                            <label style="margin-left:35px;">Govt Id status</label>
                            <select id="govtid_status" class="" style="width:150px;color:black;margin-left: 28px;">
                              <option value="">Select</option>
                              <option value="Pending">Pending</option>
                              <option value="Approved">Approved</option>
                              
                            </select>
                          </div>

                          <div class="res_msg"></div>
                          <div class="modal-footer" style="padding:8px;">
                          <a href="javascript:void(0);" onclick="update_status()" class="btn btn-primary">Update</a>
                            <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>
                            
                          </div>
                      </div>
          </section>
         
        </div>

<!-- /page content -->


    <!-- Datatables -->
   <style>
	.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
	border-bottom-right-radius: 0;
	border-top-right-radius: 0;
	display: none;
}
   </style>

    <script>
      $(document).ready(function() {
        
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });

    function get_confirm(){
        return confirm('Are you sure, you want to delete this record?');
    }

    function showModal(id,status,govt_id_status){

      user_id = id;
      status = status;
      govt_id_status = govt_id_status;
      $('#status_data').val(status);
      $('#govtid_status').val(govt_id_status);
      $('#myModal').modal();
    }

    function update_status(){

      $('.res_msg').html('');
      Id = user_id;
      status = $('#status_data').val();
      govtid_status = $('#govtid_status').val();
      if(Id!="" && status!="" && govtid_status!=""){

        $.ajax({
    
          'url': '<?php echo $this->webroot; ?>users/ajax_update_userstatus',
          'type': 'POST',
          'dataType': 'text',
          'data': {user_id:Id,status:status,govtid_status:govtid_status},
          'success': function(data){
            //alert(data);
            if(data=='success'){
              $('#myModal').hide();
              swal("Success!", "status updated successfully!", "success");
           
              swal({ 
                  title: "Success!",
                  text: "status updated successfully!",
                  type: "success" 
                },
                function(){
                 
                 location.reload();
              });
            }else{
             
              noty({
                 text: data,
                 layout: 'bottomLeft',
                 closeWith: ['click', 'hover'],
                 type: 'error'
              });
            }
          }

        });
      }else{
         noty({
           text: 'Please select status',
           layout: 'bottomLeft',
           closeWith: ['click', 'hover'],
           type: 'error'
        });
      }
    }


    </script>
    <!-- /Datatables -->
