
<?php echo $this->Html->css(array('admin/Datatables/dataTables.bootstrap','admin/Datatables/buttons.bootstrap','admin/Datatables/fixedHeader.bootstrap','admin/Datatables/responsive.bootstrap')); 
      echo $this->Html->script(array('admin/Datatables/jquery.dataTables.min','admin/Datatables/dataTables.bootstrap.min','admin/Datatables/dataTables.buttons.min','admin/Datatables/buttons.bootstrap.min','admin/Datatables/buttons.flash.min','admin/Datatables/buttons.html5.min','admin/Datatables/buttons.print.min','admin/Datatables/dataTables.fixedHeader.min','admin/Datatables/dataTables.keyTable.min','admin/Datatables/dataTables.responsive.min','admin/Datatables/responsive.bootstrap','admin/Datatables/dataTables.scroller.min','admin/Datatables/jszip.min','admin/Datatables/pdfmake.min','admin/Datatables/vfs_fonts'));
?>
<style>
.fancybox > img
{
    height: 46px;
    width: 46px;
}

.modal
{
    background: #fff none repeat scroll 0 0;
    bottom: 52% !important;
    display: none;
    height: 212px !important;
    left: 39% !important;
    top: 39% !important;
    outline: 0 none;
    overflow: hidden;
    position: fixed;
    width: 382px !important;
    z-index: 1050;
}
</style>
<!-- page content -->
      
   <div class="content-inner manageuserstap">
         

          <ul class="breadcrumb">
            <div class="container-fluid">
              <li class="breadcrumb-item"><a href="<?php echo $this->webroot; ?>users/dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active">Transactions</li>
            </div>
          </ul>
          <section class="tables">   
            <div class="container-fluid">
              <div class="row">
                
                <div class="col-lg-12">
                  <div class="card">
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Transactions</h3>
                      <div style="display:inline; float: right;margin-left: 57%;">
                          <span style="float: right;">
                          <span>Total Revenue:</span> 
                          <span style="color:#fbb917;"> <?php if($total_revenue!=''){ echo $total_revenue; }else{ echo '0'; } ?></span> &nbsp;&nbsp;||  
                          &nbsp;&nbsp;<span>Tapesquad Income:</span> 
                          <span style="color:#fbb917;"> <?php if($tape_income!=''){ echo $tape_income; }else{ echo '0'; } ?></span></span>

                      </div>
                    </div>
                    <div class="card-body user-view-body">
                <p><?php echo $this->Session->flash(); ?></p>
             
              <div class="table-responsive">
                      <table id="datatable-buttons" class="table table-striped table-bordered" data-order="[[ 0, &quot;asc&quot; ]]">
                        <thead>
                        <tr>
                          <th>S.No</th>
                          <th>Project Name</th>
                          <th>Reader Name</th>
                          <th>Client Name</th>
                          <th>Duration</th>
                          <th>Amount($)</th>
                          <th>Reader Amount($)</th>
                          <th>Tape Fee($)</th>
                          <th>Status</th>
                          <th>Created</th>
                         
                         <!-- <th>Actions</th>-->
                        </tr>
                      </thead>

                      <tbody>
                        <?php if(isset($payments) && !empty($payments)){ $i = 1; foreach($payments as $payment): ?>

                        <tr>
                        <td width="30px;"><?php echo $i; ?></td>
                          <td width="100px"><?php if(!empty($payment['Project']['project_name'])){ ?>
                            <a href="<?php echo $this->webroot; ?>users/view_project/<?php echo $payment['Payment']['project_id']; ?>" target="blank"><?php echo $payment['Project']['project_name']; ?></a>
                            <?php }else{ echo 'N/A'; } ?>
                          </td>

                          <td width="100px"><?php if(!empty($payment['Reader']['firstname']) && !empty($payment['Reader']['lastname'])){ echo $payment['Reader']['firstname'].' '.$payment['Reader']['lastname']; }elseif(!empty($payment['Client']['firstname'])){ echo $payment['Client']['firstname']; }else{ echo 'N/A'; } ?>
                          </td>

                          <td width="100px"><?php if(!empty($payment['Client']['firstname']) && !empty($payment['Client']['lastname'])){ echo $payment['Client']['firstname'].' '.$payment['Client']['lastname']; }elseif(!empty($payment['Client']['firstname'])){ echo $payment['Client']['firstname']; }else{ echo 'N/A'; } ?>
                          </td>
  <td><?= $payment['Project']['duration'] ?> min</td>
                          <td width="100px"><?php if(!empty($payment['Project']['amount'])){ echo $payment['Project']['amount']; }else{ echo 'N/A'; } ?></td>
                          <td width="100px"><?php if(!empty($payment['Project']['reader_amount'])){ echo round($payment['Project']['reader_amount'],0); }else{ echo 'N/A'; } ?></td>
                          <td width="100px"><?php if(!empty($payment['Project']['app_fees'])){ echo round($payment['Project']['app_fees'],0); }else{ echo 'N/A'; } ?></td>
                          
                          <td width="100px">
                            <?php if($payment['Project']['refund_request']==1){ ?>
                                    <span style="color:#FF5722;"><?php echo 'Refunded'; ?></span>
                            <?php }else{ ?>
                                     <span style="color:green;"><?php echo 'Completed' ?></span>
                            <?php } ?>
                          </td>

                         
                          <td width="100px"><?php echo date('m-d-Y',strtotime($payment['Payment']['created'])); ?></td>
                          
                        </tr>

                         <!--Model code -->
                    
                              <div class="modal post_modal" id="myModal">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <h4>Update Status</h4>
                                  </div>
                                  <div class="modal-body" style="height:88px;">
                                    

                                  </div>

                                  <div class="res_msg"></div>
                                  <div class="modal-footer" style="padding:8px;">
                                  <a href="javascript:void(0);" onclick="update_status()" class="btn btn-primary">Update</a>
                                    <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>
                                    
                                  </div>
                              </div>

                            <!-- model end -->


                      <?php $i++; endforeach;  }else{ ?>
                      <tr>
                        <td colspan="12">No Records found!</td>
                      </tr>
                      <?php } ?>
                    </tbody>
                      </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            
          </section>
         
        </div>

<!-- /page content -->


    <!-- Datatables -->
  <style>
	.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle)
  {
    	border-bottom-right-radius: 0;
    	border-top-right-radius: 0;
    	display: none;
  }
  </style>

    <script>
      $(document).ready(function(){
        
        $('#flashMessage').delay(5000).fadeOut('slow');
        $(".fancybox").fancybox({
          openEffect: 'none',
          closeEffect: 'none',
          nextEffect: 'none',
          prevEffect: 'none',
          padding: 0,
          margin: [20, 0, 20, 0]
        });

        var handleDataTableButtons = function(){
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });

    function get_confirm(){
        return confirm('Are you sure, you want to delete this record?');
    }

   

  </script>
  <!-- /Datatables -->
