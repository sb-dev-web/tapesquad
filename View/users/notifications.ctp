
<?php echo $this->Html->css(array('admin/Datatables/dataTables.bootstrap','admin/Datatables/buttons.bootstrap','admin/Datatables/fixedHeader.bootstrap','admin/Datatables/responsive.bootstrap')); 
      echo $this->Html->script(array('admin/Datatables/jquery.dataTables.min','admin/Datatables/dataTables.bootstrap.min','admin/Datatables/dataTables.buttons.min','admin/Datatables/buttons.bootstrap.min','admin/Datatables/buttons.flash.min','admin/Datatables/buttons.html5.min','admin/Datatables/buttons.print.min','admin/Datatables/dataTables.fixedHeader.min','admin/Datatables/dataTables.keyTable.min','admin/Datatables/dataTables.responsive.min','admin/Datatables/responsive.bootstrap','admin/Datatables/dataTables.scroller.min','admin/Datatables/jszip.min','admin/Datatables/pdfmake.min','admin/Datatables/vfs_fonts'));
?>
<style>
.fancybox > img {
    height: 46px;
    width: 46px;
}

.modal
{
    background: #fff none repeat scroll 0 0;
    bottom: 52% !important;
    display: none;
    height: 212px !important;
    left: 39% !important;
    top: 39% !important;
    outline: 0 none;
    overflow: hidden;
    position: fixed;
    width: 382px !important;
    z-index: 1050;
}
</style>
<!-- page content -->
      
   <div class="content-inner manageuserstap">
          <!-- Page Header-->
          <!--<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Tables</h2>
            </div>
          </header>-->
          <!-- Breadcrumb-->

          <ul class="breadcrumb">
            <div class="container-fluid">
              <li class="breadcrumb-item"><a href="<?php echo $this->webroot; ?>users/dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active">Notifications</li>
            </div>
          </ul>
          <section class="tables">   
            <div class="container-fluid">
              <div class="row">
                
                <div class="col-lg-12">
                  <div class="card">
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Notifications</h3>
                    </div>
                    <div class="card-body user-view-body">
              <p><?php echo $this->Session->flash(); ?></p>
              <?php  ?>
              <div class="table-responsive">
                      <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                          <th>Message</th>
                          <th>Created</th>
                          <th>Actions</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php if(isset($notifications) && !empty($notifications)){ foreach($notifications as $noti): ?>
                        <tr>
                          <td><?php if(!empty($noti['AdminNotification']['message'])){ echo $noti['AdminNotification']['message']; }else{ echo 'N/A'; } ?></td>
                          <td width="100px"><?php echo date('d-m-Y',strtotime($noti['AdminNotification']['created'])); ?></td>
                          <td width="200px">
                            <a class="btn btn-danger btn-xs" title="Delete" onclick="return get_confirm();" href="<?php echo $this->webroot; ?>users/delete_noti/<?php echo $noti['AdminNotification']['id']; ?>"><i class="fa fa-times"></i>
                            </a>
                            <?php if($noti['AdminNotification']['status']==0){ echo '<span style="margin-left: 20px;background-color: green;color: #fff;padding: 4px 7px;border-radius: 34px;">New</span>'; } ?>
                          </td>

                      </tr>

                      <?php endforeach; }else{ ?>
                      <tr>
                        <td colspan="3">No Records found!</td>
                      </tr>
                      <?php } ?>
                    </tbody>
                      </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            
          </section>
         
        </div>

<!-- /page content -->


    <!-- Datatables -->
  <style>
	.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle)
  {
    	border-bottom-right-radius: 0;
    	border-top-right-radius: 0;
    	display: none;
  }
  </style>

    <script>
      $(document).ready(function(){
        
        $('#flashMessage').delay(5000).fadeOut('slow');
        $(".fancybox").fancybox({
          openEffect: 'none',
          closeEffect: 'none',
          nextEffect: 'none',
          prevEffect: 'none',
          padding: 0,
          margin: [20, 0, 20, 0]
        });

        var handleDataTableButtons = function(){
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });

    function get_confirm(){
        return confirm('Are you sure, you want to delete this record?');
    }


  </script>
  <!-- /Datatables -->
