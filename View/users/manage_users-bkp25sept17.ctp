
<?php echo $this->Html->css(array('admin/Datatables/dataTables.bootstrap','admin/Datatables/buttons.bootstrap','admin/Datatables/fixedHeader.bootstrap','admin/Datatables/responsive.bootstrap')); 
      echo $this->Html->script(array('admin/Datatables/jquery.dataTables.min','admin/Datatables/dataTables.bootstrap.min','admin/Datatables/dataTables.buttons.min','admin/Datatables/buttons.bootstrap.min','admin/Datatables/buttons.flash.min','admin/Datatables/buttons.html5.min','admin/Datatables/buttons.print.min','admin/Datatables/dataTables.fixedHeader.min','admin/Datatables/dataTables.keyTable.min','admin/Datatables/dataTables.responsive.min','admin/Datatables/responsive.bootstrap','admin/Datatables/dataTables.scroller.min','admin/Datatables/jszip.min','admin/Datatables/pdfmake.min','admin/Datatables/vfs_fonts'));
?>
<style>
.modal{
    background: #fff none repeat scroll 0 0;
    bottom: 52% !important;
    display: none;
    height: 212px !important;
    left: 39% !important;
    top: 39% !important;
    outline: 0 none;
    overflow: hidden;
    position: fixed;
    width: 382px !important;
    z-index: 1050;
}
</style>
<!-- page content -->
       <div class="right_col" style="min-height: 940px;" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
               <!-- <h3>Users <small>List</small></h3>-->
              </div>

             
            </div>

            <div class="clearfix"></div>

            <div class="row">
              

              

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h3>Users <small>List</small></h3>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   
		    <p><?php echo $this->Session->flash(); ?></p>
		    <?php if(isset($users) && !empty($users)){ ?>
		    
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>User Role</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone No</th>
                          <th>Govt ID Status</th>
                          <th>Status</th>
                  			  <th>Created</th>
                          <th>Modified</th>
			                    <th>Actions</th>

			                  </tr>
                      </thead>


                      <tbody>
			                 <?php foreach($users as $user): ?>
                        <tr>
                         
                          <td width="100px;"><?php if($user['User']['user_role']==2){ echo 'Client'; }else{ echo 'Reader'; } ?></td>
                          <td width="200px;"><?php if(!empty($user['User']['firstname']) && !empty($user['User']['lastname'])){ echo $user['User']['firstname'].' '.$user['User']['lastname']; }elseif(!empty($user['User']['firstname'])){ echo $user['User']['firstname']; }else{ echo 'N/A'; } ?>
                          </td>
                          <td width="100px;"><?php if(!empty($user['User']['email'])){ echo $user['User']['email']; }else{ echo 'N/A'; } ?></td>
                          <td width="100px;"><?php if(!empty($user['User']['phone_no'])){ echo $user['User']['phone_no']; }else{ echo 'N/A'; } ?></td>
                          <td width="100px;"><?php echo $user['User']['govt_id_status']; ?></td>
                          <td width="50px;"><?php echo $user['User']['status']; ?></td>
                          <td width="100px;"><?php echo date('d-m-Y',strtotime($user['User']['created'])); ?></td>
			                    <td width="100px;"><?php echo date('d-m-Y',strtotime($user['User']['modified'])); ?></td>
			 
                         <td width="150px;">
                          <a class="action-class btn btn-success btn-xs" data-toogle="tooltip" title="View" data-original-title='view details' href="<?php echo $this->webroot; ?>users/view_user/<?php echo $user['User']['id']; ?>"><i class="fa fa-eye"></i>
                          </a>
                  				<a class="action-class btn btn-info btn-xs" title="Edit" href="javascript:void(0)" onclick="showModal('<?php echo $user['User']['id']; ?>','<?php echo $user['User']['status']; ?>','<?php echo $user['User']['govt_id_status']; ?>')"><i class="fa fa-edit"></i></a>
                  				<a class="action-class btn btn-danger btn-xs" title="Delete" onclick="return get_confirm();" href="<?php echo $this->webroot; ?>users/delete_user/<?php echo $user['User']['id']; ?>"><i class="fa fa-times"></i></a>
                  			</td>
                  	  </tr>
			                <?php endforeach; ?>
                      
                      </tbody>
                          </table>
                    		<?php } else { ?>
                    			<p>Sorry! No data found</p>
                    		<?php } ?>
                 
                  </div>
                </div>
              </div>

                <!======Model code ======>
                    
                      <div class="modal post_modal" id="myModal">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h5>Update Status</h5>
                          </div>
                          <div class="modal-body" style="height:88px;">
                            <label style="margin-left:35px;">User status</label>
                            <select id="status_data" class="" style="width:150px;color:black;margin-left:45px;">
                              <option value="">Select</option>
                              <option value="Active">Active</option>
                              <option value="Inactive">Inactive</option>
                              
                            </select></br>

                            <label style="margin-left:35px;">Govt Id status</label>
                            <select id="govtid_status" class="" style="width:150px;color:black;margin-left: 28px;">
                              <option value="">Select</option>
                              <option value="Pending">Pending</option>
                              <option value="Approved">Approved</option>
                              
                            </select>
                          </div>

                          <div class="res_msg"></div>
                          <div class="modal-footer" style="padding:8px;">
                            <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>
                            <a href="javascript:void(0);" onclick="update_status()" class="btn btn-primary">Update</a>
                          </div>
                      </di
              
                </div>
              </div>
            </div>
          </div>
   </div>
        <!-- /page content -->


    <!-- Datatables -->
  <style>
	 .btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
	  border-bottom-right-radius: 0;
	  border-top-right-radius: 0;
	  display: none;
  }
   </style>

    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });

    function get_confirm(){
        return confirm('Are you sure, you want to delete this record?');
    }

    function showModal(id,status,govt_id_status){

      user_id = id;
      status = status;
      govt_id_status = govt_id_status;
      $('#status_data').val(status);
      $('#govtid_status').val(govt_id_status);
      $('#myModal').modal();
    }

    function update_status(){

      $('.res_msg').html('');
      Id = user_id;
      status = $('#status_data').val();
      govtid_status = $('#govtid_status').val();
      if(Id!="" && status!="" && govtid_status!=""){

        $.ajax({
    
          'url': '<?php echo $this->webroot; ?>users/ajax_update_userstatus',
          'type': 'POST',
          'dataType': 'text',
          'data': {user_id:Id,status:status,govtid_status:govtid_status},
          'success': function(data){
            //alert(data);
            if(data=='success'){
              $('#myModal').hide();
              swal("Success!", "status updated successfully!", "success");
           
              swal({ 
                  title: "Success!",
                  text: "status updated successfully!",
                  type: "success" 
                },
                function(){
                 
                 location.reload();
              });
            }else{
             
              noty({
                 text: data,
                 layout: 'bottomLeft',
                 closeWith: ['click', 'hover'],
                 type: 'error'
              });
            }
          }

        });
      }else{
         noty({
           text: 'Please select status',
           layout: 'bottomLeft',
           closeWith: ['click', 'hover'],
           type: 'error'
        });
      }
    }


    </script>
    <!-- /Datatables -->
