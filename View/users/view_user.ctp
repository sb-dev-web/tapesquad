
<?php echo $this->Html->css(array('admin/Datatables/dataTables.bootstrap','admin/Datatables/buttons.bootstrap','admin/Datatables/fixedHeader.bootstrap','admin/Datatables/responsive.bootstrap')); 
      echo $this->Html->script(array('admin/Datatables/jquery.dataTables.min','admin/Datatables/dataTables.bootstrap.min','admin/Datatables/dataTables.buttons.min','admin/Datatables/buttons.bootstrap.min','admin/Datatables/buttons.flash.min','admin/Datatables/buttons.html5.min','admin/Datatables/buttons.print.min','admin/Datatables/dataTables.fixedHeader.min','admin/Datatables/dataTables.keyTable.min','admin/Datatables/dataTables.responsive.min','admin/Datatables/responsive.bootstrap','admin/Datatables/dataTables.scroller.min','admin/Datatables/jszip.min','admin/Datatables/pdfmake.min','admin/Datatables/vfs_fonts'));
?>
<style>

.x_content.user-content {
    margin-bottom: 74px;
    margin-top: 20px;
    width: 100%;
}

.modal-header {
    background: hsl(43, 97%, 54%) none repeat scroll 0 0 !important;
}

.viewpostImage {
    height: 171px;
    width: 180px;
}
</style>
<!-- page content -->
      
   <div class="content-inner">
          <!-- Page Header-->
          <!--<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Tables</h2>
            </div>
          </header>-->
          <!-- Breadcrumb-->
          <ul class="breadcrumb">
            <div class="container-fluid">
              <li class="breadcrumb-item"><a href="<?php echo $this->webroot; ?>users/manage_users">Manage Users</a></li>
              <li class="breadcrumb-item active">User Profile</li>
            </div>
          </ul>
         
      <section class="forms userviewforms">
          <div class="container-fluid">

             <div class="col-lg-12 lg-col-mobile">
                  <div class="card">
                    <div class="card-close">
                        
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Users</h3>
                    </div>
                    <div class="card-body user-view-body">
            
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  
                  <div class="x_content user-content">
                     <div class="x-content-one">
                      <?php if(!empty($user['User']['profile_pic'])){ ?>
                      <a href="<?php echo $this->webroot; ?>img/userImages/<?php echo $user['User']['profile_pic']; ?>" data-lightbox="<?php echo $user['User']['profile_pic']; ?>" data-title=""><img class="viewpostImage" src="<?php echo $this->webroot; ?>img/userImages/<?php echo $user['User']['profile_pic']; ?>" ></a><?php }else{ ?><img class="viewpostImage" src="<?php echo $this->webroot; ?>img/userImages/not_avail.jpg" ><?php } ?>
                     </div> 

                     <div class="x-content-two">

                        <div class="in-content">
                          <div class='left-head'>User Role:</div> 
                          <div class='right-head'><?php if($user['User']['user_role']==2){ echo 'Client'; }else{ echo 'Reader'; } ?></div>
                        </div>

                        <div class="in-content">
                          <div class='left-head'>Name:</div> 
                          <div class='right-head'><?php echo ucfirst($user['User']['firstname'].' '.$user['User']['lastname']);  ?></div>
                        </div>

                        <div class="in-content">
                          <div class='left-head'>Location:</div> 
                          <div class='right-head'><?php if($user['User']['location']!=''){ echo ucfirst($user['User']['location']); }else{ echo 'N/A'; } ?></div>
                        </div>

                        <div class="in-content">
                          <div class='left-head'>Phone:</div> 
                          <div class='right-head'><?php if($user['User']['phone_no']==2){ echo $user['User']['phone_no']; }else{ echo 'N/A'; } ?></div>
                        </div>

                        <div class="in-content">
                          <div class='left-head'>Member Since:</div> 
                          <div class='right-head'><?php echo $created = date('j F Y',strtotime($user['User']['created'])); ?></div>
                        </div>

                        
                         <div class="in-content">
                          <div class='left-head'>Reel:</div> 
                          <div class='right-head'><?php  if($user['User']['actor_reel']){ echo $user['User']['actor_reel']; }else{ echo 'N/A'; } ?></div>
                        </div>
                        
                        
                        

                        <div class="in-content">
                          <div class='left-head'>Govt Id:</div> 
                          <div class='right-head'><?php if(!empty($user['User']['govt_id_pic'])){ ?><a href="<?php echo $this->webroot; ?>img/userImages/<?php echo $user['User']['govt_id_pic']; ?>" data-lightbox="<?php echo $user['User']['govt_id_pic']; ?>" data-title=""><img class="viewgovtImage" src="<?php echo $this->webroot; ?>img/userImages/<?php echo $user['User']['govt_id_pic']; ?>" ></a><?php }else{ echo "N/A"; } ?></div>
                        </div>
                  <div class="in-content">
                          <div class='left-head'>ID Status:</div> 
                          <div class='right-head'><?php  if($user['User']['govt_id_status']){ echo $user['User']['govt_id_status']; } else { echo 'N/A'; } ?></div>
                        </div>
                     </div>

                      <div class="x-content-four">
                        <?php if($user['User']['status']=='Active'){ ?>
                        <a href="<?php echo $this->webroot; ?>users/change_status/<?php echo $user['User']['id']; ?>/Inactive" class="btn btn-lg btn-danger">Deactivate Account</a>
                        <?php } else { ?>
                        <a href="<?php echo $this->webroot; ?>users/change_status/<?php echo $user['User']['id']; ?>/Active" class="btn btn-lg btn-info">Activate Account</a>
                        <?php } ?>
                     </div>


                     <div class="x-content-three">
                      
                      <form id="date" method="get" action="<?php echo $this->webroot; ?>users/view_user/<?php echo $user['User']['id']; ?>" >
                        <label class="label">Start Date:</label>
                         <input type="text" id="min" name="dateStart" size="21" value="<?php if(isset($startDate)){ echo $startDate; } ?>">
                      <label class="label">End Date:</label>
                       <input type="text" id="max" name="dateEnd" size="21" value="<?php if(isset($endDate)){ echo $endDate; } ?>">
                      <input type="submit" id="reset" value="Submit" class="btn btn-md btn-success" style="padding: 4px 10px; !important; margin-top: 5px;">
                      <a id="refresh" href="#" class="btn btn-md btn-success" id="refresh" style="padding: 4px 8px !important;margin-top: 5px;"><i class="fa fa-refresh"></i></a>
                      </form>
                     </div> 



                  </div>
                  <div class="x_content">
                   
                  <p><?php echo $this->Session->flash(); ?></p>
                  
                  <div class="col-sm-12 custom-col">
                  <div class="table-responsive">
                    <table id="datatable-buttons" class="table table-striped table-bordered" style="min-width: 1209px;">
                      <thead>
                        <tr>
                          
                          
                          <th>Reader</th>
                          <th>Project</th>
                          <th>Duration</th>
                          <th>Price</th>
                          <th>Status</th>
                          <th>Date</th>
                          <th>Review</th>
                          <th>Created</th>

                        </tr>
                      </thead>


                      <tbody>
                       <?php if(isset($projects) && !empty($projects)){ foreach($projects as $project):

                              if($project['Project']['status']=='1'){ $status = '<span style="color:green;">Confirmed</span>'; }
                                elseif($project['Project']['status']=='2'){ $status = '<span style="color:orange;">Waiting</span>'; }
                                elseif($project['Project']['status']=='3'){ $status = '<span style="color:red;">Declined</span>'; }
                                elseif($project['Project']['status']=='4'){ $status = '<span style="color:green;">Progress</span>'; }
                                elseif($project['Project']['status']=='5'){ $status = '<span style="color:red;">Dissolve</span>'; }
                                elseif($project['Project']['status']=='6'){ $status = '<span style="color:green;">Completed</span>'; }
                                elseif($project['Project']['status']=='7'){ $status = '<span style="color:red;">Cancelled</span>'; }
                                elseif($project['Project']['status']=='8'){ $status = '<span style="color:red;">Refunded</span>'; }
                                else{ $status = 'N/A'; }

                        if(!empty($project['Project']['project_name']))
                        { 
                            if(!empty($project['Project']['script']))
                            { 
                              $script = '<p>'.$project['Project']['script'].'</p>';
                            }
                            elseif(!empty($project['Project']['script_file']))
                            {
                              $script = '<a target="_blank" href="'.$this->webroot.$project['Project']['script_path'].'" download="'.$this->webroot.'/img/scriptImages/'.$project['Project']['script_path'].'">Click here to read script</a>';
                            }else
                            {
                              $script = '<p>No script found for this project</p>';
                            }

                            if(!empty($project['Project']['reader_video'])){
                                $video = "<span>Video Link: </span>  <a target='blank' href='".SITE_URL.$project['Project']['reader_video']."'>
                                          <span>Click here to watch video</span></a>";
                            }elseif(!empty($project['Project']['client_video'])){
                                $video = "<span>Video Link: </span>  <a target='blank' href='".SITE_URL.$project['Project']['client_video']."''><span>Click here to watch video
                                          </span></a>";
                            }else{
                                $video = '<span>Video Link: </span> <p>Not Available</p>';
                            }
                        }
                          
                        ?>
                        <tr>
                         
                         
                          <td><?php if(!empty($project['User']['firstname']) && !empty($project['User']['lastname'])){ echo $project['User']['firstname'].' '.$project['User']['lastname']; }elseif(!empty($project['User']['firstname'])){ echo $project['User']['firstname']; }else{ echo 'N/A'; } ?>
                          </td>

                          <?php if(!empty($project['Project']['project_name'])){ ?>
                            <td><a href="javascript:void(0)" onclick="showModal('myModal_<?php echo $project['Project']['id']; ?>')"><?php echo $project['Project']['project_name']; ?></a></td>
                          <?php }else{ ?>
                            <td>N/A</td>
                          <?php } ?>

                          <td><?php if(!empty($project['Project']['duration'])){ echo $project['Project']['duration']; }else{ echo 'N/A'; } ?></td>
                          <td><?php if(!empty($project['Project']['amount'])){ echo $project['Project']['amount']; }else{ echo 'N/A'; } ?></td>
                         
                         
                          <td><?php echo $status; ?></td>
                          <td><?php echo date('d-m-Y',strtotime($project['Project']['schedule_time'])); ?></td>
                          <td><?php echo 'N/A'; ?></td>
                          <td><?php echo date('d-m-Y',strtotime($project['Project']['created'])); ?></td>

                      </tr>

                       <!-- Modal -->
                      <div class="modal fade" id="myModal_<?php echo $project['Project']['id']; ?>" role="dialog">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Project Details</h4>
                            </div>
                            <div class="modal-body">
                              <div class="scene"> <?php echo $script; ?></div>
                              <div class="video-link"> <?php echo $video; ?></div>

                              </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>

                      <?php endforeach; ?>
                      
                      </tbody>
                      
                        <?php } ?>
                         
                     </table>
                  </div>
                </div>
                </div>
              </div>
              </div>

              </div>
              </div>
              </div>
            </div>
         </div>

         <!======Model code ======>
                    
                      
                    
                    </div>
      </section>
              
                          

              
        <!--  User details end here -- >
         
        </div>

<!-- /page content -->


    <!-- Datatables -->
   <style>
	.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
	border-bottom-right-radius: 0;
	border-top-right-radius: 0;
	display: none;
}
   </style>
   <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />
   <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  <script>
    
      $(document).ready(function() {

        $('#min').datepicker();
        $('#max').datepicker();
        
        $('#refresh').click(function(){
           window.location.href = "<?php echo $this->webroot; ?>users/view_user/<?php echo $user['User']['id']; ?>";
        });

        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });


    function get_confirm(){
        return confirm('Are you sure, you want to delete this record?');
    }

    function showModal(id){
        $('#'+id).modal();
    }

    function update_status(){

      $('.res_msg').html('');
      Id = user_id;
      status = $('#status_data').val();
      govtid_status = $('#govtid_status').val();
      if(Id!="" && status!="" && govtid_status!=""){

        $.ajax({
    
          'url': '<?php echo $this->webroot; ?>users/ajax_update_userstatus',
          'type': 'POST',
          'dataType': 'text',
          'data': {user_id:Id,status:status,govtid_status:govtid_status},
          'success': function(data){
            //alert(data);
            if(data=='success'){
              $('#myModal').hide();
              swal("Success!", "status updated successfully!", "success");
           
              swal({ 
                  title: "Success!",
                  text: "status updated successfully!",
                  type: "success" 
                },
                function(){
                 
                 location.reload();
              });
            }else{
             
              noty({
                 text: data,
                 layout: 'bottomLeft',
                 closeWith: ['click', 'hover'],
                 type: 'error'
              });
            }
          }

        });
      }else{
         noty({
           text: 'Please select status',
           layout: 'bottomLeft',
           closeWith: ['click', 'hover'],
           type: 'error'
        });
      }
    }


    </script>
    <!-- /Datatables -->
