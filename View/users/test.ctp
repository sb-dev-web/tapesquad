<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<?php echo $this->Html->css(array('admin/Datatables/dataTables.bootstrap','admin/Datatables/buttons.bootstrap','admin/Datatables/fixedHeader.bootstrap','admin/Datatables/responsive.bootstrap')); 
      echo $this->Html->script(array('admin/Datatables/jquery.dataTables.min','admin/Datatables/dataTables.bootstrap.min','admin/Datatables/dataTables.buttons.min','admin/Datatables/buttons.bootstrap.min','admin/Datatables/buttons.flash.min','admin/Datatables/buttons.html5.min','admin/Datatables/buttons.print.min','admin/Datatables/dataTables.fixedHeader.min','admin/Datatables/dataTables.keyTable.min','admin/Datatables/dataTables.responsive.min','admin/Datatables/responsive.bootstrap','admin/Datatables/dataTables.scroller.min','admin/Datatables/jszip.min','admin/Datatables/pdfmake.min','admin/Datatables/vfs_fonts'));
?>


<?= $this->Html->css('../js/fullcalendar/fullcalendar.min.css') ?>


<?= $this->Html->script('fullcalendar/fullcalendar.min.js') ?>

<section>
            <div class="row col-sm-12" id="calendar">
      
            </div>
          </section>

          <script>
           $(document).ready(function() {
            var calendarEl = document.getElementById('calendar');

var calendar = new FullCalendar.Calendar(calendarEl, {
  defaultDate: '2019-01-12',
  editable: true,
  eventLimit: true, // allow "more" link when too many events
  events: [
    {
      title: 'All Day Event',
      start: '2019-01-01'
    },
    {
      title: 'Long Event',
      start: '2019-01-07',
      end: '2019-01-10'
    },
    {
      groupId: 999,
      title: 'Repeating Event',
      start: '2019-01-09T16:00:00'
    },
    {
      groupId: 999,
      title: 'Repeating Event',
      start: '2019-01-16T16:00:00'
    },
    {
      title: 'Conference',
      start: '2019-01-11',
      end: '2019-01-13'
    },
    {
      title: 'Meeting',
      start: '2019-01-12T10:30:00',
      end: '2019-01-12T12:30:00'
    },
    {
      title: 'Lunch',
      start: '2019-01-12T12:00:00'
    },
    {
      title: 'Meeting',
      start: '2019-01-12T14:30:00'
    },
    {
      title: 'Happy Hour',
      start: '2019-01-12T17:30:00'
    },
    {
      title: 'Dinner',
      start: '2019-01-12T20:00:00'
    },
    {
      title: 'Birthday Party',
      start: '2019-01-13T07:00:00'
    },
    {
      title: 'Click for Google',
      url: 'http://google.com/',
      start: '2019-01-28'
    }
  ]
});

calendar.render();

  });

    </script>