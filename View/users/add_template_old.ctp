
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>


 <!-- page content -->
        <div class="right_col" style="min-height: 1165px;" role="main">
          <div class="">
            <div class="page-title">
            
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?><small>CMS</small></h2>
                    <a class="GoBack btn-info btn btn-sm" href="javascript:void(0)">Go Back</a>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <?php echo $this->Form->create('EmailTemplate',array('class'=>'form-horizontal form-label-left')); ?>
		
                    <?php if(!empty($this->request->data['EmailTemplate']['title'])){ ?>
		                <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Title<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php echo $this->Form->input('title',array('type'=>'textbox','id'=>'category','class'=>'form-control validate[required] col-md-7 col-xs-12','readOnly','label'=>false)); ?>
                        </div>
                    </div>
                    <?php } else { ?>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Title<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php echo $this->Form->input('title',array('type'=>'textbox','id'=>'category','class'=>'form-control validate[required] col-md-7 col-xs-12','label'=>false)); ?>
                        </div>
                    </div>

                    <?php } ?>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">From Email<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php echo $this->Form->input('from_email',array('type'=>'textbox','id'=>'category','class'=>'form-control validate[required] col-md-7 col-xs-12','label'=>false)); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Subject<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php echo $this->Form->input('subject',array('type'=>'textbox','id'=>'category','class'=>'form-control validate[required] col-md-7 col-xs-12','label'=>false)); ?>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Content<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php echo $this->Form->input('content',array('type'=>'textarea','id'=>'category','class'=>'form-control ckeditor validate[required] col-md-7 col-xs-12','label'=>false,'width'=>'500px')); ?>
                        </div>
                    </div>
		       
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Status<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php echo $this->Form->input('status',array('type'=>'select','id'=>'status','class'=>'form-control validate[required] col-md-7 col-xs-12','label'=>false,'options'=>array('Active'=>'Active','Inactive'=>'Inactive'))); ?>
                        </div>
                    </div>
                      
		     
                    <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          
			                 <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-success')); ?>
                        </div>
                    </div>


                    
                			<?php echo $this->Session->flash(); ?>
                			<?php echo $this->Form->end(); ?>
                    <!--</form>-->
                  </div>
                </div>
              </div>
            </div>

    
          </div>
        </div>
        <!-- /page content -->

	<script>

    $(document).ready(function(){
      $('a.GoBack').click(function(){
          parent.history.back();
          return false;
      });
    });

		$(function(){
        $('#FaqAddFaqForm').validationEngine();
    });

	</script>


