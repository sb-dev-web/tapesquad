

    
     
        <div class="content-inner">
          <!-- Page Header-->
         
          <ul class="breadcrumb">
            <div class="container-fluid">
              <li class="breadcrumb-item"><a href="<?php echo $this->webroot; ?>users/dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active">Forms</li>
            </div>
          </ul>
          <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
               
                
                <!-- Form Elements -->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <a class="GoBack btn-success btn btn-sm" href="<?php echo $this->webroot; ?>users/manage_faq_topics"><i class="fa fa-list"></i> Manage Topics</a>
                      </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Add FAQs</h3>
                    </div>
                    <div class="card-body">
                     
                    
                    <?php echo $this->Form->create('FaqTopic',array('class'=>'form-horizontal form-label-left')); ?>
    
                   <div class="form-group row">
                        <label class="col-sm-3 form-control-label" for="first-name">Add Faq Topic For<span class="required">*</span></label>
                        <div class="col-sm-9">
                         <?php echo $this->Form->input('user_role',array('type'=>'select','id'=>'role','class'=>'form-control validate[required]','label'=>false,'options'=>array('3'=>'Reader','2'=>'Client','5'=>'General'))); ?>
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label" for="first-name">Topic<span class="required">*</span></label>
                        <div class="col-sm-9">
                          <?php echo $this->Form->input('topic',array('type'=>'textarea','class'=>'form-control validate[required]','label'=>false)); ?>
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                          <div class="col-sm-4 offset-sm-3">
                            <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-success')); ?>
                          </div>
                    </div>
                      <?php echo $this->Session->flash(); ?>
                      <?php echo $this->Form->end(); ?>
                    <!--</form>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          
        </div>


   <script>
      $(function(){
        $('#FaqTopicAddFaqTopicForm').validationEngine();
      });

   </script>