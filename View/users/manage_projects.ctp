
<?php echo $this->Html->css(array('admin/Datatables/dataTables.bootstrap','admin/Datatables/buttons.bootstrap','admin/Datatables/fixedHeader.bootstrap','admin/Datatables/responsive.bootstrap')); 
      echo $this->Html->script(array('admin/Datatables/jquery.dataTables.min','admin/Datatables/dataTables.bootstrap.min','admin/Datatables/dataTables.buttons.min','admin/Datatables/buttons.bootstrap.min','admin/Datatables/buttons.flash.min','admin/Datatables/buttons.html5.min','admin/Datatables/buttons.print.min','admin/Datatables/dataTables.fixedHeader.min','admin/Datatables/dataTables.keyTable.min','admin/Datatables/dataTables.responsive.min','admin/Datatables/responsive.bootstrap','admin/Datatables/dataTables.scroller.min','admin/Datatables/jszip.min','admin/Datatables/pdfmake.min','admin/Datatables/vfs_fonts'));
?>
<style>
.x_content.user-content {
    margin-bottom: 74px;
    margin-top: 20px;
    width: 100%;
}
.modal-header {
    background: hsl(43, 97%, 54%) none repeat scroll 0 0 !important;
}
</style>
<!-- page content -->

 <div class="content-inner manageuserstap">
       <ul class="breadcrumb">
            <div class="container-fluid">
              <li class="breadcrumb-item"><a href="<?php echo $this->webroot; ?>users/dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active">Manage Projects</li>
            </div>
        </ul>
        <section class="tables"> 
         <div class="container-fluid">
              <div class="row">
                
                <div class="col-lg-12">
                  <div class="card">
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Projects</h3>
                    </div>
                    <div class="card-body user-view-body">
        <p><?php echo $this->Session->flash(); ?></p>
       <div class="right_col table-responsive" style="min-height: 940px;" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
               <!-- <h3>Users <small>List</small></h3>-->
              </div>

             
            </div>

            <div class="clearfix"></div>

            <div class="row">
              
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <!--<h3>Projects <small>List</small></h3>-->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   
		    
		    <?php if(isset($projects) && !empty($projects)){ ?>
		    
                    <table id="datatable-buttons" class="table table-striped table-bordered" data-page-length="25" data-order="[[ 7, &quot;desc&quot; ]]">
                      <thead>
                        <tr>
                          
                          <th>Reader Name</th>
                          <!--<th>Reader Email</th>-->
                          <th>Client Name</th>
                          <!--<th>Client Email</th>-->
                          <th>Project Name</th>
                          <th>Schedule Time</th>
                          <th>Duration</th>
                          <th>Amount ($)</th>
                          <th>Status</th>
                  			  <th>Created</th>
                          <th>Modified</th>
			                    <!--<th>Actions</th>-->

			                  </tr>
                      </thead>
        
                      <tbody>
			                <?php foreach($projects as $project): ?>
                      <tr>
                         
                          <?php if($project['Project']['status']=='1'){ $status = '<span style="color:green;">Confirmed</span>'; }
                                elseif($project['Project']['status']=='2'){ $status = '<span style="color:orange;">Waiting</span>'; }
                                elseif($project['Project']['status']=='3'){ $status = '<span style="color:red;">Declined</span>'; }
                                elseif($project['Project']['status']=='4'){ $status = '<span style="color:green;">Progress</span>'; }
                                elseif($project['Project']['status']=='5'){ $status = '<span style="color:red;">Dissolve</span>'; }
                                elseif($project['Project']['status']=='6'){ $status = '<span style="color:green;">Completed</span>'; }
                                elseif($project['Project']['status']=='7'){ $status = '<span style="color:red;">Cancelled</span>'; }
                                elseif($project['Project']['status']=='8'){ $status = '<span style="color:#FF5722;">Refunded</span>'; }
                                else{ $status = 'N/A'; } 
                          ?>
                          <td width="80px;"><?php if(!empty($project['Reader']['firstname']) && !empty($project['Reader']['lastname'])){ echo $project['Reader']['firstname'].' '.$project['Reader']['lastname']; }elseif(!empty($project['Reader']['firstname'])){ echo $project['Reader']['firstname']; }else{ echo 'N/A'; } ?>
                          </td>
                          <!--<td width="80px;"><?php if(!empty($project['Reader']['email'])){ echo $project['Reader']['email']; }else{ echo 'N/A'; } ?></td>-->
                          <td width="80px;"><?php if(!empty($project['Client']['firstname']) && !empty($project['Client']['lastname'])){ echo $project['Client']['firstname'].' '.$project['Client']['lastname']; }elseif(!empty($project['Client']['firstname'])){ echo $project['Client']['firstname']; }else{ echo 'N/A'; } ?>
                          </td>
                          <!--<td width="100px;"><?php if(!empty($project['Client']['email'])){ echo $project['Client']['email']; }else{ echo 'N/A'; } ?></td>-->
                          
                          <?php if(!empty($project['Project']['project_name'])){ 

                            if(!empty($project['Project']['script'])){ 
                                $script = '<p>'.$project['Project']['script'].'</p>';
                            }elseif(!empty($project['Project']['script_file'])){
                                $script = '<a target="_blank" href="'.$this->webroot.$project['Project']['script_path'].'" download="'.$this->webroot.'/img/scriptImages/'.$project['Project']['script_path'].'">Click here to read script</a>';
                            }else{
                                $script = '<p>No script found for this project</p>';
                            }

                            if(!empty($project['Project']['reader_video'])){
                                $video = "<span>Video Link: </span>  <a target='blank' href='".SITE_URL.$project['Project']['reader_video']."'>
                                          <span>Click here to watch video</span></a>";
                            }elseif(!empty($project['Project']['client_video'])){
                                $video = "<span>Video Link: </span>  <a target='blank' href='".SITE_URL.$project['Project']['client_video']."''><span>Click here to watch video
                                          </span></a>";
                            }else{
                                $video = '<span>Video Link: </span> <p>Not Available</p>';
                            }

                           ?>
                          <td width="100px;">
                            <a href="javascript:void(0)" onclick='showModal("<?php echo $project['Project']['id']; ?>")'><?php echo trim($project['Project']['project_name']); ?>
                            </a>
                          </td>
                          <?php }else{ ?>
                            <td>N/A</td>
                          <?php } ?>
                          
                          <td width="100px;"><?php echo $project['Project']['schedule_time']; ?></td>
                          <td width="20px;"><?php echo $project['Project']['duration']; ?></td>
                          <td width="20px;"><?php echo $project['Project']['amount']; ?></td>
                          <td width="50px;"><?php echo $status; ?></td>
                          <td width="100px;"><?php echo date('m-d-Y',strtotime($project['Project']['created'])); ?></td>
			                    <td width="100px;"><?php echo date('m-d-Y',strtotime($project['Project']['modified'])); ?></td>
			 
                          
                  	  </tr>

                         <!======Model code ======>
                    
                      <!-- Modal -->
                      <div class="modal fade" id="myModal<?php echo $project['Project']['id']; ?>" role="dialog">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Project Details</h4>
                            </div>
                            <div class="modal-body">
                              <h6>Scene 1</h6>
                              <div class="scene"> <?php echo $script; ?>
                              
                              <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>-->
                              </div>

                              <div class="video-link"> <?php echo $video; ?>
                              <!--<span>Video Link: </span>  <a id="video-link" target="blank" href="https://www.youtube.com/watch?v=sivXk8cTPIA"><span>Click here to watch video</span></a>-->
                              </div>


                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    
                <!---  Model end here -->


			                <?php endforeach; ?>
                      
                          </tbody>
                      </table>
                      </div>
                      </div>
                      </div>
                      </div>
                      </div>

                      
		<?php } else { ?>
			<p>Sorry! No data found</p>
		<?php } ?>
                   
                  </div>
                </div>
              </div>

                <!======Model code ======>
                    
                      <!-- Modal -->
                   <!--   <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Project Details</h4>
                            </div>
                            <div class="modal-body">
                              <h6>Scene 1</h6>
                              <div class="scene">
                              
                              <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>-->
                         <!--     </div>

                              <div class="video-link">
                              <span>Video Link: </span>  <a id="video-link" target="blank" href="https://www.youtube.com/watch?v=sivXk8cTPIA"><span>Click here to watch video</span></a>
                              </div>


                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    -->
                <!---  Model end here -->

                      
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
          </section>
   </div>
   </div>
        <!-- /page content -->


    <!-- Datatables -->
   <style>
    .btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) 
    {
    	border-bottom-right-radius: 0;
    	border-top-right-radius: 0;
    	display: none;
    }
   </style>

    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });

    function get_confirm(){
        return confirm('Are you sure, you want to delete this record?');
    }

    function showModal(id){

     
     // $('.scene').html(script);
     // $('.video-link').html(video);
      $('#myModal'+id).modal();
    }
  
    function update_status(){

      $('.res_msg').html('');
      Id = user_id;
      status = $('#status_data').val();
      govtid_status = $('#govtid_status').val();
      if(Id!="" && status!="" && govtid_status!=""){

        $.ajax({
    
          'url': '<?php echo $this->webroot; ?>users/ajax_update_userstatus',
          'type': 'POST',
          'dataType': 'text',
          'data': {user_id:Id,status:status,govtid_status:govtid_status},
          'success': function(data){
            //alert(data);
            if(data=='success'){
              $('#myModal').hide();
              swal("Success!", "status updated successfully!", "success");
           
              swal({ 
                  title: "Success!",
                  text: "status updated successfully!",
                  type: "success" 
                },
                function(){
                 
                 location.reload();
              });
            }else{
             
              noty({
                 text: data,
                 layout: 'bottomLeft',
                 closeWith: ['click', 'hover'],
                 type: 'error'
              });
            }
          }

        });
      }else{
         noty({
           text: 'Please select status',
           layout: 'bottomLeft',
           closeWith: ['click', 'hover'],
           type: 'error'
        });
      }
    }


    </script>
    <!-- /Datatables -->
