<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Tape Squad</title>
	<meta name="description" content="Tape Squad">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Tape Squad">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	
	<!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>-->
	<!-- end: CSS -->
	

		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
	
			<style type="text/css">
				
				body { background: url(<?php echo $this->webroot; ?>img/bg-login.jpg) !important; }
				.login-box h2 { margin-right: 30px;}
			</style>
		
	<?php echo $this->Html->css(array('jquery-ui-1.8.21.custom','bootstrap','bootstrap.min','bootstrap-responsive','bootstrap-responsive.min','chosen','elfinder.min','font-awesome.min','fullcalendar','font-awesome-ie7.min','halflings','glyphicons','ie','ie9','jquery.cleditor','jquery.gritter','jquery.iphone.toggle','jquery.noty','noty_theme_default','style','style-forms','style-responsive','uniform.default','uploadify','mystyle')); 


 ?>


		
		
</head>

<body>
		<div class="container-fluid-full">
		<div class="row-fluid">
					
			<div class="row-fluid">
				<div class="login-box activateclass">
					<div class="icons">
						
					</div>
					
					<h2><?php echo $this->Session->flash(); ?></h2>
					
					
					<hr>
					
				</div><!--/span-->
			</div><!--/row-->
			

	</div><!--/.fluid-container-->
	
		</div><!--/fluid-row-->
	   	
	
	
</body>
</html>
