
<?php echo $this->Html->css(array('admin/Datatables/dataTables.bootstrap','admin/Datatables/buttons.bootstrap','admin/Datatables/fixedHeader.bootstrap','admin/Datatables/responsive.bootstrap')); 
      echo $this->Html->script(array('admin/Datatables/jquery.dataTables.min','admin/Datatables/dataTables.bootstrap.min','admin/Datatables/dataTables.buttons.min','admin/Datatables/buttons.bootstrap.min','admin/Datatables/buttons.flash.min','admin/Datatables/buttons.html5.min','admin/Datatables/buttons.print.min','admin/Datatables/dataTables.fixedHeader.min','admin/Datatables/dataTables.keyTable.min','admin/Datatables/dataTables.responsive.min','admin/Datatables/responsive.bootstrap','admin/Datatables/dataTables.scroller.min','admin/Datatables/jszip.min','admin/Datatables/pdfmake.min','admin/Datatables/vfs_fonts'));
?>
<style>

.x_content.user-content {
    margin-bottom: 74px;
    margin-top: 20px;
    width: 100%;
}
.modal-header {
    background: hsl(43, 97%, 54%) none repeat scroll 0 0 !important;
}

.viewpostImage {
    height: 171px;
    width: 180px;
}
</style>
<!-- page content -->
      
   <div class="content-inner">
          <!-- Page Header-->
          <!--<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Tables</h2>
            </div>
          </header>-->
          <!-- Breadcrumb-->
          <ul class="breadcrumb">
            <div class="container-fluid">
              <li class="breadcrumb-item"><a href="<?php echo $this->webroot; ?>users/dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active">Project Details</li>
            </div>
          </ul>
         
      <section class="forms">
          <div class="container-fluid">

             <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                        
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Project Details</h3>
                    </div>
            <div class="card-body user-view-body">
            
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  
                  <div class="x_content">
                   
                  <p><?php echo $this->Session->flash(); ?></p>
                 
                  <div class="table-responsive">
                    <table id="datatable-buttons" class="table table-striped table-bordered" style="min-width: 1209px;">
                      
<tbody>
                       <?php if(isset($project) && !empty($project)){ 

                                if($project['Project']['status']=='1'){     $status = '<span style="color:green;">Confirmed</span>'; }
                                elseif($project['Project']['status']=='2'){ $status = '<span style="color:orange;">Waiting</span>'; }
                                elseif($project['Project']['status']=='3'){ $status = '<span style="color:red;">Declined</span>'; }
                                elseif($project['Project']['status']=='4'){ $status = '<span style="color:green;">Progress</span>'; }
                                elseif($project['Project']['status']=='5'){ $status = '<span style="color:red;">Dissolve</span>'; }
                                elseif($project['Project']['status']=='6'){ $status = '<span style="color:green;">Completed</span>'; }
                                elseif($project['Project']['status']=='7'){ $status = '<span style="color:red;">Cancelled</span>'; }
                                else{ $status = 'N/A'; } 
                          
                        ?>
                        <tr>
                          <td>Name: </td><td><?php echo $project['Project']['project_name']; ?></td>
                        </tr>

                        <tr>
                          <td>Tape Type: </td><td><?php if($project['Project']['tape_type']==1){ echo 'Scheduled Tape';}else{ echo 'Tape Now'; } ?></td>
                        </tr>

                        <tr>
                          <td>Date & Time: </td><td><?php echo date('d-m-Y H:i:s',strtotime($project['Project']['schedule_time'])); ?></td>
                        </tr>

                        <tr>
                          <td>Duration : </td><td><?php echo $project['Project']['duration']; ?></td>
                        </tr>

                        <tr>
                          <td>Amount: </td><td><?php echo $project['Project']['amount']; ?></td>
                        </tr>

                        <tr>
                          <td>Status: </td><td><?php echo $status; ?></td>
                        </tr>

                        <tr>
                          <td>Promo Code: </td><td><?php if(!empty($project['Project']['promo_code'])){ echo $project['Project']['promo_code']; }else{ echo 'N/A'; }?></td>
                        </tr>

                        <tr>
                          <td>Reader Video: </td>
                           <td><?php if(!empty($project['Project']['reader_video'])){ ?>
                              <a href="<?php echo $this->webroot.$project['Project']['reader_video']; ?>">Download</a> 
                              <?php }else{ echo 'N/A'; } ?>
                          </td>
                        </tr>

                        <tr>
                          <tr>
                          <td>Client Video: </td>
                          <td><?php if(!empty($project['Project']['client_video'])){ ?>
                              <a href="<?php echo $this->webroot.$project['Project']['client_video']; ?>">Download</a> 
                              <?php }else{ echo 'N/A'; } ?>
                          </td>
                        </tr>
                        </tr>

                  
                      <?php }else{ ?>
                          <tr>
                          <td> No project found </td>
                        </tr>
                      <?php } ?>
                        </tbody>
                     </table>
                   </div>
                  </div>
                </div>
              </div>
              </div>

              </div>
              </div>
              </div>
         </div>

                            
                      
                         


                      
                    </div>

      </section>
              
                          

              
        <!--  User details end here -- >
         
        </div>

<!-- /page content -->


    <!-- Datatables -->
   <style>
	.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
	border-bottom-right-radius: 0;
	border-top-right-radius: 0;
	display: none;
}
   </style>
   <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />
   <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script>
      $(document).ready(function() {

        $('#min').datepicker();
        $('#max').datepicker();
        
        $('#refresh').click(function(){
           window.location.href = "<?php echo $this->webroot; ?>users/view_reader/<?php echo $user['User']['id']; ?>";
        });
        
        var handleDataTableButtons = function()
        {
          if($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });

    function get_confirm(){
        return confirm('Are you sure, you want to delete this record?');
    }

    function showModal(){
        $('#myModal').modal();
    }

    function update_status(){

      $('.res_msg').html('');
      Id = user_id;
      status = $('#status_data').val();
      govtid_status = $('#govtid_status').val();
      if(Id!="" && status!="" && govtid_status!=""){

        $.ajax({
    
          'url': '<?php echo $this->webroot; ?>users/ajax_update_userstatus',
          'type': 'POST',
          'dataType': 'text',
          'data': {user_id:Id,status:status,govtid_status:govtid_status},
          'success': function(data){
            //alert(data);
            if(data=='success'){
              $('#myModal').hide();
              swal("Success!", "status updated successfully!", "success");
           
              swal({ 
                  title: "Success!",
                  text: "status updated successfully!",
                  type: "success" 
                },
                function(){
                  location.reload();
              });
            }else{
             
              noty({
                 text: data,
                 layout: 'bottomLeft',
                 closeWith: ['click', 'hover'],
                 type: 'error'
              });
            }
          }

        });
      }else{
         noty({
           text: 'Please select status',
           layout: 'bottomLeft',
           closeWith: ['click', 'hover'],
           type: 'error'
        });
      }
    }


    </script>
    <!-- /Datatables -->
