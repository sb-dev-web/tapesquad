

    
     
        <div class="content-inner">
          <!-- Page Header-->
         
          <ul class="breadcrumb">
            <div class="container-fluid">
              <li class="breadcrumb-item"><a href="<?php echo $this->webroot; ?>users/dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active">Add Template</li>
            </div>
          </ul>
          <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
               
                
                <!-- Form Elements -->
                <div class="col-lg-12">
                  <div class="card">
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Add Template</h3>
                      <a class="GoBack btn-info btn btn-sm" href="javascript:void(0)">Go Back</a>
                    </div>
                    <div class="card-body">
                     
                    
                    <?php echo $this->Form->create('EmailTemplate',array('class'=>'form-horizontal form-label-left')); ?>

                    <?php if(!empty($this->request->data['EmailTemplate']['title'])){ ?>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label" for="first-name">Title<span class="required">*</span></label>
                        <div class="col-sm-9">
                          <?php echo $this->Form->input('title',array('id'=>'title','class'=>'form-control validate[required]','readOnly','label'=>false)); ?>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label" for="first-name">Title<span class="required">*</span></label>
                        <div class="col-sm-9">
                          <?php echo $this->Form->input('title',array('id'=>'title','class'=>'form-control validate[required]','label'=>false)); ?>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label" for="first-name">From Email<span class="required">*</span></label>
                        <div class="col-sm-9">
                          <?php echo $this->Form->input('from_email',array('type'=>'text','id'=>'from_email','class'=>'form-control validate[required]','label'=>false)); ?>
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label" for="first-name">Subject<span class="required">*</span></label>
                        <div class="col-sm-9">
                          <?php echo $this->Form->input('subject',array('type'=>'text','id'=>'subject','class'=>'form-control validate[required]','label'=>false)); ?>
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label" for="first-name">Content<span class="required">*</span></label>
                        <div class="col-sm-9">
                          <?php echo $this->Form->input('content',array('type'=>'textarea','id'=>'content','class'=>'form-control ckeditor validate[required]','label'=>false)); ?>
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label" for="first-name">Status<span class="required">*</span></label>
                        <div class="col-sm-9">
                          <?php echo $this->Form->input('status',array('type'=>'select','id'=>'status','class'=>'form-control validate[required]','label'=>false,'options'=>array('Active'=>'Active','Inactive'=>'Inactive'))); ?>
                        </div>
                    </div>
                    <div class="line"></div>  
         
                    <div class="form-group row">
                          <div class="col-sm-4 offset-sm-3">
                            <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-success')); ?>
                          </div>
                    </div>
                      <?php echo $this->Session->flash(); ?>
                      <?php echo $this->Form->end(); ?>
                    <!--</form>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          
        </div>

  <script>

    $(document).ready(function(){
      $('a.GoBack').click(function(){
          parent.history.back();
          return false;
      });
    });

    $(function(){
        $('#FaqAddFaqForm').validationEngine();
    });

  </script>


   