
<style>
.viewpostImage {
    height: 171px;
    width: 200px;
}
</style>
<!-- page content -->
       <div class="right_col" style="min-height: 940px;" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
               
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>User<small>Details</small></h2>
                    <a class="GoBack btn-success btn btn-sm" href="<?php echo $this->webroot; ?>users/manage_users"><i class="fa fa-list"></i> Users List</a>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content user-content">
                     <div class="x-content-one">
                      <?php if(!empty($user['User']['profile_pic'])){ ?><img class="viewpostImage" src="<?php echo $this->webroot; ?>img/userImages/<?php echo $user['User']['profile_pic']; ?>" ><?php }else{ ?><img class="viewpostImage" src="<?php echo $this->webroot; ?>img/userImages/not_avail.jpg" ><?php } ?>
                     </div> 
                     <div class="x-content-two">

                        <div class="in-content">
                          <div class='left-head'>User Role:</div> 
                          <div class='right-head'><?php if($user['User']['user_role']==2){ echo 'Client'; }else{ echo 'Reader'; } ?></div>
                        </div>

                        <div class="in-content">
                          <div class='left-head'>Name:</div> 
                          <div class='right-head'><?php echo ucfirst($user['User']['firstname'].' '.$user['User']['lastname']);  ?></div>
                        </div>

                        <div class="in-content">
                          <div class='left-head'>Location:</div> 
                          <div class='right-head'><?php if($user['User']['location']!=''){ echo ucfirst($user['User']['location']); }else{ echo 'N/A'; } ?></div>
                        </div>

                        <div class="in-content">
                          <div class='left-head'>Phone:</div> 
                          <div class='right-head'><?php if($user['User']['phone_no']==2){ echo $user['User']['phone_no']; }else{ echo 'N/A'; } ?></div>
                        </div>

                        <div class="in-content">
                          <div class='left-head'>Member Since:</div> 
                          <div class='right-head'><?php echo $created = date('j F Y',strtotime($user['User']['created'])); ?></div>
                        </div>

                        <div class="in-content">
                          <div class='left-head'>Govt Id:</div> 
                          <div class='right-head'><?php if(!empty($user['User']['govt_id_pic'])){ ?><img class="viewgovtImage" src="<?php echo $this->webroot; ?>img/userImages/<?php echo $user['User']['govt_id_pic']; ?>" ><?php }else{ ?><img class="viewgovtImage" src="<?php echo $this->webroot; ?>img/userImages/not_avail.jpeg" ><?php } ?></div>
                        </div>

                     </div>
                     <div class="x-content-three">
                        <a href="#" class="btn btn-sm btn-primary">Deactivate Account</a>
                     </div>  

                  </div>
                  <div class="x_content">
                   
          		    <p><?php echo $this->Session->flash(); ?></p>
          		   
        
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>User Role</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone No</th>
                          <th>Govt ID Status</th>
                          <th>Status</th>
                          <th>Created</th>
                          <th>Modified</th>
                          <th>Actions</th>

                        </tr>
                      </thead>


                      <tbody>
                       <?php if(isset($users) && !empty($users)){ foreach($users as $user): ?>
                        <tr>
                         
                          <td width="100px;"><?php if($user['User']['user_role']==2){ echo 'Client'; }else{ echo 'Reader'; } ?></td>
                          <td width="200px;"><?php if(!empty($user['User']['firstname']) && !empty($user['User']['lastname'])){ echo $user['User']['firstname'].' '.$user['User']['lastname']; }elseif(!empty($user['User']['firstname'])){ echo $user['User']['firstname']; }else{ echo 'N/A'; } ?>
                          </td>
                          <td width="100px;"><?php if(!empty($user['User']['email'])){ echo $user['User']['email']; }else{ echo 'N/A'; } ?></td>
                           <td width="100px;"><?php if(!empty($user['User']['phone_no'])){ echo $user['User']['phone_no']; }else{ echo 'N/A'; } ?>
                          </td>
                          <!-- <td width="100px;"><?php if(!empty($user['User']['language'])){ echo $user['User']['language']; }else{ echo 'N/A'; } ?>-->
                          </td>
                          <td width="100px;"><?php echo $user['User']['govt_id_status']; ?></td>
                          <td width="50px;"><?php echo $user['User']['status']; ?></td>
                          <td width="100px;"><?php echo date('d-m-Y',strtotime($user['User']['created'])); ?></td>
                          <td width="100px;"><?php echo date('d-m-Y',strtotime($user['User']['modified'])); ?></td>
       
                         <td width="150px;">
                          <a class="action-class btn btn-success btn-xs" data-toogle="tooltip" title="View" data-original-title='view details' href="<?php echo $this->webroot; ?>users/view_user/<?php echo $user['User']['id']; ?>"><i class="fa fa-eye"></i>
                          </a>
                          <a class="action-class btn btn-info btn-xs" title="Edit" href="javascript:void(0)" onclick="showModal('<?php echo $user['User']['id']; ?>','<?php echo $user['User']['status']; ?>','<?php echo $user['User']['govt_id_status']; ?>')"><i class="fa fa-edit"></i></a>
                          <a class="action-class btn btn-danger btn-xs" title="Delete" onclick="return get_confirm();" href="<?php echo $this->webroot; ?>users/delete_user/<?php echo $user['User']['id']; ?>"><i class="fa fa-times"></i></a>
                        </td>
                      </tr>
                      <?php endforeach; ?>
                      
                      </tbody>
                      
                        <?php } else { ?>
                          <p class="nodata-found-p">Sorry! No data found</p>
                        <?php } ?>
                     </table>
                  </div>
                </div>
              </div>

              
                </div>
                       

              </div>
            </div>
          </div>
   </div>
        <!-- /page content -->


    <!-- Datatables -->
<style>
	.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
	border-bottom-right-radius: 0;
	border-top-right-radius: 0;
	display: none;
  
}
</style>
