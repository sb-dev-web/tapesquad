

    
     
        <div class="content-inner">
          <!-- Page Header-->
         
          <ul class="breadcrumb">
            <div class="container-fluid">
              <li class="breadcrumb-item"><a href="<?php echo $this->webroot; ?>users/dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active">Forms</li>
            </div>
          </ul>
          <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
               
                
                <!-- Form Elements -->
                <div class="col-lg-12">
                  <div class="card">
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Add FAQs</h3>
                    </div>
                    <div class="card-body">
                     
                    
                    <?php echo $this->Form->create('Faq',array('class'=>'form-horizontal')); ?>

                    <?php if(isset($this->request->data['Faq']['topic_id']) && !empty($this->request->data['Faq']['topic_id'])){ ?>
                       <?php echo $this->Form->input('topic_id',array('type'=>'hidden','id'=>'topicid','class'=>'form-control col-md-7 col-xs-12','label'=>false,'value'=>$this->request->data['Faq']['topic_id'])); ?>
                   <?php } ?>
    
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label" for="first-name">Question<span class="required">*</span></label>
                        <div class="col-sm-9">
                          <?php echo $this->Form->input('question',array('id'=>'category','class'=>'form-control validate[required]','label'=>false)); ?>
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label" for="first-name">Answer<span class="required">*</span></label>
                        <div class="col-sm-9">
                          <?php echo $this->Form->input('answer',array('type'=>'textarea','id'=>'category','class'=>'form-control validate[required]','label'=>false)); ?>
                        </div>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label" for="first-name">Status<span class="required">*</span></label>
                        <div class="col-sm-9">
                          <?php echo $this->Form->input('status',array('type'=>'select','id'=>'status','class'=>'form-control validate[required]','label'=>false,'options'=>array('Active'=>'Active','Inactive'=>'Inactive'))); ?>
                        </div>
                    </div>
                    <div class="line"></div>  
         
                    <div class="form-group row">
                          <div class="col-sm-4 offset-sm-3">
                            <?php echo $this->Form->submit('Submit',array('class'=>'btn btn-success')); ?>
                          </div>
                    </div>
                      <?php echo $this->Session->flash(); ?>
                      <?php echo $this->Form->end(); ?>
                    <!--</form>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          
        </div>


   