
<?= $this->Html->css('../js/fullcalendar/fullcalendar.min.css') ?>
<style>
.fc-title
{
    color:#fff;
}
.fc-toolbar{ padding:50px 10px;width:95%;}
.top-row th { padding: 8px 0;}
    </style>

<?= $this->Html->script('fullcalendar/fullcalendar.min.js') ?>

<div class="container-fluid" >
    <div class="row bg-white has-shadow totaldiv" >
        <table style="width:100%;" class="top-row">
        <tr> 
            <th style="text-align:center; background:#3A87AD; color:#ffffff;">Total 30: &nbsp;&nbsp;&nbsp;&nbsp;<span id="total_30">0</span></th> 
            <th style="text-align:center; background:rgba(0, 188, 212, 0.85);color:#ffffff;">Total 45: &nbsp;&nbsp;&nbsp;&nbsp; <span id="total_45">0</span></th>
            <th style="text-align:center;background:#3A87AD;color:#ffffff;">Total 60: &nbsp;&nbsp;&nbsp;&nbsp; <span id="total_60">0</span></th>
            <th style="text-align:center;background:#ff8e22;color:#ffffff;">Total 75: &nbsp;&nbsp;&nbsp;&nbsp; <span id="total_75">0</span></th>
            <th style="text-align:center;background:#FF5722;color:#ffffff;">Total: &nbsp;&nbsp;&nbsp;&nbsp; <span id="total_all">0</span></th>
        </tr>

</table>
</div>
<div class="row bg-white has-shadow" id="calendar">

</div>
<div id="calendardara"  class="modal fade" title="My dialog" style="display:none">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<fieldset>
            <label for="Id">Project Name</label>
            <div class="projectName"></div>
            <label for="Id">Description</label>
            <div class="projectDesc"></div>
        </fieldset>
    
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>

</div>
       
</div>
    </div>


          <script>
           $(document).ready(function() {

              
            var calendarEl = document.getElementById('calendar');
            $.get('<?= SITE_URL ?>users/getCalendar',function(response){
               
                var result = JSON.parse(response);
                var calendar = new FullCalendar.Calendar(calendarEl, {
                    header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
                navLinks: true, 
                defaultDate: '2019-01-12',
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                events:result, 
                datesDestroy :  function( info )
                {
                    //alert('change');
                    var total_30 = $('#total_30').html(0);
                    var total_45 = $('#total_45').html(0);
                    var total_60 = $('#total_60').html(0);
                    var total_75 = $('#total_75').html(0);
                    var total_all = $('#total_all').html(0);
                },
                eventRender: function(info) {
                    
                    var type = info.event._def.extendedProps.type;
                    var count = info.event._def.extendedProps.count;
                    var total_30 = $('#total_30').text();
                    var total_45 = $('#total_45').text();
                    var total_60 = $('#total_60').text();
                    var total_75 = $('#total_75').text();
                    var total_all = $('#total_all').text();
                   // console.log(type);
                    if(type == '30')
                    {
                        var ttl_30 = parseInt(total_30) + parseInt(count);
                        $('#total_30').html(ttl_30);
                       
                    }
                    if(type == '60')
                    {
                        var ttl_60 = parseInt(total_60) + parseInt(count);
                        $('#total_60').html(ttl_60);
                        
                    }
                    if(type == '45')
                    {
                        var ttl_45 = parseInt(total_45) + parseInt(count);
                        $('#total_45').html(ttl_45);
                        
                    }
                    if(type == '75')
                    {
                        var ttl_75 = parseInt(total_75) + parseInt(count);
                        $('#total_75').html(ttl_75);
                        
                    }
                    if(type == 'total')
                    {
                        var ttl = parseInt(total_all) + parseInt(count);
                        $('#total_all').html(ttl);
                    }

                    //console.log(info.event.title);
                    // console.log(info.el);
                    // console.log(info.view);
                    // console.log(info.isStart);
                    // console.log(info.isEnd);
                    // console.log(info.isMirror);
                }
                });
                calendar.render();
            });
  });

    </script>