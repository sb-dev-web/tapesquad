<?php ?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <?php if($loggedUser['profile_pic']){ $img= $loggedUser['profile_pic']; } else { $img= 'logo.png' ; }?>
                      <img src="<?php echo $this->webroot; ?>img/profilePics/<?php echo $img ; ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p> <?php echo ucfirst($loggedUser['firstname']); ?></p>
          
        </div>
      </div>
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <!--<li class="header">MAIN NAVIGATION</li>-->
        <li class="<?php if($this->action=='dashboard') echo 'active' ; ?> treeview">
         <?php echo $this->Html->link('<i class="fa fa-dashboard"></i><span> Dashboard</span><span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>',array('controller'=>'users','action'=>'dashboard'),array('escape'=>false)); ?>
        </li>
       
       <li class="treeview <?php if($this->action=='manage_users') echo 'active' ; ?>">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul  class="treeview-menu <?php if($this->action=='manage_users') echo 'menu-open' ; ?>">
            
            <li <?php if($this->action=='manage_users') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> List Users',array('controller'=>'users','action'=>'manage_users'),array('escape'=>false)); ?></li>
            
          </ul>
        </li>
        
        <?php if($loggedUser['user_role_id']==1){ ?>
        <li class="treeview <?php if((in_array($this->action,array('add_service','edit_service','manage_services'))))  echo 'active' ; ?>">
          <a href="#">
          <i class="fa fa-list" aria-hidden="true"></i>

            <span>Manage Services</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        
          <ul class="treeview-menu <?php if((in_array($this->action,array('add_service','edit_service','manage_services')))) echo 'menu-open' ; ?>">
            <li <?php if($this->action=='add_service') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> Add Service',array('controller'=>'users','action'=>'add_service'),array('escape'=>false)); ?></li>
            <li <?php if($this->action=='manage_services') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> List Services',array('controller'=>'users','action'=>'manage_services'),array('escape'=>false)); ?></li>
          </ul>
        </li>
        <?php } ?>

        <?php if($loggedUser['user_role_id']==1){ ?>
        <li class="treeview <?php if((in_array($this->action,array('add_member','manage_staff'))))  echo 'active' ; ?>">
          <a href="#">
          <i class="fa fa-group" aria-hidden="true"></i>

            <span>Manage Staff</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        
          <ul  class="treeview-menu <?php if((in_array($this->action,array('add_member','manage_staff')))) echo 'menu-open' ; ?>">
            <li <?php if($this->action=='add_member') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> Add Member',array('controller'=>'users','action'=>'add_member'),array('escape'=>false)); ?></li>
            <li <?php if($this->action=='manage_staff') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> List Members',array('controller'=>'users','action'=>'manage_staff'),array('escape'=>false)); ?></li>
          </ul>
        </li>
        <?php } ?>

        <?php //if($loggedUser['user_role_id']==1){ ?>
        <li class="treeview <?php if((in_array($this->action,array('manage_requests','request_details'))))  echo 'active' ; ?>">
          <a href="#">
          <i class="fa fa-paper-plane " aria-hidden="true"></i>

            <span>Manage Requests</span>
            <span class="pull-right-container">
              <?php if(isset($unreadReq)){ ?>
                  <small class="label pull-right bg-red"><?php echo $unreadReq; ?></small>
              <?php }else{ ?>
                  <i class="fa fa-angle-left pull-right"></i>
              <?php } ?>
            </span>
          </a>
        
          <ul class="treeview-menu <?php if((in_array($this->action,array('manage_requests','request_details')))) echo 'menu-open' ; ?>">
            <li <?php if($this->action=='manage_requests') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> List Requests',array('controller'=>'users','action'=>'manage_requests'),array('escape'=>false)); ?></li>
            
          </ul>
        </li>
        <?php //} ?>

        <?php //if($loggedUser['user_role_id']==1){ ?>
        <li class="treeview <?php if((in_array($this->action,array('manage_inbox','send_notification','view_chat'))))  echo 'active' ; ?>">
          <a href="#">
          <i class="fa fa-envelope-o " aria-hidden="true"></i>

            <span>Manage Inbox</span>
            <span class="pull-right-container">
              <?php if(isset($unreadMsgs)){ ?>
                  <small class="label pull-right bg-green"><?php echo $unreadMsgs; ?></small>
              <?php }else{ ?>
              <i class="fa fa-angle-left pull-right"></i>
              <?php } ?>
            </span>
          </a>
        
          <ul  class="treeview-menu <?php if((in_array($this->action,array('manage_inbox','send_message','view_chat')))) echo 'menu-open' ; ?>">
            <li <?php if($this->action=='manage_inbox') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> Inbox',array('controller'=>'users','action'=>'manage_inbox'),array('escape'=>false)); ?></li>
            <li <?php if($this->action=='send_message') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> Send Message',array('controller'=>'users','action'=>'send_message'),array('escape'=>false)); ?></li>
          </ul>
        </li>
        <?php //} ?>

        <li class="treeview <?php if($this->action=='manage_notifications')  echo 'active' ; ?>">
          <a href="#">
          <i class="fa fa-bell-o " aria-hidden="true"></i>

            <span>Manage Notification</span>
            <span class="pull-right-container">
              <?php if(isset($unreadNoti)){ ?>
                  <small class="label pull-right bg-orange"><?php echo $unreadNoti; ?></small>
              <?php }else{ ?>
                  <i class="fa fa-angle-left pull-right"></i>
              <?php } ?>
            </span>
          </a>
        
          <ul class="treeview-menu <?php if($this->action=='manage_notifications') echo 'menu-open' ; ?>">
            <li <?php if($this->action=='manage_notifications') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> List Notification',array('controller'=>'users','action'=>'manage_notifications'),array('escape'=>false)); ?></li>
            
          </ul>
        </li>

        <?php //if($loggedUser['user_role_id']==1){ ?>
        <li class="treeview <?php if((in_array($this->action,array('add_faq','manage_faqs'))))  echo 'active' ; ?>">
          <a href="#">
          <i class="fa fa-question" aria-hidden="true"></i>

            <span>Manage FAQs</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        
          <ul  class="treeview-menu <?php if((in_array($this->action,array('add_faq','manage_faqs')))) echo 'menu-open' ; ?>">
            <li <?php if($this->action=='add_faq') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> Add FAQ',array('controller'=>'users','action'=>'add_faq'),array('escape'=>false)); ?></li>
            <li <?php if($this->action=='manage_faqs') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> List FAQs',array('controller'=>'users','action'=>'manage_faqs '),array('escape'=>false)); ?></li>
          </ul>
        </li>
        <?php //} ?>

         <?php if($loggedUser['user_role_id']==1){ ?>
        <li class="treeview <?php if($this->action == 'manage_payments')  echo 'active' ; ?>">
          <a href="#">
          <i class="fa fa-dollar" aria-hidden="true"></i>

            <span>Manage Payments</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        
          <ul  class="treeview-menu <?php if($this->action == 'manage_payments') echo 'menu-open' ; ?>">
            <li <?php if($this->action=='manage_payments') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> List Payments',array('controller'=>'users','action'=>'manage_payments'),array('escape'=>false)); ?></li>
          
          </ul>
        </li>
        <?php } ?>


         <?php if($loggedUser['user_role_id']==1){ ?>
        <li class="treeview <?php if((in_array($this->action,array('add_template','manage_templates'))))  echo 'active' ; ?>">
          <a href="#">
          <i class="fa fa-envelope" aria-hidden="true"></i>

            <span>Email Template</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        
          <ul class="treeview-menu <?php if((in_array($this->action,array('add_template','manage_templates')))) echo 'menu-open' ; ?>">
            <li <?php if($this->action=='add_template') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> Add Template',array('controller'=>'users','action'=>'add_template'),array('escape'=>false)); ?></li>
            <li <?php if($this->action=='manage_templates') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> List Templates',array('controller'=>'users','action'=>'manage_templates'),array('escape'=>false)); ?></li>
          </ul>
        </li>
        <?php } ?>

        <?php if($loggedUser['user_role_id']==1){ ?>
        <li class="treeview <?php if((in_array($this->action,array('add_cms','edit_cms','cms_index'))))  echo 'active' ; ?>">
          <a href="#">
          <i class="fa fa-file" aria-hidden="true"></i>

            <span>Manage Cms</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        
          <ul  class="treeview-menu <?php if((in_array($this->action,array('add_cms','edit_cms','cms_index')))) echo 'menu-open' ; ?>">
            <li <?php if($this->action=='add_cms') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> Add Content',array('controller'=>'users','action'=>'add_cms'),array('escape'=>false)); ?></li>
            <li <?php if($this->action=='cms_index') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> List Content',array('controller'=>'users','action'=>'cms_index'),array('escape'=>false)); ?></li>
          </ul>
        </li>
        <?php } ?>

      <?php if($loggedUser['user_role_id']==1){ ?>
      <li class="treeview <?php if((in_array($this->action,array('general_setting','smtp_setting','account_setting')))) echo 'active';?>">
          <a href="#">
          <i class="fa fa-gear" aria-hidden="true"></i>

            <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        
          <ul  class="treeview-menu <?php if((in_array($this->action,array('general_setting','smtp_setting','account_setting')))) echo 'menu-open' ; ?>">
            <li <?php if($this->action=='general_setting') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> General Settings',array('controller'=>'users','action'=>'general_setting'),array('escape'=>false)); ?></li>
            <li <?php if($this->action=='account_setting') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> Account Settings',array('controller'=>'users','action'=>'account_setting'),array('escape'=>false)); ?></li>
            <li <?php if($this->action=='smtp_setting') echo 'class="active"' ; ?>><?php echo $this->Html->link('<i class="fa fa-circle-o"></i> SMTP Settings',array('controller'=>'users','action'=>'smtp_setting'),array('escape'=>false)); ?></li>
          </ul>
      </li>
      <?php } ?>
	 
      </ul>
      
       
    </section>
    <!-- /.sidebar -->
  </aside>
