 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.9.3
    </div>
    <strong>Copyright &copy; 2017-2018 <a href="javascript:void(0)">MyVisa</a>.</strong> All rights
    reserved.
  </footer>