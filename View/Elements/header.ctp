<header class="main-header">
    <!-- Logo -->
    <a href="javascript:void(0)" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <?php $companyName = $companyData['Company']['name']; ?>
      <span class="logo-mini"><!--<img class="admin-logo" src="<?php echo $this->webroot; ?>img/companyPics/<?php echo $companyData['Company']['logo'] ?>">-->MV</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg" style="font-size:33px;font-weight:bold;"><!--<img class="admin-logo" src="<?php echo $this->webroot; ?>img/companyPics/<?php echo $companyData['Company']['logo'] ?>">--><?php echo $companyName; ?></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

           <li class="dropdown messages-menu">
            <a href="<?php echo $this->webroot; ?>users/manage_notifications" class="dropdown-toggle">
              <i class="fa fa-bell-o"></i>
              <?php if(isset($unreadNoti)){ ?>
              <span class="label label-success"><?php echo $unreadNoti; ?></span>
              <?php } ?>
            </a>
          </li>
      
          <li class="dropdown messages-menu">
            <a href="<?php echo $this->webroot; ?>users/manage_inbox" class="dropdown-toggle">
              <i class="fa fa-envelope-o"></i>
              <?php if(isset($unreadMsgs)){ ?>
              <span class="label label-warning"><?php echo $unreadMsgs; ?></span>
              <?php } ?>
            </a>
          </li>

          <li class="dropdown messages-menu">
            <a href="<?php echo $this->webroot; ?>users/manage_requests" class="dropdown-toggle">
              <i class="fa fa-registered"></i>
              <?php if(isset($unreadReq)){ ?>
              <span class="label label-danger"><?php echo $unreadReq; ?></span>
              <?php } ?>
            </a>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu" onclick="$('.user-info-toggle').toggle();">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               <?php if($loggedUser['profile_pic']){ $img= $loggedUser['profile_pic']; } else { $img= 'logo.png' ; }?>
                        <img src="<?php echo $this->webroot; ?>img/profilePics/<?php echo $img ; ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"> <?php echo $loggedUser['firstname'].' | '.date('d F, Y g:i A',strtotime($loggedUser['last_login_date'])); ?></span>
            </a>
            <ul class="dropdown-menu user-info-toggle">
              <!-- User image -->
              <li class="user-header">
                <?php if($loggedUser['profile_pic']){ $img= $loggedUser['profile_pic']; } else { $img= 'logo.png' ; }?>
                        <img src="<?php echo $this->webroot; ?>img/profilePics/<?php echo $img ; ?>" class="img-circle" alt="User Image">

                <p><?php echo $companyName; ?><small></small></p>
              </li>
             
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                
                <div class="pull-right">
                  <a href="<?php echo $this->webroot; ?>users/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!--<li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
        </ul>
      </div>
    </nav>
  </header>
