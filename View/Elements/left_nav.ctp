<nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <?php if($loggedUser['profile_pic']){ $img= $loggedUser['profile_pic']; } else { $img= 'not_avail.jpg' ; }?>
            <div class="avatar"><img src="<?php echo $this->webroot; ?>img/userImages/<?php echo $img ; ?>" alt="..." class="img-fluid rounded-circle"></div>
            <div class="title">
              <h1 class="h4"><?php echo ucfirst($loggedUser['firstname']).' '.ucfirst($loggedUser['lastname']);  ?> </h1>
              <p>Admin</p>
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><!--<span class="heading">Main</span>-->
          <ul class="list-unstyled">
          <li class="<?php if($this->action=='dashboard') echo 'active' ; ?>"> <a href="<?php echo $this->webroot; ?>users/dashboard"><i class="icon-home"></i>Dashboard</a></li>
          
          <li><a href="#dashvariants" aria-expanded="<?php if($this->action=='manage_users' || $this->action=='view_user') { echo 'true'; }else{ echo 'false';} ?>" data-toggle="collapse"> <i class="fa fa-users"></i>Manage Users </a>
              <ul id="dashvariants" class="collapse list-unstyled <?php if($this->action=='manage_users' || $this->action=='view_user'){ echo 'show'; }?>" aria-expanded="<?php if($this->action=='manage_users' || $this->action=='view_user') { echo 'true'; }else{ echo 'false';} ?>">
                <li class="<?php if($this->action=='manage_users' || $this->action=='view_user'){ echo 'active'; } ?>"><a href="<?php echo $this->webroot; ?>users/manage_users">List Users</a></li>
               
              </ul>
          </li>

          <li><a href="#readers" aria-expanded="<?php if($this->action=='manage_readers' || $this->action=='view_reader') { echo 'true'; }else{ echo 'false';} ?>" data-toggle="collapse"> <i class="fa fa-users"></i>Manage Readers </a>
              <ul id="readers" class="collapse list-unstyled <?php if($this->action=='manage_readers' || $this->action=='view_reader'){ echo 'show'; }?>" aria-expanded="<?php if($this->action=='manage_readers' || $this->action=='view_reader') { echo 'true'; }else{ echo 'false';} ?>">
                <li class="<?php if($this->action=='manage_readers' || $this->action=='view_reader'){echo 'active'; } ?>"><a href="<?php echo $this->webroot; ?>users/manage_readers">List Readers</a></li>
                
              </ul>
          </li>

          <li class="<?php if($this->action=='manage_posts') echo 'active' ; ?>"> <a href="<?php echo $this->webroot; ?>users/manage_posts"><i class="fa fa-list"></i>Manage Posts</a></li>

          <li><a href="#projects" aria-expanded="<?php if($this->action=='manage_projects') { echo 'true'; }else{ echo 'false';} ?>" data-toggle="collapse"> <i class="fa fa-list"></i>Manage Projects </a>
              <ul id="projects" class="collapse list-unstyled <?php if($this->action=='manage_projects'){ echo 'show'; }?>" aria-expanded="<?php if($this->action=='manage_projects') { echo 'true'; }else{ echo 'false';} ?>">
                <li class="<?php if($this->action=='manage_projects'){echo 'active'; } ?>"><a href="<?php echo $this->webroot; ?>users/manage_projects">List Projects</a></li>
                
              </ul>
          </li>

         
         <li><a href="#faqs" aria-expanded="<?php if((in_array($this->action,array('add_faq','manage_faqs','add_faq_topic','manage_faq_topics')))) { echo 'true'; }else{ echo 'false';} ?>" class="<?php if((in_array($this->action,array('add_faq','manage_faqs','add_faq_topic','manage_faq_topics')))) { echo 'active'; } ?>" data-toggle="collapse"> <i class="fa fa-question"></i>Manage FAQs </a>
              <ul id="faqs" class="collapse list-unstyled <?php if((in_array($this->action,array('add_faq','manage_faqs','add_faq_topic','manage_faq_topics')))){ echo 'show'; } ?>" aria-expanded="<?php if((in_array($this->action,array('add_faq','manage_faqs','add_faq_topic','manage_faq_topics')))){ echo 'true'; }else{ echo 'false';} ?>">
                
                  <li class="<?php if($this->action=='manage_faqs'){echo 'active'; } ?>">
                      <a href="<?php echo $this->webroot; ?>users/manage_faq_topics">List FAQs</a>
                  </li>

                  <li class="<?php if($this->action=='add_faq_topic'){echo 'active'; } ?>">
                      <a href="<?php echo $this->webroot; ?>users/add_faq_topic">Add Topic</a>
                  </li>

              </ul>
          </li>

          <li class="<?php if($this->action=='notifications') echo 'active' ; ?>"> <a href="<?php echo $this->webroot; ?>users/notifications"><i class="fa fa-bell"></i>Notifications  <?php if(isset($noticount)){ echo '<span style="margin-left: 67px;background-color: red;color: #fff;padding: 0px 10px;border-radius: 131px;">'.$noticount.'</span>'; } ?></a>
          </li>

          <li class="<?php if(in_array($this->action,array('manage_rewards'))) echo 'active' ; ?>"> <a href="<?php echo $this->webroot; ?>users/manage_rewards"><i class="fa fa-trophy"></i>Manage Rewards</a>
          </li>

          <li class="<?php if(in_array($this->action,array('reported_posts'))) echo 'active' ; ?>"> <a href="<?php echo $this->webroot; ?>users/reported_posts"><i class="fa fa-ban"></i>Reported Posts</a>
          </li>

          <li class="<?php if($this->action=='manage_disputes') echo 'active' ; ?>"> <a href="<?php echo $this->webroot; ?>users/manage_disputes"><i class="fa fa-crosshairs"></i>Manage Disputes</a></li>

          <li class="<?php if($this->action=='manage_payments') echo 'active' ; ?>"> <a href="<?php echo $this->webroot; ?>users/manage_payments"><i class="fa fa-cc-stripe"></i>Transactions</a></li>

          <li><a href="#templates" aria-expanded="<?php if(in_array($this->action,array('manage_templates','add_template'))) { echo 'true'; }else{ echo 'false';} ?>" data-toggle="collapse"> <i class="fa fa-envelope"></i>Manage Templates </a>
              <ul id="templates" class="collapse list-unstyled <?php if(in_array($this->action,array('manage_templates','add_template'))){ echo 'show'; }?>" aria-expanded="<?php if($this->action=='manage_templates') { echo 'true'; }else{ echo 'false';} ?>">
                <li class="<?php if($this->action=='manage_templates'){echo 'active'; } ?>"><a href="<?php echo $this->webroot; ?>users/manage_templates">List Templates</a></li>
                <li class="<?php if($this->action=='add_template'){echo 'active'; } ?>"><a href="<?php echo $this->webroot; ?>users/add_template">Add Template</a></li>
              </ul>
          </li>

          <li><a href="#cms" aria-expanded="<?php if(in_array($this->action,array('manage_cms','add_cms'))) { echo 'true'; }else{ echo 'false';} ?>" data-toggle="collapse"> <i class="fa fa-envelope"></i>Manage CMS </a>
              <ul id="cms" class="collapse list-unstyled <?php if(in_array($this->action,array('manage_cms','add_cms'))){ echo 'show'; }?>" aria-expanded="<?php if($this->action=='manage_cms') { echo 'true'; }else{ echo 'false';} ?>">
                <li class="<?php if($this->action=='manage_cms'){echo 'active'; } ?>"><a href="<?php echo $this->webroot; ?>users/manage_cms">List CMS</a></li>
                <li class="<?php if($this->action=='add_cms'){echo 'active'; } ?>"><a href="<?php echo $this->webroot; ?>users/add_cms">Add CMS</a></li>
              </ul>
          </li>

         
          </ul>
          
        </nav>