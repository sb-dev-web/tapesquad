<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
//$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
    <!-- Meta, title, CSS, favicons, etc. -->
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

	<title><?php echo $this->fetch('title'); ?></title>
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
	 <link rel="stylesheet" href="https://file.myfontastic.com/da58YPMQ7U5HY8Rb6UxkNf/icons.css"> 
	 <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

	<?php
		//echo $this->Html->meta('favicon.ico');
    echo $this->Html->meta('icon','img/tapelogo.png',array('type' => 'image/png'));

		echo $this->Html->css(array('admin/jquery-te/jquery-te-1.4.0','material/bootstrap.min','material/bootstrap-responsive.min','admin/daterangepicker','admin/nprogress','material/custom','material/style.default','admin/jqvmap.min','admin/froala_editor.min','admin/froala_style.min','validationEngine.jquery','admin/sweetalert','admin/jquery.noty','admin/noty_theme_default','lightbox','style','material/font-awesome','admin/fancybox/source/jquery.fancybox'));
    echo $this->Html->css(array('admin/Datatables/dataTables.bootstrap','admin/Datatables/buttons.bootstrap','admin/Datatables/fixedHeader.bootstrap','admin/Datatables/responsive.bootstrap')); 

		echo $this->Html->script(array('admin/jquery.min','lightbox','material/tether.min','material/bootstrap','material/bootstrap.min','admin/fastclick','admin/gauge.min','admin/moment.min.js','admin/daterangepicker','admin/icheck.min','admin/froala_editor.min','jquery.validationEngine','jquery.validationEngine-en','admin/jquery.sparkline','admin/date','admin/sweetalert.min','admin/jquery.noty','admin/fancybox/source/jquery.fancybox','admin/fancybox/source/helpers/jquery.fancybox-media','ckeditor/ckeditor','material/front','material/jquery.validate.min','material/jquery.cookie'));

    echo $this->Html->script(array('admin/Datatables/jquery.dataTables.min','admin/Datatables/dataTables.bootstrap.min','admin/Datatables/dataTables.buttons.min','admin/Datatables/buttons.bootstrap.min','admin/Datatables/buttons.flash.min','admin/Datatables/buttons.html5.min','admin/Datatables/buttons.print.min','admin/Datatables/dataTables.fixedHeader.min','admin/Datatables/dataTables.keyTable.min','admin/Datatables/dataTables.responsive.min','admin/Datatables/responsive.bootstrap','admin/Datatables/dataTables.scroller.min','admin/Datatables/jszip.min','admin/Datatables/pdfmake.min','admin/Datatables/vfs_fonts'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');

		echo $scripts_for_layout; ?>
	
    <!-- Start of xicom Zendesk Widget script -->
<!--<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="xicom.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();
/*]]>*/</script>-->
<!-- End of xicom Zendesk Widget script -->
<style> 
/*a {
  color: hsl(39, 100%, 50%) !important;
  }
a:focus, a:hover {
    color: hsl(247, 79%, 52%) !important;
    
}*/
</style>
</head>
<body>
	<div id="container">
		<div class="page home-page">
			<div id="header">
				<?php echo $this->element('admin_header'); ?>
			</div>
				
			<div id="content">
				<div class="page-content d-flex align-items-stretch" style="min-height: 637px;">
				<?php echo $this->Flash->render(); ?>
				<?php echo $this->element('left_nav'); ?>
				<?php echo $this->fetch('content'); ?>
				</div>
			</div>
			<div id="footer">
				<?php echo $this->element('admin_footer'); ?>
			</div>
		</div>
	</div>
	<?php //echo $this->element('sql_dump'); ?>
	    <script>
      $(document).ready(function() {
        var data1 = [
          [gd(2012, 1, 1), 17],
          [gd(2012, 1, 2), 74],
          [gd(2012, 1, 3), 6],
          [gd(2012, 1, 4), 39],
          [gd(2012, 1, 5), 20],
          [gd(2012, 1, 6), 85],
          [gd(2012, 1, 7), 7]
        ];

        var data2 = [
          [gd(2012, 1, 1), 82],
          [gd(2012, 1, 2), 23],
          [gd(2012, 1, 3), 66],
          [gd(2012, 1, 4), 9],
          [gd(2012, 1, 5), 119],
          [gd(2012, 1, 6), 6],
          [gd(2012, 1, 7), 9]
        ];
        $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
          data1, data2
        ], {
          series: {
            lines: {
              show: false,
              fill: true
            },
            splines: {
              show: true,
              tension: 0.4,
              lineWidth: 1,
              fill: 0.4
            },
            points: {
              radius: 0,
              show: true
            },
            shadowSize: 2
          },
          grid: {
            verticalLines: true,
            hoverable: true,
            clickable: true,
            tickColor: "#d5d5d5",
            borderWidth: 1,
            color: '#fff'
          },
          colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
          xaxis: {
            tickColor: "rgba(51, 51, 51, 0.06)",
            mode: "time",
            tickSize: [1, "day"],
            //tickLength: 10,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10
          },
          yaxis: {
            ticks: 8,
            tickColor: "rgba(51, 51, 51, 0.06)",
          },
          tooltip: false
        });

        function gd(year, month, day) {
          return new Date(year, month - 1, day).getTime();
        }
      });
    </script>
    <!-- /Flot -->

 
	<script>

	// Sidebar
$(document).ready(function() {
  var CURRENT_URL = window.location.href.split('#')[0].split('?')[0];
    // TODO: This is some kind of easy fix, maybe we can improve this
   	var setContentHeight = function () {
        // reset height
        $RIGHT_COL.css('min-height', $(window).height());

        var bodyHeight = $BODY.outerHeight(),
            footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
            leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
            contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

        // normalize content
        contentHeight -= $NAV_MENU.height() + footerHeight;
	      $RIGHT_COL.css('min-height', contentHeight);
    };
	

	$('#sidebar-menu a').click(function(){
        var $li = $(this).parent();

        if ($li.is('.active')) {
            $li.removeClass('active active-sm');
            $('ul:first', $li).slideUp(function() {
                setContentHeight();
            });
        } else {
            // prevent closing menu if we are on child menu
            if (!$li.parent().is('.child_menu')) {
                $('#sidebar-menu').find('li').removeClass('active active-sm');
                $('#sidebar-menu').find('li ul').slideUp();
            }
            
            $li.addClass('active');
	    $('ul:first', $li).slideDown(function() {
                setContentHeight();
            });
        }
    });

    // toggle small or large menu
   $('#menu_toggle').click(function(){
        if ($('body').hasClass('nav-md')) {
            $('#sidebar-menu').find('li.active ul').hide();
            $('#sidebar-menu').find('li.active').addClass('active-sm').removeClass('active');
        } else {
            $('#sidebar-menu').find('li.active-sm ul').show();
            $('#sidebar-menu').find('li.active-sm').addClass('active').removeClass('active-sm');
        }

        $('body').toggleClass('nav-md nav-sm');

        setContentHeight();
    });

    // check active menu
    $('#sidebar-menu').find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

     $('#sidebar-menu').find('a').filter(function () {
        return this.href == CURRENT_URL;
    }).parent('li').addClass('current-page').parents('ul').slideDown(function() {
        setContentHeight();
    }).parent().addClass('active');

    // recompute content when resizing
    $(window).smartresize(function(){  
        setContentHeight();
    });

    setContentHeight();

    // fixed sidebar
    if ($.fn.mCustomScrollbar) {
        $('.menu_fixed').mCustomScrollbar({
            autoHideScrollbar: true,
            theme: 'minimal',
            mouseWheel:{ preventDefault: true }
        });
    }
});
// /Sidebar
$(document).ready(function() {
    $(".comingsoon").click(function() {
          alert('Coming Soon!');
    });
});
	</script>
</body>
</html>

<script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='//www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X');ga('send','pageview');
</script>
