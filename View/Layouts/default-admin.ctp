<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
//$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TapeSquad</title>
	<?php echo $this->Html->css(array('admin/jquery-te/jquery-te-1.4.0','admin/style.css','admin/bootstrap.min','admin/font-awesome.min','admin/nprogress','admin/custom.min','admin/green','admin/bootstrap-progressbar-3.3.4.min','admin/jqvmap.min','admin/daterangepicker','admin/froala_editor.min','admin/froala_style.min','validationEngine.jquery','admin/sweetalert','admin/jquery.noty','admin/noty_theme_default','style'));

	      echo $this->Html->script(array('admin/jquery.min','admin/jquery-te/jquery-te-1.4.0.min','admin/custom','admin/bootstrap.min','admin/fastclick','admin/gauge.min','admin/moment.min.js','admin/daterangepicker','admin/icheck.min','admin/froala_editor.min','jquery.validationEngine','jquery.validationEngine-en','admin/jquery.sparkline','admin/Chart.min','admin/echarts.min','admin/date','admin/sweetalert.min','admin/jquery.noty','ckeditor/ckeditor'));
	      
	      echo $scripts_for_layout; ?>
<script type="text/javascript">
jQuery(function(){
	if(jQuery('textarea').hasClass('text-editor')){
	   jQuery('.text-editor').jqte();
	}
});
</script>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="javascript:void(0)" class="site_title"><i class="fa fa-paw"></i> <span>TapeSquad App</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
		<?php if($loggedUser['profile_pic']!=''){ ?>
		
		<img src="<?php echo $this->webroot; ?>img/userImages/<?php echo $loggedUser['profile_pic']; ?>" alt="..." class="img-circle profile_img">
		<?php } else { ?>
                <img src="<?php echo $this->webroot; ?>img/userImages/not_avail.jpg" alt="..." class="img-circle profile_img">
		<?php } ?>
              </div>
              <div class="profile_info">
                <span>Welcome</span>
                <h2><?php echo $loggedUser['firstname']; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />



            <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!--<h3>General</h3>-->
		<div><br><br></div>
  <ul class="nav side-menu">
		
      <li><a href="<?php echo $this->webroot; ?>users/dashboard"><i class="fa fa-home"></i>Dashboard </a></li>
		
      <li><a href="<?php echo $this->webroot; ?>users/manage_users"><i class="fa fa-users"></i>Manage Users<span  class="fa fa-chevron-right"></span></a>
		  </li>

       <li><a href="<?php echo $this->webroot; ?>users/manage_projects"><i class="fa fa-users"></i>Manage Projects<span  class="fa fa-chevron-right"></span></a>
      </li>

		  <li><a><i class="fa fa-question"></i>Manage Faqs<span class="fa fa-chevron-down"></span></a>
		        <ul class="nav child_menu">
                  <li><a href="<?php echo $this->webroot; ?>users/add_faq_topic">Add Faq Topic</a></li>
                  <li><a href="<?php echo $this->webroot; ?>users/manage_faq_topics">Manage Faq Topics</a></li>
		              <!--<li><a href="<?php echo $this->webroot; ?>users/add_client_faq">Add Client Faq</a></li>
                  <li><a href="<?php echo $this->webroot; ?>users/manage_client_faqs">Manage Client Faqs</a></li>
                  <li><a href="<?php echo $this->webroot; ?>users/add_reader_faq">Add Reader Faq</a></li>
                  <li><a href="<?php echo $this->webroot; ?>users/manage_reader_faqs">Manage Reader Faqs</a></li>-->
		         </ul>
      </li>

     
	
		  <li><a><i class="fa fa-th-list"></i>CMS Mnagement<span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                  <li><a href="<?php echo $this->webroot; ?>users/add_cms">Add CMS</a></li>
                  <li><a href="<?php echo $this->webroot; ?>users/manage_cms">Manage CMS</a></li>
             </ul>
      </li>

      <!--<li><a href="<?php echo $this->webroot; ?>users/manage_posts"><i class="fa fa-tasks"></i>Manage Posts<span  class="fa fa-chevron-right"></span></a>
      </li>

      <li><a href="<?php echo $this->webroot; ?>users/manage_feeds"><i class="fa fa-tasks"></i>Manage Feeds<span  class="fa fa-chevron-right"></span></a>
      </li>-->

      <li><a><i class="fa fa-envelope"></i>Manage Templates <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                  <li><a href="<?php echo $this->webroot; ?>users/add_template">Add Template</a></li>
                  <li><a href="<?php echo $this->webroot; ?>users/manage_templates">Manage Templates</a></li>
             </ul>
      </li>

     <!-- <li><a><i class="fa fa-shopping-cart"></i>Manage Products<span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                  <li><a href="<?php echo $this->webroot; ?>users/add_product">Add Product</a></li>
                  <li><a href="<?php echo $this->webroot; ?>users/manage_products">Manage Products</a></li>
             </ul>
      </li>

      <li><a><i class="fa fa-question"></i>Manage Questions<span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                  <li><a href="<?php echo $this->webroot; ?>users/add_question">Add Question</a></li>
                  <li><a href="<?php echo $this->webroot; ?>users/manage_questions">Manage Questions</a></li>
             </ul>
      </li>-->

    </ul>
	      </div>
           </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a href="<?php echo $this->webroot; ?>users/logout" data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:void(0);" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		    <?php if($loggedUser['profile_pic']!='' && $loggedUser['profile_pic']!='NULL'){ ?>
		    <img src="<?php echo $this->webroot; ?>img/userImages/<?php echo $loggedUser['profile_pic']; ?>" alt=""><?php echo $loggedUser['firstname']; ?>
		    <?php }else{ ?>
                    <img src="<?php echo $this->webroot; ?>img/userImages/not_avail.jpg" alt=""><?php  echo 'Admin';  ?>
		    <?php } ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo $this->webroot; ?>users/settings">Settings</a></li>
		    <li><a href="<?php echo $this->webroot; ?>users/reset_adminpassword">Reset Password</a></li>
                    <li><a href="<?php echo $this->webroot; ?>users/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

        
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
       	<?php echo $this->Session->flash(); ?>
	<?php echo $content_for_layout; ?>

        <!-- footer content -->
        <footer>
          <div class="pull-right">
           TapeSquad App <a href=""></a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

  

    <!-- Flot -->
    <script>
      $(document).ready(function() {
        var data1 = [
          [gd(2012, 1, 1), 17],
          [gd(2012, 1, 2), 74],
          [gd(2012, 1, 3), 6],
          [gd(2012, 1, 4), 39],
          [gd(2012, 1, 5), 20],
          [gd(2012, 1, 6), 85],
          [gd(2012, 1, 7), 7]
        ];

        var data2 = [
          [gd(2012, 1, 1), 82],
          [gd(2012, 1, 2), 23],
          [gd(2012, 1, 3), 66],
          [gd(2012, 1, 4), 9],
          [gd(2012, 1, 5), 119],
          [gd(2012, 1, 6), 6],
          [gd(2012, 1, 7), 9]
        ];
        $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
          data1, data2
        ], {
          series: {
            lines: {
              show: false,
              fill: true
            },
            splines: {
              show: true,
              tension: 0.4,
              lineWidth: 1,
              fill: 0.4
            },
            points: {
              radius: 0,
              show: true
            },
            shadowSize: 2
          },
          grid: {
            verticalLines: true,
            hoverable: true,
            clickable: true,
            tickColor: "#d5d5d5",
            borderWidth: 1,
            color: '#fff'
          },
          colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
          xaxis: {
            tickColor: "rgba(51, 51, 51, 0.06)",
            mode: "time",
            tickSize: [1, "day"],
            //tickLength: 10,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10
          },
          yaxis: {
            ticks: 8,
            tickColor: "rgba(51, 51, 51, 0.06)",
          },
          tooltip: false
        });

        function gd(year, month, day) {
          return new Date(year, month - 1, day).getTime();
        }
      });
    </script>
    <!-- /Flot -->

 
	<script>

	// Sidebar
$(document).ready(function() {
    // TODO: This is some kind of easy fix, maybe we can improve this
   	var setContentHeight = function () {
        // reset height
        $RIGHT_COL.css('min-height', $(window).height());

        var bodyHeight = $BODY.outerHeight(),
            footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
            leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
            contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

        // normalize content
        contentHeight -= $NAV_MENU.height() + footerHeight;
	$RIGHT_COL.css('min-height', contentHeight);
    };
	

	$('#sidebar-menu a').click(function(){
        var $li = $(this).parent();

        if ($li.is('.active')) {
            $li.removeClass('active active-sm');
            $('ul:first', $li).slideUp(function() {
                setContentHeight();
            });
        } else {
            // prevent closing menu if we are on child menu
            if (!$li.parent().is('.child_menu')) {
                $('#sidebar-menu').find('li').removeClass('active active-sm');
                $('#sidebar-menu').find('li ul').slideUp();
            }
            
            $li.addClass('active');
	    $('ul:first', $li).slideDown(function() {
                setContentHeight();
            });
        }
    });

    // toggle small or large menu
   $('#menu_toggle').click(function(){
        if ($('body').hasClass('nav-md')) {
            $('#sidebar-menu').find('li.active ul').hide();
            $('#sidebar-menu').find('li.active').addClass('active-sm').removeClass('active');
        } else {
            $('#sidebar-menu').find('li.active-sm ul').show();
            $('#sidebar-menu').find('li.active-sm').addClass('active').removeClass('active-sm');
        }

        $('body').toggleClass('nav-md nav-sm');

        setContentHeight();
    });

    // check active menu
    $('#sidebar-menu').find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

     $('#sidebar-menu').find('a').filter(function () {
       return this.href == CURRENT_URL;
    }).parent('li').addClass('current-page').parents('ul').slideDown(function() {
        setContentHeight();
    }).parent().addClass('active');

    // recompute content when resizing
    $(window).smartresize(function(){  
        setContentHeight();
    });

    setContentHeight();

    // fixed sidebar
    if ($.fn.mCustomScrollbar) {
        $('.menu_fixed').mCustomScrollbar({
            autoHideScrollbar: true,
            theme: 'minimal',
            mouseWheel:{ preventDefault: true }
        });
    }
});
// /Sidebar
$(document).ready(function() {
    $(".comingsoon").click(function() {
          alert('Coming Soon!');
    });
});
	</script>
  </body>
</html>
